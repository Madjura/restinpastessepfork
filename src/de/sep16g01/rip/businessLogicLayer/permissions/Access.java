package de.sep16g01.rip.businessLogicLayer.permissions;

/**
 * Enum of the accesses a user can have.
 */
public enum Access {
	READ("read", 1), WRITE("write", 2);

	/**
	 * Constructor for the enum.
	 * 
	 * @param name
	 *            The access as a string.
	 */
	Access(String name, int priority) {
		this.name = name;
		this.priority = priority;
	}

	/**
	 * The string representation of the enum.
	 */
	private String name;

	/**
	 * The priority of the access over the other accesses.
	 */
	private int priority;

	public int getPriority() {
		return priority;
	}

	public String getName() {
		return name;
	}

	/**
	 * Returns an access by name.
	 * 
	 * @param name
	 *            The name of the access.
	 * @return The access with the name.
	 */
	public static Access getByName(String name) {
		for (Access acs : Access.values()) {
			if (acs.name.equals(name))
				return acs;
		}
		return null;
	}

	/**
	 * Returns an access by ID.
	 * 
	 * @param id
	 *            The ID of the access.
	 * @return The access with the ID.
	 */
	public static Access getById(int id) {
		for (Access acs : Access.values()) {
			if (acs.priority == id)
				return acs;
		}
		return null;
	}

	public boolean isGreaterEquals(Access access) {
		return this.priority >= access.priority;
	}
}
