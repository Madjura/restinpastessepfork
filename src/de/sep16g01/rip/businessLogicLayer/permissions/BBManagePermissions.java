package de.sep16g01.rip.businessLogicLayer.permissions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing for the custom right giving functionality. Holds the users input for
 * people he wants to give custom rights to for a certain object.
 */
@ViewScoped
@Named
public class BBManagePermissions implements Serializable {
	private final transient static Logger logger = Logger
			.getLogger(BBManagePermissions.class);

	@Inject
	private BBChangeLanguage lang;

	private static final long serialVersionUID = 1L;

	public BBManagePermissions() {
		usernamesToAdd = new ArrayList<>();
		usersWithPermission = new ArrayList<>();
		usersWithPermissionArray = new String[0];
		groupsToAdd = new ArrayList<>();
		groupsWithPermission = new ArrayList<>();
		groupsWithPermissionArray = new String[0];
	}

	/**
	 * Fills the lists of users/groups that have permissions.
	 * 
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		// load from request or make new
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String clipboardIdString = params.get(SystemSettings.CLIPBOARDID);
		String groupIdString = params.get(SystemSettings.GROUPID);
		String accessIdString = params.get(SystemSettings.ACCESSID);
		try {
			this.access = Access.getById(Integer.parseInt(accessIdString));
		} catch (NumberFormatException e) {
			// no id given or format wrong
		}
		try {
			this.setClipboardId(Integer.parseInt(clipboardIdString));
		} catch (NumberFormatException e) {
			// no id given or format wrong
		}
		try {
			this.setGroupId(Integer.parseInt(groupIdString));
		} catch (NumberFormatException e) {
			// no id given or format wrong
		}
		if (clipboardIdString == null && groupIdString == null) {
			// TODO react in some way
			FacesContext
					.getCurrentInstance()
					.addMessage(
							"ERROR",
							new FacesMessage(
									"Illegal ID provided. Group or Clipboard does not exist."));
		}

		if (clipboardIdString != null && !"".equals(clipboardIdString)) {
			permissibleType = SystemElementType.CLIPBOARD;
		} else if (groupIdString != null && !"".equals(groupIdString)) {
			permissibleType = SystemElementType.GROUP;
		}

		try {
			if (permissibleType.equals(SystemElementType.CLIPBOARD)) {
				usersWithPermission = Permission.getClipboardUsersWithAccess(
						this.getClipboardId(), access);
				groupsWithPermission = Permission.getClipboardGroupsWithAccess(
						getClipboardId(), access);
			} else if (permissibleType.equals(SystemElementType.GROUP)) {
				usersWithPermission = Permission.getGroupUsersWithAccess(
						getGroupId(), access);
			} else {
				// FIXME handle this another way
				throw new RuntimeException("BBManagePermissions: "
						+ SystemSettings.ACCESSID + " not set!");
				// TODO else redirect to exception or something
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return;
		}
		usersWithPermissionArray = usersWithPermission.toArray(new String[0]);
	}

	/**
	 * The clipboard that is being modified.
	 */
	private int clipboardId;
	/**
	 * The group that is being modified.
	 */
	private int groupId;
	/**
	 * Clipboard or Group
	 */
	private SystemElementType permissibleType;
	/**
	 * The access that is being modified.
	 */
	private Access access;

	/**
	 * The usernames of the users that are being added to the element.
	 */
	private List<String> usernamesToAdd = null;
	private String usernamesToAddString = null;

	/**
	 * The users that have the permission on this object.
	 */
	private List<String> usersWithPermission = null;
	private String[] usersWithPermissionArray = null;

	/**
	 * The groups that are being added to the element.
	 */
	private List<String> groupsToAdd = null;
	private String groupsToAddString = null;

	/**
	 * The groups that have the permission on this object.
	 */
	private List<String> groupsWithPermission = null;
	private String[] groupsWithPermissionArray = null;

	public String listToString(List<String> inputList) {
		StringBuilder sb = new StringBuilder();
		for (String line : inputList) {
			sb.append(line + "\n");
		}
		return sb.toString();
	}

	/**
	 * Saves the permission changes.
	 * 
	 * @return A link to the page of the element whose permissions were
	 *         modified.
	 */
	public String save() {
		switch (permissibleType) {
			case CLIPBOARD:
				try {
					Permission.assignClipboardUsersAccess(getClipboardId(),
							usersWithPermission, access);
					Permission.assignClipboardGroupsAccess(getClipboardId(),
							groupsWithPermission, access);
					logger.info("Assigning " + usersWithPermission + " and "
							+ groupsWithPermission + " " + access
							+ " to Clipboard with id " + getClipboardId());
				} catch (DataSourceException e) {
					logger.error("Exception while assigning "
							+ usersWithPermission + " and "
							+ groupsWithPermission + " " + access
							+ " to Clipboard with id " + getClipboardId(), e);
					BBExceptionHandler.handleException(e);
					return "";
				}
				break;
			case GROUP:
				try {
					Permission.assignGroupUsersAccess(getGroupId(),
							usersWithPermission, access);
					logger.info("Assigning " + usersWithPermission + " "
							+ access + " to Group with id " + getClipboardId());
				} catch (DataSourceException e) {
					BBExceptionHandler.handleException(e);
					return "";
				}
				break;
			default:
				break;
		}
		// TODO RETURN STATUS IF SUCCESSFUL
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, lang
						.getLangString("BBManagePermissions_save_success"),
						null));

		StringBuilder sb = new StringBuilder();
		if (getClipboardId() != 0) {
			sb.append("/view/clipboard/createClipboard.xhtml?faces-redirect=true");
			sb.append("&" + SystemSettings.CLIPBOARDID + "=" + getClipboardId());
		} else if (getGroupId() != 0) {
			sb.append("/view/group/createGroup.xhtml?faces-redirect=true");
			sb.append("&" + SystemSettings.GROUPID + "=" + getGroupId());
		}
		return sb.toString();
	}

	public void addGroups(ActionEvent event) {
		for (String newGroup : groupsToAddString.split("\n")) {
			newGroup = newGroup.trim();
			if (!newGroup.isEmpty() && !groupsWithPermission.contains(newGroup))
				groupsWithPermission.add(newGroup);
		}
	}

	public void removeGroups(ActionEvent event) {
		List<String> groupsToRemove = Arrays.asList(groupsWithPermissionArray);
		for (String toRemove : groupsToRemove) {
			groupsWithPermission.remove(toRemove);
		}
	}

	public void addUsers(ActionEvent event) {
		for (String newUsername : usernamesToAddString.split("\n")) {
			newUsername = newUsername.trim();
			if (!newUsername.isEmpty()
					&& !usersWithPermission.contains(newUsername))
				usersWithPermission.add(newUsername);
		}
	}

	public void removeUsers(ActionEvent event) {
		List<String> usersToRemove = Arrays.asList(usersWithPermissionArray);
		for (String toRemove : usersToRemove) {
			usersWithPermission.remove(toRemove);
		}
	}

	public Access getAccess() {
		return access;
	}

	public void setAccess(Access access) {
		this.access = access;
	}

	public List<String> getUsernamesToAdd() {
		return this.usernamesToAdd;
	}

	public void setUsernamesToAdd(List<String> usernamesToAdd) {
		this.usernamesToAdd = usernamesToAdd;
	}

	public List<String> getUsersWithPermission() {
		return this.usersWithPermission;
	}

	public void setUsersWithPermission(List<String> usersWithPermission) {
		this.usersWithPermission = usersWithPermission;
	}

	public List<String> getGroupsToAdd() {
		return this.groupsToAdd;
	}

	public void setGroupsToAdd(List<String> groupsToAdd) {
		this.groupsToAdd = groupsToAdd;
	}

	public List<String> getGroupsWithPermission() {
		return this.groupsWithPermission;
	}

	public void setGroupsWithPermission(List<String> groupsWithPermission) {
		this.groupsWithPermission = groupsWithPermission;
	}

	public String getUsernamesToAddString() {
		return listToString(usernamesToAdd);
	}

	public void setUsernamesToAddString(String usernamesToAddString) {
		usernamesToAdd = new ArrayList<>();
		this.usernamesToAddString = usernamesToAddString;
	}

	public String getGroupsToAddString() {
		return listToString(groupsToAdd);
	}

	public void setGroupsToAddString(String groupsToAddString) {
		this.groupsToAddString = groupsToAddString;
	}

	public String[] getUsersWithPermissionArray() {
		return usersWithPermissionArray;
	}

	public void setUsersWithPermissionArray(String[] usersWithPermissionArray) {
		this.usersWithPermissionArray = usersWithPermissionArray;
	}

	public String[] getGroupsWithPermissionArray() {
		return this.groupsWithPermissionArray;
	}

	public void setGroupsWithPermissionArray(String[] groupsWithPermissionArray) {
		this.groupsWithPermissionArray = groupsWithPermissionArray;
	}

	public int getClipboardId() {
		return clipboardId;
	}

	public void setClipboardId(int clipboardId) {
		this.clipboardId = clipboardId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public SystemElementType getPermissibleType() {
		return this.permissibleType;
	}

}
