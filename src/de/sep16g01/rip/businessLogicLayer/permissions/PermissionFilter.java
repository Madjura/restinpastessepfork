/**
 *   File:     de.sep16g01.rip.businessLogicLayer.permissions.PermissionFilter.java
 *   Created:  14.06.2016, 15:00:35 by Albert
 */
package de.sep16g01.rip.businessLogicLayer.permissions;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.permission.DAOPermission;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

/**
 *
 */
public class PermissionFilter implements Filter {

	@Inject
	private UserData userData;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// TODO think about:
		// changePassword, forgotPassword, login, register, commentList,
		// createComment, invite, managePermissions, keywords, bookmarks
		//
		// TODO remove all the unneccessary return;s

		// check path
		HttpServletRequest req = (HttpServletRequest) request;
		String pathInfo = req.getRequestURI().substring(
				req.getContextPath().length());
		HttpServletResponse res = (HttpServletResponse) response;

		if (pathInfo.startsWith("/faces/javax.faces.resource/")) {
			// skip "/faces/javax.faces.resource/"
		} else if (pathInfo.startsWith("/faces/view/administration/")) {
			if (!userData.isLoggedIn()
					|| !userData.getUser().getRole()
							.isGreaterEquals(Role.MODERATOR)) {
				res.sendError(403); // TODO create page
				return;
			}
		} else if (pathInfo.startsWith("/faces/view/profile/")) {
			if (filterProfile(pathInfo, request, response, chain, req, res))
				return;
		} else if (pathInfo.startsWith("/faces/view/clipboard/")) {
			if (filterClipboard(pathInfo, request, response, chain, req, res))
				return;
		} else if (pathInfo.startsWith("/faces/view/group/")) {
			if (filterGroup(pathInfo, request, response, chain, req, res))
				return;
		} else if (pathInfo.startsWith("/faces/view/paste/")) {
			if (filterPaste(pathInfo, request, response, chain, req, res))
				return;
		} else if (pathInfo
				.startsWith("/faces/view/permissions/managePermissions.xhtml")) {
			if (filterManagePermissions(pathInfo, request, response, chain,
					req, res))
				return;
		} else if (pathInfo
				.startsWith("/faces/view/general/emailConfirm.xhtml")) {
			if (filterEmailConfirm(pathInfo, request, response, chain, req, res))
				return;
		}

		chain.doFilter(request, response);
		return;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param chain
	 * @param req
	 * @param res
	 * @return whether the request was already handled or not
	 * @throws IOException
	 * @throws ServletException
	 */
	private boolean filterProfile(String pathInfo, ServletRequest request,
			ServletResponse response, FilterChain chain,
			HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		Integer userid = getParameterInteger(SystemSettings.USERID, req);
		if (userid != null) {
			try {
				if (userid <= -1 || !DAOUser.checkExistance(userid)) {
					res.sendError(404); // TODO create page
					return true;
				}

				if (pathInfo.contains("editProfile")) {
					if (!userData.isLoggedIn()
							|| (userData.getUser().getId() != userid && userData
									.getUser().getRole() != Role.ADMINISTRATOR)) {
						res.sendError(403); // TODO create page
						return true;
					}
				}
			} catch (DataSourceException e) {
				res.sendError(500); // TODO create page
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param chain
	 * @param req
	 * @param res
	 * @return whether the request was already handled or not
	 * @throws IOException
	 * @throws ServletException
	 */
	private boolean filterClipboard(String pathInfo, ServletRequest request,
			ServletResponse response, FilterChain chain,
			HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		Integer clipboardid = getParameterInteger(SystemSettings.CLIPBOARDID,
				req);
		if (clipboardid != null) {
			try {
				if (!pathInfo.contains("createClipboard")
						&& (clipboardid <= -1 || !DAOClipboard
								.checkExistance(clipboardid))) {
					res.sendError(404);
					return true;
				}
				int userid = 0;
				if (userData.isLoggedIn()) {
					userid = userData.getUser().getId();
				}
				Access access = DAOPermission.checkClipboardUserAccess(
						clipboardid, userid);
				if (access != null) {
					// request.setAttribute(SystemSettings.ACCESSID,
					// access.getPriority());

					if (pathInfo.contains("createComment")
							&& access == Access.READ) {
						res.sendError(403); // TODO create page
						return true;
					}

					AdditionalParameterRequest addRequest = new AdditionalParameterRequest(
							req);
					addRequest.addParameter(SystemSettings.ACCESSID,
							access.getPriority());
					chain.doFilter(addRequest, response);
					return true;
				}
				res.sendError(403); // TODO create page
				return true;
			} catch (DataSourceException e) {
				res.sendError(500); // TODO create page
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param chain
	 * @param req
	 * @param res
	 * @return whether the request was already handled or not
	 * @throws IOException
	 * @throws ServletException
	 */
	private boolean filterGroup(String pathInfo, ServletRequest request,
			ServletResponse response, FilterChain chain,
			HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		Integer groupid = getParameterInteger(SystemSettings.GROUPID, req);
		if (groupid != null) {
			try {
				if (groupid <= -1 || !DAOGroup.checkExistance(groupid)) {
					res.sendError(404);
					return true;
				}
				int userid = 0;
				if (userData.isLoggedIn()) {
					userid = userData.getUser().getId();
				}
				Access access = DAOPermission.checkGroupUserAccess(groupid,
						userid);
				if (access != null) {
					if (pathInfo.contains("createGroup")
							&& access == Access.READ) {
						res.sendError(403); // TODO create page
						return true;
					}

					AdditionalParameterRequest addRequest = new AdditionalParameterRequest(
							req);
					addRequest.addParameter(SystemSettings.ACCESSID,
							access.getPriority());
					chain.doFilter(addRequest, response);
					return true;
				}
				res.sendError(403); // TODO create page
				return true;
			} catch (DataSourceException e) {
				res.sendError(500); // TODO create page
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param chain
	 * @param req
	 * @param res
	 * @return whether the request was already handled or not
	 * @throws IOException
	 * @throws ServletException
	 */
	private boolean filterPaste(String pathInfo, ServletRequest request,
			ServletResponse response, FilterChain chain,
			HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		Integer pasteid = getParameterInteger(SystemSettings.PASTEID, req);
		if (pasteid != null) {
			try {
				if (pasteid <= -1 || !DAOPaste.checkExistance(pasteid)) {
					res.sendError(404);
					return true;
				}
				int userid = 0;
				if (userData.isLoggedIn()) {
					userid = userData.getUser().getId();
				}
				Access access = DAOPermission.checkPasteUserAccess(pasteid,
						userid);
				if (access != null) {
					if (pathInfo.contains("createPaste")
							&& access == Access.READ) {
						res.sendError(403); // TODO create page
						return true;
					}

					AdditionalParameterRequest addRequest = new AdditionalParameterRequest(
							req);
					addRequest.addParameter(SystemSettings.ACCESSID,
							access.getPriority());
					chain.doFilter(addRequest, response);
					return true;
				}
				res.sendError(403); // TODO create page
				return true;
			} catch (DataSourceException e) {
				res.sendError(500); // TODO create page
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param chain
	 * @param req
	 * @param res
	 * @return whether the request was already handled or not
	 * @throws IOException
	 * @throws ServletException
	 */
	private boolean filterManagePermissions(String pathInfo,
			ServletRequest request, ServletResponse response,
			FilterChain chain, HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		Integer groupid = getParameterInteger(SystemSettings.GROUPID, req);
		Integer clipboardid = getParameterInteger(SystemSettings.CLIPBOARDID,
				req);
		Integer accessid = getParameterInteger(SystemSettings.ACCESSID, req);
		if ((groupid == null && clipboardid == null) || accessid == null) {
			res.sendError(404);
			return true;
		}
		int userid = 0;
		if (userData.isLoggedIn()) {
			userid = userData.getUser().getId();
			if (userData.getUser().getRole() == Role.ADMINISTRATOR) {
				return false;
			}
		}
		if (clipboardid != null && clipboardid != 0) {
			try {
				Clipboard clipboard = DAOClipboard.get(clipboardid);
				if (Access.getById(accessid) == Access.READ
						&& clipboard.getVisibilityRead() != Visibility.CUSTOM) {
					res.sendError(404);
					return true;
				}
				if (Access.getById(accessid) == Access.WRITE
						&& clipboard.getVisibilityWrite() != Visibility.CUSTOM) {
					res.sendError(404);
					return true;
				}
				Access checkAccess = DAOPermission.checkClipboardUserAccess(
						clipboardid, userid);
				if (checkAccess == null
						|| !checkAccess.isGreaterEquals(Access
								.getById(accessid))) {
					res.sendError(403);
					return true;
				}
			} catch (DataSourceException e) {
				res.sendError(500); // TODO create page
				return true;
			}
		} else if (groupid != null) {
			try {
				Group group = DAOGroup.get(groupid);
				if (Access.getById(accessid) == Access.READ
						&& group.getVisibilityRead() != Visibility.CUSTOM) {
					res.sendError(404);
					return true;
				}
				if (Access.getById(accessid) == Access.WRITE
						&& group.getVisibilityWrite() != Visibility.CUSTOM) {
					res.sendError(404);
					return true;
				}
				Access checkAccess = DAOPermission.checkGroupUserAccess(
						groupid, userid);
				if (checkAccess == null
						|| !checkAccess.isGreaterEquals(Access
								.getById(accessid))) {
					res.sendError(403);
					return true;
				}
			} catch (DataSourceException e) {
				res.sendError(500); // TODO create page
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param chain
	 * @param req
	 * @param res
	 * @return whether the request was already handled or not
	 * @throws IOException
	 * @throws ServletException
	 */
	private boolean filterEmailConfirm(String pathInfo, ServletRequest request,
			ServletResponse response, FilterChain chain,
			HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		Integer groupid = getParameterInteger(SystemSettings.GROUPID, req);
		Integer clipboardid = getParameterInteger(SystemSettings.CLIPBOARDID,
				req); // TODO
		String userid = getParameterString(SystemSettings.USERID, req);
		String token = getParameterString(SystemSettings.TOKEN, req);
		if ((groupid == null && clipboardid == null && userid == null)
				|| token == null || token.isEmpty()) {
			res.sendError(404);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	// Helpers (may be refactored to some utility class)
	// ------------------------------------------

	private static Integer getParameterInteger(String paramName,
			HttpServletRequest request) {
		String sId = request.getParameter(paramName);
		Integer id = null;
		if (sId != null && !sId.equals("") && sId != "0") {
			try {
				id = Integer.parseInt(sId);
			} catch (NumberFormatException e) {
				return -1; // parameter exists but is not filled correctly
				// exception here is okay, just use -1
			}
		}
		return id; // null in case of missing parameter
	}

	private static String getParameterString(String paramName,
			HttpServletRequest request) {
		String sParam = request.getParameter(paramName);
		return sParam;
	}

}
