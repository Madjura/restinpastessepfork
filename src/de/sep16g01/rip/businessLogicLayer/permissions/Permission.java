package de.sep16g01.rip.businessLogicLayer.permissions;

import java.util.List;

import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.permission.DAOPermission;

/**
 * Utility class for easy access to permission-checking-methods. Relays calls to
 * DAOPermission to check whether a specific user has access to a SystemElement
 */
public final class Permission {
	// private Permission() {
	// }
	//
	// public static boolean checkClipboardAccess(int clipboardId, int userId,
	// Access access) throws DataSourceException {
	// Access checkClipboardUserAccess = DAOPermission
	// .checkClipboardUserAccess(clipboardId, userId);
	// if (checkClipboardUserAccess == Access.WRITE
	// || (checkClipboardUserAccess == Access.READ
	// && access == Access.READ)) {
	// return true;
	// }
	// return false;
	// }
	//
	// public static boolean checkGroupAccess(int groupId, int userId,
	// Access access) throws DataSourceException {
	// Access checkGroupUserAccess = DAOPermission
	// .checkGroupUserAccess(groupId, userId);
	// if (checkGroupUserAccess == Access.WRITE
	// || (checkGroupUserAccess == Access.READ
	// && access == Access.READ)) {
	// return true;
	// }
	// return false;
	// }
	//
	// public static boolean checkPasteAccess(int pasteId, int userId,
	// Access access) throws DataSourceException {
	// Access checkPasteUserAccess = DAOPermission
	// .checkPasteUserAccess(pasteId, userId);
	// if (checkPasteUserAccess == Access.WRITE
	// || (checkPasteUserAccess == Access.READ
	// && access == Access.READ)) {
	// return true;
	// }
	// return false;
	// }

	public static void assignClipboardUsersAccess(int clipboardId,
			List<String> users, Access access) throws DataSourceException {
		DAOPermission.assignClipboardUsersAccess(clipboardId, users, access);
	}

	public static void assignClipboardGroupsAccess(int clipboardId,
			List<String> groups, Access access) throws DataSourceException {
		DAOPermission.assignClipboardGroupsAccess(clipboardId, groups, access);
	}

	public static void assignGroupUsersAccess(int groupId, List<String> users,
			Access access) throws DataSourceException {
		DAOPermission.assignGroupUsersAccess(groupId, users, access);
	}

	public static List<String> getClipboardUsersWithAccess(int clipboardId,
			Access access) throws DataSourceException {
		return DAOPermission.getClipboardUsersWithAccess(clipboardId, access);
	}

	public static List<String> getClipboardGroupsWithAccess(int clipboardId,
			Access access) throws DataSourceException {
		return DAOPermission.getClipboardGroupsWithAccess(clipboardId, access);
	}

	public static List<String> getGroupUsersWithAccess(int groupId,
			Access access) throws DataSourceException {
		return DAOPermission.getGroupUsersWithAccess(groupId, access);
	}

}
