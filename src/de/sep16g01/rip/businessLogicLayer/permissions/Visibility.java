package de.sep16g01.rip.businessLogicLayer.permissions;

import java.util.ArrayList;
import java.util.List;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Enum of the visibilities in the system.
 */
public enum Visibility {

	CUSTOM("custom_name", 1), PUBLIC("public_name", 2), REGISTERED(
			"registered_name", 3), PRIVATE("private_name", 4);

	Visibility(String name, int id) {
		this.name = name;
		this.id = id;
	}

	private String name;
	private int id;

	public String getName() {
		return BBChangeLanguage.getLangStringStatic(name);
	}

	public int getId() {
		return id;
	}

	/**
	 * Returns a visibility by ID.
	 * 
	 * @param id
	 *            The ID of the visibility.
	 * @return The visibility with the ID.
	 */
	public static Visibility getById(int id) {
		for (Visibility vis : Visibility.values()) {
			if (vis.id == id)
				return vis;
		}
		return null;
	}

	public static List<Visibility> getAllVisibilities2() { // TODO rename
		List<Visibility> list = new ArrayList<Visibility>();
		list.add(CUSTOM);
		list.add(PUBLIC);
		list.add(REGISTERED);
		list.add(PRIVATE);
		return list;
	}
}
