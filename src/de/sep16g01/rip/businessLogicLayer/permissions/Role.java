package de.sep16g01.rip.businessLogicLayer.permissions;

import java.util.Arrays;
import java.util.List;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Enum of the roles in the system.
 */
public enum Role {
	ANONYM("anonym_identifier", 1), REGISTERED("registered_identifier", 2), MODERATOR(
			"moderator_identifier", 3), ADMINISTRATOR("admin_identifier", 4);

	Role(String name, int priority) {
		this.name = name;
		this.priority = priority;
	}

	private String name;
	private int priority;

	public String getName() {
		return BBChangeLanguage.getLangStringStatic(name);
	}

	public int getPriority() {
		return priority;
	}

	/**
	 * Returns a role by name.
	 * 
	 * @param name
	 *            The name of the role.
	 * @return The role with the name.
	 */
	public static Role getByName(String name) {
		for (Role role : Role.values()) {
			if (role.name.equals(name))
				return role;
		}
		return null;
	}

	/**
	 * Returns a role by ID.
	 * 
	 * @param id
	 *            The ID of the role.
	 * @return The role with the ID.
	 */
	public static Role getById(int id) {
		for (Role role : Role.values()) {
			if (role.priority == id) {
				return role;
			}
		}
		return null;
	}

	/**
	 * @return A list of all roles.
	 */
	public static List<Role> getAllRoles() {
		return Arrays.asList(Role.values());
	}

	public boolean isGreaterEquals(Role role) {
		return this.priority >= role.priority;
	}
}
