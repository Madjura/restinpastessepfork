package de.sep16g01.rip.businessLogicLayer.permissions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;

/**
 *
 */
public class AdditionalParameterRequest extends HttpServletRequestWrapper {
	private Map<String, String> additionalParameters;

	public AdditionalParameterRequest(HttpServletRequest request) {
		super(request);
		additionalParameters = new HashMap<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.ServletRequestWrapper#getParameter(java.lang.String)
	 */
	@Override
	public String getParameter(String name) {
		if (additionalParameters.containsKey(name)) {
			return additionalParameters.get(name);
		}
		return super.getParameter(name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.ServletRequestWrapper#getParameterNames()
	 */
	@Override
	public Enumeration<String> getParameterNames() {
		Enumeration<String> parameterNames = super.getParameterNames();
		List<String> newParamNames = new ArrayList<>();
		while (parameterNames.hasMoreElements()) {
			newParamNames.add(parameterNames.nextElement());
		}
		newParamNames.add(SystemSettings.ACCESSID);
		return Collections.enumeration(newParamNames);
	}

	public void addParameter(String key, String value) {
		additionalParameters.put(key, value);
	}

	public void addParameter(String key, int value) {
		additionalParameters.put(key, String.valueOf(value));
	}

}
