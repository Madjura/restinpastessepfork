package de.sep16g01.rip.businessLogicLayer.email;

/**
 * Lists all the various emails that our system has.
 */
public enum EmailType {
	CLIPBOARD_INVITE, GROUP_INVITE, KEYWORD_EMAIL, PASSWORD_RESET, REGISTRATION_CONFIRM, SYSTEM_INVITE, EMAIL_CHANGE, CHANGE_NOTIFY,
}
