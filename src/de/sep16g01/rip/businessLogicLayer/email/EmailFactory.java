package de.sep16g01.rip.businessLogicLayer.email;

import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import de.sep16g01.rip.businessLogicLayer.administration.Config;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.notify.Changes;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

/**
 * Factory that returns the default email for a certain email.
 */
public class EmailFactory {

	/**
	 * Default text for the group invite email.
	 */
	private final static String INVITE_GROUP = "";

	/**
	 * Default text for the registration email.
	 */
	private final static String REGISTRATION = "Your Registration Confirmation";

	/**
	 * Default text for the system invite email.
	 */
	private final static String INVITE_SYSTEM = "You have been invited!";

	/**
	 * Default text for the clipboard invite email.
	 */
	private final static String INVITE_CLIPBOARD = "";

	/**
	 * Default text for the password reset email.
	 */
	private final static String PASSWORD_RESET = "Your Request To Reset Your "
			+ "Password";

	/**
	 * Default text for the keyword notification email.
	 */
	private final static String KEYWORD = "Excitement! A paste contains keywords!";

	/**
	 * Default text for the change notification email.
	 */
	private final static String CHANGE_NOTIFY = "Change notification";

	/**
	 * Default text for the email change confirmation email.
	 */
	private static final String EMAIL_CHANGE = "Confirm your new email";

	/**
	 * Creates a default email for group invite with a certain access.
	 * 
	 * @param group
	 *            The group a user is being invited to.
	 * @param access
	 *            The
	 * @return
	 */
	public static Email getGroupInvite(Group group, Access access) {
		Email email = new Email(EmailType.GROUP_INVITE);
		email.setSubject(INVITE_GROUP);
		String englishText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_en").getString("group_invite");
		String germanText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_de").getString("group_invite");
		String emailText = englishText + "\n " + germanText;
		email.setElement(group);
		email.setAccess(access);
		// add content to email, then return
		email.setContent(emailText);
		return email;
	}

	/**
	 * @return a registration confirmation email with the title and subject set.
	 */
	public static Email getRegistrationConfirm(String userName) {
		Email email = new Email(EmailType.REGISTRATION_CONFIRM, userName);
		email.setSubject(REGISTRATION);

		// both languages, we dont know what language the user has because
		// they are not registered yet
		String englishText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_en").getString(
				"registration_confirmation");
		String germanText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_de").getString(
				"registration_confirmation");
		String emailText = englishText + "\n " + germanText;

		// add content to email, then return
		email.setContent(emailText);
		return email;
	}

	/**
	 * 
	 * @param inviter
	 *            The username of the person doing the invitation.
	 * @return A system invite email with the content and subject set.
	 */
	public static Email getSystemInvite(String inviter) {
		Email email = new Email(EmailType.SYSTEM_INVITE);
		email.setSubject(INVITE_SYSTEM);

		String englishText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_en")
				.getString("system_invite_email");
		String germanText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_de")
				.getString("system_invite_email");
		String emailText = englishText + "\n " + germanText;
		emailText = emailText.replace("{{ INVITER }}", inviter);

		// default invite message, user can change this before sending
		email.setContent(emailText + Config.getInstance().getBaseURL()
				+ "/view/home/home.xhtml");
		return email;
	}

	/**
	 * 
	 * @param clipboard
	 *            The clipboard that is being invited to.
	 * @param access
	 *            The access that is being given to the invitee.
	 * @return a clipboard invite email with the title and subject set.
	 */
	public static Email getClipboardInvite(Clipboard clipboard, Access access) {
		Email email = new Email(EmailType.CLIPBOARD_INVITE);
		email.setSubject(INVITE_CLIPBOARD);
		String englishText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_en").getString("clipboard_invite");
		String germanText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_de").getString("clipboard_invite");
		String emailText = englishText + "\n " + germanText;
		email.setElement(clipboard);
		email.setAccess(access);
		// add content to email, then return
		email.setContent(emailText);
		return email;
	}

	/**
	 * 
	 * @return a password reset email with the title and subject set.
	 * @throws DataSourceException
	 */
	public static Email getPasswordReset(String newEmail) {

		// fish out user with the email
		String user = null;
		try {
			user = DAOUser.getUserByEmail(newEmail);
			if (user == null) {
				return null;
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}

		Email email = new Email(EmailType.PASSWORD_RESET, user);
		email.setSubject(PASSWORD_RESET);

		// both languages, we dont know what language the user has because
		// the user cannot login, so he is unknown to us
		String englishText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_en").getString("password_reset");
		String germanText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_de").getString("password_reset");
		String emailText = englishText + "\n " + germanText;
		email.setContent(emailText);
		return email;
	}

	/**
	 * 
	 * @param keywords
	 *            The list of keywords that a user wants to be notified about.
	 * @param pastes
	 *            A list of pastes that contains the keywords.
	 * @return a keyword email with the title and subject set.
	 */
	public static Email getKeywordEmail(List<String> keywords, Paste paste) {

		Email email = new Email(EmailType.KEYWORD_EMAIL);
		email.setSubject(KEYWORD);
		String englishText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_en").getString("keyword");
		String germanText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_de").getString("keyword");
		String emailText = englishText + "\n " + germanText;

		StringBuilder linkBuilder = new StringBuilder();
		Config config = Config.getInstance();

		linkBuilder.append(" \n " + config.getBaseURL() + paste.getLink());

		emailText = emailText.replace("{{ LINK }}", linkBuilder.toString());

		StringBuilder keywordsBuilder = new StringBuilder();
		for (String keyword : keywords) {
			keywordsBuilder.append(" \n " + keyword);
		}
		emailText = emailText.replace("{{ KEYWORD }}",
				keywordsBuilder.toString());
		email.setContent(emailText);
		return email;
	}

	/**
	 * 
	 * @param changes
	 *            A set of changes that have occured in a given element.
	 * @param entity
	 *            The element that was changed.
	 * @return a notification change email with the title and subject set.
	 */
	public static Email getChangesEmail(Set<Changes> changes,
			SystemElement entity) {
		Email email = new Email(EmailType.CHANGE_NOTIFY);
		Config config = Config.getInstance();
		email.setSubject(CHANGE_NOTIFY);
		String englishText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_en").getString("change_paste");
		String germanText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_de").getString("change_paste");

		StringBuilder englishBuilder = new StringBuilder();
		for (Changes change : changes) {
			englishBuilder.append(change.getEnglish() + " \n ");
		}
		StringBuilder germanBuilder = new StringBuilder();
		for (Changes change : changes) {
			germanBuilder.append(change.getGerman() + " \n ");
		}
		englishText = englishText.replace("{{ CHANGES }}",
				englishBuilder.toString());
		germanText = germanText.replace("{{ CHANGES }}",
				germanBuilder.toString());

		String emailText = englishText + "\n " + germanText;
		emailText = emailText.replace("{{ LINK }}", config.getBaseURL()
				+ entity.getLink());
		email.setContent(emailText);
		return email;
	}

	/**
	 * @param user
	 *            The user who changed his email.
	 * @return an email change confirmation email with the title and subject
	 *         set.
	 */
	public static Email getEmailChangeConfirmationEmail(String emailNew,
			String user) {
		Email email = new Email(EmailType.EMAIL_CHANGE, user);
		email.setSubject(EMAIL_CHANGE);
		String englishText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_en").getString("email_change_email");
		String germanText = ResourceBundle.getBundle(
				"de.sep16g01.rip.lang.lang_de").getString("email_change_email");
		email.setContent(englishText + " \n " + germanText);
		email.setNewEmail(emailNew);
		return email;
	}
}
