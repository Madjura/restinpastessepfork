package de.sep16g01.rip.businessLogicLayer.email;

import java.io.Serializable;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.Config;
import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.dataAccessLayer.email.DAOEmailToken;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * Sends emails to a recipient with a given subject and content.
 */
public class Email implements Serializable {

	/**
	 * Logger instance for this class.
	 */
	private final static Logger logger = Logger.getLogger(Email.class);

	private static final long serialVersionUID = 1L;

	/**
	 * The recipient of the email.
	 */
	private String recipient;

	/**
	 * The content of the email.
	 */
	private String content;

	/**
	 * The subject of the email.
	 */
	private String subject;

	/**
	 * The element the email is about.
	 */
	private SystemElement element;

	/**
	 * The access that is being invited to.
	 */
	private Access access;

	/**
	 * The type of the email.
	 */
	private EmailType type;

	/**
	 * Name of the user sending the email.
	 */
	private String userName;

	/**
	 * The new email. Only used for email change emails.
	 */
	private String newEmail;

	/**
	 * Request parameter String that is used for the "type" in the request for
	 * registration confirmation emails.
	 */
	public static final String IDENTIFIER_REG = "reg";

	/**
	 * Request parameter String that is used for the "type" in the request for
	 * email change confirmation emails.
	 */
	public static final String IDENTIFIER_CHANGE = "change";

	/**
	 * Request parameter String that is used for the "type" in the request for
	 * password reset emails.
	 */
	public static final String IDENTIFIER_RESET = "reset";

	/**
	 * Request parameter String that is used for the "type" in the request for
	 * invite emails.
	 */
	public static final String IDENTIFIER_GROUPINVITE = "groupinvite";

	/**
	 * Request parameter STring that is used for the "type" in the request for
	 * system invite emails.
	 */
	public static final String IDENTIFIER_CLIPBOARDINVITE = "clipboardinvite";

	/**
	 * Constructor
	 * 
	 * @param type
	 *            The type the email is going to be.
	 */
	public Email(EmailType type) {
		this.type = type;
	}

	/**
	 * Constructor
	 * 
	 * @param type
	 *            The type the email is going to be.
	 * @param userName
	 *            The name of the user who is registering.
	 */
	public Email(EmailType type, String userName) {
		this.type = type;
		this.userName = userName;
	}

	public void setContent(String value) {
		this.content = value;
	}

	public String getContent() {
		return this.content;
	}

	public void setSubject(String value) {
		this.subject = value;
	}

	public String getSubject() {
		return this.subject;
	}

	/**
	 * Performs the email sending logic.
	 * 
	 * @throws MessagingException
	 * @throws AddressException
	 * @return A message object, ready to be sent.
	 */
	private Message initializeEmailSending(Set<String> emails) {

		// get data from config file
		final Config config = Config.getInstance();

		Session session = getSession();

		// initialize message and send email
		Message message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(config.getMailFrom()));
			if (emails != null) {
				for (String email : emails) {
					if (Email.isValidEmailAddress(email)) {
						message.addRecipients(Message.RecipientType.BCC,
								InternetAddress.parse(email));
					}
				}
			} else {
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(recipient));
			}
			message.setSubject(subject);
			message.setText(content);
		} catch (MessagingException e) {
			logger.warn("Unable to send email. Exception: " + e.getMessage());
		}
		return message;
	}

	/**
	 * Sends an email and modifies content before sending if necessary.
	 * 
	 * @param multipleRecipients
	 *            A set of emails, used for sending to multiple recipients. Can
	 *            be null if sending only to single person.
	 */
	public Message send(Set<String> multipleRecipients) {
		Message email = initializeEmailSending(multipleRecipients);

		// registration confirmation email
		try {
			if (type == EmailType.REGISTRATION_CONFIRM) {
				email = fillRegistrationEmail(email);
			} else if (type == EmailType.EMAIL_CHANGE) {
				email = fillEmailChangeEmail(email);
			} else if (type == EmailType.PASSWORD_RESET) {
				email = fillPasswortResetEmail(email);
			} else if (type == EmailType.GROUP_INVITE) {
				email = fillGroupInviteEmail(email);
			} else if (type == EmailType.CLIPBOARD_INVITE) {
				email = fillClipboardInviteEmail(email);
			} else if (type == EmailType.EMAIL_CHANGE) {
				email = fillEmailChangeEmail(email);
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		try {
			Transport.send(email);
		} catch (MessagingException e) {
			logger.warn("Email sending has failed. " + e.getMessage());
		}
		return email;
	}

	/**
	 * Fills an email for a registration confirmation email.
	 * 
	 * @param email
	 *            The message being filled to be sent.
	 * @return A message with the link including the confirmation token.
	 * @throws DataSourceException
	 *             , which is caught when user sends request
	 */
	private Message fillRegistrationEmail(Message email)
			throws DataSourceException {

		// config used to get access to the base url
		Config config = Config.getInstance();

		// make random token
		UUID id = generateToken();

		// store with user name
		DAOEmailToken.storeRegistrationToken(id, userName);

		// insert link to email
		String emailText = content.replace("{{ LINK }}",
				config.getBaseURL() + "/view/general/emailConfirm.xhtml?"
						+ SystemSettings.TYPE + "=" + IDENTIFIER_REG + "&"
						+ SystemSettings.TOKEN + "=" + id.toString() + "&"
						+ SystemSettings.USERID + "=" + userName);
		try {
			email.setText(emailText);
			return email;
		} catch (MessagingException e) {
			logger.warn("Registration email generation has failed.", e);
		}
		return null;
	}

	/**
	 * Fills an email change confirmation email.
	 * 
	 * @param email
	 *            A message with the link to confirm the email change.
	 * @return
	 * @throws DataSourceException
	 *             , which is caught when user sends request
	 */
	private Message fillEmailChangeEmail(Message email)
			throws DataSourceException {
		Config config = Config.getInstance();

		// generate random token
		UUID id = generateToken();

		// store new email, user and token
		// new email is later set to current upon clicking of link
		DAOEmailToken.storeEmailChangeToken(id, userName, newEmail);
		String emailText = content.replace("{{ LINK }}", config.getBaseURL()
				+ "/view/general/emailConfirm.xhtml?type=" + IDENTIFIER_CHANGE
				+ "&" + SystemSettings.TOKEN + "=" + id.toString() + "&"
				+ SystemSettings.USERID + "=" + userName);
		try {
			email.setText(emailText);
			return email;
		} catch (MessagingException e) {
			logger.warn("Email-Change email generation has failed.", e);
		}
		return email;
	}

	/**
	 * Fills a password reset confirmation email.
	 * 
	 * @param email
	 *            The message being filled.
	 * @return A message with the link to reset the password.
	 * @throws DataSourceException
	 */
	private Message fillPasswortResetEmail(Message email)
			throws DataSourceException {
		Config config = Config.getInstance();

		// generate token
		UUID id = generateToken();

		// store token with user
		try {
			DAOEmailToken.storePasswordResetToken(id, userName);
		} catch (DataSourceException e) {
			logger.warn("Password reset email generation has failed.", e);
		}
		// generate text
		String emailText = content.replace("{{ LINK }}", config.getBaseURL()
				+ "/view/auth/forgotPasswordChange.xhtml?"
				+ SystemSettings.TYPE + "=" + IDENTIFIER_RESET + "&"
				+ SystemSettings.TOKEN + "=" + id.toString() + "&"
				+ SystemSettings.USERID + "=" + userName);
		try {
			email.setText(emailText);
			return email;
		} catch (MessagingException e) {
			logger.warn("Password reset email generation has failed.", e);
		}
		return null;
	}

	/**
	 * Fills a group invite email email and stores the unique token to the DB.
	 * 
	 * @param email
	 *            The email being sent.
	 * @return The message with the link added.
	 * @throws DataSourceException
	 */
	private Message fillGroupInviteEmail(Message email)
			throws DataSourceException {
		Config config = Config.getInstance();

		UUID id = generateToken();
		DAOEmailToken.storeGroupToken(id, (Group) element, access);

		String emailText = content.concat(" " + config.getBaseURL()
				+ "/view/general/emailConfirm.xhtml?" + SystemSettings.TYPE
				+ "=" + IDENTIFIER_GROUPINVITE + "&" + SystemSettings.GROUPID
				+ "=" + element.getId() + "&" + SystemSettings.TOKEN + "="
				+ id.toString());

		try {
			email.setText(emailText);
			return email;
		} catch (MessagingException e) {
			logger.warn("Group invite email generation has failed."
					+ e.getMessage());
		}
		return null;
	}

	/**
	 * Fills a clipboard invite email email and stores the unique token to the
	 * database.
	 * 
	 * @param email
	 *            The email being sent.
	 * @return The message with the link added.
	 * @throws DataSourceException
	 */
	private Message fillClipboardInviteEmail(Message email)
			throws DataSourceException {
		Config config = Config.getInstance();

		UUID id = generateToken();
		DAOEmailToken.storeClipboardToken(id, (Clipboard) element, access);

		String emailText = content.concat(" " + config.getBaseURL()
				+ "/view/general/emailConfirm.xhtml?" + SystemSettings.TYPE
				+ "=" + IDENTIFIER_CLIPBOARDINVITE + "&"
				+ SystemSettings.CLIPBOARDID + "=" + element.getId() + "&"
				+ SystemSettings.TOKEN + "=" + id.toString());

		try {
			email.setText(emailText);
			return email;
		} catch (MessagingException e) {
			logger.warn("Group invite email generation has failed."
					+ e.getMessage());
		}
		return null;
	}

	/**
	 * Generates a unique ID that can be stored in the database to verify
	 * whether or not a user followed a valid link get invited to an object.
	 * Called by {@link #send} if the email that is being sent requires a token.
	 * Whether or not this is required is determined by {@link #type}.
	 * 
	 * @return A randomly generated ID.
	 */
	private UUID generateToken() {
		return UUID.randomUUID();
	}

	public SystemElement getElement() {
		return element;
	}

	public void setElement(SystemElement element) {
		this.element = element;
	}

	public EmailType getType() {
		return type;
	}

	public void setType(EmailType type) {
		this.type = type;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getNewEmail() {
		return newEmail;
	}

	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;
	}

	public String getUserName() {
		return this.userName;
	}

	public Access getAccess() {
		return this.access;
	}

	public void setAccess(Access access) {
		this.access = access;
	}

	/**
	 * Validates email addresses.
	 * 
	 * @param email
	 *            The email.
	 * @return True if the email is valid, false if not.
	 */
	public static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	@Override
	public String toString() {
		return "Email [recipient=" + this.recipient + ", element="
				+ (this.element != null ? this.element.getId() : "")
				+ ", access=" + this.access + ", type=" + this.type
				+ ", userName=" + this.userName + ", newEmail=" + this.newEmail
				+ "]";
	}

	public Session getSession() {
		final Config config = Config.getInstance();

		// debug stuff for greenmail
		if (config.getDebug()) {
			Properties properties = System.getProperties();
			properties.put("mail.smtp.host", "127.0.0.1");
			properties.put("mail.smtp.port", "3025");
			return Session.getInstance(properties,
					new javax.mail.Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(config
									.getMailLogin(), config.getMailPassword());
						}
					});
		}

		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "465");

		return Session.getDefaultInstance(properties,
				new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								config.getMailLogin(), config.getMailPassword());
					}
				});
	}
}