package de.sep16g01.rip.businessLogicLayer.home;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.session.UserData;

/**
 * Backing bean for the menubar that is always displayed at the top. Holds the
 * search field, the logout button, the create button, the help button and the
 * link the user's profile.
 */
@ViewScoped
@Named
public class BBMenuBar implements Serializable {

	@Inject
	private UserData userData;

	public UserData getUserData() {
		return this.userData;
	}

	private static final long serialVersionUID = 1L;

	/**
	 * @return the systemelement types a user can create
	 */
	public List<SystemElementType> getUserCreateTypes() {
		List<SystemElementType> list = new ArrayList<SystemElementType>();
		list.add(SystemElementType.CLIPBOARD);
		list.add(SystemElementType.PASTE);

		if (userData.isLoggedIn()) {
			list.add(SystemElementType.GROUP);
		}
		return list;
	}

	/**
	 * The value selected in the dropdown to create elements.
	 */
	private SystemElementType createNewDropDownElement;

	/**
	 * Directs the user to the invite page for the system invite.
	 * 
	 * @return A link to the invite page for the system invite.
	 */
	public String invite() {
		return "/view/permissions/invite?faces-redirect=true";
	}

	/**
	 * Logs a user out and also removes his entry from the session.
	 * 
	 * @return A link to the main page of the system.
	 */
	public String logout() {
		FacesContext.getCurrentInstance().addMessage("success",
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Logout successful!", null));
		userData.setUser(null);
		return "/view/home/home?faces-redirect=true"; // TODO test
	}

	/**
	 * Directs the user to the creation page of the selected item.
	 * 
	 * @return A link to the creation page of the chosen object.
	 */
	public String create() {

		if (createNewDropDownElement == SystemElementType.CLIPBOARD) {
			return "/view/clipboard/createClipboard.xhtml?faces-redirect=true";
		} else if (createNewDropDownElement == SystemElementType.PASTE) {
			return "/view/paste/createPaste.xhtml?faces-redirect=true";
		} else if (createNewDropDownElement == SystemElementType.GROUP) {
			return "/view/group/createGroup.xhtml?faces-redirect=true";
		}

		return null;
	}

	/**
	 * Sets the locale to German and displays the text in German.
	 * 
	 * @return A link to the page the user is on to force a refresh.
	 */
	public String setLocaleGerman() {
		// moved to BBLang
		return null;
	}

	/**
	 * Sets the locale to English and displays the text in English.
	 * 
	 * @return A link to the page the user is on to force a refresh.
	 */
	public String setLocaleEnglish() {
		// moved to BBLang
		return null;
	}

	public SystemElementType getCreateNewDropDownElement() {
		return createNewDropDownElement;
	}

	public void setCreateNewDropDownElement(
			SystemElementType createNewDropDownElement) {
		this.createNewDropDownElement = createNewDropDownElement;
	}

	public String showOwnProfile() {

		if (userData.isLoggedIn()) {
			return "/view/profile/profile.xhtml?faces-redirect=true" + "&"
					+ SystemSettings.USERID + "=" + userData.getUser().getId();
		}
		return null;
	}

	public String getProfilePictureLink() {
		if (userData.isLoggedIn()) {
			return "/data?id=" + userData.getUser().getId()
					+ "&type=profilepicture";
		}
		return null; // TODO add default
	}
}
