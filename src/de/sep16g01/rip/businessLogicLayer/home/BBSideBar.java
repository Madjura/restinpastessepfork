package de.sep16g01.rip.businessLogicLayer.home;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.session.UserData;

/**
 * Backing bean for the sidebar that displays the user's clipboards and all the
 * groups he is a member of.
 */
@ViewScoped
@Named
public class BBSideBar extends BBExceptionHandler implements Serializable {

	@Inject
	private UserData userData;

	private static final long serialVersionUID = 1L;

	/**
	 * Holds all the clipboards of the currently logged in user.
	 */
	private BBPaginator<Clipboard> userClipboards;

	/**
	 * Holds all the groups of the currently logged in user.
	 */
	private BBPaginator<Group> userGroups;

	public void setUserClipboards(BBPaginator<Clipboard> value) {
		this.userClipboards = value;
	}

	public BBPaginator<Clipboard> getUserClipboards() {
		return this.userClipboards;
	}

	public void setUserGroups(BBPaginator<Group> value) {
		this.userGroups = value;
	}

	public BBPaginator<Group> getUserGroups() {
		return this.userGroups;
	}

	/**
	 * Fills {@link #userClipboards} and {@link userGroups} with the user's data
	 * from the database.
	 * 
	 */
	@PostConstruct
	private void init() {
		if (userData.isLoggedIn()) {
			Map<String, String> params = FacesContext.getCurrentInstance()
					.getExternalContext().getRequestParameterMap();

			Map<String, String> copy = new HashMap<String, String>();
			for (Map.Entry<String, String> entry : params.entrySet()) {
				copy.put(entry.getKey(), entry.getValue());
			}

			String paginateQueryClipboards = "userId="
					+ userData.getUser().getId() + ";type=user";
			int pageClipboard = 1;
			if (params.get("pageClipboard") != null) {
				pageClipboard = Integer.parseInt(params.get("pageClipboard"));
			}
			userClipboards = new BBPaginator<Clipboard>(new Clipboard());
			userClipboards.setQuery(paginateQueryClipboards);
			userClipboards.setPageParamName("pageClipboard");
			userClipboards.gotoPage(pageClipboard);
			userClipboards.setOtherPageParameters(copy);

			String paginateQueryGroups = "userId=" + userData.getUser().getId()
					+ ";type=content;value=";
			int pageGroup = 1;
			if (params.get("pageGroup") != null) {
				pageGroup = Integer.parseInt(params.get("pageGroup"));
			}
			userGroups = new BBPaginator<Group>(new Group());
			userGroups.setQuery(paginateQueryGroups);
			userGroups.setPageParamName("pageGroup");
			userGroups.gotoPage(pageGroup);
			userGroups.setOtherPageParameters(copy);

			userClipboards.putOtherPageParameter("pageGroup",
					params.get("pageGroup"));
			userGroups.putOtherPageParameter("pageClipboard",
					params.get("pageClipboard"));
			// add page param from BBMain
			userClipboards.putOtherPageParameter("page", params.get("page"));
			userGroups.putOtherPageParameter("page", params.get("page"));
		} else {

		}
	}

	public String goToBookmarks() {
		return "/view/profile/bookmarks.xhtml?faces-redirect=true&"
				+ SystemSettings.BOOKMARKID + "=" + userData.getUser().getId();

	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}
}
