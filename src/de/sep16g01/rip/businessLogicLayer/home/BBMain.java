package de.sep16g01.rip.businessLogicLayer.home;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the main page of the site.
 */
@ViewScoped
@Named
public class BBMain extends BBExceptionHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	private BBPaginator<Clipboard> clipboards;

	public BBPaginator<Clipboard> getClipboards() {
		return clipboards;
	}

	@Inject
	private UserData userData;

	public BBMain() {
	}

	/**
	 * Loads the clipboards of the user
	 * 
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String page = params.get("page");
		if (userData.isLoggedIn()) {
			clipboards = new BBPaginator<Clipboard>(new Clipboard());
			clipboards.setQuery(
					"type=content;value=;userId=" + userData.getUser().getId());
			clipboards.setSystemElementType(SystemElementType.CLIPBOARD);
			clipboards.gotoPage(page);
			// add page params from BBSideBar
			clipboards.putOtherPageParameter("pageClipboard",
					params.get("pageClipboard"));
			clipboards.putOtherPageParameter("pageGroup",
					params.get("pageGroup"));
		}
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

}
