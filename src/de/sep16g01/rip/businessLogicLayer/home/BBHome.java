package de.sep16g01.rip.businessLogicLayer.home;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

/**
 * Backing bean for the home page of the site. Responsible for letting the user
 * continue as an anonymous user.
 */
@RequestScoped
@Named
public class BBHome implements Serializable {

	@Inject
	private UserData userData;

	private static final long serialVersionUID = 1L;

	/**
	 * Lets a user use the system as anonymous user.
	 * 
	 * @return A link to the main page of the system.
	 */
	public String continueAsAnonymous() {
		if (!userData.isLoggedIn()) {
			try {
				userData.setUser(DAOUser.getById(-1));
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
		}
		return "/view/home/main.xhtml?faces-redirect=true";
	}

}
