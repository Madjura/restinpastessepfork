package de.sep16g01.rip.businessLogicLayer.group;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.DisplayPage;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.businessLogicLayer.util.ParameterUtils;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for group creation and edit. Creation or edit mode is determined
 * by presence of an identifier in the url. If a group with the identifier
 * exists and the user can write to it, he can edit it. Otherwise an error is
 * returned to him. Holds the group name and provides the functionality to
 * create a group.
 */
@ViewScoped
@Named
public class BBCreateGroup extends DisplayPage {
	private final static Logger logger = Logger.getLogger(BBCreateGroup.class);

	private static final long serialVersionUID = 1L;

	/**
	 * Used to access the user and checking whether or not the user is logged in
	 * or anonymous easily.
	 */
	@Inject
	private UserData userData;

	/**
	 * The group that is being created or modified.
	 */
	private Group group;

	/**
	 * The id of the group.
	 */
	private Integer groupId;

	/**
	 * The read visibility of this group.
	 */
	private Visibility read = Visibility.PUBLIC;

	/**
	 * The write visibility of this group.
	 */
	private Visibility write = Visibility.PUBLIC;

	@Inject
	private BBChangeLanguage lang;

	public String cancel() {
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		Integer groupIdInteger = ParameterUtils.readIntegerParameter(params,
				SystemSettings.GROUPID);
		return "/view/group/group.xhtml?" + SystemSettings.GROUPID + "="
				+ groupIdInteger;
	}

	/**
	 * Responsible for having the {@link BBCreateGroup#group} saved to the
	 * database.
	 * 
	 */
	public String save() {
		if (!userData.isLoggedIn()) {

		}
		if (group.getId() == -1) {
			group.setCreationDate(new Date());
		}

		try {
			if (group.getId() == SystemElement.DEFAULT_ID) {
				if (Group.getGroupByName(group.getTitle()) != null) {
					FacesContext.getCurrentInstance()
							.addMessage(
									"error",
									new FacesMessage(lang
											.getLangString("group_exists")));
					return null;
				}
			}
			group.setLastModifiedDate(new Date());
			group.setVisibilityRead(read);
			group.setVisibilityWrite(write);

			int groupId1 = Group.store(group);

			// add creator
			Group.addUser(userData.getUser().getId(), groupId1);
			logger.info("Saving " + group + ", User: " + userData.getUser());
			return "/view/group/group.xhtml?faces-redirect=true&"
					+ SystemSettings.GROUPID + "=" + String.valueOf(groupId1);
		} catch (DataSourceException e) {
			logger.info("Exception while saving " + group + ", User: "
					+ userData.getUser());
			BBExceptionHandler.handleException(e);
		}
		return "";
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	/**
	 * Creates or loads the {@link #group} object.
	 * 
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		// load from request or make new
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		Integer groupIdInteger = ParameterUtils.readIntegerParameter(params,
				SystemSettings.GROUPID);
		if (groupIdInteger == null
				|| groupIdInteger == SystemElement.DEFAULT_ID) {
			// create new, empty group object
			group = new Group();
		} else {
			try {
				groupId = groupIdInteger;
				group = Group.get(groupIdInteger);
			} catch (NumberFormatException | DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}

			if (group == null) {
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(
								"Illegal ID provided. Group does not exist."));
			} else {
				// visibilites are set in the DAO
				read = group.getVisibilityRead();
				write = group.getVisibilityWrite();
			}
		}
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	public List<Visibility> getVisibilities() {
		return Visibility.getAllVisibilities2();
	}

	public Visibility getRead() {
		return read;
	}

	public void setRead(Visibility read) {
		this.read = read;
	}

	public Visibility getWrite() {
		return write;
	}

	public void setWrite(Visibility write) {
		this.write = write;
	}

	/**
	 * Redirects to the invite page for the given access.
	 * 
	 * @param access
	 *            The access that is being invited to.
	 * @return
	 */
	public String addPermissions(Access access) {
		String url = "/view/permissions/managePermissions.xhtml?faces-redirect=true&"
				+ SystemSettings.GROUPID
				+ "="
				+ group.getId()
				+ "&"
				+ SystemSettings.ACCESSID + "=" + access.getPriority();
		return url;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.DisplayPage#checkRead()
	 */
	@Override
	public boolean checkRead() {
		if (!userData.isLoggedIn()) {
			return group.getVisibilityRead() == Visibility.PUBLIC;
		}
		if (group.getId() == SystemElement.DEFAULT_ID) {
			return false;
		}

		try {
			Access access = Group.checkGroupUserAccess(group.getId(), userData
					.getUser().getId());
			if (access.getPriority() >= Access.READ.getPriority()) {
				return true;
			} else if (Group.checkMembership(group.getId(), userData.getUser()
					.getId())) {
				return true;
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.DisplayPage#checkWrite()
	 */
	@Override
	public boolean checkWrite() {
		if (!userData.isLoggedIn()) {
			return group.getVisibilityWrite() == Visibility.PUBLIC;
		}
		if (group.getId() == SystemElement.DEFAULT_ID) {
			return false;
		}

		try {
			Access access = Group.checkGroupUserAccess(group.getId(), userData
					.getUser().getId());
			if (access.getPriority() >= Access.READ.getPriority()) {
				return true;
			} else if (Group.checkMembership(group.getId(), userData.getUser()
					.getId())) {
				return true;
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return false;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
}
