package de.sep16g01.rip.businessLogicLayer.group;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.pagination.Paginatable;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.permission.DAOPermission;

/**
 * DTO for the groups. Groups are a collection of users that own clipboards
 * together. Holds a list of members, the name of the group and a list of
 * clipboards belonging to this group.
 */
public class Group extends SystemElement implements Paginatable<Group> {

	private static final long serialVersionUID = 1L;

	private Visibility visibilityRead;
	private Visibility visibilityWrite;

	public Group() {
		this.setTitle("Group");
	}

	public Visibility getVisibilityRead() {
		return visibilityRead;
	}

	public void setVisibilityRead(Visibility visibilityRead) {
		this.visibilityRead = visibilityRead;
	}

	public Visibility getVisibilityWrite() {
		return visibilityWrite;
	}

	public void setVisibilityWrite(Visibility visibilityWrite) {
		this.visibilityWrite = visibilityWrite;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.SystemElement#getLink()
	 */
	@Override
	public String getLink() {
		return "/view/group/group.xhtml?groupid=" + this.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.sep16g01.rip.businessLogicLayer.pagination.Paginatable#paginate(java.
	 * lang.String, int, int)
	 */
	@Override
	public PaginatedDataModel<Group> paginate(String query, int page,
			int entriesPerPage) {
		try {
			return DAOGroup.paginate(query, page, entriesPerPage);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return null;
	}

	public static Group get(int id) throws DataSourceException {
		return DAOGroup.get(id);

	}

	public static boolean checkMembership(int userId, int groupId)
			throws DataSourceException {
		return DAOGroup.checkMembership(userId, groupId);
	}

	public static int getMemberCount(int groupId) throws DataSourceException {
		return DAOGroup.getMemberCount(groupId);
	}

	public static void dropGroup(String name) throws DataSourceException {
		DAOGroup.dropGroup(name);
	}

	public static void removeUser(int userId, int groupId)
			throws DataSourceException {
		DAOGroup.removeUser(userId, groupId);
	}

	public static void addUser(int userId, int groupId)
			throws DataSourceException {
		DAOGroup.addUser(userId, groupId);
	}

	public static Access checkGroupUserAccess(int groupId, int userId)
			throws DataSourceException {
		return DAOPermission.checkGroupUserAccess(groupId, userId);
	}

	public static Group getGroupByName(String name) throws DataSourceException {
		return DAOGroup.getGroupByName(name);
	}

	public static int store(Group group) throws DataSourceException {
		return DAOGroup.store(group);
	}

	@Override
	public String toString() {
		return "Group [id=" + this.id + ", visibilityRead="
				+ this.visibilityRead + ", visibilityWrite="
				+ this.visibilityWrite + "]";
	}

}
