package de.sep16g01.rip.businessLogicLayer.group;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.DisplayPage;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.invite.BBInvite;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for a single group. Used to display and hold the information of
 * a single group.
 */
@ViewScoped
@Named
public class BBGroup extends DisplayPage {
	private final static Logger logger = Logger.getLogger(BBGroup.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The group whose information is being displayed.
	 */
	private Group group;

	/**
	 * Paginator for clipboards.
	 */
	private BBPaginator<Clipboard> clipboards;

	/**
	 * Paginator for members.
	 */
	private BBPaginator<User> members;

	/**
	 * Injected user data.
	 */
	@Inject
	private UserData userData;

	/**
	 * Injected lang bean.
	 */
	@Inject
	private BBChangeLanguage lang;

	/**
	 * Whether user must confirm leaving. Used when user is last member and is
	 * leaving.
	 */
	private boolean deleteConfirmLeave = false;

	/**
	 * Whether use must confirm deleting group.
	 */
	private boolean deleteConfirmGroup = false;

	/**
	 * Whether user must confirm removing a user. Used when the last member is
	 * about to be removed.
	 */
	private boolean letRemove = false;

	/**
	 * Loads the {@link group} from the database, based on the id in the request
	 * parameters.
	 * 
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String id = params.get(SystemSettings.GROUPID);
		if (id == null || id.equals("")) {
			group = null;
			FacesContext.getCurrentInstance().addMessage("error",
					new FacesMessage(lang.getLangString("no_group")));
			return;
		}

		try {
			group = Group.get(Integer.parseInt(id));

			clipboards = new BBPaginator<Clipboard>(new Clipboard());
			clipboards.setQuery("type=group;groupId=" + group.getId());
			clipboards.setSystemElementType(SystemElementType.CLIPBOARD);
			String pageClipboards = params.get("pageClipboards");
			clipboards.setPageParamName("pageClipboards");
			clipboards.gotoPage(pageClipboards);

			members = new BBPaginator<User>(new User());
			members.setQuery("type=group;groupId=" + group.getId());
			members.setSystemElementType(SystemElementType.USER);
			String pageMembers = params.get("pageMembers");
			members.setPageParamName("pageMembers");
			members.gotoPage(pageMembers);

			clipboards.putOtherPageParameter("pageMembers", pageMembers);
			members.putOtherPageParameter("pageClipboards", pageClipboards);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
	}

	/**
	 * Lets a user leave the group. Deletes the group if user was the last
	 * member.
	 * 
	 * @param letLeave
	 *            Whether or not the user can leave without an additional
	 *            confirmation.
	 * @return
	 */
	public String leave(boolean letLeave) {
		if (!userData.isLoggedIn()) {
			return "";
		}

		User user = userData.getUser();
		try {
			if (Group.checkMembership(user.getId(), group.getId())) {
				if (!letLeave && Group.getMemberCount(group.getId()) == 1) {
					FacesContext.getCurrentInstance().addMessage(
							"error",
							new FacesMessage(lang
									.getLangString("group_leave_warn")));
					setDeleteConfirmLeave(true);
					return null;
				}
				DAOGroup.removeUser(user.getId(), group.getId());
				if (Group.getMemberCount(group.getId()) == 0) {
					Group.dropGroup(group.getTitle());
				}
			} else {
				logger.info(user + " tried to leave " + group
						+ "! The user was no group member!");
				FacesContext.getCurrentInstance()
						.addMessage(
								"error",
								new FacesMessage(lang
										.getLangString("not_group_member")));
			}
			if (lang != null) {
				logger.info(user + " left " + group);
				FacesContext.getCurrentInstance().addMessage(
						"success",
						new FacesMessage(lang
								.getLangString("group_leave_success")));
			}
		} catch (DataSourceException e) {
			logger.error("Exception while " + user + " tried to leave " + group);
			BBExceptionHandler.handleException(e);
		}
		return "/view/home/main.xhtml?faces-redirect=true";
	}

	/**
	 * Creates a clipboard for the group.
	 * 
	 * @return
	 */
	public String createClipboard() {
		return "/view/clipboard/createClipboard.xhtml?faces-redirect=true"
				+ SystemSettings.GROUPID + "=" + group.getId();
	}

	/**
	 * Removes a user from the group. Requires write permission on the group.
	 * 
	 * @param user
	 *            The user that is being removed.
	 * @return A link to the group page.
	 */
	public String deleteUser(User user, boolean letRemove1) {
		if (userData.getUser().getRole().getPriority() < Role.ADMINISTRATOR
				.getPriority() || !checkWrite()) {

			logger.info(userData.getUser() + " tried to delete " + user
					+ " from " + group);
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(lang
							.getLangString("group_delete_member_permission")));
			return null;
		}
		try {
			if (!letRemove1 && Group.getMemberCount(group.getId()) == 1) {
				setLetRemove(true);
				logger.info(userData.getUser() + " tried to remove the last "
						+ user + " from " + group);
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(lang
								.getLangString("last_member_remove")));
				return null;
			}
		} catch (DataSourceException e1) {
			BBExceptionHandler.handleException(e1);
			return "";
		}

		try {
			Group.removeUser(user.getId(), group.getId());
			logger.info(userData.getUser() + " removed " + user + " from "
					+ group);
			if (Group.getMemberCount(group.getId()) == 0) {
				Group.dropGroup(group.getTitle());
				return "/view/home/main.xhtml?faces-redirect=true";
			}

		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return "";
		}
		logger.info(userData.getUser() + " deleted " + user + " from " + group);
		FacesContext.getCurrentInstance().addMessage("info",
				new FacesMessage(lang.getLangString("group_member_removed")));
		return "/view/group/group.xhtml?faces-redirect=true&"
				+ SystemSettings.GROUPID + "=" + String.valueOf(group.getId());
	}

	/**
	 * Lets a user join the group.
	 * 
	 * @return A link to the group page.
	 */
	public String join() {
		if (!userData.isLoggedIn()) {
			return "";
		}
		User user = userData.getUser();
		try {
			if (checkWrite()
					&& !Group.checkMembership(user.getId(), group.getId())) {
				logger.info(user + " joined " + group);
				Group.addUser(userData.getUser().getId(), group.getId());
			} else {
				logger.info(user + " tried to join " + group
						+ "! Already group member!");
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(lang
								.getLangString("group_join_already_member")));
			}
		} catch (DataSourceException e) {
			logger.error("Exception while " + user + " tried to join " + group
					+ "!");
			BBExceptionHandler.handleException(e);
			return "";
		}
		return "/view/group/group.xhtml?faces-redirect=true&"
				+ SystemSettings.GROUPID + "=" + String.valueOf(group.getId());
	}

	public void setGroup(Group value) {
		this.group = value;
	}

	public Group getGroup() {
		return this.group;
	}

	/**
	 * Directs to the edit page of the group. Requires write permission.
	 * 
	 * @return A link to the edit page of the group.
	 */
	public String edit() {
		if (checkWrite()) {
			return "/view/group/createGroup.xhtml?faces-redirect=true"
					+ SystemSettings.GROUPID + "=" + group.getId()
					+ "&backurl=/view/home/home.xhtml";
		}
		FacesContext.getCurrentInstance().addMessage("error",
				new FacesMessage(lang.getLangString("group_edit_permission")));
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public boolean checkRead() {
		if (!userData.isLoggedIn()) {
			return group.getVisibilityRead() == Visibility.PUBLIC;
		}
		if (group == null) {
			return false;
		}
		try {
			Access access = Group.checkGroupUserAccess(group.getId(), userData
					.getUser().getId());
			if (access.getPriority() >= Access.READ.getPriority()) {
				return true;
			} else if (Group.checkMembership(group.getId(), userData.getUser()
					.getId())) {
				return true;
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return false;
	}

	public BBPaginator<Clipboard> getClipboards() {
		return clipboards;
	}

	public BBPaginator<User> getMembers() {
		return members;
	}

	/**
	 * @return the userData
	 */
	public UserData getUserData() {
		return userData;
	}

	/**
	 * @param userData
	 *            the userData to set
	 */
	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.DisplayPage#checkWrite()
	 */
	@Override
	public boolean checkWrite() {
		if (!userData.isLoggedIn()) {
			return group.getVisibilityWrite() == Visibility.PUBLIC;
		}
		if (group == null) {
			return false;
		}

		try {
			Access access = Group.checkGroupUserAccess(group.getId(), userData
					.getUser().getId());
			if (access.getPriority() >= Access.READ.getPriority()) {
				return true;
			} else if (Group.checkMembership(group.getId(), userData.getUser()
					.getId())) {
				return true;
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return false;
	}

	/**
	 * Checks whether or not the logged in user is a member of the group.
	 * 
	 * @return Whether or not the user is a member of the group.
	 */
	public boolean isMember() {
		if (!userData.isLoggedIn() || group == null) {
			return false;
		}

		try {
			return Group.checkMembership(userData.getUser().getId(),
					group.getId());
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return false;
	}

	public String inviteWrite() {
		return "/view/permissions/invite.xhtml?faces-redirect=true"
				+ SystemSettings.ACCESSID + "=" + Access.WRITE.getPriority()
				+ "&" + SystemSettings.ID_IDENTIFIER + "=" + group.getId()
				+ "&" + BBInvite.INVITE_IDENTIFY + "=" + "group&"
				+ SystemSettings.GROUPID + "=" + group.getId();
	}

	public String inviteRead() {
		return "/view/permissions/invite.xhtml?faces-redirect=true"
				+ SystemSettings.ACCESSID + "=" + Access.READ.getPriority()
				+ "&" + SystemSettings.ID_IDENTIFIER + "=" + group.getId()
				+ "&" + BBInvite.INVITE_IDENTIFY + "=" + "group&"
				+ SystemSettings.GROUPID + "=" + group.getId();
	}

	public boolean isDeleteConfirmLeave() {
		return deleteConfirmLeave;
	}

	public void setDeleteConfirmLeave(boolean deleteConfirm) {
		this.deleteConfirmLeave = deleteConfirm;
	}

	public BBChangeLanguage getLang() {
		return lang;
	}

	public void setLang(BBChangeLanguage lang) {
		this.lang = lang;
	}

	public boolean isLetRemove() {
		return letRemove;
	}

	public void setLetRemove(boolean letRemove) {
		this.letRemove = letRemove;
	}

	public String delete(boolean letDelete) {
		if ((isMember() && checkWrite())
				|| userData.getUser().getRole().getPriority() >= Role.ADMINISTRATOR
						.getPriority()) {
			if (!letDelete) {
				logger.info(userData.getUser() + " tried to delete " + group
						+ "! User needs to confirm first!");
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(lang
								.getLangString("group_delete_confirm")));
				setDeleteConfirmGroup(true);
				return null;
			}
			try {
				Group.dropGroup(group.getTitle());
				logger.info(userData.getUser() + " deleted " + group);
			} catch (DataSourceException e) {
				logger.error("Exception while " + userData.getUser()
						+ " tried to delete " + group);
				BBExceptionHandler.handleException(e);
			}
			return "/view/home/main.xhtml?faces-redirect=true";
		}
		return null;
	}

	public boolean isDeleteConfirmGroup() {
		return deleteConfirmGroup;
	}

	public void setDeleteConfirmGroup(boolean deleteConfirmGroup) {
		this.deleteConfirmGroup = deleteConfirmGroup;
	}
}
