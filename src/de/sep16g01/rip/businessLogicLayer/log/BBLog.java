package de.sep16g01.rip.businessLogicLayer.log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

/**
 * Reads in System logging event from the current log file and displays them.
 */
@RequestScoped
@Named
public class BBLog implements Serializable {

	/**
	 * Logger instance for this class.
	 */
	private static final Logger logger = Logger.getLogger(BBLog.class);

	private static final long serialVersionUID = 1L;

	/**
	 * The string representation of the system log.
	 */
	private String log;

	public String getLog() {
		return log;
	}

	/**
	 * Loads the log.
	 */
	@PostConstruct
	private void init() {
		Logger logger1 = Logger.getRootLogger();
		FileAppender appender = (FileAppender) logger1.getAppender("file");

		try (BufferedReader br = new BufferedReader(new FileReader(
				appender.getFile()));) {
			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}

			log = sb.toString();
		} catch (FileNotFoundException e) {
			BBLog.logger.warn("Log file not found.", e);
		} catch (IOException e) {
			BBLog.logger.warn("Error while reading lines from log.", e);
		}
	}

	public String refresh() {
		return "";
	}

	public void testInit() {
		init();
	}
}
