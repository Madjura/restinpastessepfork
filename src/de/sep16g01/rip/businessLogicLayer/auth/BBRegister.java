package de.sep16g01.rip.businessLogicLayer.auth;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.Config;
import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.email.Email;
import de.sep16g01.rip.businessLogicLayer.email.EmailFactory;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.util.HashString;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.email.DAOEmailToken;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.permission.DAOPermission;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the register page. Holds the user's username, the password
 * entered twice to prevent typos and the user's email. Displays error message
 * if there are any errors with the input (i.e. username is already taken). Lets
 * the user register and directs the user to the main page of the system.
 */
@ViewScoped
@Named
public class BBRegister implements Serializable {
	private final static Logger logger = Logger.getLogger(BBRegister.class);
	
	private static final long serialVersionUID = 1L;

	private final boolean TEST_MODE = Config.getInstance().getDebug();

	/**
	 * The name the user has entered.
	 */
	private String name;

	/**
	 * The password the user has entered.
	 */
	private String password = null;

	/**
	 * The password confirmation the user has entered.
	 */
	private String passwordConfirmation;

	/**
	 * The email the user has entered.
	 */
	private String email;

	private String type = null;
	private UUID token = null;
	private int id = -1;

	@Inject
	private BBChangeLanguage lang;

	public void setName(String value) {
		this.name = value;
	}

	public String getName() {
		return this.name;
	}

	public void setPassword(String value) {
		this.password = value;
	}

	public String getPassword() {
		return this.password;
	}

	public void setEmail(String value) {
		this.email = value;
	}

	public String getEmail() {
		return this.email;
	}

	public void setPasswordConfirmation(String value) {
		this.passwordConfirmation = value;
	}

	public String getPasswordConfirmation() {
		return this.passwordConfirmation;
	}

	/**
	 * Sends an email with a confirmation link to the given email address.
	 * 
	 * @throws DataSourceException
	 */
	private void sendRegistrationEmail() throws DataSourceException {
		Email registrationConfirm = EmailFactory.getRegistrationConfirm(name);
		registrationConfirm.setRecipient(email);
		registrationConfirm.send(null);
		logger.info("Sending Registration-Confirmation-Email " + email + ".");
	}

	/**
	 * Creates and returns a user object from the data that was entered.
	 * 
	 * @return A {@link de.sep16g01.rip.businessLogicLayer.profile.User User}
	 *         object holding the entered data.
	 */
	private User makeUserFromInput() {

		// new random salt
		byte[] salt = HashString.getNextSalt();

		// random salt + input
		byte[] passwordHash = HashString
				.hash(this.password.toCharArray(), salt);
		User user = new User(name, passwordHash, salt);
		user.setEmail(email);

		// default role is always registered
		user.setRole(Role.REGISTERED);
		try {
			user.setFreeCapacity(DAOSystemSettings.loadMaxSizePerRole().get(
					Role.REGISTERED));
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return null;
		} // TODO check Exception

		// current date
		Date date = new java.util.Date();
		user.setRegistrationDate(date);
		return user;
	}

	/**
	 * Checks whether or not a user with a certain name already exists in the
	 * system. by checking against the entries in the database.
	 * 
	 * @param name1
	 *            The name that is being checked.
	 * @return {@code true} if a user with the name does not already exist in
	 *         the system. {@code false} if a user with the name already does
	 *         exist in in the system.
	 * @throws DataSourceException
	 */
	private boolean isNameUnique(String name1) throws DataSourceException {
		return User.isNameUnique(name1);
	}

	/**
	 * Checks whether or not a user with a certain email already exists in the
	 * system.
	 * 
	 * @param email1
	 * @return
	 * @throws DataSourceException
	 */
	private boolean isEmailUnique(String email1) throws DataSourceException {
		return User.isEmailUnique(email1);
	}

	/**
	 * Creates a new user in the system by taking the user's input and creating
	 * a new entry in the database. Displays a message to the user if the chosen
	 * username already exists or if the data he has entered was invalid (such
	 * as an invalid email).
	 * 
	 * @return A link to the main page of the system if the registration was
	 *         successful.
	 * @throws DataSourceException
	 */
	public String register() throws DataSourceException {

		// show information that name is taken, for usability
		if (!isNameUnique(name)) {
			logger.info("User " + name
					+ "tried to register. Username is already taken!");
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(BBChangeLanguage
							.getLangStringStatic("name_taken")));
			return "";
		}

		// show information that email is taken
		if (!isEmailUnique(email)) {
			logger.info("User " + name + "tried to register with Email: "
					+ email + ". Email is already taken!");
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Email already taken!", null));
			return "";
		}

		// create user and store
		User user = makeUserFromInput();
		if (user == null) {
			return "";
		}
		DAOUser.store(user);

		// store first, then send email - if storing fails, email is useless
		sendRegistrationEmail();
		addUserWithPermission();

		logger.info("User " + name + ", Email: " + email
				+ " has registered successfully.");
		FacesContext.getCurrentInstance().addMessage(
				"success",
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"User has been created successfully.", null));
		return "/view/home/main";
	}

	public BBChangeLanguage getLang() {
		return lang;
	}

	public void setLang(BBChangeLanguage lang) {
		this.lang = lang;
	}

	private void addUserWithPermission() {
		// if user is added but no never confirms, they can never log in and
		// will be deleted from cleanupthread

		// adds new user to the permission he was assigned

		// may be null if it is normal register
		if (type != null && id != -1) {
			try {
				List<String> userL = new ArrayList<String>();
				userL.add(name);

				// permission-register, must have token
				if (token != null) {
					if (type.equals("group")) {

						// re-confirm token validity, then remove
						Access access = DAOEmailToken.checkTokenGroup(token,
								id, true);
						DAOPermission.assignGroupUsersAccess(id, userL, access);
						DAOGroup.addUser(DAOUser.getUserByName(name).getId(),
								id);
					} else if (type.equals("clipboard")) {
						Access access = DAOEmailToken.checkTokenClipboard(
								token, id, true);
						DAOPermission.assignClipboardUsersAccess(id, userL,
								access);
					} else {
						FacesContext.getCurrentInstance().addMessage(
								"error",
								new FacesMessage(lang
										.getLangString("invalid_token_reg")));
					}
				} else {
					FacesContext.getCurrentInstance().addMessage(
							"error",
							new FacesMessage(lang
									.getLangString("invalid_token_reg")));
				}
			} catch (NumberFormatException e) {
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(lang
								.getLangString("invalid_token_reg")));
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
		}

	}

	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		if (params.get(SystemSettings.TYPE) != null) {
			this.token = UUID.fromString(params.get(SystemSettings.TOKEN));
		}
		if (params.get(SystemSettings.TYPE) != null) {
			this.type = params.get(SystemSettings.TYPE);
		}
		if (params.get(SystemSettings.ID_IDENTIFIER) != null) {
			this.id = Integer
					.parseInt(params.get(SystemSettings.ID_IDENTIFIER));
		}
	}

	public boolean isTEST_MODE() {
		return TEST_MODE;
	}
}
