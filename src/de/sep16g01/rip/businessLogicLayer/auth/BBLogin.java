package de.sep16g01.rip.businessLogicLayer.auth;

import java.io.Serializable;
import java.util.Arrays;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.businessLogicLayer.util.HashString;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the login functionality. Holds the user's name and the
 * password he input. Responsible for verifying whether or not the user is
 * permitted to login (by checking the validity of his input against what is in
 * the database by pulling the stored user from the database) and then letting
 * the user log in or rejecting the login and displaying an error if the user is
 * not permitted to login.
 */
@RequestScoped
@Named
public class BBLogin implements Serializable {
	private final static Logger logger = Logger.getLogger(BBLogin.class);

	@Inject
	private UserData userdata;

	@Inject
	private BBChangeLanguage lang;

	private static final long serialVersionUID = 1L;

	/**
	 * The password the user has entered.
	 */
	private String password;

	/**
	 * The name the user has entered.
	 */
	private String name;

	public void setPassword(String value) {
		this.password = value;
	}

	public String getPassword() {
		return this.password;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getName() {
		return this.name;
	}

	/**
	 * Checks if the username/password combination is correct. Called by
	 * {@link #login() login} method.
	 * 
	 * @param name1
	 *            The name of the user.
	 * @param password1
	 *            The password of the user.
	 * @return {@code true} if the credentials are valid, {@code false} if they
	 *         are not.
	 */
	private boolean areCredentialsValid(String name1, String password1,
			User user) {
		String userName = user.getName();
		byte[] userPasswordHash = user.getPasswordHash();
		byte[] userPasswordSalt = user.getPasswordSalt();

		if (!name1.equals(userName)) {
			return false;
		}

		// compare equality of hash we have with password input + hash from user
		if (Arrays.equals(userPasswordHash,
				HashString.hashPassword(password1, userPasswordSalt)))
			return true;

		return false;
	}

	/**
	 * Attempts to perform the login with the input the user has entered.
	 * Displays a message to the user if the credentials are invalid.
	 * 
	 * @return A link to the main page of the system if the login was
	 *         successful.
	 * @throws DataSourceException
	 */
	public String login() {
		User user = null;
		try {
			user = User.getUserByName(name);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return "";
		}

		// user does not exist or information is wrong
		if (user == null || !areCredentialsValid(name, password, user)) {

			// display login failed to hide information
			// we do not want the user to know whether or not a user
			// with the name he used exists
			logger.info("User " + name + " tried to login. Login failed!");
			FacesContext.getCurrentInstance().addMessage("error",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							lang.getLangString("login_failed"), null));
			return ""; // TODO test
		}

		// if user has token, then he has not confirmed, deny access
		try {
			if (User.hasRegistrationToken(name)) {
				logger.info("User " + name
						+ " tried to login. Account not activated!");
				FacesContext.getCurrentInstance().addMessage("error",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								lang.getLangString("account_not_activated"),
								null));
				return "";
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return "";
		}

		// all other cases: user is fine, let him log in
		logger.info("User " + name + " logged in successfully.");
		FacesContext.getCurrentInstance().addMessage("success",
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						lang.getLangString("login_successful"), null));
		user.setName(name);
		userdata.setUser(user);

		return "/view/home/main";
	}

	/**
	 * @return a link to the forgotPassword site.
	 */
	public String forgotPassword() {
		return "/view/auth/forgotPassword.xhtml?faces-redirect=true"
				+ "&backurl=/view/home/home.xhtml";
	}

	public void setUserData(UserData userData) {
		this.userdata = userData;
	}
}
