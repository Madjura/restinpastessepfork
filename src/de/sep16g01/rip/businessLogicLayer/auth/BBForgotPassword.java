package de.sep16g01.rip.businessLogicLayer.auth;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.email.Email;
import de.sep16g01.rip.businessLogicLayer.email.EmailFactory;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the password reset page. Holds the user who wants to reset
 * the password and two Strings representing the user's input new password,
 * twice. The two password inputs are checked for equality and the DAO functions
 * are called to reset the user's password.
 */
@RequestScoped
@Named
public class BBForgotPassword {
	private final static Logger logger = Logger
			.getLogger(BBForgotPassword.class);

	private String email;

	/**
	 * Sends a password reset email with a link that redirects a user to the
	 * page where he can change the password.
	 * 
	 * @return A link to the main page.
	 * @throws DataSourceException
	 */
	public String sendPasswordResetEmail() throws DataSourceException {
		String userEmail = null;
		if (email.contains("@")) {
			if (DAOUser.getUserByEmail(email) != null) {
				userEmail = email;
			}
		} else {
			User user = DAOUser.getUserByName(email);
			if (user != null) {
				userEmail = user.getEmail();
			}
		}

		if (userEmail == null) {
			FacesContext.getCurrentInstance().addMessage("error",
					new FacesMessage(BBChangeLanguage
							.getLangStringStatic("forgot_password_fail")));
			return "";
		}
		Email email1 = EmailFactory.getPasswordReset(userEmail);
		email1.setRecipient(userEmail);
		email1.send(null);
		logger.info("Password-Reset-Email " + email1 + " sent.");
		return "/view/home/main.xhtml"; //!
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
