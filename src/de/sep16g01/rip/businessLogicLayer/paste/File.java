package de.sep16g01.rip.businessLogicLayer.paste;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.apache.catalina.core.ApplicationPart;
import org.apache.tomcat.util.http.fileupload.IOUtils;

/**
 * A file belonging to a paste.
 */
public class File implements Serializable {

	public File(byte[] content) {
		this();
		this.content = content;
	}

	/**
	 * empty File contructor
	 */
	public File() {
	}

	/**
	 * @param uploadFile
	 * @throws IOException
	 */
	public File(ApplicationPart uploadFile, int ownerId) throws IOException {
		InputStream is = uploadFile.getInputStream();

		byte[] bytes = new byte[is.available()];
		IOUtils.readFully(is, bytes);
		setContent(bytes);
		setSize((uploadFile.getSize() / 1000) + 1);
		setOwner(ownerId);
		setFilename(uploadFile.getSubmittedFileName());
		this.part = uploadFile;
	}

	/**
	 * @param profilePicture
	 * @throws IOException
	 */
	public File(ApplicationPart profilePicture) throws IOException {
		InputStream is = profilePicture.getInputStream();

		byte[] bytes = new byte[is.available()];
		IOUtils.readFully(is, bytes);
		setContent(bytes);
	}

	private static final long serialVersionUID = 1L;

	/**
	 * The file size of the file in KB.
	 */
	private long size;

	/**
	 * The id of the file.
	 */
	private int id;

	/**
	 * The name of the file.
	 */
	private String filename;

	/**
	 * The id of the owner.
	 */
	private int owner;

	/**
	 * The id of the paste in which the file ist contained.
	 */
	private int pasteId;

	private ApplicationPart part;

	/**
	 * The content of the file.
	 */
	private byte[] content;

	public void setSize(long value) {
		this.size = value;
	}

	public long getSize() {
		return this.size;
	}

	public void setId(int value) {
		this.id = value;
	}

	public int getId() {
		return this.id;
	}

	public void setFilename(String value) {
		this.filename = value;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setOwner(int value) {
		this.owner = value;
	}

	public int getOwner() {
		return this.owner;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public int getPasteId() {
		return pasteId;
	}

	public void setPasteId(int paste) {
		this.pasteId = paste;
	}

	@Override
	public String toString() {
		return "File [id=" + this.id + ", filename=" + this.filename
				+ ", pasteId=" + this.pasteId + "]";
	}

	/**
	 * @return the part
	 */
	public ApplicationPart getPart() {
		return part;
	}

}
