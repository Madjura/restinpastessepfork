package de.sep16g01.rip.businessLogicLayer.paste;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.core.ApplicationPart;
import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.Bookmarkable;
import de.sep16g01.rip.businessLogicLayer.general.DisplayPage;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.keyword.Keyword;
import de.sep16g01.rip.businessLogicLayer.notify.NotifyOnChange;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.businessLogicLayer.util.Pair;
import de.sep16g01.rip.businessLogicLayer.util.ParameterUtils;
import de.sep16g01.rip.businessLogicLayer.util.TxtToPaste;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.profile.DAOBookmark;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for a single paste that is being displayed. Responsible for
 * holding all the relevant data of a single paste so that it can be displayed
 * to the user. Responsible for letting the user download the files, download
 * the paste or delete the paste.
 */
@ViewScoped
@Named
public class BBPaste extends DisplayPage implements Bookmarkable {
	/**
	 * The logger for the BBPaste class.
	 */
	private final transient static Logger logger = Logger
			.getLogger(BBPaste.class);

	private static final long serialVersionUID = 1L;

	/**
	 * The new File which should be updated.
	 */
	private transient ApplicationPart newFile;

	/**
	 * The paste that is being displayed to the user.
	 */
	private Paste paste;

	/**
	 * The clipboard the current paste belongs to.
	 */
	private Clipboard clipboard;

	/**
	 * Checks whether the paste has been changed or not.
	 */
	private boolean changeBeforeSave = false;

	/**
	 * Injects the language.
	 */
	@Inject
	private BBChangeLanguage lang;

	/**
	 * The paste whose content has been changed during the edit time
	 */
	private Paste changedPaste;

	/**
	 * The injected userdata.
	 */
	@Inject
	private UserData userData;

	/**
	 * Whether or not a user can delete a paste without a confirmation.
	 */
	private boolean letDelete = false;

	public Paste getPaste() {
		return this.paste;
	}

	/**
	 * The files belonging to the current paste.
	 */
	private DataModel<File> pastemodel = new ListDataModel<File>();

	/**
	 * Checks the access for the user logged in.
	 */
	private Access access;
	/**
	 * maxlength of content
	 */
	private int MAXLENGTH_CONTENT = 65536;

	/**
	 * Deletes a paste.
	 * 
	 * @return The link the user is redirected to.
	 * @throws DataSourceException
	 */
	public String delete(boolean delete) {
		if (checkWrite()) {
			// Get's number of pastes in corresponding clipboard. Necessary to
			// know whether or not paste is the last one in this clipboard.
			BBPaginator<Paste> pastes = new BBPaginator<Paste>(new Paste());
			try {
				pastes.setQuery("type=clipboard;clipboardId="
						+ DAOClipboard.getClipboardIdFromPasteId(paste.getId()));
			} catch (DataSourceException e1) {
				BBExceptionHandler.handleException(e1);
				return "";
			}
			pastes.setSystemElementType(SystemElementType.PASTE);
			pastes.setPageParamName("pageClipboards");
			pastes.gotoPage(1);
			int result = pastes.getPaginatedDataModel().getCountResults();
			if (result == 1 && !delete) {
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(BBChangeLanguage
								.getLangStringStatic("final_paste_delete")));
				setLetDelete(true);
				return "";
			} else if (result == 1 && delete) {
				try {
					Clipboard clipboard1 = DAOClipboard
							.getClipboardFromPaste(paste);
					DAOClipboard.delete(clipboard1.getId());
					if (clipboard1.getNotify()) {
						NotifyOnChange.notifyOfClipboardDelete(clipboard1);
						return "/view/home/main.xhtml?faces-redirect=true";
					}
				} catch (DataSourceException e) {
					BBExceptionHandler.handleException(e);
					return "";
				}
			}
			// Checks for email notifications
			if (paste.getNotify()
					&& userData.getUser().getId() != paste.getCreator().getId()) {
				NotifyOnChange.notifyOfPasteDelete(paste);
			}
			// Increases the user capacities for the deleted files
			try {
				for (File file1 : pastemodel) {
					DAOUser.increaseUserCapacity(file1.getOwner(),
							file1.getSize());
				}
				// Deletes the paste
				DAOPaste.delete(paste.getId());
				logger.info(userData.getUser() + " deleted " + paste + ".");
			} catch (DataSourceException e) {
				logger.error("Exception while " + userData.getUser()
						+ " tried to delete " + paste + "!", e);
				BBExceptionHandler.handleException(e);
				return "";
			}
		}
		if (userData.isLoggedIn())
			return "/view/profile/profile.xhtml?faces-redirect=true&"
					+ SystemSettings.USERID + "=" + userData.getUser().getId();
		return "/view/home/main.xhtml?faces-redirect=true";
	}

	/**
	 * Lets the user download the paste as a .txt file.
	 * 
	 * @return A link to the page where he can download the file.
	 */
	public void downloadTxt() {

		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		byte[] tosave = paste.getContent().getBytes();

		ec.responseReset();
		ec.setResponseContentType("application/octet-stream");
		ec.setResponseContentLength(tosave.length);
		ec.setResponseHeader("Content-Disposition", "attachment; filename=\""
				+ paste.getTitle() + ".txt\"");

		OutputStream output;
		try {
			output = ec.getResponseOutputStream();
			output.write(tosave);
			output.flush();
			output.close();
		} catch (IOException e) {
			BBExceptionHandler.handleException(e);
			return;
		}

		fc.responseComplete();

	}

	/**
	 * Lets the user download a single file of the paste.
	 * 
	 * @return A link to the page where he can download the file.
	 */
	public String downloadFile() {

		File f = pastemodel.getRowData();
		try {
			f = DAOPaste.getFile(f.getId());
		} catch (DataSourceException e2) {
			BBExceptionHandler.handleException(e2);
			return "";
		}
		HttpServletResponse response = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition",
				"attachment;filename=" + f.getFilename());
		try {
			response.getOutputStream().write(f.getContent());
			response.getOutputStream().flush();
			response.getOutputStream().close();
		} catch (IOException e1) {
			logger.warn("Unable to download file. " + e1.getMessage());
		}

		FacesContext.getCurrentInstance().responseComplete();
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws DataSourceException
	 */
	@Override
	public String bookmark() {
		try {
			if (!isBookmarked()) {
				DAOBookmark.bookmarkPaste(userData.getUser().getId(),
						paste.getId());
				logger.info("Bookmarking " + paste + ", User: "
						+ userData.getUser());
			} else {
				DAOBookmark.unbookmarkPaste(userData.getUser().getId(),
						paste.getId());
				logger.info("Unbookmarking " + paste + ", User: "
						+ userData.getUser());
			}
		} catch (DataSourceException e) {
			logger.error("Exception while Bookmarking/Unbookmarking " + paste
					+ ", User: " + userData.getUser());
			BBExceptionHandler.handleException(e);
			return "";
		}
		return "/view/paste/paste.xhtml?faces-redirect=true"
				+ SystemSettings.PASTEID + "=" + paste.getId();
	}

	/**
	 * Checks if the current paste is bookmarked.
	 * 
	 * @return false if not bookmarked, true if bookmarked
	 */
	public boolean isBookmarked() {
		if (userData.isLoggedIn()) {
			try {
				if (paste != null) {
					return DAOBookmark.isPasteBookmarked(userData.getUser()
							.getId(), paste.getId());
				}
				return false;
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
		}
		return false;
	}

	/**
	 * Directs the user to the paste edit page.
	 * 
	 * @return
	 */
	public String edit() {
		return "/view/paste/createPaste.xhtml?faces-redirect=true"
				+ SystemSettings.PASTEID + "=" + paste.getId();
	}

	/**
	 * Loads the paste from the database.
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String id = params.get(SystemSettings.PASTEID);
		if (id == null || id.equals("")) {
			paste = null;
			FacesContext.getCurrentInstance().addMessage("error",
					new FacesMessage(lang.getLangString("no_paste")));
			return;
		}
		this.access = ParameterUtils.readAccessParameter(params);
		try {
			int pasteId = Integer.parseInt(id);
			paste = DAOPaste.getById(pasteId);
			pastemodel = new ListDataModel<File>(paste.getFiles());
			clipboard = DAOClipboard.get(paste.getClipboardId());

		} catch (DataSourceException e) {
			logger.warn("Unable to init BBPaste." + e.getMessage());
			paste = null;
		}
	}

	public boolean contentChanged() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkRead() {
		return access == Access.READ;
	}

	/**
	 * Deletes a single file from the paste. Requires write permission.
	 * 
	 * @param delFile
	 *            The file that is being deleted.
	 * @return A link to the page of the paste.
	 * @throws DataSourceException
	 */
	public String deleteFile(File delFile) {
		try {
			paste.deleteFile(delFile);
			userData.getUser().setFreeCapacity(
					DAOUser.increaseUserCapacity(delFile.getOwner(),
							delFile.getSize()));

			logger.info(userData.getUser() + " deleted " + delFile + " from "
					+ paste + ".");
		} catch (DataSourceException e) {
			logger.error("Exception while " + userData.getUser()
					+ " tried to delete " + delFile + " from " + paste + "!", e);
			BBExceptionHandler.handleException(e);
			return "";
		}
		return "/view/paste/paste.xhtml?faces-redirect=true" + "&"
				+ SystemSettings.PASTEID + "=" + paste.getId();
	}

	/*
	 * paste (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.DisplayPage#checkWrite()
	 */
	@Override
	public boolean checkWrite() {
		return access == Access.WRITE;
	}

	public Paste getChangedPaste() {
		return changedPaste;
	}

	public void setChangedPaste(Paste changedPaste) {
		this.changedPaste = changedPaste;
	}

	/**
	 * Saves the current paste.
	 * 
	 * @param writeThrough
	 *            Whether or not changes should be overwritten.
	 * @return The link the user is redirected to.
	 */
	public String save(boolean writeThrough) {
		// Checks whether or not a paste is empty. If it is empty, it will be
		// deleted.
		if (paste.getContent().equals("") && paste.getFiles().isEmpty()) {
			if (checkWrite()) {
				if (paste.getNotify()) {
					NotifyOnChange.notifyOfPasteDelete(paste);
				}
				try {
					DAOPaste.delete(paste.getId());
					logger.info(userData.getUser() + " deleted " + paste
							+ " by setting its content to \"\"");
				} catch (DataSourceException e) {
					logger.error("Exception while " + userData.getUser()
							+ " tried to delete " + paste
							+ " by setting its content to \"\"", e);
					BBExceptionHandler.handleException(e);
				}
			}
			return "/view/home/main.xhtml?faces-redirect=true";
		}

		// get id of paste for keyword email
		try {
			Keyword.checkForKeywords(paste, false);
			Pair<Integer, Boolean> pair = DAOPaste.store(paste, writeThrough);
			int id = pair.getFirst();
			paste.setId(id);
			setChangeBeforeSave(pair.getSecond());
			if (paste.getNotify()
					&& userData.getUser().getId() != paste.getCreator().getId()) {
				NotifyOnChange.checkChangesForPaste(paste);
			}
			// Upload the new file if one is picked.
			if (newFile != null) {
				File uploadFile = new File(newFile, userData.getUser().getId());
				uploadFile.setPasteId(paste.getId());
				if (userData.getUser().getFreeCapacity() >= uploadFile
						.getSize()) {
					if (newFile.getContentType().equals("text/plain")) {
						TxtToPaste.addTxtToPaste(newFile, paste);
						newFile = null;
						save(false);
					} else if (DAOPaste.storeFile(uploadFile)) {
						userData.getUser().setFreeCapacity(
								DAOUser.decreaseUserCapacity(userData.getUser()
										.getId(), uploadFile.getSize()));
					}
				} else {
					FacesContext.getCurrentInstance().addMessage(
							"error",
							new FacesMessage(FacesMessage.SEVERITY_ERROR, lang
									.getLangString("fileSizeTooBig")
									+ uploadFile.getFilename(), null));
					return "";
				}
			}
			logger.info("Saving " + paste + ", User: " + userData.getUser());
		} catch (DataSourceException e) {
			logger.error("Exception while saving " + paste + ", User: "
					+ userData.getUser(), e);
			BBExceptionHandler.handleException(e);
			return "";
		} catch (IOException e) {
			logger.error("Exception while saving " + paste + ", User: "
					+ userData.getUser(), e);
			BBExceptionHandler.handleException(e);
			return "";
		}
		/**
		 * Loads the changed paste if the current paste changed while being
		 * edited.
		 */
		if (changeBeforeSave) {
			try {
				changedPaste = DAOPaste.getById(paste.getId());
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
			return "";
		}
		return "/view/paste/paste.xhtml?faces-redirect=true" + "&"
				+ SystemSettings.PASTEID + "=" + paste.getId();
	}

	public DataModel<File> getPastemodel() {
		return pastemodel;
	}

	public void setPastemodel(DataModel<File> pastemodel) {
		this.pastemodel = pastemodel;
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	/**
	 * Uploads the file picked by the user.
	 * 
	 * @return
	 */
	public String upload() {
		File file1 = null;
		// Checks if the user picked a file
		if (newFile != null) {
			try {
				// Checks if the size of the file ist bigger than the free user
				// capacity
				file1 = new File(newFile, userData.getUser().getId());
				if (userData.getUser().getFreeCapacity() >= file1.getSize()) {
					file1.setPasteId(paste.getId());
					if (newFile.getContentType().equals("text/plain")
							&& (newFile.getSize() + paste.getContent().length()) <= MAXLENGTH_CONTENT) {
						TxtToPaste.addTxtToPaste(newFile, paste);
					} else if (DAOPaste.storeFile(file1)) {
						userData.getUser().setFreeCapacity(
								DAOUser.decreaseUserCapacity(userData.getUser()
										.getId(), file1.getSize()));

					}

				} else {
					FacesContext.getCurrentInstance().addMessage(
							"error",
							new FacesMessage(FacesMessage.SEVERITY_ERROR, lang
									.getLangString("fileSizeTooBig")
									+ file1.getFilename(), null));
					return "";
				}
			} catch (DataSourceException | IOException e) {
				BBExceptionHandler.handleException(e);
				return "";
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, lang
							.getLangString("noFilePicked"), null));
			return "";
		}
		return "";
	}

	public ApplicationPart getNewFile() {
		return newFile;
	}

	public void setNewFile(ApplicationPart newFile) {
		this.newFile = newFile;
	}

	public boolean isChangeBeforeSave() {
		return changeBeforeSave;
	}

	public void setChangeBeforeSave(boolean changeBeforeSave) {
		this.changeBeforeSave = changeBeforeSave;
	}

	public Clipboard getClipboard() {
		return clipboard;
	}

	public void setClipboard(Clipboard clipboard) {
		this.clipboard = clipboard;
	}

	public boolean isLetDelete() {
		return letDelete;
	}

	public void setLetDelete(boolean letDelete) {
		this.letDelete = letDelete;
	}
}
