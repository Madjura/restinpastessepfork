package de.sep16g01.rip.businessLogicLayer.paste;

import java.util.Date;
import java.util.List;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.comments.Comment;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.pagination.Paginatable;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;

/**
 * DTO for a single paste. Holds relevant paste information, including a list of
 * files that are in it.
 */
public class Paste extends SystemElement implements Paginatable<Paste> {

	private static final long serialVersionUID = 1L;

	public Paste() {
		this.setTitle("Paste");
	}

	/**
	 * The creator of the paste.
	 */
	private User creator;

	/**
	 * The files contained in this paste.
	 */
	private List<File> files;

	/**
	 * The text content of this paste.
	 */
	private String content;

	/**
	 * The comments belonging to this paste.
	 */
	private List<Comment> comments;

	/**
	 * How long the paste is going to live.
	 */
	private Date expirationDate;

	private int clipboardId = DEFAULT_ID;

	private boolean notify;

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public int getClipboardId() {
		return clipboardId;
	}

	public void setClipboardId(int clipboardId) {
		this.clipboardId = clipboardId;
	}

	public void addFile(File file) throws DataSourceException {
		DAOPaste.storeFile(file);
	}

	public void deleteFile(File file) throws DataSourceException {
		DAOPaste.deleteFile(file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.SystemElement#getLink()
	 */
	@Override
	public String getLink() {
		return "/view/paste/paste.xhtml?" + SystemSettings.PASTEID + "="
				+ this.getId();
	}

	public boolean getNotify() {
		return notify;
	}

	public void setNotify(boolean notify) {
		this.notify = notify;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.sep16g01.rip.businessLogicLayer.pagination.Paginatable#paginate(java.
	 * lang.String, int, int)
	 */
	@Override
	public PaginatedDataModel<Paste> paginate(String query, int page,
			int entriesPerPage) {
		try {
			return DAOPaste.paginate(query, page, entriesPerPage);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return null;
	}

	@Override
	public String toString() {
		return "Paste [id=" + this.id + ", creator="
				+ (this.creator != null ? this.creator.getId() : "")
				+ ", expirationDate=" + this.expirationDate + ", clipboardId="
				+ this.clipboardId + ", notify=" + this.notify + "]";
	}

}
