package de.sep16g01.rip.businessLogicLayer.paste;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.core.ApplicationPart;
import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.DisplayPage;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.keyword.Keyword;
import de.sep16g01.rip.businessLogicLayer.notify.NotifyOnChange;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.businessLogicLayer.util.Pair;
import de.sep16g01.rip.businessLogicLayer.util.ParameterUtils;
import de.sep16g01.rip.businessLogicLayer.util.TxtToPaste;
import de.sep16g01.rip.businessLogicLayer.validators.TimeSpanValidator;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.permission.DAOPermission;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the paste creation and edit. Creation or edit is determined
 * by the existence of an identifier in the URl. Responsible for letting the
 * user input data to create a new paste or edit an existing paste if he has
 * write access.
 */
@ViewScoped
@Named
public class BBCreatePaste extends DisplayPage {

	private static final long serialVersionUID = 1L;

	/**
	 * The creation date of the previous paste if there was a collision.
	 */
	private Date previousCreationDate;

	@Inject
	private BBChangeLanguage lang;
	/**
	 * Used to access the user and checking whether or not the user is logged in
	 * or anonymous easily.
	 */
	@Inject
	private UserData userData;

	private transient List<File> files = new ArrayList<File>();

	/**
	 * The paste that is being displayed to the user.
	 */
	private Paste paste;

	/**
	 * The paste whose content has been changed during the edit time
	 */
	private Paste changedPaste;

	/**
	 * The clipboard the paste is being created in or for.
	 */
	private Clipboard clipboard;

	/**
	 * JSF does not support timepickers, parse a timespan-string entered by the
	 * user instead; of format "1y1m1w" etc. see TimeSpanValidator for details.
	 */
	private String expirestring = "7d";

	/**
	 * The link of the clipboard this paste is being moved to.
	 */
	private String moveLink;

	/**
	 * maxlength of content
	 */
	private int MAXLENGTH_CONTENT = 65536;

	private final transient static Logger logger = Logger
			.getLogger(BBCreatePaste.class);

	private boolean ignoreUpdates = false;

	private int newClipId;

	private Integer clipboardId;

	private boolean letDelete = false;

	/**
	 * @return the newClipId
	 */
	public int getNewClipId() {
		return this.newClipId;
	}

	/**
	 * @param newClipId
	 *            the newClipId to set
	 */
	public void setNewClipId(int newClipId) {
		this.newClipId = newClipId;
	}

	private transient ApplicationPart newFile;

	public boolean contentChanged() {
		return changeBeforeSave;
	}

	private boolean notify;

	private boolean changeBeforeSave = false;

	private Access access;

	private DataModel<File> pastemodel = new ListDataModel<File>();

	/**
	 * Loads and sets the {@link #paste} and {@link #clipboard} objects.
	 */
	@PostConstruct
	public void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String pasteId = params.get(SystemSettings.PASTEID);
		String clipboardId1 = params.get(SystemSettings.CLIPBOARDID);

		try {
			expirestring = DAOSystemSettings.loadMaxExpirationDurationPerRole()
					.get(userData.getUser().getRole());
		} catch (DataSourceException e1) {
			expirestring = "1h";
		}

		this.access = ParameterUtils.readAccessParameter(params);
		if (pasteId == null) {
			paste = new Paste();
			if (clipboardId1 != null)
				paste.setClipboardId(Integer.parseInt(clipboardId1));
			paste.setCreator(userData.getUser());
			Date date = new java.util.Date();
			paste.setCreationDate(date);
			paste.setLastModifiedDate(date);
			paste.setExpirationDate(date);
			access = Access.WRITE;
		} else {
			try {
				paste = DAOPaste.getById(Integer.parseInt(pasteId));
				pastemodel = new ListDataModel<File>(paste.getFiles());
			} catch (DataSourceException e) {
				logger.warn("Unable to init BBCreatepaste " + e.getMessage());
				BBExceptionHandler.handleException(e);
			}
		}
	}

	/**
	 * Moves the paste to the new clipboard.
	 * 
	 * @return A link to the clipboard the paste was moved to.
	 */
	public String move() {
		try {
			Clipboard newClipboard = DAOClipboard.get(newClipId);
			Access clipboardAccess = DAOPermission.checkClipboardUserAccess(
					newClipId, userData.getUser().getId());
			if (newClipboard != null) {
				if (clipboardAccess == Access.WRITE) {
					paste.setClipboardId(newClipId);
					DAOPaste.store(paste, true);
					return "/view/clipboard/clipboard.xhtml?faces-redirect=true"
							+ "&"
							+ SystemSettings.CLIPBOARDID
							+ "="
							+ newClipboard.getId();
				}
				logger.info(userData.getUser() + " tried to move " + paste
						+ " to " + newClipboard + "! Permission denied!");
				FacesContext.getCurrentInstance().addMessage("error",
						new FacesMessage("no permission to move paste"));

			} else {
				logger.info(userData.getUser() + " tried to move " + paste
						+ "! Clipboard does not exist!");
				FacesContext.getCurrentInstance().addMessage("error",
						new FacesMessage("Clipboard does not exist"));
			}
		} catch (DataSourceException e) {
			logger.error("Exception while " + userData.getUser()
					+ " tried to move " + paste + "!", e);
			BBExceptionHandler.handleException(e);
		}
		return "";
	}

	/**
	 * Lets the user download a single file of the paste.
	 * 
	 * @return A link to the page where he can download the file.
	 */
	public String downloadFile(File file1) {
		File f = pastemodel.getRowData();
		try {
			f = DAOPaste.getFile(f.getId());
		} catch (DataSourceException e2) {
			BBExceptionHandler.handleException(e2);
		}
		HttpServletResponse response = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();

		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition",
				"attachment;filename=" + f.getFilename());
		try {
			response.getOutputStream().write(f.getContent());
			response.getOutputStream().flush();
			response.getOutputStream().close();
		} catch (IOException e1) {
			logger.warn("Unable to download file. " + e1.getMessage());
		}

		FacesContext.getCurrentInstance().responseComplete();

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/data?type=pastefile&id=" + f.getId());
		} catch (IOException e) {
			logger.warn("Unable to download file. " + e.getMessage());
			BBExceptionHandler.handleException(e);
		}
		return null;
	}

	/**
	 * Deletes the paste. Requires write permission.
	 * 
	 * @return A link to the page of the clipboard the paste was in.
	 * @throws DataSourceException
	 */
	public String delete(boolean delete) {
		if (checkWrite()) {

			BBPaginator<Paste> pastes = new BBPaginator<Paste>(new Paste());
			try {
				pastes.setQuery("type=clipboard;clipboardId="
						+ DAOClipboard.getClipboardIdFromPasteId(paste.getId()));
			} catch (DataSourceException e1) {
				BBExceptionHandler.handleException(e1);
			}
			pastes.setSystemElementType(SystemElementType.PASTE);
			pastes.setPageParamName("pageClipboards");
			pastes.gotoPage(1);
			int result = pastes.getPaginatedDataModel().getCountResults();
			if (result == 1 && !delete) {
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(BBChangeLanguage
								.getLangStringStatic("final_paste_delete")));
				setLetDelete(true);
				return "";
			} else if (result == 1 && delete) {
				try {
					Clipboard clipboard1 = DAOClipboard
							.getClipboardFromPaste(paste);
					DAOClipboard.delete(clipboard1.getId());
					if (clipboard1.getNotify()) {
						NotifyOnChange.notifyOfClipboardDelete(clipboard1);
						return "/view/home/main.xhtml?faces-redirect=true";
					}
				} catch (DataSourceException e) {
					BBExceptionHandler.handleException(e);
				}
			}
			// Notifies the creator of the paste if he chose to be notified..
			if (paste.getNotify()
					&& userData.getUser().getId() != paste.getCreator().getId()) {
				NotifyOnChange.notifyOfPasteDelete(paste);
			}

			// Decreases the user capacities of the owners of the files
			// belonging to the current paste.
			try {
				for (File file1 : files) {
					DAOUser.increaseUserCapacity(file1.getOwner(),
							file1.getSize());
				}
				DAOPaste.delete(paste.getId());
				logger.info(userData.getUser() + " deleted " + paste + ".");
			} catch (DataSourceException e) {
				logger.error("Exception while " + userData.getUser()
						+ " tried to delete " + paste + "!", e);
				BBExceptionHandler.handleException(e);
			}
		}
		return "/view/home/main.xhtml?faces-redirect=true";
	}

	/**
	 * Deletes a single file from the paste. Requires write permission.
	 * 
	 * @param delFile
	 *            The file that is being deleted.
	 * @return A link to the page of the paste.
	 * @throws DataSourceException
	 */
	public String deleteFile(File delFile) {
		if (checkWrite()) {
			try {
				paste.deleteFile(delFile);
				userData.getUser().setFreeCapacity(
						DAOUser.increaseUserCapacity(delFile.getOwner(),
								delFile.getSize()));
				logger.info(userData.getUser() + " deleted " + delFile
						+ " from " + paste + ".");
			} catch (DataSourceException e) {
				logger.error("Exception while " + userData.getUser()
						+ " tried to delete " + delFile + " from " + paste
						+ "!", e);
				BBExceptionHandler.handleException(e);
			}
		} else {

		}

		return "/view/paste/createPaste.xhtml?faces-redirect=true" + "&"
				+ SystemSettings.PASTEID + "=" + paste.getId();
	}

	/**
	 * Saves the changes made to the paste.
	 * 
	 * @return A link to the page of the paste.
	 * @throws DataSourceException
	 * @throws IOException
	 */
	public String save(boolean writeThrough) {
		// return if he user tries to create an empty paste.
		if (paste.getId() == SystemElement.DEFAULT_ID
				&& paste.getContent().equals("")) {
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(BBChangeLanguage
							.getLangStringStatic("empty_paste")));
			return "";
		}
		// deletes the paste if it is empty after the changes
		if (paste.getContent().equals("") && paste.getFiles().isEmpty()) {
			if (checkWrite()) {
				if (paste.getNotify()) {
					NotifyOnChange.notifyOfPasteDelete(paste);
				}
				try {
					DAOPaste.delete(paste.getId());
					logger.info(userData.getUser() + " deleted " + paste
							+ " by setting its content to \"\"");
				} catch (DataSourceException e) {
					logger.error("Exception while " + userData.getUser()
							+ " tried to delete " + paste
							+ " by setting its content to \"\"", e);
					BBExceptionHandler.handleException(e);
				}
			}
			return "/view/home/main.xhtml?faces-redirect=true";
		}

		// calculates the expirationdate of the paste if the paste is new.
		boolean newPaste = paste.getId() == SystemElement.DEFAULT_ID;
		Date expirationDate = null;
		try {
			String maxexpforuser = DAOSystemSettings
					.loadMaxExpirationDurationPerRole().get(
							userData.getUser().getRole());
			Date now = new Date();
			Date maxExpDate = TimeSpanValidator.parseExpirationDateFromString(
					now, maxexpforuser);
			expirationDate = TimeSpanValidator.parseExpirationDateFromString(
					now, expirestring);
			if (expirationDate.after(maxExpDate)) {
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Expiration date too large. Max expiration date: "
										+ maxexpforuser, null));
				return null;
			}

			if (expirationDate.equals(now)) {

				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Expiration date too small or empty.", null));
				return null;
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		} catch (ParseException e) {
			BBExceptionHandler.handleException(e);
		}

		try {
			if (expirestring != null && newPaste) {
				expirationDate = TimeSpanValidator
						.parseExpirationDateFromString(new Date(), expirestring);
				paste.setExpirationDate(expirationDate);
			}
		} catch (ParseException e) {
			BBExceptionHandler.handleException(e);
		}
		// creates a new clipboard for the paste if it is created without
		// choosing a clipboard
		if (paste.getClipboardId() == SystemElement.DEFAULT_ID) {
			Clipboard newClipboard = new Clipboard();
			newClipboard.setTitle(paste.getTitle());
			newClipboard.setDescription(paste.getDescription());
			newClipboard.setCreator(paste.getCreator());
			newClipboard.setCreationDate(paste.getCreationDate());
			newClipboard.setExpirationDate(paste.getExpirationDate());
			newClipboard.setLastModifiedDate(paste.getLastModifiedDate());

			int newClipId1 = 0;
			try {
				newClipId1 = DAOClipboard.store(newClipboard);
				logger.info("Saving Clipboard " + newClipboard + " for new "
						+ paste + ", User: " + userData.getUser());
			} catch (DataSourceException e) {
				logger.error(
						"Exception while saving Clipboard " + newClipboard
								+ " for new " + paste + ", User: "
								+ userData.getUser(), e);
				BBExceptionHandler.handleException(e);
			}
			paste.setClipboardId(newClipId1);

		}

		// Checks if the user has permission for the chosen clipboard

		if (clipboardId != null) {
			try {
				if (DAOPermission.checkClipboardUserAccess(clipboardId,
						userData.getUser().getId()) == Access.WRITE) {
					paste.setClipboardId(clipboardId);
				}
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
		}

		// get id of paste for keyword email
		Pair<Integer, Boolean> pair = null;
		try {

			// old paste needs content from before it goes to database
			if (!newPaste) {
				Keyword.checkForKeywords(paste, false);
			}
			// Notifies the creator if he wanted to be notified
			if (paste.getNotify()
					&& userData.getUser().getId() != paste.getCreator().getId()) {
				NotifyOnChange.checkChangesForPaste(paste);
			}
			pair = DAOPaste.store(paste, writeThrough);
			logger.info("Saving " + paste + ", User: " + userData.getUser());
		} catch (DataSourceException e) {
			logger.error("Exception while saving " + paste + ", User: "
					+ userData.getUser(), e);
			BBExceptionHandler.handleException(e);
		}
		int id = pair.getFirst();
		paste.setId(id);
		// new paste needs id
		if (newPaste) {
			try {
				Clipboard clipboard1 = DAOClipboard.get(paste.getClipboardId());
				if (clipboard1.getNotify()
						&& userData.getUser().getId() != clipboard1
								.getCreator().getId()) {
					NotifyOnChange.notifyOfPasteCreate(clipboard1, paste);
				}
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
			Keyword.checkForKeywords(paste, true);
		}
		setChangeBeforeSave(pair.getSecond());
		// uploads files

		if (!files.isEmpty()) {
			// file upload for new pastes with multiple files
			for (File file1 : files) {
				try {
					if (userData.getUser().getFreeCapacity() >= file1.getSize()) {
						file1.setPasteId(paste.getId());
						if (DAOPaste.storeFile(file1)) {
							userData.getUser()
									.setFreeCapacity(
											DAOUser.decreaseUserCapacity(
													userData.getUser().getId(),
													file1.getSize()));
						}
						logger.info("Saving file " + file1 + " for " + paste
								+ ", User: " + userData.getUser());
					} else {
						FacesContext.getCurrentInstance().addMessage(
								"success",
								new FacesMessage(FacesMessage.SEVERITY_ERROR,
										lang.getLangString("fileSizeTooBig")
												+ file1.getFilename(), null));
					}
				} catch (DataSourceException e) {
					logger.error(
							"Exception while saving file " + file1 + " for "
									+ paste + ", User: " + userData.getUser(),
							e);
					BBExceptionHandler.handleException(e);
					return "";
				}
			}
		} else if (newFile != null) {
			upload();

		}
		if (changeBeforeSave) {
			try {
				changedPaste = DAOPaste.getById(paste.getId());
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
			return "";
		}

		return "/view/paste/paste.xhtml?faces-redirect=true" + "&"
				+ SystemSettings.PASTEID + "=" + paste.getId();

	}

	public Clipboard getClipboard() {
		return clipboard;
	}

	public void setClipboard(Clipboard clipboard) {
		this.clipboard = clipboard;
	}

	public Paste getPaste() {
		return paste;
	}

	public void setPaste(Paste paste) {
		this.paste = paste;
	}

	public String getMoveLink() {
		return moveLink;
	}

	public void setMoveLink(String moveLink) {
		this.moveLink = moveLink;
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkRead() {
		return access == Access.READ;
	}

	public Date getPreviousCreationDate() {
		return previousCreationDate;
	}

	public void setPreviousCreationDate(Date previousCreationDate) {
		this.previousCreationDate = previousCreationDate;
	}

	public Paste getChangedPaste() {
		return changedPaste;
	}

	public void setChangedPaste(Paste changedPaste) {
		this.changedPaste = changedPaste;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.DisplayPage#checkWrite()
	 */
	@Override
	public boolean checkWrite() {
		return access == Access.WRITE;
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	/**
	 * Uploads the file picked by the user
	 * 
	 * @return The link the user is redirected to.
	 */
	public String upload() {
		if (newFile != null) {
			// when the paste is new
			if (paste.getId() == SystemElement.DEFAULT_ID) {
				try {
					if (newFile.getContentType().equals("text/plain")
							&& (newFile.getSize() + paste.getContent().length()) <= MAXLENGTH_CONTENT) {
						TxtToPaste.addTxtToPaste(newFile, paste);
					} else {
						files.add(new File(newFile, userData.getUser().getId()));
					}
					return "";
				} catch (IOException e) {
					BBExceptionHandler.handleException(new DataSourceException(
							e));
				}
			} else {
				// when the paste already exists
				try {
					File uploadFile = new File(newFile, userData.getUser()
							.getId());
					if (userData.getUser().getFreeCapacity() >= uploadFile
							.getSize()) {
						if (newFile.getContentType().equals("text/plain")) {
							TxtToPaste.addTxtToPaste(newFile, paste);
							DAOPaste.store(paste, true);
						} else {
							uploadFile.setPasteId(paste.getId());
							if (DAOPaste.storeFile(uploadFile)) {
								userData.getUser().setFreeCapacity(
										DAOUser.decreaseUserCapacity(userData
												.getUser().getId(), uploadFile
												.getSize()));
							}
						}
					} else {
						FacesContext.getCurrentInstance().addMessage(
								"error",
								new FacesMessage(FacesMessage.SEVERITY_ERROR,
										lang.getLangString("fileSizeTooBig")
												+ uploadFile.getFilename(),
										null));
						return "";
					}
				} catch (DataSourceException e) {
					BBExceptionHandler.handleException(e);
					return "";
				} catch (IOException e) {
					BBExceptionHandler.handleException(e);
					return "";
				}
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, lang
							.getLangString("noFilePicked"), null));
			return "";
		}
		return "";
	}

	public ApplicationPart getNewFile() {
		return newFile;
	}

	public void setNewFile(ApplicationPart newFile) {
		this.newFile = newFile;
	}

	public boolean getNotify() {
		return notify;
	}

	public void setNotify(boolean notify) {
		this.notify = notify;
	}

	public boolean isChangeBeforeSave() {
		return changeBeforeSave;
	}

	public void setChangeBeforeSave(boolean changeBeforeSave) {
		this.changeBeforeSave = changeBeforeSave;
	}

	public boolean isIgnoreUpdates() {
		return ignoreUpdates;
	}

	public void setIgnoreUpdates(boolean ignoreUpdates) {
		this.ignoreUpdates = ignoreUpdates;
	}

	/**
	 * @return the expirestring
	 */
	public String getExpirestring() {
		return this.expirestring;
	}

	/**
	 * @param expirestring
	 *            the expirestring to set
	 */
	public void setExpirestring(String expirestring) {
		this.expirestring = expirestring;
	}

	public DataModel<File> getPastemodel() {
		return pastemodel;
	}

	public void setPastemodel(DataModel<File> pastemodel) {
		this.pastemodel = pastemodel;
	}

	public Integer getClipboardId() {
		return clipboardId;
	}

	public void setClipboardId(Integer clipboardId) {
		this.clipboardId = clipboardId;
	}

	public boolean isLetDelete() {
		return letDelete;
	}

	public void setLetDelete(boolean letDelete) {
		this.letDelete = letDelete;
	}

}
