package de.sep16g01.rip.businessLogicLayer.administration;

import java.util.Date;

import de.sep16g01.rip.businessLogicLayer.comments.Comment;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.pagination.Paginatable;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.dataAccessLayer.administration.DAOReport;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * DTO for reports. Reports have a reporter, the reported item and a marker
 * whether or not they were handled already. This class is used to pass and
 * display data of a single report to the view and to let moderators mark the
 * report as handled.
 */
public class Report extends SystemElement implements Paginatable<Report> {

	private static final long serialVersionUID = 1L;

	/**
	 * The User that created the report.
	 */
	private User creator = new User();

	/**
	 * {@code true} if the report has been marked as handled by a moderator or
	 * administrator. {@code false} if the report has not yet been handled.
	 */
	private boolean handled = false;

	/**
	 * The element that was reported.
	 */
	private Comment reportedComment = new Comment();

	private Date expirationdate;

	/**
	 * @return the creator
	 */
	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public Comment getReportedComment() {
		return reportedComment;
	}

	public void setReportedComment(Comment reportedComment) {
		this.reportedComment = reportedComment;
	}

	public boolean isHandled() {
		return handled;
	}

	public void setHandled(boolean handled) {
		this.handled = handled;
	}

	public Date getExpirationDate() {
		return expirationdate;
	}

	public void setExpirationDate(Date expirationdate) {
		this.expirationdate = expirationdate;
	}

	@Override
	public String getLink() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.sep16g01.rip.businessLogicLayer.pagination.Paginatable#paginate(java.
	 * lang.String, int, int)
	 */
	@Override
	public PaginatedDataModel<Report> paginate(String query, int page,
			int entriesPerPage) {
		try {
			return DAOReport.paginate(query, page, entriesPerPage);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return null;
	}

	@Override
	public String toString() {
		return "Report [" + "id=" + this.id + ", creator="
				+ this.creator.getId() + ", handled=" + this.handled
				+ ", reportedComment=" + this.reportedComment.getId()
				+ ", expirationdate=" + this.expirationdate + "]";
	}

}
