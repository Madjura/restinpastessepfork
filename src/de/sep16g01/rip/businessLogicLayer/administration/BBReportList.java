package de.sep16g01.rip.businessLogicLayer.administration;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.dataAccessLayer.administration.DAOReport;
import de.sep16g01.rip.dataAccessLayer.comments.DAOComment;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * BBReportList contains a BBPaginator<Report> object, to iterate over all
 * pastes in the system with Pagination.
 */
@ViewScoped
@Named
public class BBReportList extends BBExceptionHandler implements Serializable {
	
	private final static Logger logger = Logger.getLogger(BBReportList.class);

	private static final long serialVersionUID = 1L;

	/**
	 * A list of all the reports that are in the system. This includes the
	 * reports that are marked as handled but have not yet been cleaned up by
	 * {@link de.sep16g01.rip.businessLogicLayer.general.CleanupThread}.
	 */
	private BBPaginator<Report> reports;

	/**
	 * Loads the reports from the database and stores them in {@link #reports}.
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();

		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		reports = new BBPaginator<Report>(new Report());
		reports.setQuery("type=reports");
		reports.setPageParamName("pageReports");
		reports.gotoPage(params.get("pageReports"));

		//add params from Sidebar
		reports.putOtherPageParameter("pageGroup",
				params.get("pageGroup"));
		reports.putOtherPageParameter("pageClipboard",
				params.get("pageClipboard"));
	}

	/**
	 * Marks a report as handled by setting the report's
	 * {@link Report#wasHandled}.
	 * 
	 * @return reload link
	 */
	public String markAsHandled() {

		Report report = reports.getPaginatedDataModel().getRowData();
		report.setHandled(!report.isHandled());

		try {
			DAOReport.store(report);
			logger.info(report + " has been marked as handled.");
		} catch (DataSourceException e) {
			logger.error("Exception while marking " + report + " as handled!",
					e);
			BBExceptionHandler.handleException(e);
			return "";
		}

		return "/view/administration/reportList.xhtml?faces-redirect=true";
	}

	/**
	 * Delete the reported comment.
	 * 
	 * @return reload link
	 */
	public String deleteOffending() {

		Report report = reports.getPaginatedDataModel().getRowData();
		try {
			DAOComment.delete(report.getReportedComment().getId());
			DAOReport.deleteReport(report.getId());
			logger.info("Offending SystemElement of " + report
					+ " has been deleted.");
		} catch (DataSourceException e) {
			logger.error("Exception while deleting offending SystemElement of "
					+ report + "!", e);
			BBExceptionHandler.handleException(e);
			return "";
		}

		return "/view/administration/reportList.xhtml?faces-redirect=true";
	}

	/**
	 * Delete the report.
	 * 
	 * @return redirection link.
	 */
	public String deleteReport() {

		Report report = reports.getPaginatedDataModel().getRowData();
		try {
			DAOReport.deleteReport(report.getId());
			logger.info(report + " has been deleted.");
		} catch (DataSourceException e) {
			logger.error("Exception while deleting " + report + "!", e);
			BBExceptionHandler.handleException(e);
			return "";
		}

		return "/view/administration/reportList.xhtml?faces-redirect=true";
	}

	public BBPaginator<Report> getReports() {
		return this.reports;
	}
}
