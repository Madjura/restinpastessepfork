package de.sep16g01.rip.businessLogicLayer.administration;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * DTO for system settings. Holds the path to the logo, the system name and the
 * maximum eligible file size per role of the system. This class is used to
 * store and load the settings described above.
 */
@ApplicationScoped
@Named
public class SystemSettings implements Serializable {

	private static final long serialVersionUID = 1L;

	public final static String USERID = "userid";
	public final static String PASTEID = "pasteid";
	public final static String CLIPBOARDID = "clipboardid";
	public final static String GROUPID = "groupid";
	public final static String ACCESSID = "accessid";
	public final static String BOOKMARKID = "bookmarkid";
	public final static String TYPE = "type";
	public final static String TOKEN = "token";
	public final static String ID_IDENTIFIER = "id";
	public final static String BACKURL = "backurl";

	public static String getCurrentURL() {
		HttpServletRequest req = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		StringBuffer url = req.getRequestURL();
		url.append("?");
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String[] identifier = new String[] { USERID, PASTEID, CLIPBOARDID,
				GROUPID, ACCESSID, BOOKMARKID, TYPE, TOKEN, ID_IDENTIFIER,
				BACKURL };
		for (String i : identifier) {
			String value = params.get(i);
			if (value != null) {
				url.append(i);
				url.append("=");
				url.append(value);
				url.append("&");
			}
		}
		if (url.charAt(url.length() - 1) == '&') {
			url.deleteCharAt(url.length() - 1);
		}
		return url.toString();
	}

	/**
	 * Maps a {@link de.sep16g01.rip.businessLogicLayer.permissions.Role Role}
	 * to a long value, describing the maximum lifetime duration of elements
	 * members of this role may create.
	 */
	private HashMap<Role, String> maxExpirationDurationPerRole = new HashMap<Role, String>();

	/**
	 * 
	 * Maps a {@link de.sep16g01.rip.businessLogicLayer.permissions.Role Role}
	 * to a long value, describing how big a
	 * 
	 * single file that users of a certain role can upload may be.
	 */

	private HashMap<Role, Long> maxFileSizePerRole = new HashMap<Role, Long>();

	/**
	 * Maps a {@link de.sep16g01.rip.businessLogicLayer.permissions.Role Role}
	 * to a long value, describing how much total file credit a certain role has
	 * in the system.
	 */

	private HashMap<Role, Long> maxSizePerRole = new HashMap<Role, Long>();

	/**
	 * The name of the system.
	 */
	private String name = "";

	/**
	 * The terms of service text.
	 */
	private String termsOfService = ""; // TODO

	/**
	 * The imprint text.
	 */
	private String imprint = ""; // TODO

	/**
	 * The logo file of the system.
	 */
	private File logo;

	/**
	 * the count of results for paginatable objects
	 */
	public static final int COUNT_RESULTS_PER_PAGE = 10;

	/**
	 * Stores the settings to the database.
	 * 
	 * @throws DataSourceException
	 */
	public void store() {
		try {
			DAOSystemSettings.store(this);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
	}

	// /**
	// * Loads the settings from the database.
	// */
	// @PostConstruct
	// private void init() {
	//
	// try {
	// SystemSettings settings = DAOSystemSettings.loadSystemSettings();
	// setName(settings.getName());
	// setLogo(settings.getLogo());
	// setImprint(settings.getImprint());
	// setTermsOfService(settings.getTermsOfService());
	// } catch (DataSourceException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTermsOfService() {
		return termsOfService;
	}

	public void setTermsOfService(String termsOfService) {
		this.termsOfService = termsOfService;
	}

	public String getImprint() {
		return imprint;
	}

	public void setImprint(String imprint) {
		this.imprint = imprint;
	}

	public File getLogo() {
		return logo;
	}

	public void setLogo(File logo) {
		this.logo = logo;
	}

	public HashMap<Role, String> getMaxExpirationDurationPerRole() {
		return maxExpirationDurationPerRole;
	}

	public void setMaxExpirationDurationPerRole(
			HashMap<Role, String> maxExpirationDurationPerRole) {
		this.maxExpirationDurationPerRole = maxExpirationDurationPerRole;
	}

	public void addToMaxExpirationDuration(Role role, String duration) {
		maxExpirationDurationPerRole.put(role, duration);

	}

	public static String getUserid() {
		return USERID;
	}

	public static String getPasteid() {
		return PASTEID;
	}

	public static String getClipboardid() {
		return CLIPBOARDID;
	}

	public static String getGroupid() {
		return GROUPID;
	}

	public static String getAccessid() {
		return ACCESSID;
	}

	public static String getBookmarkid() {
		return BOOKMARKID;
	}

	public static String getType() {
		return TYPE;
	}

	public static String getToken() {
		return TOKEN;
	}

	public static String getIdIdentifier() {
		return ID_IDENTIFIER;
	}

	public static String getBackurl() {
		return BACKURL;
	}

	public static int getCountResultsPerPage() {
		return COUNT_RESULTS_PER_PAGE;
	}

	public HashMap<Role, Long> getMaxSizePerRole() {
		return maxSizePerRole;
	}

	public void setMaxSizePerRole(HashMap<Role, Long> maxSizePerRole) {
		this.maxSizePerRole = maxSizePerRole;
	}

	public void setMaxFileSizePerRole(HashMap<Role, Long> maxFileSizePerRole) {
		this.maxFileSizePerRole = maxFileSizePerRole;
	}

	@Override
	public String toString() {
		return "SystemSettings [name=" + this.name + ", termsOfService="
				+ this.termsOfService + ", imprint=" + this.imprint + ", logo="
				+ this.logo + "]";
	}

	public static String loadName() throws DataSourceException {
		return DAOSystemSettings.loadSystemName();
	}
}
