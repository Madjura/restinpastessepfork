package de.sep16g01.rip.businessLogicLayer.administration;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.catalina.core.ApplicationPart;
import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.BBFooter;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Holds the changes for the system settings made on the administrator page.
 * This class is also used to display currently selected options on the page
 * correctly. Administrators can make changes to those values and they are
 * passed to this class so they can be saved.
 *
 */
@ViewScoped
@Named
public class BBSystemSettings implements Serializable {
	private final static Logger logger = Logger
			.getLogger(BBSystemSettings.class);

	@Inject
	private BBFooter bBFooter;

	private static final long serialVersionUID = 1L;

	/**
	 * The {@link SystemSettings} object whose information is being displayed.
	 */
	private SystemSettings systemSettings = new SystemSettings();

	private String limitAnonymous;
	private String limitRegistered;
	private String limitModerator;
	private String limitAdmin;

	private Long fileSizeLimit = null;
	private Long singleFileSizeLimit = null;

	/**
	 * The site logo.
	 */
	private transient ApplicationPart picture;

	/**
	 * Passes {@link #systemSettings} to the DAO for storing to the database.
	 * 
	 * @return
	 */
	public void save() {
		if (picture != null) {
			try {
				systemSettings.setLogo(new File(picture));
			} catch (IOException e) {
				BBExceptionHandler.handleException(e);
			}

		}

		try {
			if (fileSizeLimit != null) {
				DAOSystemSettings.storeFileSizeLimit(fileSizeLimit);
			}
			if (singleFileSizeLimit != null) {
				DAOSystemSettings.storeSingleFileSizeLimit(singleFileSizeLimit);
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}

		systemSettings.getMaxExpirationDurationPerRole().put(Role.ANONYM,
				limitAnonymous);
		systemSettings.getMaxExpirationDurationPerRole().put(Role.REGISTERED,
				limitRegistered);
		systemSettings.getMaxExpirationDurationPerRole().put(Role.MODERATOR,
				limitModerator);
		systemSettings.getMaxExpirationDurationPerRole().put(
				Role.ADMINISTRATOR, limitAdmin);
		try {
			DAOSystemSettings.store(systemSettings);
			DAOSystemSettings.storeMaxExpirationDurationPerRole(systemSettings
					.getMaxExpirationDurationPerRole());
			logger.info(systemSettings + " saved.");
		} catch (DataSourceException e) {
			logger.error("Exception while saving " + systemSettings + "!", e);
			BBExceptionHandler.handleException(e);
		}
		bBFooter.setImprint(systemSettings.getImprint());
	}

	public SystemSettings getSystemSettings() {
		return systemSettings;
	}

	public void setSystemSettings(SystemSettings systemSettings) {
		this.systemSettings = systemSettings;
	}

	/**
	 * Loads {@link #systemSettings} from the DAO.
	 * 
	 * 
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		try {
			systemSettings = DAOSystemSettings.loadSystemSettings();
			systemSettings.setMaxFileSizePerRole(DAOSystemSettings
					.loadMaxFileSizePerRole());
			systemSettings.setMaxSizePerRole(DAOSystemSettings
					.loadMaxSizePerRole());
			systemSettings.setMaxExpirationDurationPerRole(DAOSystemSettings
					.loadMaxExpirationDurationPerRole());
			limitAdmin = systemSettings.getMaxExpirationDurationPerRole().get(
					Role.ADMINISTRATOR);
			limitModerator = systemSettings.getMaxExpirationDurationPerRole()
					.get(Role.MODERATOR);
			limitRegistered = systemSettings.getMaxExpirationDurationPerRole()
					.get(Role.REGISTERED);
			limitAnonymous = systemSettings.getMaxExpirationDurationPerRole()
					.get(Role.ANONYM);
		} catch (DataSourceException e) {
			logger.error("Exception while loading " + systemSettings + "!", e);
			BBExceptionHandler.handleException(e);
		}

	}

	/**
	 * @return the picture
	 */
	public ApplicationPart getPicture() {
		return picture;
	}

	/**
	 * @param picture
	 *            the picture to set
	 */
	public void setPicture(ApplicationPart picture) {
		this.picture = picture;
	}

	public String getLimitAnonymous() {
		return limitAnonymous;
	}

	public void setLimitAnonymous(String limitAnonymous) {
		this.limitAnonymous = limitAnonymous;
	}

	public String getLimitRegistered() {
		return limitRegistered;
	}

	public void setLimitRegistered(String limitRegistered) {
		this.limitRegistered = limitRegistered;
	}

	public String getLimitModerator() {
		return limitModerator;
	}

	public void setLimitModerator(String limitModerator) {
		this.limitModerator = limitModerator;
	}

	public String getLimitAdmin() {
		return limitAdmin;
	}

	public void setLimitAdmin(String limitAdmin) {
		this.limitAdmin = limitAdmin;
	}

	public Long getFileSizeLimit() {
		return fileSizeLimit;
	}

	public void setFileSizeLimit(Long fileSizeLimit) {
		this.fileSizeLimit = fileSizeLimit;
	}

	public Long getSingleFileSizeLimit() {
		return singleFileSizeLimit;
	}

	public void setSingleFileSizeLimit(Long singleFileSizeLimit) {
		this.singleFileSizeLimit = singleFileSizeLimit;
	}
}
