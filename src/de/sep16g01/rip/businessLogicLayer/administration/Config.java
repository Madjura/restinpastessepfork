package de.sep16g01.rip.businessLogicLayer.administration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Singelton. Reads and holds properties from the config.properties
 * configuration file.
 */
public class Config {

	/**
	 * Logger instance for this class.
	 */
	private final static Logger logger = Logger.getLogger(Config.class);

	private static Config config = new Config();

	public static Config getInstance() {
		return config;
	}

	/*
	 * defaults used in initialization
	 */
	private String adminname;
	private String adminemail;
	private String adminemailpasswort;
	private String adminpasswort;

	/**
	 * The config file is located above the /lib/ directory.
	 */
	private final String configFile = "../config.properties";

	/**
	 * Load hardcoded sep2016 default settings.
	 */
	private void loadDefaults() {
		dbUser = "sep16g01";
		dbPassword = "Aive9eeB";
		dbName = "sep16g01";
		dbDriver = "org.postgresql.Driver";
		
		dbHost = "bueno.fim.uni-passau.de";
		dbPoolSize = 15;
		mailLogin = "donotreply.restinpastes@gmail.com";
		mailPassword = "passwordrestinpastes";
		mailPort = 465;
		mailServer = "smtp.gmail.com";
		baseURL = "http://localhost:8080/sep16g01/faces";
		mailFrom = "restinpastes.de";
		logPath = ".";
		debug = "true";
		adminname = "admin2";
		adminpasswort = "passwort";
		adminemail = "restinpastes.host2@gmail.com";
		adminemailpasswort = "passwortpasswort";
	}

	/**
	 * Absolute path for logging
	 */
	String logPath;

	/**
	 * Constructs config object from file. Settings not specified in the config
	 * file are initialized with their default values.
	 */
	private Config() {

		loadDefaults();
		loadFromFile();
	}

	/**
	 * The username of the database.
	 */
	private String dbUser;

	/**
	 * The password of {@link #dbUser}.
	 */
	private String dbPassword;

	/**
	 * The name of the database.
	 */
	private String dbName;

	/**
	 * The driver of the database.
	 */
	private String dbDriver;

	/**
	 * The hostname of the database.
	 */
	private String dbHost;

	/**
	 * The login name of the email-sending account.
	 */
	private String mailLogin;

	/**
	 * The password of the email-sending account.
	 */
	private String mailPassword;

	/**
	 * The mail server.
	 */
	private String mailServer;

	/**
	 * The port of the email sending functionality.
	 */
	private int mailPort;

	/**
	 * The SMTP mail sender.
	 */
	private String mailFrom;

	private String baseURL;

	/**
	 * The number of connections in the database pool.
	 */
	private int dbPoolSize;

	private String debug;

	/**
	 * loads the settings from the configuration file.
	 */
	public void loadFromFile() {
		try {
			InputStream input = getClass().getClassLoader()
					.getResourceAsStream(configFile);
			if (input == null) {
				throw new IOException();
			}

			Properties prop = new Properties();
			prop.load(input);

			// load from the file
			dbUser = prop.getProperty("dbUser");
			dbPassword = prop.getProperty("dbPassword");
			dbName = prop.getProperty("dbName");
			dbDriver = prop.getProperty("dbDriver");
			dbHost = prop.getProperty("dbHost");
			debug = prop.getProperty("debug");
			adminname = prop.getProperty("adminname");
			adminpasswort = prop.getProperty("adminpasswort");
			adminemail = prop.getProperty("adminemail");
			adminemailpasswort = prop.getProperty("adminemailpasswort");

			try {
				dbPoolSize = Integer.parseInt(prop.getProperty("dbPoolSize"));
			} catch (NumberFormatException ex) {
				logger.fatal("Error parsing dbPoolSize integer.");
			}

			mailLogin = prop.getProperty("mailLogin");
			mailPassword = prop.getProperty("mailPassword");
			mailServer = prop.getProperty("mailServer");
			mailFrom = prop.getProperty("mailFrom");
			baseURL = prop.getProperty("baseURL");

			try {
				mailPort = Integer.parseInt(prop.getProperty("mailPort"));
			} catch (NumberFormatException ex) {
				logger.fatal("Error parsing mailPort integer.");
			}
		} catch (IOException ex) {
			logger.fatal("Error reading config file.", ex);
		}
	}

	public String getDbUser() {
		return dbUser;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public String getDbName() {
		return dbName;
	}

	public String getDbDriver() {
		return dbDriver;
	}

	public String getDbHost() {
		return dbHost;
	}

	public String getMailLogin() {
		return mailLogin;
	}

	public String getMailPassword() {
		return mailPassword;
	}

	public String getMailServer() {
		return mailServer;
	}

	public int getMailPort() {
		return mailPort;
	}

	public int getDbPoolSize() {
		return dbPoolSize;
	}

	public String getMailFrom() {
		return mailFrom;
	}

	public String getBaseURL() {
		return baseURL;
	}

	public String getLogPath() {
		return logPath;
	}

	public boolean getDebug() {
		return Boolean.parseBoolean(debug);
	}

	public String getAdminname() {
		return adminname;
	}

	public String getAdminemail() {
		return adminemail;
	}

	public String getAdminemailpasswort() {
		return adminemailpasswort;
	}

	public String getAdminpasswort() {
		return adminpasswort;
	}
}
