package de.sep16g01.rip.businessLogicLayer.general;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.businessLogicLayer.util.HashString;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;


/**
 * Backing bean for the confirmation when a critical element is about to be
 * deleted, such as the profile. The password must be entered by the user when
 * they wish to perform a critical deletion, which is then checked here for
 * correctness.
 */
@ViewScoped
@Named
public class BBConfirmation implements Serializable {
	private final static Logger logger = Logger.getLogger(BBConfirmation.class);

	private static final long serialVersionUID = 1L;

	@Inject
	private UserData userData;

	@Inject
	private BBChangeLanguage lang;

	/**
	 * The warning text that is displayed to the user.
	 */
	private String warning;

	/**
	 * The password the user entered to confirm deletion.
	 */
	private String password;

	private int userid = SystemElement.DEFAULT_ID;

	public String getPassword() {
		return this.password;
	}

	public String getWarning() {
		return this.warning;
	}

	/**
	 * Fills {@link warning} and finds out whose profile will be deleted.
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		if (params.get("userid") != null) {
			userid = Integer.parseInt(params.get("userid"));
		}
		warning = lang.getLangString("delete_profile_info");

	}

	/**
	 * Performs the deletion of the User. All the User's Pastes and clipboards
	 * are deleted from the database on cascade.
	 */
	public String performDeletion() {
		if (validPassword(password, userData.getUser())) {
			if (userid != SystemElement.DEFAULT_ID) {
				try {
					DAOUser.getById(userid).dropUser();
				} catch (DataSourceException e) {
					BBExceptionHandler.handleException(e);
				}
				logger.info("Deletion of User with id " + userid
						+ " has been successful.");
				FacesContext.getCurrentInstance().addMessage(
						"success",
						new FacesMessage(FacesMessage.SEVERITY_INFO, lang
								.getLangString("user_deleted"), null));
			}
			return "/view/home/home";
		}
		logger.info("Deletion of User with id " + userid
				+ " has failed! Password incorrect!");
		FacesContext.getCurrentInstance().addMessage(
				"error",
				new FacesMessage(FacesMessage.SEVERITY_ERROR, lang
						.getLangString("password_not_correct"), null));
		return "";

	}

	public void setPassword(String value) {
		this.password = value;
	}

	public void setWarning(String value) {
		this.warning = value;
	}

	private boolean validPassword(String password1, User user) {
		byte[] userPasswordHash = user.getPasswordHash();
		byte[] userPasswordSalt = user.getPasswordSalt();
		// compare equality of hash we have with password input + hash from user
		if (Arrays.equals(userPasswordHash,
				HashString.hashPassword(password1, userPasswordSalt))) {
			return true;
		}
		return false;
	}
}
