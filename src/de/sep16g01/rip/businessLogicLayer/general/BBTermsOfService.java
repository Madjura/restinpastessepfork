package de.sep16g01.rip.businessLogicLayer.general;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * Backing bean for the terms of service. Holds the terms of service.
 */
@ViewScoped
@Named
public class BBTermsOfService implements Serializable {

	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(BBTermsOfService.class);

	/**
	 * Loads the terms of service and stores them in {@link #termsOfService}.
	 */
	public String getTermsOfService() {
		SystemSettings systemSettings = new SystemSettings();
		try {
			systemSettings = DAOSystemSettings.loadSystemSettings();
		} catch (DataSourceException e) {
			logger.warn("Initializing Terms of Service has failed.",e);
		}

		if (systemSettings.getTermsOfService() != null) {
			return systemSettings.getTermsOfService();
		}

		return "";

	}
}
