package de.sep16g01.rip.businessLogicLayer.general;

import java.util.ArrayList;
import java.util.List;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * The types of system element in the system.
 */
public enum SystemElementType {
	CLIPBOARD("clipboard_name", 1), PASTE("paste_name", 2), USER("user_name",
			3), GROUP("group_name",
					4), ALL("all_name", 5), REPORT("report_name", 6);
	/**
	 * The string representation of the element.
	 */
	private String name;
	private int id;

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            The string representation of the element.
	 */
	private SystemElementType(String name, int id) {
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return BBChangeLanguage.getLangStringStatic(name);
	}

	/**
	 * 
	 * @return
	 */
	public static List<SystemElementType> dropDownValues() {
		List<SystemElementType> list = new ArrayList<SystemElementType>();
		list.add(ALL);
		list.add(PASTE);
		list.add(GROUP);
		list.add(USER);
		list.add(CLIPBOARD);
		return list;
	}

	public int getId() {
		return id;
	}

	public static SystemElementType getById(int id1) {
		if (id1 == 1) {
			return CLIPBOARD;
		} else if (id1 == 2) {
			return PASTE;
		} else if (id1 == 3) {
			return USER;
		} else if (id1 == 4) {
			return GROUP;
		} else if (id1 == 5) {
			return ALL;
		} else if (id1 == 6) {
			return REPORT;
		}
		return null;
	}
}
