package de.sep16g01.rip.businessLogicLayer.general;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.annotation.Eager;
import de.sep16g01.rip.businessLogicLayer.email.Email;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.permissions.Permission;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.email.DAOEmailToken;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the confirmation emails, such as registration confirmation
 * and email change confirmation.
 */
@RequestScoped
@Named
@Eager
public class BBEmailConfirm {

	private final transient static Logger logger = Logger
			.getLogger(BBEmailConfirm.class);

	@Inject
	private UserData userData;

	/**
	 * Holds whether or not the token was valid.
	 */
	private boolean validToken = false;

	@Inject
	private BBChangeLanguage lang;

	/**
	 * Pulls the tokens from the URL and checks against the database for
	 * validity.
	 * 
	 */
	@PostConstruct
	public void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		// get URL request parameters
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();

		String type = params.get(SystemSettings.TYPE);
		String user = params.get(SystemSettings.USERID);
		String token = params.get(SystemSettings.TOKEN);
		try {
			UUID.fromString(token);
		} catch (IllegalArgumentException e) {
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(BBChangeLanguage
							.getLangStringStatic("confirm_failure")));
			return;
		}

		// check if the link was invalid
		if (type == null || token == null) {
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);

			String error = lang.getLangString("email_confirm_invalid");
			message.setSummary(error);
			message.setDetail(error);
			FacesContext.getCurrentInstance().addMessage(
					"confirmForm:confirmText", message);
		}

		try {
			// reg token, check if valid
			if (type != null && type.equals(Email.IDENTIFIER_REG)) {
				validToken = DAOEmailToken.checkTokenRegistration(
						UUID.fromString(token), user);
				if (validToken) {

					// valid, remove token from row
					DAOEmailToken.removeRegistrationToken(user);
				}
				// email change, check if token valid
			} else if (type != null && type.equals(Email.IDENTIFIER_CHANGE)) {
				validToken = DAOEmailToken.checkEmailChangeToken(
						UUID.fromString(token), user);
				if (validToken) {

					// valid, update email
					DAOEmailToken.updateEmail(user);
				}
			} else if (type != null
					&& type.equals(Email.IDENTIFIER_GROUPINVITE)) {
				int groupId = Integer.parseInt(params
						.get(SystemSettings.GROUPID));
				Access access = DAOEmailToken.checkTokenGroup(
						UUID.fromString(token), groupId, false);
				if (access != null) {
					if (userData.isLoggedIn()) {
						List<String> add = new ArrayList<String>();
						add.add(userData.getUser().getName());
						Permission.assignGroupUsersAccess(Integer
								.parseInt(params.get(SystemSettings.GROUPID)),
								add, DAOEmailToken.checkTokenGroup(
										UUID.fromString(token), groupId, true));
						DAOGroup.addUser(userData.getUser().getId(), groupId);
						FacesContext.getCurrentInstance().addMessage(
								"success",
								new FacesMessage(
										"You have been added to the group."));
					} else {
						try {
							FacesContext
									.getCurrentInstance()
									.getExternalContext()
									.redirect(
											FacesContext.getCurrentInstance()
													.getExternalContext()
													.getRequestContextPath()
													+ "/faces/view/auth/register.xhtml?access="
													+ access.getPriority()
													+ "&"
													+ SystemSettings.ID_IDENTIFIER
													+ "="
													+ groupId
													+ "&"
													+ SystemSettings.TOKEN
													+ "="
													+ token
													+ "&"
													+ SystemSettings.TYPE
													+ "=group");
						} catch (IOException e) {
							logger.warn("Unable to redirect to registration for group id"
									+ e.getMessage());
						}
					}
				}
			} else if (type != null
					&& type.equals(Email.IDENTIFIER_CLIPBOARDINVITE)) {
				int clipboardId = Integer.parseInt(params
						.get(SystemSettings.CLIPBOARDID));
				Access access = DAOEmailToken.checkTokenClipboard(
						UUID.fromString(token), clipboardId, false);
				if (access != null) {
					if (userData.isLoggedIn()) {
						List<String> add = new ArrayList<String>();
						add.add(userData.getUser().getName());
						Permission.assignClipboardUsersAccess(clipboardId, add,
								DAOEmailToken.checkTokenClipboard(
										UUID.fromString(token), clipboardId,
										true));
					} else {
						try {
							FacesContext
									.getCurrentInstance()
									.getExternalContext()
									.redirect(
											FacesContext.getCurrentInstance()
													.getExternalContext()
													.getRequestContextPath()
													+ "/faces/view/auth/register.xhtml?access="
													+ access.getPriority()
													+ "&"
													+ SystemSettings.ID_IDENTIFIER
													+ "="
													+ clipboardId
													+ "&"
													+ SystemSettings.TOKEN
													+ "="
													+ token
													+ "&"
													+ SystemSettings.TYPE
													+ "=clipboard");
						} catch (IOException e) {
							logger.warn("Unable to redirect to registration for clipboard id "
									+ e.getMessage());
						}
					}
				}
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
	}

	public boolean isValidToken() {
		return validToken;
	}
}
