package de.sep16g01.rip.businessLogicLayer.general;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the help page.
 */
@ViewScoped
@Named
public class BBHelp implements Serializable {

	private static final long serialVersionUID = 1L;

	private String showHelp;
	private String hideHelp;

	@Inject
	private UserData userData;

	@Inject
	private BBChangeLanguage lang;

	/**
	 * The help text that is being displayed.
	 */
	private String help;

	public String getHelp() {
		return help;
	}

	public void setHelp(String help) {
		this.help = help;
	}
	
	/**
	 * Boolean to render the view of the help. Help can be faded in and 
	 * faded out.
	 */
	private boolean show = false;

	public boolean getShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}
	
	/**
	 * Method that is called for rendering the view of the helptext.
	 */
	public void changeView() {
		show = !show;
		if (helpButton.equals(showHelp)) {
			helpButton = hideHelp;
		} else if (helpButton.equals(hideHelp)) {
			helpButton = showHelp;
		}
	}

	/**
	 * For rendering the text on the help button.
	 */
	private String helpButton;

	public void setHelpButton(String helpButton) {
		this.helpButton = helpButton;
	}

	public String getHelpButton() {
		return helpButton;
	}

	/**
	 * Loads the help and fills {@link #help} dependent of the site the user 
	 * is currently located on.
	 */
	@PostConstruct
	private void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap();
		showHelp = lang.getLangString("show_help");
		hideHelp = lang.getLangString("hide_help");
		helpButton = showHelp;
		HttpServletRequest request = (HttpServletRequest) FacesContext.
				getCurrentInstance().getExternalContext()
				.getRequest();
		String path = request.getPathInfo();
		if (path.equals("/view/home/home.xhtml")) {
			help = lang.getLangString("helptext_login");
		} else if (path.equals("/view/paste/paste.xhtml")) {
			help = lang.getLangString("helptext_paste");
		} else if (path.equals("/view/clipboard/clipboard.xhtml")) {
			help = lang.getLangString("helptext_clipboard");
		} else if (path.equals("/view/group/group.xhtml")) {
			help = lang.getLangString("helptext_group");
		} else if (path.equals("/view/profile/profile.xhtml")) {
			if (userData.getUser().getId() == Integer.parseInt(params.get("userid"))) {
				help = lang.getLangString("helptext_profile");
			} else {
				help = lang.getLangString("helptext_otherProfile");
			}
		} else if (path.equals("/view/administration/administrationHome.xhtml")) {
			help = lang.getLangString("helptext_administrationHome");
		} else if (path.equals("/view/auth/forgotPassword.xhtml")) {
			help = lang.getLangString("helptext_forgotPassword");
		} else if (path.equals("/view/auth/register.xhtml")) {
			help = lang.getLangString("helptext_register");
		} else if (path.equals("/view/clipboard/createClipboard.xhtml")) {
			if (params.get("clipboardid") != null) {
				help = lang.getLangString("helptext_editClipboard");
			} else {
				help = lang.getLangString("helptext_createClipboard");
			}
		} else if (path.equals("/view/group/createGroup.xhtml")) {
			if (params.get("groupid") != null) {
				help = lang.getLangString("helptext_editGroup");
			} else {
				help = lang.getLangString("helptext_createGroup");
			}
		} else if (path.equals("/view/group/group.xhtml")) {
			help = lang.getLangString("helptext_group");
		} else if (path.equals("/view/home/main.xhtml")) {
			help = lang.getLangString("helptext_main");
		} else if (path.equals("/view/paste/createPaste.xhtml")) {
			if (params.get("pasteid") != null) {
				help = lang.getLangString("helptext_editPaste");
			} else {
				help = lang.getLangString("helptext_createPaste");
			}
		} else if (path.equals("/view/permissions/invite.xhtml")) {
			help = lang.getLangString("helptext_invite");
		} else if (path.equals("/view/permissions/managePermissions.xhtml")) {
			help = lang.getLangString("helptext_managePermissions");
		} else if (path.equals("/view/profile/editProfile.xhtml")) {
			help = lang.getLangString("helptext_editProfile");
		} else if (path.equals("/view/general/confirmation.xhtml")) {
			help = lang.getLangString("helptext_confirmation");
		}
	}

}
