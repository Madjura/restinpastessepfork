package de.sep16g01.rip.businessLogicLayer.general;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;

import de.sep16g01.rip.businessLogicLayer.annotation.Eager;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.database.TableInitializer;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;

/**
 * Creates the tables and inserts the default values.
 */
@Eager
@ApplicationScoped
public class BBTableCreater {

	@PostConstruct
	private void init(
			@Observes @Initialized(ApplicationScoped.class) Object init) {
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection())) {
			TableInitializer.createAllTables(cw);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}

	}
}
