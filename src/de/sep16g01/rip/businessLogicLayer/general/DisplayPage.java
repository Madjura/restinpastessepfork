/**
 * 
 */
package de.sep16g01.rip.businessLogicLayer.general;

import java.io.Serializable;

import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * Used to give classes the methods to check if an object exists when it is
 * about to be displayed to the user.
 */
public abstract class DisplayPage implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Checks whether or not a user has access (read) to the object.
	 * 
	 * @param user
	 *            The user that is being checked for.
	 * @return {@code true} if the user can see the object, {@code false} if the
	 *         user cannot.
	 * @throws DataSourceException
	 */
	public abstract boolean checkRead() throws DataSourceException;

	/**
	 * Checks whether or not a user has access (read) to the object.
	 * 
	 * @param user
	 *            The user that is being checked for.
	 * @return {@code true} if the user can see the object, {@code false} if the
	 *         user cannot.
	 * @throws DataSourceException
	 */
	public abstract boolean checkWrite() throws DataSourceException;
}
