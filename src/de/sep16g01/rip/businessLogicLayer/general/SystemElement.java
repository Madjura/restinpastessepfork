package de.sep16g01.rip.businessLogicLayer.general;

import java.io.Serializable;
import java.util.Date;

/**
 * Groups, Pastes, Clipboards and User extend off this. Used to give the important system elements the same
 * functionality, such as getting a unique link for each of the elements so users can bookmark them.
 */
public abstract class SystemElement implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Default ID for newly created objects that have not yet been stored to the database.
	 */
	public final static int DEFAULT_ID = -1;

	/**
	 * The ID of the object.
	 */
	protected int id = DEFAULT_ID;

	/**
	 * The title/name of the object.
	 */
	private String title;

	/**
	 * The description text of the object.
	 */
	private String description;

	/**
	 * The date of creation of the object.
	 */
	private Date creationDate;

	/**
	 * The date the object was last modified on.
	 */
	private Date lastModifiedDate;

	public void setId(int value) {
		this.id = value;
	}

	public int getId() {
		return this.id;
	}

	public void setTitle(String value) {
		this.title = value;
	}

	public String getTitle() {
		return this.title;
	}

	public void setDescription(String value) {
		this.description = value;
	}

	public String getDescription() {
		return this.description;
	}

	/**
	 * Generates and returns a link for the element. Default is /<type of the element>/?id=id.
	 * 
	 * @return The link to the element.
	 */
	public abstract String getLink();

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
