package de.sep16g01.rip.businessLogicLayer.general;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.annotation.Eager;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

/**
 * Is started at system start and stopped when system shuts down. Responsible
 * for removing expired pastes.
 */
@Eager
@ApplicationScoped
public class CleanupThread extends TimerTask {


	private final static Logger logger = Logger.getLogger(CleanupThread.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void run() {
		try {
			deleteUnconfirmedUsers();
			deleteExpiredClipboards();
			deleteExpiredPastes();
		} catch (DataSourceException e) {
			logger.fatal("CleanupThread could not be executed properly.", e);
		}
	}

	/**
	 * Delete all the Users who got registered, but didn't confirm their 
	 * account after one day.
	 * 
	 * @throws DataSourceException
	 */
	private static void deleteUnconfirmedUsers() throws DataSourceException {
		Date currentdate = new java.util.Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentdate);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		//unconfirmed users will be deleted if the don't confirm after one day
		Date date = cal.getTime();
		Timestamp time = new Timestamp(date.getTime());
		String query = "SELECT id FROM TUsers WHERE registrationDate <= ? "
				+ "AND confirmationToken IS NOT NULL";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().
				getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.
						prepareStatement(query));) {
			ps.setTimestamp(1, time);
			try (ResultSetWrapper result = new ResultSetWrapper(ps.executeQuery())) {
				while (result.next()) {
					int userid = result.getInt(1);
					DAOUser.dropUser(userid);
				}
			}
		}
	}
	
	/**
	 * Delete all the clipboards whose expiration date has run out.
	 * 
	 * @throws DataSourceException when the Database is down
	 */
	private static void deleteExpiredClipboards() throws DataSourceException {
		Date date = new java.util.Date();
		Timestamp time = new Timestamp(date.getTime());
		String query = "SELECT id FROM TClipboards WHERE expirationDate <= ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().
				getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.
						prepareStatement(query));) {
			ps.setTimestamp(1, time);
			try (ResultSetWrapper result = new ResultSetWrapper(ps.executeQuery())) {

				while (result.next()) {
					int clipboardId = result.getInt(1);
					DAOClipboard.delete(clipboardId);
				}
			}
		}
	}

	/**
	 * Delete all the pastes whose expiration date has run out.
	 * 
	 * @throws DataSourceException when the Database is down
	 */
	private static void deleteExpiredPastes() throws DataSourceException {
		Date date = new java.util.Date();
		Timestamp time = new Timestamp(date.getTime());
		String query = "SELECT id FROM TPastes WHERE expirationDate <= ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setTimestamp(1, time);
			try (ResultSetWrapper result = new ResultSetWrapper(ps.executeQuery())) {

				while (result.next()) {
					int pasteId = result.getInt(1);
					DAOPaste.delete(pasteId);
				}
			}
		}
	}
	
	/*
	 * This method is performed and runs directly after the server started.
	 */
	@PostConstruct
	private void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
		Timer t = new Timer();
		t.scheduleAtFixedRate(new CleanupThread(), 
				0, // run first occurrence immediately
				30 * 60 * 1000); // CleanupThread is executed every minute
	}

}
