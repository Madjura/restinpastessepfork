/**
 *   Copyright subkom GmbH. All rights reserved. subkom.de
 *   File:     de.sep16g01.rip.businessLogicLayer.general.BBTitle.java
 *   Created:  06.07.2016, 13:24:00 by hubertho
 */
package de.sep16g01.rip.businessLogicLayer.general;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 *
 */
@RequestScoped
@Named
public class BBTitle {
	private String title;

	@PostConstruct
	private void init() {
		try {
			this.title = SystemSettings.loadName();
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return;
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
