/**
 * 
 */
package de.sep16g01.rip.businessLogicLayer.general;

import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * Interface for bookmarkable elements. The bookmarkability in this system does
 * not only describe the bookmark functionality of the browser. Rather it is its
 * own system where a user can add a bookmark on elements of the system and look
 * at his own bookmarks and share the link to them.
 */
public interface Bookmarkable {

	/**
	 * Adds the element to the user's bookmarks.
	 * 
	 * @return A link to the current page, with the bookmark button now visibly
	 *         changed.
	 * @throws DataSourceException
	 */
	public String bookmark() throws DataSourceException;
}
