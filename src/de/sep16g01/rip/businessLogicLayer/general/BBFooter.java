package de.sep16g01.rip.businessLogicLayer.general;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;

import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * The backing bean for the footer. Holds the imprint.
 */
@ApplicationScoped
@Named
public class BBFooter implements Serializable {
	
	private final static Logger logger = Logger.getLogger(BBFooter.class);

	@PostConstruct
	private void init() {
		
		SystemSettings systemSettings;
		try {
			systemSettings = DAOSystemSettings.loadSystemSettings();

			if (systemSettings.getImprint() != null) {
				this.imprint = systemSettings.getImprint();
			}
		} catch (DataSourceException e) {
			logger.warn("Initializing footer has failed.", e);
		}
	}

	private static final long serialVersionUID = 1L;

	/**
	 * The imprint for the footer.
	 */
	private String imprint;

	// TODO cache Footer

	/**
	 * @param imprint
	 *            the imprint to set
	 */
	public void setImprint(String imprint) {
		this.imprint = imprint;
	}

	/**
	 * Loads the imprint and stores it in {@link imprint}.
	 */
	public String getImprint() {
		return this.imprint;
	}
}
