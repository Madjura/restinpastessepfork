/**
 * 
 */
package de.sep16g01.rip.businessLogicLayer.general;

import java.io.IOException;
import java.text.ParseException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Handles exceptions in the backing beans that bubble up to there from the
 * lower layer(s), such as exceptions related to the database.
 */
public class BBExceptionHandler {


	/**
	 * Handles exceptions in the backing bean to prevent the user from seeing
	 * the exception. Displays a FacesMessage to the user.
	 * 
	 * @param e
	 *            An exception that is caught in the backing beans.
	 */
	public static void handleException(Exception e) {
		if (e instanceof IOException) {
			FacesContext.getCurrentInstance().addMessage("error",
					new FacesMessage(
							FacesMessage.SEVERITY_INFO, BBChangeLanguage
									.getLangStringStatic("IOException_error"),
							null));
		} else if (e instanceof DataSourceException) {
			FacesContext.getCurrentInstance()
					.addMessage("error",
							new FacesMessage(FacesMessage.SEVERITY_INFO,
									BBChangeLanguage.getLangStringStatic(
											"DataSourceException_error"),
									null));
		} else if (e instanceof NumberFormatException) {
			FacesContext.getCurrentInstance()
					.addMessage("error",
							new FacesMessage(FacesMessage.SEVERITY_INFO,
									BBChangeLanguage.getLangStringStatic(
											"NumberFormatException_error"),
									null));
		} else if (e instanceof ParseException) {
			FacesContext.getCurrentInstance().addMessage("error",
					new FacesMessage(FacesMessage.SEVERITY_INFO,
							BBChangeLanguage.getLangStringStatic(
									"ParseException_error"),
							null));
		}
	}
}
