package de.sep16g01.rip.businessLogicLayer.pagination;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * Backing bean for the navigation controls for paginatable lists.
 * 
 * @param <E>
 *            The List-entry type
 */
@ViewScoped
@Named
public class BBPaginator<E> implements Serializable {
	private String query;

	/**
	 * The type of the systemElement
	 */
	private SystemElementType systemElementType;

	private Paginatable<E> paginatable;

	public BBPaginator(Paginatable<E> paginatable) {
		this.paginatable = paginatable;
		this.otherPageParameters = new HashMap<>();
	}

	private Map<String, String> pageParams;

	public void gotoPage(int page) {
		this.paginatedDataModel = paginatable.paginate(query, page,
				SystemSettings.COUNT_RESULTS_PER_PAGE);
	}

	public void gotoPage(String page) {
		if (page != null && !"".equals(page)) {
			try {
				gotoPage(Integer.parseInt(page));
			} catch (NumberFormatException e) {
				gotoPage(1);
			}
		} else {
			gotoPage(1);
		}
	}

	private static final long serialVersionUID = 1L;

	/**
	 * The list of paginatable elements.
	 */
	private PaginatedDataModel<E> paginatedDataModel = null;

	private String pageParamName = "page"; // "page" is default param name

	private Map<String, String> otherPageParameters;

	public PaginatedDataModel<E> getPaginatedDataModel() {
		return paginatedDataModel;
	}

	public void setPaginatedDataModel(PaginatedDataModel<E> paginatedDataModel) {
		this.paginatedDataModel = paginatedDataModel;
	}

	public String page(int page) {
		gotoPage(page);
		String viewId = FacesContext.getCurrentInstance().getViewRoot()
				.getViewId();
		return viewId + "?faces-redirect=true&" + pageParamName + "=" + page
				+ convertOtherPageParametersToString();
	}

	/**
	 * Go back to the previous pagination list.
	 * 
	 * @return A link to the page.
	 * @throws DataSourceException
	 */
	public String previous() {
		return page(getPage() - 1);
	}

	/**
	 * @return the current page
	 */
	private int getPage() {
		return paginatedDataModel.getCurrentPage();
	}

	/**
	 * Go to the next page of the pagination list.
	 * 
	 * @return A link to the page.
	 * @throws DataSourceException
	 */
	public String next() {
		return page(getPage() + 1);
	}

	/**
	 * Go to the first page of the pagination list.
	 * 
	 * @return A link to the page.
	 * @throws DataSourceException
	 */
	public String first() {
		return page(1);
	}

	/**
	 * Go back two pages on the pagination list.
	 * 
	 * @return A link to the page.
	 */
	public String twoBefore() {
		return page(getPage() - 2);
	}

	/**
	 * Go back one page on the pagination list.
	 * 
	 * @return A link to the page.
	 * @throws DataSourceException
	 */
	public String oneBefore() {
		return previous();
	}

	/**
	 * Go to the next page of the pagination list.
	 * 
	 * @return
	 * @throws DataSourceException
	 */
	public String oneAfter() {
		return next();
	}

	/**
	 * Go back two pages on the pagination list.
	 * 
	 * @return
	 */
	public String twoAfter() {
		return page(getPage() + 2);
	}

	/**
	 * Go to the last page of the pagination list.
	 * 
	 * @return
	 * @throws DataSourceException
	 */
	public String last() {
		int page = paginatedDataModel.getLastPage();
		return page(page);
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public SystemElementType getSystemElementType() {
		return systemElementType;
	}

	public void setSystemElementType(SystemElementType systemElementType) {
		this.systemElementType = systemElementType;
	}

	public void setPageParamName(String pageParamName) {
		this.pageParamName = pageParamName;
	}

	public void setOtherPageParameters(Map<String, String> other) {
		otherPageParameters = other;
	}

	public void putOtherPageParameter(String key, String page) {
		otherPageParameters.put(key, page);
	}

	private String convertOtherPageParametersToString() {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, String> entry : otherPageParameters.entrySet()) {
			sb.append("&");
			sb.append(entry.getKey());
			sb.append("=");
			if (entry.getValue() != null)
				sb.append(entry.getValue());
			else
				sb.append("1");
		}
		return sb.toString();
	}

	public Map<String, String> getPageParams() {
		return pageParams;
	}

	public void setPageParams(Map<String, String> pageParams) {
		this.pageParams = pageParams;
	}
}
