package de.sep16g01.rip.businessLogicLayer.pagination;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.DataModel;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;

/**
 * Describes a single element of the result of a search in the system.
 *
 * @param <E>
 *            The type of the search result items.
 */
public class PaginatedDataModel<E> extends DataModel<E> implements Serializable {

	private static final long serialVersionUID = 1L;

	public PaginatedDataModel(int currentPage, int countResults, List<E> values) {
		super();
		this.currentPage = currentPage;
		this.countResults = countResults;
		this.values = values;
		calculateDisplayPages();
	}

	/**
	 * the current page
	 */
	private int currentPage;

	/**
	 * The number of search results.
	 */
	private int countResults;

	private List<E> values;
	private int rowIndex;

	private List<Integer> displayPages;
	private int lastPage;

	public int getCountResults() {
		return countResults;
	}

	public void setCountResults(int countResults) {
		this.countResults = countResults;
	}

	/**
	 * calculates the index of the first, the last and the 2 pages before and after the current page
	 */
	private void calculateDisplayPages() {
		displayPages = new ArrayList<Integer>();

		displayPages.add(1);

		int pages = countResults / SystemSettings.COUNT_RESULTS_PER_PAGE;
		int mod = countResults % SystemSettings.COUNT_RESULTS_PER_PAGE;
		this.lastPage = (mod == 0 ? pages : pages + 1);

		for (int i = currentPage - 2; i <= currentPage + 2; i++) {
			if (i == 1)
				continue;
			if (i < 1)
				continue;
			if (i >= lastPage) {
				continue;
			}
			displayPages.add(i);
		}
		displayPages.add(lastPage);

	}

	private String checkDisplayPage(int page) {
		if (displayPages.contains(page) && page != lastPage && page != 1) {
			return String.valueOf(page);
		}
		return "";
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public String getDisplayPageTwoBefore() {
		return checkDisplayPage(currentPage - 2);
	}

	public String getDisplayPageOneBefore() {
		return checkDisplayPage(currentPage - 1);
	}

	public String getDisplayPageOneAfter() {
		return checkDisplayPage(currentPage + 1);
	}

	public String getDisplayPageTwoAfter() {
		return checkDisplayPage(currentPage + 2);
	}

	public String getDisplayPageLast() {
		return String.valueOf(lastPage);
	}

	@Override
	public void setWrappedData(Object arg0) {
		values = (List<E>) arg0;
		calculateDisplayPages();
	}

	@Override
	public void setRowIndex(int arg0) {
		rowIndex = arg0;
	}

	@Override
	public boolean isRowAvailable() {
		if (0 <= rowIndex && rowIndex < values.size()) {
			return true;
		}
		return false;
	}

	@Override
	public Object getWrappedData() {
		return values;
	}

	@Override
	public int getRowIndex() {
		return rowIndex;
	}

	@Override
	public E getRowData() {
		return values.get(rowIndex);
	}

	@Override
	public int getRowCount() {
		return values.size();
	}

	public int getLastPage() {
		return this.lastPage;
	}
}
