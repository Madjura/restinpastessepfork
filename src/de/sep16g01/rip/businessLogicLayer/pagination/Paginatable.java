package de.sep16g01.rip.businessLogicLayer.pagination;

import java.io.Serializable;

/**
 * Pagination implementation for list like structures and elements. Enables
 * users to easily browse list-like structures and elements by providing
 * functions to go back a page or forward and show the elements on this page,
 * where each page displays a maximum number of items.
 * 
 * @param <E>
 */
public interface Paginatable<E> extends Serializable {

	/**
	 * Allows browsing between pages of lists or list-like elements.
	 * 
	 * @param query
	 *            a search query
	 * @param page
	 *            The page that is going to be displayed.
	 * @param entriesPerPage
	 *            How many elements are going to be displayed.
	 * @return A list of elements on the page that is going to be displayed,
	 *         with as many elements being shown as described by
	 *         {@code entriesPerPage}.
	 */
	public abstract PaginatedDataModel<E> paginate(String query, int page,
			int entriesPerPage);
}
