package de.sep16g01.rip.businessLogicLayer.notify;

/**
 * Holds all the changes that can be done to elements that let the user get notifications on change.
 */
public enum Changes {
	TITLE("Titel", "Title"), CONTENT("Inhalt", "Content"), FILE_DELETE(
			"Datei entfernt", "File deleted"), FILE_UPLOAD("Datei hochgeladen",
			"File uploaded"), NEW_PASTE("Neues Paste", "New Paste"), PASTE_DELETE(
			"Paste entfernt", "Paste deleted"), NEW_RIGHTS("Neue Rechte",
			"New rights"), DELETE("Entfernt", "Deleted"), VISIBILITY_READ(
			"Leserecht", "Visibility (Read)"), VISIBILITY_WRITE("Schreibrecht",
			"Visibility (Write)"), DESCRIPTION("Beschreibung", "Description"), CLIPBOARD_DELETE(
			"Clipboard entfernt", "Clipboard deleted");

	private String german;
	private String english;

	private Changes(String german, String english) {
		this.german = german;
		this.english = english;
	}

	public String getGerman() {
		return german;
	}

	public String getEnglish() {
		return english;
	}
}
