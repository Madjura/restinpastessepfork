package de.sep16g01.rip.businessLogicLayer.notify;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.email.Email;
import de.sep16g01.rip.businessLogicLayer.email.EmailFactory;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;

/**
 * Responsible for detecting changes in pastes or clipboards. For a list of
 * possible changes see {@link Changes}.
 */
public class NotifyOnChange {

	private final static Logger logger = Logger.getLogger(NotifyOnChange.class);

	/**
	 * Called by the
	 * {@link de.sep16g01.rip.businessLogicLayer.paste.BBCreatePaste#save()
	 * BBCreatePaste.save()} method. Checks a paste for changes based on what is
	 * stored in the database.
	 * 
	 * @param paste
	 *            The paste that is being checked for changes.
	 * @throws DataSourceException
	 */
	public static void checkChangesForPaste(Paste paste) {
		Set<Changes> changes = null;
		try {
			changes = getChangesForPaste(paste);
		} catch (DataSourceException e) {
			logger.warn(
					"Check changes for Paste didnt word. PasteID"
							+ paste.getId(), e);
			return;
		}
		Email email = EmailFactory.getChangesEmail(changes, paste);
		email.setRecipient(paste.getCreator().getEmail());
		email.send(null);
	}

	/**
	 * Called by the
	 * {@link de.sep16g01.rip.businessLogicLayer.clipboard.BBCreateClipboard#save()
	 * BBCreateClipboard.save()} method. Checks a clipboard for changes based on
	 * what is stored in the database.
	 * 
	 * @param clipboard
	 *            The clipboard that is being checked for changes.
	 * @throws DataSourceException
	 */
	public static void checkChangesForClipboard(Clipboard clipboard) {
		Set<Changes> changes = null;
		try {
			changes = getChangesForClipboard(clipboard);
			Email email = EmailFactory.getChangesEmail(changes, clipboard);
			email.setRecipient(clipboard.getCreator().getEmail());
			email.send(null);
		} catch (DataSourceException e) {
			logger.warn("Check changes for clipboard didn't work", e);
		}
	}

	/**
	 * Performs the check of changes for a paste by pulling the paste based on
	 * its ID from the database and comparing it to the paste it was passed,
	 * then returns a Set of changes.
	 * 
	 * @param paste
	 *            The paste that is being checked.
	 * @return The Set of changes.
	 * @throws DataSourceException
	 */
	private static Set<Changes> getChangesForPaste(Paste paste)
			throws DataSourceException {
		Paste check = DAOPaste.getById(paste.getId());
		Set<Changes> changes = new HashSet<Changes>();

		// check title
		if (!paste.getTitle().equals(check.getTitle())) {
			changes.add(Changes.TITLE);

			// check content
		}
		if (!paste.getContent().equals(check.getContent())) {
			changes.add(Changes.CONTENT);

			// check if file was added
		}
		if (paste.getFiles() != null
				&& paste.getFiles().size() > check.getFiles().size()) {
			changes.add(Changes.FILE_UPLOAD);

			// check if file was deleted
		}
		if (paste.getFiles() != null
				&& paste.getFiles().size() < check.getFiles().size()) {
			changes.add(Changes.FILE_DELETE);

			// check description
		}
		if (!paste.getDescription().equals(check.getDescription())) {
			changes.add(Changes.DESCRIPTION);
		}
		return changes;
	}

	/**
	 * Performs the check of changes for a clipboard by pulling the clipboard
	 * based on its ID from the database and comparing it to the clipboard it
	 * was passed, then returns a Set of changes.
	 * 
	 * @param clipboard
	 *            The clipboard that is being checked.
	 * @return The Set of changes.
	 * @throws DataSourceException
	 */
	private static Set<Changes> getChangesForClipboard(Clipboard clipboard)
			throws DataSourceException {
		Clipboard check = DAOClipboard.get(clipboard.getId());
		Set<Changes> changes = new HashSet<Changes>();

		// check title
		if (!clipboard.getTitle().equals(check.getTitle())) {
			changes.add(Changes.TITLE);

			// check if read was changed
		}
		if (clipboard.getVisibilityRead().getId() != check.getVisibilityRead()
				.getId()) {
			changes.add(Changes.VISIBILITY_READ);

			// check if write was changed
		}
		if (clipboard.getVisibilityWrite().getId() != check
				.getVisibilityWrite().getId()) {
			changes.add(Changes.VISIBILITY_WRITE);

			// check if description was changed
		}
		if (!clipboard.getDescription().equals(check.getDescription())) {
			changes.add(Changes.DESCRIPTION);

			// check if if paste was added
		}

		return changes;
	}

	/**
	 * Called when pastes with notify = true are deleted.
	 * 
	 * @param paste
	 *            The paste that is being deleted.
	 * @throws DataSourceException
	 */
	public static void notifyOfPasteDelete(Paste paste) {
		Set<Changes> changes = new HashSet<Changes>();
		changes.add(Changes.PASTE_DELETE);
		Email email = EmailFactory.getChangesEmail(changes, paste);
		email.setRecipient(paste.getCreator().getEmail());
		email.send(null);
	}

	/**
	 * Called when pastes with notify = true are created..
	 * 
	 * @param paste
	 *            The paste that is being deleted.
	 * @throws DataSourceException
	 */
	public static void notifyOfPasteCreate(Clipboard clipboard, Paste paste) {
		Set<Changes> changes = new HashSet<Changes>();
		changes.add(Changes.NEW_PASTE);
		Email email = EmailFactory.getChangesEmail(changes, clipboard);
		email.setRecipient(clipboard.getCreator().getEmail());
		email.send(null);
	}

	/**
	 * Called when clipboards with notify = true are deleted.
	 * 
	 * @param clipboard
	 *            The clipboard that is being deleted.
	 * @throws DataSourceException
	 */
	public static void notifyOfClipboardDelete(Clipboard clipboard) {
		Set<Changes> changes = new HashSet<Changes>();
		changes.add(Changes.CLIPBOARD_DELETE);
		Email email = EmailFactory.getChangesEmail(changes, clipboard);
		email.setRecipient(clipboard.getCreator().getEmail());
		email.send(null);
	}

	/**
	 * Used for tests only.
	 * 
	 * @param paste
	 *            The paste being checked for changes.
	 * @return A Set of changes.
	 * @throws DataSourceException
	 */
	public static Set<Changes> checkPasteChanges(Paste paste) {
		try {
			return getChangesForPaste(paste);
		} catch (DataSourceException e) {
			logger.warn("Check Paste changes didn't work for paste with id:"
					+ paste.getId(), e);
		}
		return null;
	}

	/**
	 * Used for tests only.
	 * 
	 * @param paste
	 *            The paste being checked for changes.
	 * @return A Set of changes.
	 * @throws DataSourceException
	 */
	public static Set<Changes> checkClipboardChanges(Clipboard clipboard) {
		try {
			return getChangesForClipboard(clipboard);
		} catch (DataSourceException e) {
			logger.warn(
					"Check Clipboard changes didn't work for clipboards with id:"
							+ clipboard.getId(), e);
		}
		return null;
	}
}
