package de.sep16g01.rip.businessLogicLayer.comments;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.pagination.Paginatable;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.dataAccessLayer.comments.DAOComment;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * DTO of the comments in our system. Holds the creator and content of a
 * comment. Comments are small texts that can be posted under pastes,
 * clipboards, groups and profiles.
 */
public class Comment extends SystemElement implements Paginatable<Comment> {

	private static final long serialVersionUID = 1L;

	/**
	 * The creator of the comment.
	 */
	private User creator;

	/**
	 * The text content of the comment.
	 */
	private String content;

	/**
	 * The {@link de.sep16g01.rip.businessLogicLayer.general.SystemElement
	 * SystemElement} the comment belongs to.
	 */
	private SystemElement belongsTo;

	public SystemElement getBelongsTo() {
		return belongsTo;
	}

	public String getContent() {
		return this.content;
	}

	public User getCreator() {
		return this.creator;
	}

	/* 
	 * unused inherited method
	 */
	@Override
	public String getLink() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.sep16g01.rip.businessLogicLayer.pagination.Paginatable#paginate(java.
	 * lang.String, int, int)
	 */
	@Override
	public PaginatedDataModel<Comment> paginate(String query, int page,
			int entriesPerPage) {
		try {
			return DAOComment.paginate(query, page, entriesPerPage);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return null;
	}

	public void setBelongsTo(SystemElement belongsTo) {
		this.belongsTo = belongsTo;
	}

	

	public void setContent(String value) {
		this.content = value;
	}

	public void setCreator(User value) {
		this.creator = value;
	}

	@Override
	public String toString() {
		return "Comment [id=" + this.id + ", creator="
				+ (this.creator != null ? this.creator.getId() : "")
				+ ", belongsTo=" + this.belongsTo + "]";
	}

}
