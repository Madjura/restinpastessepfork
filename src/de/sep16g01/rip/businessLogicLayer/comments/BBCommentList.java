package de.sep16g01.rip.businessLogicLayer.comments;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.Report;
import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.administration.DAOReport;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
//import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.comments.DAOComment;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
//import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the list of comments that is displayed under groups, pastes,
 * profiles and clipboards. Holds a list of comment objects and a pagination
 * object so the comments can be displayed to the user. Additionally provides
 * the report and delete functionalities, which lets users report comments or
 * delete them (if authorized, i.e. if they are a moderator or higher)
 * respectively.
 */
@ViewScoped
@Named
public class BBCommentList extends BBExceptionHandler implements Serializable {
	private final static Logger logger = Logger.getLogger(BBCommentList.class);

	private static final long serialVersionUID = 1L;

	@Inject
	UserData userData;

	/**
	 * The list of comments belong to the {@link #element SystemElement}.
	 */
	private BBPaginator<Comment> comments;

	/**
	 * The {@link de.sep16g01.rip.businessLogicLayer.general.SystemElement
	 * SystemElement} the comments belong to.
	 */
	private SystemElement element;

	/**
	 * Creates a report object for a comment and stores it in the database.
	 * 
	 * @param comment
	 *            The comment that is being reported.
	 * @return A link to the same page that the user was on, with the report
	 *         button now not-visible.
	 */
	public String report() {

		Comment c = comments.getPaginatedDataModel().getRowData();
		Report r = new Report();
		r.setCreator(userData.getUser());
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.WEEK_OF_MONTH, 2); // 2 weeks lifetime for reports
		r.setExpirationDate(cal.getTime());
		r.setReportedComment(c);
		try {
			DAOReport.store(r);
			logger.info("Creating " + r + " for " + c);
		} catch (DataSourceException e) {
			logger.error("Exception while creating " + r + " for " + c);
			BBExceptionHandler.handleException(e);
			return "";
		}

		return null;
	}

	public boolean alreadyReported(int commentid) {
		try {
			return DAOReport.isAlreadyReported(commentid);
		} catch (DataSourceException e) {
			// ignore, just dont show the report link
		}
		return false;
	}

	/**
	 * Deletes a comment. The button that is calling this function is only
	 * visible to moderators and higher.
	 * 
	 * @param comment
	 *            The comment that is being deleted.
	 * @return
	 */
	public String delete(Comment com) {
		try {
			DAOComment.delete(com.getId());
			logger.info("Deleted " + com + ", User: " + userData.getUser());
		} catch (DataSourceException e) {
			logger.error("Exception while deleting " + com + ", User: "
					+ userData.getUser());
			BBExceptionHandler.handleException(e);
			return "";
		}
		return null;
	}

	public BBPaginator<Comment> getComments() {
		return this.comments;
	}

	public SystemElement getElement() {
		return element;
	}

	public void setElement(SystemElement element) {
		this.element = element;
	}

	/**
	 * Loads all the comments for the {@link #element SystemElement} from the
	 * database.
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String groupid = params.get(SystemSettings.GROUPID);
		String userid = params.get(SystemSettings.USERID);
		String pasteid = params.get(SystemSettings.PASTEID);
		String clipboardid = params.get(SystemSettings.CLIPBOARDID);
		if (userid != null) {
			try {
				element = DAOUser.getById(Integer.parseInt(userid));
				comments = new BBPaginator<Comment>(new Comment());
				comments.setQuery("type=comment;userId=" + element.getId());
				// comments.setSystemElementType(SystemElementType.CLIPBOARD);
				comments.setSystemElementType(SystemElementType.USER);
				String page = params.get("page");
				comments.gotoPage(page);
				comments.putOtherPageParameter(SystemSettings.USERID, userid);
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else if (groupid != null) {
			try {
				element = DAOGroup.get(Integer.parseInt(groupid));
				comments = new BBPaginator<Comment>(new Comment());
				comments.setQuery("type=comment;groupId=" + element.getId());
				comments.setSystemElementType(SystemElementType.GROUP);
				String page = params.get("page");
				comments.gotoPage(page);
				comments.putOtherPageParameter(SystemSettings.GROUPID, groupid);
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else if (pasteid != null) {
			try {
				element = DAOPaste.getById(Integer.parseInt(pasteid));
				comments = new BBPaginator<Comment>(new Comment());
				comments.setQuery("type=comment;pasteId=" + element.getId());
				comments.setSystemElementType(SystemElementType.PASTE);
				String page = params.get("page");
				comments.gotoPage(page);
				comments.putOtherPageParameter(SystemSettings.PASTEID, pasteid);
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else if (clipboardid != null) {
			try {
				element = DAOClipboard.get(Integer.parseInt(clipboardid));
				comments = new BBPaginator<Comment>(new Comment());
				comments.setQuery("type=comment;clipboardId=" + element.getId());
				comments.setSystemElementType(SystemElementType.CLIPBOARD);
				String page = params.get("page");
				comments.gotoPage(page);
				comments.putOtherPageParameter(SystemSettings.CLIPBOARDID,
						clipboardid);
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		}
	}
}
