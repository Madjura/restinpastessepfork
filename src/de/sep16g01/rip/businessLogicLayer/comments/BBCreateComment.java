package de.sep16g01.rip.businessLogicLayer.comments;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
//import javax.faces.application.FacesMessage;
//import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
//import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.comments.DAOComment;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
//import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the comment creation field. Stores the creator's name, the
 * element the comment belongs to and the content of the comment. Provides save
 * functionality to the user and is responsible to get the comment saved in the
 * database by calling the DAO function.
 */
@ViewScoped
@Named
public class BBCreateComment implements Serializable {
	private final static Logger logger = Logger
			.getLogger(BBCreateComment.class);

	private static final long serialVersionUID = 1L;

	/**
	 * Used to access the user and checking whether or not the user is logged in
	 * or anonymous easily.
	 */
	@Inject
	private UserData userData;

	/**
	 * The comment that is being created.
	 */
	private Comment comment;

	/**
	 * The SystemElement the comment belongs to.
	 */
	private SystemElement element;

	/**
	 * Saves the type of the Object the comment belongs to.
	 */
	private String type;

	/**
	 * Creates and saves the comment the user has written to the database.
	 */
	public String save() {
		if (!comment.getContent().equals("")) {
			comment.setBelongsTo(element);
			comment.setCreator(userData.getUser());
			Date date = new java.util.Date();
			comment.setCreationDate(date);
			try {
				DAOComment.store(comment, type);
				logger.info("Creating " + comment);
			} catch (DataSourceException e) {
				logger.info("Error while creating " + comment);
				BBExceptionHandler.handleException(e);
				return "";
			}

			// reload page
			if (type.equals("User")) {
				return "/view/profile/profile.xhtml?faces-redirect=true" + "&"
						+ SystemSettings.USERID + "=" + element.getId();
			} else if (type.equals("Group")) {
				return "/view/group/group.xhtml?faces-redirect=true" + "&"
						+ SystemSettings.GROUPID + "=" + element.getId();
			} else if (type.equals("Paste")) {
				return "/view/paste/paste.xhtml?faces-redirect=true" + "&"
						+ SystemSettings.PASTEID + "=" + element.getId();
			} else if (type.equals("Clipboard")) {
				return "/view/clipboard/clipboard.xhtml?faces-redirect=true"
						+ "&" + SystemSettings.CLIPBOARDID + "="
						+ element.getId();
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(FacesMessage.SEVERITY_INFO,
							BBChangeLanguage
									.getLangStringStatic("comment_not_empty"),
							null));
		}
		return "";
	}

	/**
	 * @return the comment
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(Comment comment) {
		this.comment = comment;
	}

	/**
	 * Creates an empty {@link Comment}. Finds out to which system-element the
	 * comment belongs to.
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		comment = new Comment();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String groupid = params.get(SystemSettings.GROUPID);
		String userid = params.get(SystemSettings.USERID);
		String pasteid = params.get(SystemSettings.PASTEID);
		String clipboardid = params.get(SystemSettings.CLIPBOARDID);
		if (userid != null) {
			int id = Integer.parseInt(userid);
			try {
				element = DAOUser.getById(id);
				type = "User";
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else if (groupid != null) {
			int id = Integer.parseInt(groupid);
			try {
				element = DAOGroup.get(id);
				type = "Group";
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else if (pasteid != null) {
			try {
				element = DAOPaste.getById(Integer.parseInt(pasteid));
				type = "Paste";
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else if (clipboardid != null) {
			try {
				element = DAOClipboard.get(Integer.parseInt(clipboardid));
				type = "Clipboard";
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		}
	}

}
