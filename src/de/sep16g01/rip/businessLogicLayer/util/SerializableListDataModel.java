/**
 *   Copyright subkom GmbH. All rights reserved. subkom.de
 *   File:     de.sep16g01.rip.businessLogicLayer.util.SerializableListDataModel.java
 *   Created:  21.06.2016, 09:53:14 by hubertho
 */
package de.sep16g01.rip.businessLogicLayer.util;

import java.io.Serializable;

import javax.faces.model.ListDataModel;

/**
 *
 */
public class SerializableListDataModel<T> extends ListDataModel<T> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
