package de.sep16g01.rip.businessLogicLayer.util;

import java.util.Map;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;

public final class ParameterUtils {
	private ParameterUtils() {
	}

	public static Access readAccessParameter(Map<String, String> params) {
		return Access
				.getById(readIntegerParameter(params, SystemSettings.ACCESSID));
	}

	public static Integer readIntegerParameter(Map<String, String> params,
			String paramName) {
		String sId = params.get(paramName);
		int id = -1;
		try {
			id = Integer.parseInt(sId);
		} catch (NumberFormatException e) {

		}
		return id;
	}
}
