/**
 *   Copyright subkom GmbH. All rights reserved. subkom.de
 *   File:     de.sep16g01.rip.businessLogicLayer.util.LoadImae.java
 *   Created:  23.06.2016, 14:16:03 by hubertho
 */
package de.sep16g01.rip.businessLogicLayer.util;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 *
 */
public class LoadImage {
	public static byte[] extractBytes(String ImageName) throws IOException {
		// open image
		File imgPath = new File(ImageName);
		BufferedImage bufferedImage = ImageIO.read(imgPath);

		// get DataBufferBytes from Raster
		WritableRaster raster = bufferedImage.getRaster();
		DataBufferByte data = (DataBufferByte) raster.getDataBuffer();

		return (data.getData());
	}
}
