package de.sep16g01.rip.businessLogicLayer.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.catalina.core.ApplicationPart;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;

/**
 * Utility class to append contents of a .txt file to a paste.
 */
public class TxtToPaste {

	/**
	 * Appends content of a file to a paste. Check the header before for
	 * text/plain before calling this.
	 * 
	 * @param newFile
	 *            The uploaded file.
	 * @param paste
	 *            The paste.
	 */
	public static void addTxtToPaste(ApplicationPart newFile, Paste paste) {
		BufferedReader reader;
		try {
			reader = new BufferedReader(
					new InputStreamReader(newFile.getInputStream()));
			StringBuilder out = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line + "\n");
			}
			paste.setContent(paste.getContent() + "\n" + out.toString());
		} catch (IOException e) {
			BBExceptionHandler.handleException(e);
		}
	}
}
