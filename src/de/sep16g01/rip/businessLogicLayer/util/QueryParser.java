/**
 *   Copyright subkom GmbH. All rights reserved. subkom.de
 *   File:     de.sep16g01.rip.businessLogicLayer.util.QueryParser.java
 *   Created:  07.06.2016, 15:12:57 by hubertho
 */
package de.sep16g01.rip.businessLogicLayer.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Parses pagination queries into key/value pairs.
 */
public class QueryParser {
	public static final String DELIMITER_VALUE = "=";
	public static final String DELIMITER_PAIRS = ";";
	public static final String DELIMITER_QUERY = ",";

	/**
	 * Parses a pagination query into key/value pairs and returns it.
	 * 
	 * @param query
	 *            The query being parsed.
	 * @return A Map of key/value pairs, null if query was not balanced.
	 */
	public static Map<String, String> parsePaginationQuery(String query) {
		Map<String, String> map = new HashMap<String, String>();
		String[] splitValues = query.split(DELIMITER_PAIRS);

		for (String pair : splitValues) {
			String[] value = pair.split(DELIMITER_VALUE);
			if (value.length == 0) {
				continue;
			}
			if (value.length % 2 == 1) {
				map.put(value[0], "");
			} else {
				map.put(value[0], value[1]);
			}
		}
		return map;
	}

	/**
	 * Parses a WHERE LIKE statement based on a query and column.
	 * 
	 * @param query
	 *            The value being looked for.
	 * @param column
	 *            The column being searched.
	 * @return A string ready for use in WHERE LIKE statements.
	 */
	public static String createWHEREQuery(int countQueries, String column) {

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < countQueries; i++) {
			sb.append(" " + column + " LIKE " + "? ");
			if (i + 1 < countQueries) {
				sb.append(" OR ");
			}
		}
		return sb.toString();
	}
}
