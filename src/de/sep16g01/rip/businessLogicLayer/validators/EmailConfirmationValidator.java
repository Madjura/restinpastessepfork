package de.sep16g01.rip.businessLogicLayer.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Checks for equality of two email fields.
 */
@FacesValidator("sep16g01.EmailConfirmationValidator")
public class EmailConfirmationValidator implements Validator {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		String email = value.toString();

		UIInput uiInputConfirmEmail = (UIInput) component.getAttributes().get("confirmEmail");
		if (uiInputConfirmEmail != null) {

			String confirmEmail = uiInputConfirmEmail.getSubmittedValue().toString();
			
			if (!email.equals(confirmEmail)) {
				uiInputConfirmEmail.setValid(false);
				FacesMessage msg = new FacesMessage(BBChangeLanguage.getLangStringStatic("error_email_mismatch"));
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage("error", msg);
				throw new ValidatorException(msg);
			}
		}
	}
}
