package de.sep16g01.rip.businessLogicLayer.validators;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Checks the validity of an entered timespan.
 */
@FacesValidator("sep16g01.TimeSpanValidator")
public class TimeSpanValidator implements Validator {

	/**
	 * Parses a timespan string entered by the user into an expiration date.
	 * The string is given as a string of number-character pairs, e.g.
	 * 
	 * "1w1d1h" - one week + one day + one hour
	 * 
	 * The order of the time chunks is not important, "1d1w" = "1w1d", and neither is
	 * capitalization.
	 * 
	 * @param base start date
	 * @param lifetime lifetime as expiration string
	 * @return the absolute expiration date
	 * @throws ParseException
	 */
	public static Date parseExpirationDateFromString(Date base, String lifetime)
			throws ParseException {

		if(lifetime == null)
			return base;
		
		String cleaned = lifetime.toLowerCase().replaceAll(" ", "");
		if(cleaned.isEmpty())
			return base;
		
		Map<String, Integer> result = new HashMap<String, Integer>();

		String tmpnum = "";
		for (int i = 0; i < cleaned.length(); i++) {
			char c = cleaned.charAt(i); // no for..each iterating over chars in
										// string
			if (Character.isDigit(c)) {
				tmpnum += c;
				continue;
			} else if (!tmpnum.isEmpty()) {
				result.put(Character.toString(c), Integer.parseInt(tmpnum));
				tmpnum = "";
			} else {
				// error
				throw new ParseException("Invalid timespan format.", i);
			}
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(base);
		if (result.containsKey("y"))
			cal.add(Calendar.YEAR, result.get("y"));
		if (result.containsKey("w"))
			cal.add(Calendar.WEEK_OF_YEAR, result.get("w"));
		if (result.containsKey("d"))
			cal.add(Calendar.DAY_OF_MONTH, result.get("d"));
		if (result.containsKey("h"))
			cal.add(Calendar.HOUR, result.get("h"));
		if (result.containsKey("m"))
			cal.add(Calendar.MINUTE, result.get("m"));

		return cal.getTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) {

		String timestamp = arg2.toString();

		try {
			@SuppressWarnings("unused")
			Date expire = parseExpirationDateFromString(new Date(), timestamp);

		} catch (ParseException e) {

			FacesMessage msg = new FacesMessage("Invalid Timestamp.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
