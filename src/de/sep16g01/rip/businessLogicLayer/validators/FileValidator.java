package de.sep16g01.rip.businessLogicLayer.validators;

import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;

/**
 * Checks the file size of a file.
 */
public class FileValidator implements Validator {

	@Inject
	UserData userdata;
	
	private final transient static Logger logger = Logger
			.getLogger(DAOGroup.class);


	/**
	 * Checks whether or not the file is too big.
	 */
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) {

		Part file = (Part) arg2;
		
		try {
			HashMap<Role, Long> maxsize = DAOSystemSettings.loadSystemSettings().getMaxSizePerRole();
			Long maxSizeCurrentUser = maxsize.get(userdata.getUser().getRole());

			
			if (maxSizeCurrentUser == null || file.getSize() > maxSizeCurrentUser) {
				throw new ValidatorException(new FacesMessage("File too big."));
			}

		} catch (DataSourceException e) {

			logger.warn("Error accessing DB in FileValidator.");
			throw new ValidatorException(new FacesMessage("File not valid"));
		}

	}
}
