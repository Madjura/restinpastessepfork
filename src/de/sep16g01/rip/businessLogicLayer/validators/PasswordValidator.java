package de.sep16g01.rip.businessLogicLayer.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Checks the validity of a password.
 */
@FacesValidator("sep16g01.PasswordValidator")
public class PasswordValidator implements Validator {

	public static final int MIN_PW_LEN = 8;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
			throws ValidatorException {

		String password = arg2.toString();

		if (password.length() < MIN_PW_LEN) {
			FacesMessage msg = new FacesMessage(
					BBChangeLanguage.getLangStringStatic("error_short_pw"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("error", msg);
			throw new ValidatorException(msg);
		}
	}
}
