package de.sep16g01.rip.businessLogicLayer.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Checks the equality of two password fields.
 */
@FacesValidator("sep16g01.PasswordConfirmationValidator")
public class PasswordConfirmationValidator implements Validator {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		String password = value.toString();

		UIInput uiInputConfirmPassword = (UIInput) component.getAttributes()
				.get("confirmPassword");
		if (uiInputConfirmPassword != null) {

			String confirmPassword = uiInputConfirmPassword.getSubmittedValue()
					.toString();

			if (!password.equals(confirmPassword)) {
				uiInputConfirmPassword.setValid(false);
				FacesMessage msg = new FacesMessage(
						BBChangeLanguage.getLangStringStatic("error_pw_mismatch"));
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage("error",msg);
				throw new ValidatorException(msg);
			}
		}
	}
}
