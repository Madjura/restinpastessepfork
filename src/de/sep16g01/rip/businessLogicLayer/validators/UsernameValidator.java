package de.sep16g01.rip.businessLogicLayer.validators;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Checks the validity of a username.
 */
@FacesValidator("sep16g01.UsernameValidator")
public class UsernameValidator implements Validator {

	public static final int MAX_NAME_LEN = 32;

	private static final String UNAME_PATTERN = "^[a-zA-Z0-9_]*$";
	private Pattern pattern;

	public UsernameValidator() {
		pattern = Pattern.compile(UNAME_PATTERN);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) {

		String name = arg2.toString();
		String errormsg = null;
		if (!pattern.matcher(name).matches()) {
			errormsg = "error_invalid_name";
		} else if (name.length() > MAX_NAME_LEN) {
			errormsg = "error_long_name";
		}

		if (errormsg != null) {
			FacesMessage msg = new FacesMessage(
					BBChangeLanguage.getLangStringStatic(errormsg));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("error", msg);
			throw new ValidatorException(msg);
		}
	}
}
