/**
 * 
 */
package de.sep16g01.rip.businessLogicLayer.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Checks the validity of a single email.
 * 
 * An email address must be three characters or longer and contain '@'.
 * 
 */
@FacesValidator("sep16g01.EmailValidator")
public class EmailValidator implements Validator {

	
	private static final int MAX_EMAIL_LENGHT = 3;
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {

		String email = arg2.toString();

		if (!email.contains("@") || email.length() < MAX_EMAIL_LENGHT) {
			
			FacesMessage msg = new FacesMessage(
					BBChangeLanguage.getLangStringStatic("error_email_invalid"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("error", msg);
			throw new ValidatorException(msg);
		}
	}
}
