package de.sep16g01.rip.businessLogicLayer.session;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

/**
 * Holds the user's data for a session, who or what he is and whether or not he
 * is logged in.
 */
@SessionScoped
@Named
public class UserData implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The currently logged in user, or null if no user is logged in.
	 */
	private User user = new User();

	/**
	 * Method to check whether the user that is hold by this UserData-Object is
	 * logged in or not.
	 * 
	 * @return true, if the user is logged in
	 */
	public boolean isLoggedIn() {
		if (user == null) {
			try {
				user = DAOUser.getById(-1);
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return false;
			}
		}
		return user.getId() != -1;
	}

	public void setUser(User value) {
		this.user = value;
	}

	public User getUser() {
		return this.user;
	}
}
