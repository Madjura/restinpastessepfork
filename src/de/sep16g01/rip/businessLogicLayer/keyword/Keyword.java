package de.sep16g01.rip.businessLogicLayer.keyword;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.email.Email;
import de.sep16g01.rip.businessLogicLayer.email.EmailFactory;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.keyword.DAOKeyword;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.permission.DAOPermission;

/**
 * Provides the keyword functionality. Checks pastes for keywords by checking
 * the list of keywords in the database against the terms in the paste and
 * notifies people who want to be notified when the keyword appears.
 */
public class Keyword implements Serializable {

	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(Keyword.class);

	/**
	 * Checks a paste for the existence of keywords. Called whenever a paste is
	 * stored to the database in any form.
	 * 
	 * @param paste
	 *            The paste that is being checked.
	 * @throws DataSourceException
	 *             When the database cannot be contacted.
	 */
	public static void checkForKeywords(Paste pasteIn, boolean isNew) {
		Paste paste = new Paste();
		paste.setContent(pasteIn.getContent());
		paste.setId(pasteIn.getId());
		HashMap<String, List<Integer>> keywords = null;
		try {
			keywords = DAOKeyword.getAllKeyWords();
		} catch (DataSourceException e) {
			logger.warn("Checking Paste for Keywords has failed. PasteID: "
					+ paste.getId(), e);
		}
		List<String> content = null;

		// stores ids of users who already got their email
		Set<Integer> handledUsers = new HashSet<Integer>();
		if (isNew) {
			content = Arrays.<String> asList(paste.getContent()
					.replaceAll("\\p{P}", "").split(" "));
		} else {
			try {
				content = getNew(paste, DAOPaste.getById(paste.getId()));
				paste.setContent("");
				StringBuilder sb = new StringBuilder();
				for (String t : content) {
					sb.append(t + " ");
				}
				paste.setContent(sb.toString());
			} catch (DataSourceException e) {
				logger.warn("Checking Paste for Keywords has failed. PasteID: "
						+ paste.getId(), e);
				return;
			}
		}
		for (String term : content) {
			if (keywords.containsKey(term)) {
				List<Integer> userIds = keywords.get(term);

				// clear users who got their email, to ensure only 1 email/user
				userIds.removeAll(handledUsers);
				if (userIds.isEmpty()) {
					// no users, do nothing
					continue;
				}
				handledUsers.addAll(userIds);
				try {
					sendEmails(term, paste, DAOKeyword.getEmailsById(userIds));
				} catch (DataSourceException e) {
					logger.warn(
							"Send Email for keyword check of new paste has failed. PasteID: "
									+ paste.getId(), e);
				}
			}
		}
	}

	/**
	 * Responsible for getting the keyword emails sent to the users.
	 * 
	 * @param keyword
	 *            A keyword found in a paste.
	 * @param pastes
	 *            The paste that contains the keywords.
	 * @param hashMap
	 *            The emails of the users that want to be notified about the
	 *            keywords.
	 * @throws DataSourceException
	 */
	private static void sendEmails(String keyword, Paste paste,
			HashMap<Integer, String> idToEmail) throws DataSourceException {
		List<String> keywordList = new ArrayList<String>();
		List<Integer> handledUsers = new ArrayList<Integer>();
		List<String> pasteContent = Arrays.<String> asList(paste.getContent()
				.replaceAll("\\p{P}", "").split("\\W+"));

		for (Map.Entry<Integer, String> entry : idToEmail.entrySet()) {
			int userId = entry.getKey();

			// check whether user got keyword mail already for previous keyword
			if (handledUsers.contains(userId)) {
				continue;
			}
			if (DAOPermission.checkPasteUserAccess(paste.getId(), userId) != null) {

				// get all keywords of user, check if paste contains it, if yes
				// include in email
				List<String> keywordsOfUser = DAOKeyword
						.loadKeywordsForUser(userId);
				for (String keywordUser : keywordsOfUser) {
					if (pasteContent.contains(keywordUser)) {
						keywordList.add(keywordUser);
					}
				}
				Email email = EmailFactory.getKeywordEmail(keywordList, paste);
				email.setRecipient(entry.getValue());
				email.send(null);
				handledUsers.add(userId);
			}
		}
	}

	/**
	 * Called by {@link #checkForKeywords(Paste paste)} if the paste is one that
	 * was edited and not created newly. Checks a
	 * {@link de.sep16g01.rip.businessLogicLayer.paste.Paste Paste} against its
	 * old version from the database and returns a {@link java.util.Set Set}
	 * containing only the terms that are in the first Paste but not in the
	 * other.
	 * 
	 * @param toCheck
	 *            The paste that is being checked for new words.
	 * @param fromDb
	 *            The old version from the paste, pulled from the database.
	 * @return A {@link java.util.Set Set<String>} containing only the terms
	 *         that are new.
	 */
	private static List<String> getNew(Paste toCheck, Paste fromDb) {
		Set<String> newPaste = new HashSet<String>();
		Set<String> oldPaste = new HashSet<String>();

		String[] splitToCheck = toCheck.getContent().replaceAll("\\p{P}", "")
				.split("\\W");
		String[] splitFromDb = fromDb.getContent().replaceAll("\\p{P}", "")
				.split("\\W");
		for (String term : splitToCheck) {
			newPaste.add(term);
		}

		for (String term : splitFromDb) {
			oldPaste.add(term);
		}

		newPaste.removeAll(oldPaste);
		return new ArrayList<String>(newPaste);
	}
}
