package de.sep16g01.rip.businessLogicLayer.clipboard;

import java.util.Date;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.pagination.Paginatable;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * DTO of the clipboards. Stores the creator of the clipboard, the name and a
 * list of clipboards belonging to it. This represents clipboards in our system.
 * A clipboard is simply a collection of pastes that are inside the clipboard.
 */
public class Clipboard extends SystemElement implements Paginatable<Clipboard> {

	private static final long serialVersionUID = 1L;

	private Visibility visibilityRead = Visibility.PUBLIC;
	private Visibility visibilityWrite = Visibility.PUBLIC;

	public Clipboard() {
		this.setTitle("Clipboard");
	}

	/**
	 * The creator of the clipboard.
	 */
	private User creator;

	/**
	 * The pastes belonging to this clipboard.
	 */
	private BBPaginator<Paste> pastes;

	/**
	 * The group this clipboard belongs to.
	 */
	private Group group;

	/**
	 * How long the clipboard is going to live.
	 */
	private Date expirationDate;

	private boolean notify = false;

	public void setCreator(User value) {
		this.creator = value;
	}

	public User getCreator() {
		return this.creator;
	}

	public void setPastes(BBPaginator<Paste> value) {
		this.pastes = value;
	}

	public BBPaginator<Paste> getPastes() {
		return this.pastes;
	}

	public void addPaste(Paste p) {

	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Visibility getVisibilityRead() {
		return visibilityRead;
	}

	public void setVisibilityRead(Visibility visibilityRead) {
		this.visibilityRead = visibilityRead;
	}

	public Visibility getVisibilityWrite() {
		return visibilityWrite;
	}

	public void setVisibilityWrite(Visibility visibilityWrite) {
		this.visibilityWrite = visibilityWrite;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.SystemElement#getLink()
	 */
	@Override
	public String getLink() {
		return "/view/clipboard/clipboard.xhtml?faces-redirect=true&"
				+ SystemSettings.CLIPBOARDID + "=" + getId();
	}

	public boolean getNotify() {
		return notify;
	}

	public void setNotify(boolean notify) {
		this.notify = notify;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.sep16g01.rip.businessLogicLayer.pagination.Paginatable#paginate(java.
	 * lang.String, int, int)
	 */
	@Override
	public PaginatedDataModel<Clipboard> paginate(String query, int page,
			int entriesPerPage) {
		try {
			return DAOClipboard.paginate(query, page, entriesPerPage);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return null;
	}

	@Override
	public String toString() {
		return "Clipboard [id=" + this.id + ", visibilityRead="
				+ this.visibilityRead + ", visibilityWrite="
				+ this.visibilityWrite + ", creator="
				+ (this.creator != null ? this.creator.getId() : "")
				+ ", group=" + this.group + ", expirationDate="
				+ this.expirationDate + ", notify=" + this.notify + "]";
	}

}
