package de.sep16g01.rip.businessLogicLayer.clipboard;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.Bookmarkable;
import de.sep16g01.rip.businessLogicLayer.general.DisplayPage;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.invite.BBInvite;
import de.sep16g01.rip.businessLogicLayer.notify.NotifyOnChange;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.businessLogicLayer.util.ParameterUtils;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOBookmark;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for a single clipboard. Required to display the data and content
 * of a clipboard to the user. Holds the object representation of the clipboard
 * that it represents and provides the necessary functions for all the different
 * ways the user can interact with the clipboard: Bookmarking, deleting (if
 * authorized), directing the user to the edit page and creating a new paste in
 * this clipboard (if authorized).
 */
@ViewScoped
@Named
public class BBClipboard extends DisplayPage implements Bookmarkable {
	private final static Logger logger = Logger.getLogger(BBClipboard.class);

	/**
	 * access (read, write) of the current user on this clipboard. This property
	 * is set by the permission filter.
	 */
	private Access access;

	private static final long serialVersionUID = 1L;

	/**
	 * The pastes belonging to this clipboard.
	 */
	private BBPaginator<Paste> pastes;

	/**
	 * The clipboard object being represented by the page.
	 */
	private Clipboard clipboard;

	/**
	 * for the currently logged in user
	 */
	@Inject
	private UserData userData;

	private boolean deleteConfirm = false;

	/**
	 * Directs the user to the invite page where they can invite currently
	 * non-registered people to join the site and the clipboard or already
	 * registered users to this clipboard. Requires write permission on the
	 * clipboard.
	 * 
	 * @return A link to the invite page.
	 */
	public String inviteRead() {
		if (checkRead()) {
			return "/view/permissions/invite.xhtml?faces-redirect=true"
					+ BBInvite.INVITE_ACCESS + "=" + Access.READ.getPriority()
					+ "&" + BBInvite.INVITE_ID + "=" + clipboard.getId() + "&"
					+ BBInvite.INVITE_IDENTIFY + "=" + "clipboard&"
					+ SystemSettings.CLIPBOARDID + "=" + clipboard.getId();
		}

		return null;
	}

	/**
	 * Write equivalent for inviteRead().
	 * 
	 * @return A link to the invite page.
	 */
	public String inviteWrite() {
		if (checkWrite()) {
			return "/view/permissions/invite.xhtml?faces-redirect=true"
					+ BBInvite.INVITE_ACCESS + "=" + Access.WRITE.getPriority()
					+ "&" + BBInvite.INVITE_ID + "=" + clipboard.getId() + "&"
					+ BBInvite.INVITE_IDENTIFY + "=" + "clipboard&"
					+ SystemSettings.CLIPBOARDID + "=" + clipboard.getId();
		}

		return null;
	}

	/**
	 * Deletes this clipboard. Requires write permission on the clipboard.
	 * 
	 * @return
	 * @throws DataSourceException
	 */
	public String delete(boolean letDelete) {
		if (checkWrite()) {
			try {

				if (letDelete) {
					NotifyOnChange.notifyOfClipboardDelete(clipboard);
					DAOClipboard.delete(clipboard.getId());
					logger.info("Deleting " + clipboard + ", User: "
							+ userData.getUser());
					return userData.getUser().getLink() + "faces-redirect=true";
				}
				
				deleteConfirm = true;
				FacesContext
						.getCurrentInstance()
						.addMessage(
								"error",
								new FacesMessage(
										BBChangeLanguage
												.getLangStringStatic("clipboard_delete_confirm")));
				return null;
			} catch (DataSourceException e) {
				logger.error("Exception while deleting " + clipboard
						+ ", User: " + userData.getUser() + "!");
				BBExceptionHandler.handleException(e);
			}
		}
		return null;
	}

	/**
	 * Creates a new paste in this clipboard. Requires write permission on the
	 * clipboard.
	 * 
	 * @return
	 */
	public String newPaste() {

		if (checkWrite()) {
			return "/view/paste/createPaste.xhtml?faces-redirect=true"
					+ SystemSettings.CLIPBOARDID + "=" + clipboard.getId();
		}

		return null;
	}

	/**
	 * Adds this clipboard to a user's bookmarks.
	 * 
	 * @return
	 * 
	 * @throws DataSourceException
	 */
	@Override
	public String bookmark() {
		try {
			if (!isBookmarked()) {
				logger.info("Bookmarking " + clipboard + ", User: "
						+ userData.getUser());
				DAOBookmark.bookmarkClipboard(userData.getUser().getId(),
						clipboard.getId());
			} else {
				logger.info("Unbookmarking " + clipboard + ", User: "
						+ userData.getUser());
				DAOBookmark.unbookmarkClipboard(userData.getUser().getId(),
						clipboard.getId());
			}
		} catch (DataSourceException e) {
			logger.error("Exception while Bookmarking/Unbookmarking "
					+ clipboard + ", User: " + userData.getUser());
			BBExceptionHandler.handleException(e);
		}
		return "/view/clipboard/clipboard.xhtml?faces-redirect=true"
				+ SystemSettings.CLIPBOARDID + "=" + clipboard.getId();
	}

	/**
	 * returns whether the clipboard is bookmarked.
	 * @return
	 */
	public boolean isBookmarked() {
		if (userData.isLoggedIn()) {
			try {
				return DAOBookmark.isClipboardBookmarked(userData.getUser()
						.getId(), clipboard.getId());
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
		}
		return false;
	}

	/**
	 * Directs the user to the edit page of this clipboard.
	 * 
	 * @return A link the edit page of this clipboard.
	 */
	public String edit() {
		if (checkWrite()) {
			return "/view/clipboard/createClipboard.xhtml?faces-redirect=true&"
					+ SystemSettings.CLIPBOARDID + "=" + clipboard.getId();
		}

		return null;
	}

	/**
	 * Sets the {@link #clipboard} object by pulling it from the database.
	 */
	@PostConstruct
	public void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();

		Map<String, String> copy = new HashMap<String, String>();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}

		String id = params.get(SystemSettings.CLIPBOARDID);
		this.access = ParameterUtils.readAccessParameter(params);

		try {
			int clipid = Integer.parseInt(id);
			clipboard = DAOClipboard.get(clipid);

			pastes = new BBPaginator<Paste>(new Paste());
			pastes.setQuery("type=clipboard;clipboardId=" + clipboard.getId());
			pastes.setSystemElementType(SystemElementType.PASTE);
			String pageClipboards = params.get("pageClipboards");
			pastes.setOtherPageParameters(copy);
			pastes.setPageParamName("pageClipboards");
			pastes.gotoPage(pageClipboards);

		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			clipboard = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkRead() {
		return access.getPriority() >= Access.READ.getPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkWrite() {
		return access.getPriority() >= Access.WRITE.getPriority();
	}

	public BBPaginator<Paste> getPastes() {
		return pastes;
	}

	public Clipboard getClipboard() {
		return this.clipboard;
	}

	public boolean isDeleteConfirm() {
		return deleteConfirm;
	}
}
