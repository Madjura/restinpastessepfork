package de.sep16g01.rip.businessLogicLayer.clipboard;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.notify.NotifyOnChange;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.businessLogicLayer.util.ParameterUtils;
import de.sep16g01.rip.businessLogicLayer.validators.TimeSpanValidator;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.permission.DAOPermission;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the creation and edit of a clipboard by a user. Holds the
 * user object of the creator and the chosen name of the user. Whether or not
 * editing or creation of a new clipboard is being performed is determined by
 * the existence of an ID in the url. Provides the necessary functions for all
 * the different ways the user can interact with the clipboard: Giving custom
 * rights and saving, also holds the chosen name of the clipboard.
 */
@ViewScoped
@Named
public class BBCreateClipboard implements Serializable {
	private final static Logger logger = Logger
			.getLogger(BBCreateClipboard.class);

	private static final long serialVersionUID = 1L;

	/**
	 * id of the clipboard being edited, if one is edite.
	 */
	private Integer clipboardId = null;

	/**
	 * Readvisibility of the current clipboard.
	 */
	private Visibility read = Visibility.PUBLIC;

	/**
	 * Writevisibility of the current clipboard.
	 */
	private Visibility write = Visibility.PUBLIC;

	/**
	 * Used to access the user and checking whether or not the user is logged in
	 * or anonymous easily.
	 */
	@Inject
	private UserData userData;

	/**
	 * The clipboard that is being created or edited.
	 */
	private Clipboard clipboard;

	/**
	 * backing property of the "notify user" checkbox.
	 */
	private boolean notify;

	private String group;

	/**
	 * JSF does not support timepickers, parse a timespan-string entered by the
	 * user instead; of format "1y1m1w" etc. see TimeSpanValidator for details.
	 */
	private String expirestring = "7d";

	/**
	 * Saves the clipboard to the database.
	 * 
	 * @return
	 */
	public String save() {

		try {
			// old clipboard
			if (clipboard.getId() != SystemElement.DEFAULT_ID) {
				if (clipboard.getNotify()) {
					NotifyOnChange.checkChangesForClipboard(clipboard);
				}
			}
			clipboard.setVisibilityRead(read);
			clipboard.setVisibilityWrite(write);

			/*
			 * check for min/max expiration date //todo: move to validator?
			 * Would require static method to get loggedin user...
			 */

			String maxexpforuser = DAOSystemSettings
					.loadMaxExpirationDurationPerRole().get(
							userData.getUser().getRole());
			Date now = new Date();
			Date maxExpDate = TimeSpanValidator.parseExpirationDateFromString(
					now, maxexpforuser);
			Date expirationDate = TimeSpanValidator
					.parseExpirationDateFromString(now, expirestring);

			if (expirationDate.after(maxExpDate)) {
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Expiration date too large. Max expiration date: "
										+ maxexpforuser, null));
				return null;
			}

			if (expirationDate.equals(now)) {

				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Expiration date too small or empty.", null));
				return null;
			}

			clipboard.setExpirationDate(expirationDate);

			if (group != null) {
				Group group1 = DAOGroup.getGroupByName(group);
				if (group1 != null) {
					if (DAOPermission.checkGroupUserAccess(group1.getId(),
							userData.getUser().getId()) == Access.WRITE) {
						clipboard.setGroup(group1);
					} else {
						FacesContext
								.getCurrentInstance()
								.addMessage(
										"error",
										new FacesMessage(
												BBChangeLanguage
														.getLangStringStatic("clipboard_group_no_access")));
						return null;
					}
				}
			}

			int id = DAOClipboard.store(clipboard);
			logger.info("Saving " + clipboard + ", User: " + userData.getUser());
			return "clipboard.xhtml?faces-redirect=true&"
					+ SystemSettings.CLIPBOARDID + "=" + id;
		} catch (ParseException e) {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							"error",
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									BBChangeLanguage
											.getLangStringStatic("error_saving_clipboard"),
									null));
			logger.error("Exception while saving " + clipboard + ", User: "
					+ userData.getUser());
			return null;
		} catch (DataSourceException e) {
			logger.error("Exception while saving " + clipboard + ", User: "
					+ userData.getUser());
			BBExceptionHandler.handleException(e);
			return null;
		}
	}

	/**
	 * In edit mode, pulls a {@link #clipboard} from the database for editing.
	 * In create mode a new, empty clipboard is created.
	 * 
	 * If a clipboardid paramter is passed, we are in edit mode.
	 * 
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();

		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		Integer clipboardIdInteger = ParameterUtils.readIntegerParameter(
				params, SystemSettings.CLIPBOARDID);

		try {
			expirestring = DAOSystemSettings.loadMaxExpirationDurationPerRole()
					.get(userData.getUser().getRole());
		} catch (DataSourceException e1) {
			expirestring = "1h";
		}

		if (clipboardIdInteger != null
				&& clipboardIdInteger != SystemElement.DEFAULT_ID) {
			try {
				this.clipboardId = clipboardIdInteger;
				clipboard = DAOClipboard.get(clipboardIdInteger);
				read = clipboard.getVisibilityRead();
				write = clipboard.getVisibilityWrite();
			} catch (NumberFormatException | DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else {
			clipboard = new Clipboard();
			clipboard.setCreator(userData.getUser());
			clipboard.setCreationDate(new Date());
			clipboard.setLastModifiedDate(new Date());
			clipboard.setExpirationDate(new Date());
			clipboard.setTitle("Title...");
			clipboard.setDescription("Description...");
			clipboard.setVisibilityRead(Visibility.PUBLIC);
			clipboard.setVisibilityWrite(Visibility.PUBLIC);
			clipboard.setGroup(new Group());
		}

		if (params.get(SystemSettings.GROUPID) != null) {
			int groupId = Integer.parseInt(params.get(SystemSettings.GROUPID));
			try {
				clipboard.setGroup(DAOGroup.get(groupId));
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		}
	}

	/**
	 * Directs the user to the permission management page.
	 * 
	 * @param access
	 *            The access that is being edited.
	 * @return The link to the permission management page.
	 */
	public String addPermissions(Access access) {
		String url = "/view/permissions/managePermissions.xhtml?faces-redirect=true&"
				+ SystemSettings.CLIPBOARDID
				+ "="
				+ clipboard.getId()
				+ "&"
				+ SystemSettings.ACCESSID + "=" + access.getPriority();
		return url;
	}

	public String getExpirestring() {
		return this.expirestring;
	}

	public void setExpirestring(String expirestring) {
		this.expirestring = expirestring;
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	public boolean getNotify() {
		return notify;
	}

	public void setNotify(boolean notify) {
		this.notify = notify;
	}

	public Visibility getRead() {
		return read;
	}

	public void setRead(Visibility read) {
		this.read = read;
	}

	public Visibility getWrite() {
		return write;
	}

	public void setWrite(Visibility write) {
		this.write = write;
	}

	public Clipboard getClipboard() {
		return clipboard;
	}

	public void setClipboard(Clipboard clipboard) {
		this.clipboard = clipboard;
	}

	public List<Visibility> getVisibilities() {
		return Visibility.getAllVisibilities2();
	}

	public Integer getClipboardId() {
		return clipboardId;
	}

	public void setClipboardId(Integer clipboardId) {
		this.clipboardId = clipboardId;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
}
