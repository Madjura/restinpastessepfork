/**
 * 
 */
package de.sep16g01.rip.businessLogicLayer.invite;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.Config;
import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.email.Email;
import de.sep16g01.rip.businessLogicLayer.email.EmailFactory;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the invite functionality. Holds the object that is being
 * invited to and the user input of who he wants to invite. Responsible to get
 * the email sent.
 */
@ViewScoped
@Named
public class BBInvite implements Serializable {

	public static final String INVITE_IDENTIFY = "invite";
	public static final String INVITE_ID = "id";
	public static final String INVITE_ACCESS = "access";
	private String invite;

	@Inject
	private BBChangeLanguage lang;

	@Inject
	private UserData userData;

	/**
	 * Logger instance for this class.
	 */
	private transient final static Logger logger = Logger
			.getLogger(BBInvite.class);

	private static final long serialVersionUID = 1L;

	/**
	 * The email object that is being sent to the invitee(s).
	 */
	private Email email;

	/**
	 * Initiates the invite process.
	 * 
	 */
	public String invite() {
		if (email == null) {
			FacesContext.getCurrentInstance().addMessage(
					"error",
					new FacesMessage(BBChangeLanguage
							.getLangStringStatic("invalid_invite")));
			return "";
		}

		// append URL to page at the end in case user forgets the add it
		email.setContent(email.getContent().concat(
				" \n\n\n" + Config.getInstance().getBaseURL())
				+ "/view/home/home.xhtml");

		String[] sendTo = invite.split("\\r\n");
		Set<String> recipients = new HashSet<String>();
		for (String recipient : sendTo) {
			if (recipient.contains("@")) {
				recipients.add(recipient);
				try {
					String userName = DAOUser.getUserByEmail(recipient);

					// check is user exists
					User user = DAOUser.getUserByName(userName);
					if (user == null) {
						return "";
					}
				} catch (DataSourceException e) {
					BBExceptionHandler.handleException(e);
					return "";
				}
			} else {
				User user;
				try {
					user = DAOUser.getUserByName(recipient);
					if (user != null) {
						recipients.add(user.getEmail());
						FacesContext
								.getCurrentInstance()
								.addMessage(
										"success",
										new FacesMessage(
												FacesMessage.SEVERITY_INFO,
												recipient
														+ " "
														+ lang.getLangString("has_been_invited"),
												null));
					} else {
						FacesContext
								.getCurrentInstance()
								.addMessage(
										"success",
										new FacesMessage(
												FacesMessage.SEVERITY_INFO,
												recipient
														+ " "
														+ lang.getLangString("not_found"),
												null));
					}
				} catch (DataSourceException e) {
					BBExceptionHandler.handleException(e);
					return "";
				}
			}
		}
		email.send(recipients);

		return "";
	}

	/**
	 * Loads the invite email for the object that is being invited to from
	 * {@link de.sep16g01.rip.businessLogicLayer.email.EmailFactory
	 * EmailFactory}.
	 * 
	 */
	@PostConstruct
	private void loadInviteEmail() {
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String inviteObject = params.get(INVITE_IDENTIFY);

		// no object means system invite
		if (inviteObject == null) {
			this.email = EmailFactory.getSystemInvite(userData.getUser()
					.getName());

			// clipboard invite
		} else if (inviteObject.equals("clipboard")) {

			// load clipboard email
			try {
				this.email = EmailFactory.getClipboardInvite(DAOClipboard
						.get(Integer.parseInt(params.get(INVITE_ID))), Access
						.getById(Integer.parseInt(params.get("access"))));
			} catch (NumberFormatException e) {
				logger.warn("Invalid clipboard ID specified for invite.");
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(lang
								.getLangString("invalid_clipboard_invite")));
			}
			// group invite
			catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else if (inviteObject.equals("group")) {
			try {
				this.email = EmailFactory.getGroupInvite(DAOGroup.get(Integer
						.parseInt(params.get(INVITE_ID))), Access
						.getById(Integer.parseInt(params
								.get(SystemSettings.ACCESSID))));
			} catch (NumberFormatException e) {
				logger.warn("Invalid group ID specified for invite.");
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(lang
								.getLangString("invalid_group_invite")));
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return;
			}
		} else {
			this.email = null;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(BBChangeLanguage
							.getLangStringStatic("invalid_invite")));
		}
	}

	public Email getEmail() {
		return this.email;
	}

	public String getInvite() {
		return invite;
	}

	public void setInvite(String invite) {
		this.invite = invite;
	}
}
