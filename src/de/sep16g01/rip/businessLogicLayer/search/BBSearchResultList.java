package de.sep16g01.rip.businessLogicLayer.search;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the search results. Holds a list of result items so those
 * can be displayed to the user.
 */
@ViewScoped
@Named
public class BBSearchResultList implements Serializable {

	private static final long serialVersionUID = 1L;

	private BBPaginator<User> resultUser;
	private BBPaginator<Paste> resultPaste;
	private BBPaginator<Clipboard> resultClipboard;
	private BBPaginator<Group> resultGroup;

	private final static String CLIPBOARD_PAGINATE = "searchClipboard";
	private final static String GROUP_PAGINATE = "searchGroup";
	private final static String USER_PAGINATE = "searchUser";
	private final static String PASTE_PAGINATE = "searchPaste";

	@Inject
	private UserData userData;

	/**
	 * The index of the page that is currently displayed.
	 */
	private int currentPage;

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	private SystemElementType type;

	private String query;

	@PostConstruct
	private void init() {
		
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		if (query != null && type != null) {
			return;
		}

		User user = userData.getUser();

		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		Map<String, String> copy = new HashMap<String, String>();

		if (query == null) {
			query = params.get(BBSearch.REQUEST_QUERY);
			copy.put(BBSearch.REQUEST_QUERY, query);
		}

		if (type == null) {
			if (params.get(BBSearch.REQUEST_QUERYTYPE) == null) {
				FacesContext.getCurrentInstance().addMessage(
						"error",
						new FacesMessage(BBChangeLanguage
								.getLangStringStatic("something_wrong")));
				return;
			}

			type = SystemElementType.getById(Integer.parseInt(params
					.get(BBSearch.REQUEST_QUERYTYPE)));
			copy.put(BBSearch.REQUEST_QUERYTYPE, String.valueOf(type.getId()));
		}
		String paginateQuery = "userId=" + user.getId()
				+ ";type=content;value=" + query;

		// display clipboards
		if (type == SystemElementType.CLIPBOARD
				|| type == SystemElementType.ALL) {
			int pageClipboard = 1;
			if (params.get(CLIPBOARD_PAGINATE) != null) {
				pageClipboard = Integer
						.parseInt(params.get(CLIPBOARD_PAGINATE));
			}
			resultClipboard = new BBPaginator<Clipboard>(new Clipboard());
			resultClipboard.setQuery(paginateQuery);
			resultClipboard.setPageParamName(CLIPBOARD_PAGINATE);
			resultClipboard.setOtherPageParameters(copy);
			resultClipboard.gotoPage(pageClipboard);

		}

		// display groups
		if (type == SystemElementType.GROUP || type == SystemElementType.ALL) {
			int pageGroup = 1;
			if (params.get(GROUP_PAGINATE) != null) {
				pageGroup = Integer.parseInt(params.get(GROUP_PAGINATE));
			}
			resultGroup = new BBPaginator<Group>(new Group());
			resultGroup.setQuery(paginateQuery);
			resultGroup.setPageParamName(GROUP_PAGINATE);
			resultGroup.setOtherPageParameters(copy);
			resultGroup.gotoPage(pageGroup);

		}

		// display users
		if (type == SystemElementType.USER || type == SystemElementType.ALL) {
			int pageUser = 1;
			if (params.get(USER_PAGINATE) != null) {
				pageUser = Integer.parseInt(params.get(USER_PAGINATE));
			}
			resultUser = new BBPaginator<User>(new User());
			resultUser.setQuery(paginateQuery);
			resultUser.setPageParamName(USER_PAGINATE);
			resultUser.setOtherPageParameters(copy);
			resultUser.gotoPage(pageUser);

		}

		// display pastes
		if (type == SystemElementType.PASTE || type == SystemElementType.ALL) {
			int pagePaste = 1;
			if (params.get(PASTE_PAGINATE) != null) {
				pagePaste = Integer.parseInt(params.get(PASTE_PAGINATE));
			}
			resultPaste = new BBPaginator<Paste>(new Paste());
			resultPaste.setQuery(paginateQuery);
			resultPaste.setPageParamName(PASTE_PAGINATE);
			resultPaste.setOtherPageParameters(copy);
			resultPaste.gotoPage(pagePaste);

		}
	}

	public BBPaginator<User> getResultUser() {
		return resultUser;
	}

	public BBPaginator<Paste> getResultPaste() {
		return resultPaste;
	}

	public BBPaginator<Group> getResultGroup() {
		return resultGroup;
	}

	public BBPaginator<Clipboard> getResultClipboard() {
		return resultClipboard;
	}

	public SystemElementType getType() {
		return type;
	}

	public void setType(SystemElementType type) {
		this.type = type;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
