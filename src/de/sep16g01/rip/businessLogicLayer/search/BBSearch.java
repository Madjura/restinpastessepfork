package de.sep16g01.rip.businessLogicLayer.search;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * Backing bean for the search. Holds the user's selected type of query (i.e. he
 * searches for users or groups) and performs the query.
 */
@RequestScoped
@Named
public class BBSearch implements Serializable {

	private static final long serialVersionUID = 1L;

	public final static String REQUEST_QUERY = "query";
	public final static String REQUEST_QUERYTYPE = "type";

	/**
	 * The type of element that is being searched for.
	 */
	private SystemElementType systemElementSearch = null;

	/**
	 * The query of the user.
	 */
	private String query;

	public void setQuery(String value) {
		this.query = value;
	}

	public String getQuery() {
		return this.query;
	}

	/**
	 * Performs the search and returns a list of the result items.
	 * 
	 * @return A list of the result items.
	 * @throws DataSourceException
	 */
	public String search() {
		return "/view/search/searchResultList.xhtml?faces-redirect=true"
				+ BBSearch.REQUEST_QUERYTYPE + "="
				+ systemElementSearch.getId() + "&" + BBSearch.REQUEST_QUERY
				+ "=" + query;
	}

	public SystemElementType getSystemElementSearch() {
		return systemElementSearch;
	}

	public void setSystemElementSearch(SystemElementType systemElementSearch) {
		this.systemElementSearch = systemElementSearch;
	}

	/**
	 * @return all the different types of system elements.
	 */
	public List<SystemElementType> getAll() {
		return SystemElementType.dropDownValues();
	}
}
