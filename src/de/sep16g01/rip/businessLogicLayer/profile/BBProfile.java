package de.sep16g01.rip.businessLogicLayer.profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.DisplayPage;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the user's profile. Holds the user's stored information and
 * used to display the data to the user.
 */
@ViewScoped
@Named
public class BBProfile extends DisplayPage {
	private static final long serialVersionUID = 1L;

	@Inject
	private UserData userData;


	private boolean deleteConfirm = false;

	/**
	 * The user this profile belongs to.
	 */
	private User user;

	private Role createNewDropDownElement;

	private boolean onOwnProfile = false;

	/**
	 * link for the administrator to change a user's roles
	 * 
	 * @return the page of the user
	 */
	/**
	 * Changes the user roll to the role chosen by the admin.
	 * 
	 * @return the profile link to reload the page.
	 */
	public String changeUserRole() {
		int userId = user.getId();
		try {
			if (createNewDropDownElement == Role.ADMINISTRATOR) {
				DAOUser.changeUserRole(userId, Role.ADMINISTRATOR.getPriority());
			} else if (createNewDropDownElement == Role.MODERATOR) {
				DAOUser.changeUserRole(userId, Role.MODERATOR.getPriority());
			} else if (createNewDropDownElement == Role.REGISTERED) {
				DAOUser.changeUserRole(userId, Role.REGISTERED.getPriority());
			}
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return "/view/profile/profile.xhtml?faces-redirect=true"
				+ SystemSettings.USERID + "=" + user.getId();
	}

	/**
	 * {@inheritDoc}
	 * inherited method that is not needed for user-profiles
	 */
	@Override
	public boolean checkRead() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 * inherited method that is not needed for user-profiles
	 */
	@Override
	public boolean checkWrite() {
		return false;
	}

	/*
	 * returns a link to the editProfile.xhtml
	 */
	public String edit() {
		return "/view/profile/editProfile.xhtml?faces-redirect=true"
				+ SystemSettings.USERID + "=" + user.getId();
	}

	public Role getCreateNewDropDownElement() {
		return createNewDropDownElement;
	}

	public User getUser() {
		return user;
	}

	public UserData getUserData() {
		return userData;
	}
	
	/**
	 * Method to find out which roles the user has.
	 * @return
	 */
	public List<Role> getUserRoleTypes() {
		List<Role> list = new ArrayList<Role>();
		if (user.getRole() != Role.ADMINISTRATOR) {
			list.add(Role.ADMINISTRATOR);
		}
		if (user.getRole() != Role.MODERATOR) {
			list.add(Role.MODERATOR);
		}
		if (user.getRole() != Role.REGISTERED) {
			list.add(Role.REGISTERED);
		}
		return list;
	}

	/**
	 * Load the user from database.
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();

		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();

		int userid = userData.getUser().getId();
		if (params.get(SystemSettings.USERID) != null) {
			userid = Integer.parseInt(params.get(SystemSettings.USERID));
		}
		try {
			user = DAOUser.getById(userid);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return;
		}
		int currentuser = userData.getUser().getId();
		if (currentuser == userid) {
			onOwnProfile = true;
		}
	}

	public boolean isAdmin() {
		return userData.getUser().getRole() == Role.ADMINISTRATOR;
	}

	public boolean isOnOwnProfile() {
		return onOwnProfile;
	}

	/**
	 * Saves the changes if a user's role was changed.
	 */
	public void saveChanges() {
		try {
			DAOUser.store(user);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
	}

	public void setCreateNewDropDownElement(Role createNewDropDownElement) {
		this.createNewDropDownElement = createNewDropDownElement;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String uploadProfilePicture() {
		return "/data?id=" + user.getId() + "&amp;type=profilepicture";
	}

}
