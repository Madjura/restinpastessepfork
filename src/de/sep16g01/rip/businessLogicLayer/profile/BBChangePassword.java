package de.sep16g01.rip.businessLogicLayer.profile;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;
//import java.util.ResourceBundle;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.businessLogicLayer.util.HashString;
import de.sep16g01.rip.dataAccessLayer.email.DAOEmailToken;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the password change in the profile. User types in the new
 * password and a repeat and the two are checked for equality before being
 * accepted as new password and stored in the database.
 */
@ViewScoped
@Named
public class BBChangePassword implements Serializable {
	private final transient static Logger logger = Logger.getLogger(BBChangePassword.class);

	private static final long serialVersionUID = 1L;

	@Inject
	private UserData userData;

	@Inject
	private BBChangeLanguage lang;

	private boolean validToken;

	private String oldPassword = null;

	/**
	 * The password that is was entered.
	 */
	private String password = null;

	/**
	 * The repeat of the password.
	 */
	private String passwordRepeat;

	private String user;

	private UUID token;
	private String type;
	
	/**
	 * Method to check if the tokens fit when the user resets his password via
	 * email.
	 */
	private void checkToken() {
		if (type == null || user == null || token == null) {
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			String error = lang.getLangString("password_reset_invalid");
			message.setSummary(error);
			message.setDetail(error);
			FacesContext.getCurrentInstance().
			addMessage("confirmForm:confirmText", message);
			validToken = false;
			return;
		}
		try {
			validToken = DAOEmailToken.checkPasswordResetToken(token, user);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return;
		}
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public String getPassword() {
		return this.password;
	}

	public String getPasswordRepeat() {
		return this.passwordRepeat;
	}

	public UserData getUserData() {
		return userData;
	}

	/**
	 * Find out which user wants to change his password (important for 
	 * changing password via email)
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap();
		if (params.get(SystemSettings.USERID) != null) {
			user = params.get(SystemSettings.USERID);
		}
		if (params.get(SystemSettings.TOKEN) != null) {
			try {
				token = UUID.fromString(params.get(SystemSettings.TOKEN));
			} catch (IllegalArgumentException e) {
				return;
			}
		}
		if (params.get(SystemSettings.TYPE) != null) {
			type = params.get(SystemSettings.TYPE);
		}

	}

	public boolean isValidToken() {
		return validToken;
	}

	/**
	 * Saves the password and stores the new password to the database when user
	 * resets passwort via email.
	 * 
	 * @return The user will be leaded to the Home-Page of RIP when he submits
	 */
	public String save() {
		if (!userData.isLoggedIn()) {
			try {
				checkToken();

				if (validToken) {
					User user1 = DAOUser.getUserByName(user);
					byte[] newPassword = HashString.hashPassword(password, 
							user1.getPasswordSalt());
					user1.setPasswordHash(newPassword);
					DAOUser.store(user1);
					logger.info(user1 
							+ " has successfully changed their password.");
				} else {
					if (userData.isLoggedIn()) {
						logger.info(userData.getUser() 
								+ " tried to change their password!");
						FacesContext.getCurrentInstance().addMessage("error", 
								new FacesMessage(
										FacesMessage.SEVERITY_ERROR, 
										lang.getLangString
										("followed_link_invalid"), null));
					} else {
						// if the user changes the token after accessing the
						// page with the right token.
						return "/view/general/error404.xhtml";
					}
				}
			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				return "";
			}
		}
		return "/view/home/home";
	}

	/**
	 * Saves the password and stores the new password to the database.
	 * 
	 */
	public void saveVoid() {
		// if oldPassword, password and passWordRepeat are empty, do nothing
		// if oldPassword is empty, but the rest is not empty
		if (oldPassword.length() == 0 && (passwordRepeat.length() > 0 ||
				password.length() > 0)) {
			FacesContext.getCurrentInstance().addMessage("success",
					new FacesMessage(FacesMessage.SEVERITY_INFO, 
							lang.getLangString("missing_old_password"), null));
			logger.info(userData.getUser() 
					+ " tried to change their password! Missing old password");
		// if user entered an oldPassword
		} else if (oldPassword.length() > 0) {
			//if user enters the wrong old password
			if (!validPassword(oldPassword, userData.getUser())) {
				logger.info(userData.getUser() 
						+ " tried to change their password! "
						+ "Wrong password entered");
				FacesContext.getCurrentInstance().addMessage("success", 
						new FacesMessage(FacesMessage.SEVERITY_INFO,
						lang.getLangString("wrong_password_entered"), null));
			//if user enters right old password and matching new passwords
			} else if (password.length() >0 && password.equals(passwordRepeat)){
				User user1 = userData.getUser();
				byte[] salt = HashString.getNextSalt();
				user1.setPasswordSalt(salt);
				byte[] passwordHash = 
						HashString.hash(this.password.toCharArray(), salt);
				user1.setPasswordHash(passwordHash);
				try {
					user1.store();
				} catch (DataSourceException e) {
					BBExceptionHandler.handleException(e);
					return;
				}
				logger.info(user1 + " has successfully changed their password.");
				FacesContext.getCurrentInstance().addMessage("success", 
						new FacesMessage(FacesMessage.SEVERITY_INFO,
						lang.getLangString("successful_password_change"), null));
				// if changing password was not successfull due to pw mismatch
			} else if (password.length() >0 || passwordRepeat.length() > 0){
				FacesContext.getCurrentInstance().addMessage("success", 
						new FacesMessage(FacesMessage.SEVERITY_INFO,
						lang.getLangString("pw_mismatch"), null));
				// if user didn't enter any new passwords.
			} else if (password.length() ==0 && passwordRepeat.length() == 0){
				FacesContext.getCurrentInstance().addMessage("success", 
						new FacesMessage(FacesMessage.SEVERITY_INFO,
						lang.getLangString("missing_new_password"), null));
			}
		}

	}

	public void setOldPassword(String oldpassword) {
		this.oldPassword = oldpassword;
	}

	public void setPassword(String value) {
		this.password = value;
	}
	
	public void setPasswordRepeat(String value) {
		this.passwordRepeat = value;
	}

	/**
	 * @param userData
	 */
	public void setUserData(UserData userData) {
		this.userData = userData;

	}

	public void setValidToken(boolean validToken) {
		this.validToken = validToken;
	}

	/**
	 * Method to check whether the user entered the right old password.
	 * 
	 * @param password1 the user's old password	
	 * @param user1 user whose password will be checked	
	 * @return true, if the password is valid
	 */
	private boolean validPassword(String password1, User user1) {
		byte[] userPasswordHash = user1.getPasswordHash();
		byte[] userPasswordSalt = user1.getPasswordSalt();

		// compare equality of hash we have with password input + hash from user
		if (Arrays.equals(userPasswordHash, HashString.hashPassword(password1, 
				userPasswordSalt))) {
			return true;
		}
		return false;
	}
}
