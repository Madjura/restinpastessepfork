package de.sep16g01.rip.businessLogicLayer.profile;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.catalina.core.ApplicationPart;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
//import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.keyword.DAOKeyword;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing Bean for the profile edit. Lets a user edit their profile and their
 * account data, with the exception of their username.
 *
 */
@ViewScoped
@Named
public class BBEditProfile implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String REQUEST_GROUP = "id";

	private String keywords;

	/**
	 * The user whose data is being displayed.
	 */
	@Inject
	private UserData userData;

	/**
	 * Inject the current language setting.
	 */
	@Inject
	private BBChangeLanguage lang;

	private ApplicationPart profilePicture;

	/**
	 * Leads the user to the link where he can perform the deletion of his 
	 * account.
	 * 
	 * @return the link to the confirmation-page
	 */
	public String deleteUser() {
		return "/view/general/confirmation.xhtml?faces-redirect=true"
				+ SystemSettings.USERID + "=" + userData.getUser().getId();
	}

	public String getKeywords() {
		return keywords;
	}
	

	public void setKeywords(String keywords) {
		this.keywords = keywords; 
	}
	

	public ApplicationPart getProfilePicture() {
		return profilePicture;
	}

	public String getProfilePictureLink() {
		return "/data?id=" + userData.getUser().getId()
				+ "&amp;type=profilepicture";
	}

	public User getUser() {
		return userData.getUser();
	}

	public UserData getUserData() {
		return userData;
	}

	/**
	 * Loads the user and their data from the profile and stores it in
	 * {@link #user}.
	 * 
	 * @throws DataSourceException
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		loadKeywords();

	}

	/**
	 * Loads the keywords of the user that will be displayed on the page where 
	 * the user can edit his profile data. 
	 * 
	 */
	private void loadKeywords() {
		List<String> keywords1 = null;
		try {
			keywords1 = DAOKeyword.loadKeywordsForUser(userData.getUser()
					.getId());
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
			return;
		}
		StringBuilder sb = new StringBuilder();

		for (String word : keywords1) {
			sb.append(word + "\n");
		}
		this.keywords = sb.toString();
	}

	/**
	 * Saves the changes done to the profile to the database, then directs the
	 * user to the profile page.
	 */
	public void save() {
		User userToStore = getUser();
		if (profilePicture != null) {
			try {
				userToStore.setProfilePicture(new File(profilePicture));
				userToStore.store();
				FacesContext.getCurrentInstance().addMessage(
						"success",
						new FacesMessage(FacesMessage.SEVERITY_INFO, lang
								.getLangString("profile_picture_saved"), null));
			} catch (IOException | DataSourceException e) {
				BBExceptionHandler.handleException(e);
			}
		}

	}

	public void setProfilePicture(ApplicationPart profilePicture) {
		this.profilePicture = profilePicture;
	}

	public void setUser(User user) {
		this.userData.setUser(user);
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}
	

	/**
	 * Stores the keywords that the user set. The user later will always be 
	 * informed if his/her keywords appear in Pastes. 
	 */
	public void storeKeywords() {
		String[] split = keywords.split("\\W+");
		Set<String> keywordSet = new HashSet<String>(Arrays.asList(split));
		try {
			DAOKeyword.storeKeywordsMultiple(keywordSet, userData.getUser()
					.getId());
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
	}
}
