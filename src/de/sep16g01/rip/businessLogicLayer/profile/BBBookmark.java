package de.sep16g01.rip.businessLogicLayer.profile;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.general.SystemElementType;
import de.sep16g01.rip.businessLogicLayer.pagination.BBPaginator;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the bookmarks of a user. Holds a list of all the user's
 * bookmarked elements and can generate a link that the user can give to other
 * people so they can see the contents of the bookmark.
 */
@ViewScoped
@Named
public class BBBookmark implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	UserData userData;

	/**
	 * The bookmarked elements of a user.
	 */
	private BBPaginator<Paste> pastes;
	private BBPaginator<Clipboard> clipboards;

	/**
	 * Loads the bookmarks of the user from the database and stores them in
	 * {@link #bookmarks}.
	 */
	@PostConstruct
	private void init() {
		BBChangeLanguage.currentURL = SystemSettings.getCurrentURL();
		Map<String, String> params = FacesContext.getCurrentInstance()
				.getExternalContext().getRequestParameterMap();
		String id = params.get(SystemSettings.BOOKMARKID);

		//fix for T16200: show user bookmarks when no id is specified
		int bookmarkId = userData.getUser().getId();
		if(id != null)
			bookmarkId = Integer.parseInt(id);
		
		pastes = new BBPaginator<Paste>(new BookmarkPastes());
		pastes.setQuery("type=paste;ownerId=" + bookmarkId + ";userId="
				+ userData.getUser().getId());
		pastes.setSystemElementType(SystemElementType.PASTE);
		String pagePastes = params.get("pagePastes");
		pastes.setPageParamName("pagePastes");
		pastes.gotoPage(pagePastes);

		clipboards = new BBPaginator<Clipboard>(new BookmarkClipboards());
		clipboards.setQuery("type=clipboard;ownerId=" + bookmarkId + ";userId="
				+ userData.getUser().getId());
		clipboards.setSystemElementType(SystemElementType.CLIPBOARD);
		String pageClipboards = params.get("pageClipboards");
		clipboards.setPageParamName("pageClipboards");
		clipboards.gotoPage(pageClipboards);

		pastes.putOtherPageParameter("pageClipboards", pageClipboards);
		clipboards.putOtherPageParameter("pagePastes", pagePastes);

	}

	public BBPaginator<SystemElement> getPaginator() {
		return null;
	}

	/**
	 * Creates and stores a bookmark link for a user.
	 * 
	 * @return A link to the bookmarks.
	 */
	public String generateBookmarkLink() {
		// TODO
		return "/view/profile/bookmarks.xhtml?faces-redirect=true" + "&"
				+ SystemSettings.BOOKMARKID + "=" + userData.getUser().getId();
	}

	public BBPaginator<Paste> getPastes() {
		return pastes;
	}

	public void setPastes(BBPaginator<Paste> pastes) {
		this.pastes = pastes;
	}

	public BBPaginator<Clipboard> getClipboards() {
		return clipboards;
	}

	public void setClipboards(BBPaginator<Clipboard> clipboards) {
		this.clipboards = clipboards;
	}
}
