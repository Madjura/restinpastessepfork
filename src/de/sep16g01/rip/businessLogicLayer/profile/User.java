package de.sep16g01.rip.businessLogicLayer.profile;

import java.util.Date;

import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.pagination.Paginatable;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.dataAccessLayer.email.DAOEmailToken;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

/**
 * DTO for the user objects. Holds the user's data. A user is someone using the
 * system, they have a role and have a certain free data capacity.
 */
public class User extends SystemElement implements Paginatable<User> {

	private static final long serialVersionUID = 1L;

	/**
	 * The name of the user.
	 */
	private String name;

	/**
	 * The role of the user.
	 */
	private Role role;

	/**
	 * The hash of the user's password, including salt.
	 */
	private byte[] passwordHash;

	/**
	 * The salt belonging to the user.
	 */
	private byte[] passwordSalt;

	/**
	 * The email of the user.
	 */
	private String email;

	/**
	 * The profile picture of the user.
	 */
	private File profilePicture = new File();

	/**
	 * The registration date of the user.
	 */
	private Date registrationDate;

	/**
	 * The free capacity the user still has in the system.
	 */
	private long freeCapacity;

	/**
	 * Constructor for the user.
	 * 
	 * @param name
	 *            The name of the user.
	 * @param hash
	 *            The hashed password of the user.
	 */
	public User(String name, byte[] hash, byte[] salt) {
		this();
		this.setTitle("User");
		this.name = name;
		this.passwordHash = hash;
		this.passwordSalt = salt;
	}

	/**
	 * default constructor
	 */
	public User() {
	}

	public void setFreeCapacity(long value) {
		this.freeCapacity = value;
	}

	public long getFreeCapacity() {
		return this.freeCapacity;
	}

	public void setRole(Role value) {
		this.role = value;
	}

	public Role getRole() {
		return this.role;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getName() {
		return this.name;
	}

	public void setEmail(String value) {
		this.email = value;
	}

	public String getEmail() {
		return this.email;
	}

	public void setPasswordHash(byte[] value) {
		this.passwordHash = value;
	}

	public byte[] getPasswordHash() {
		return this.passwordHash;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public File getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(File profilePicture) {
		this.profilePicture = profilePicture;
	}

	public byte[] getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(byte[] passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	public static boolean isNameUnique(String name) throws DataSourceException {
		return DAOUser.getUserByName(name) == null;
	}

	public static boolean isEmailUnique(String email)
			throws DataSourceException {
		return DAOUser.getUserByEmail(email) == null;
	}

	public static User getUserByName(String name) throws DataSourceException {
		return DAOUser.getUserByName(name);

	}

	public void store() throws DataSourceException {
		DAOUser.store(this);
	}

	public void dropUser() throws DataSourceException {
		DAOUser.dropUser(this.getId());
	}

	public static boolean hasRegistrationToken(String name)
			throws DataSourceException {
		return DAOEmailToken.hasRegistrationToken(name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.sep16g01.rip.businessLogicLayer.general.SystemElement#getLink()
	 */
	@Override
	public String getLink() {
		return "/view/profile/profile.xhtml?userid=" + this.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.sep16g01.rip.businessLogicLayer.pagination.Paginatable#paginate(java.
	 * lang.String, int, int)
	 */
	@Override
	public PaginatedDataModel<User> paginate(String query, int page,
			int entriesPerPage) {
		try {
			return DAOUser.paginate(query, page, entriesPerPage);
		} catch (DataSourceException e) {
			BBExceptionHandler.handleException(e);
		}
		return null;
	}

	@Override
	public String toString() {
		return "User [id=" + this.id + ", name=" + this.name + ", role="
				+ this.role + ", email=" + this.email + ", registrationDate="
				+ this.registrationDate + ", freeCapacity=" + this.freeCapacity
				+ "]";
	}

}
