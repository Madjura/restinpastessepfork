package de.sep16g01.rip.businessLogicLayer.profile;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.email.Email;
import de.sep16g01.rip.businessLogicLayer.email.EmailFactory;
import de.sep16g01.rip.businessLogicLayer.general.BBExceptionHandler;
import de.sep16g01.rip.businessLogicLayer.session.UserData;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Backing bean for the email change in the profile. User types in the new email
 * twice and equality is ensured before being stored to the database.
 */
@ViewScoped
@Named
public class BBEditEmail implements Serializable {
	private final transient static Logger logger = Logger.getLogger(BBEditEmail.class); // TODO
																						// logging

	private static final long serialVersionUID = 1L;

	@Inject
	private UserData userData;

	@Inject
	private BBChangeLanguage lang;

	// private User user2 = userData.getUser();

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	/**
	 * Email the user entered.
	 */
	private String email = null;

	/**
	 * Repeat of the email the user entered, to check equality and prevent
	 * typos.
	 */
	private String emailRepeat = null;

	public void setEmail(String value) {
		this.email = value;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmailRepeat(String value) {
		this.emailRepeat = value;
	}

	public String getEmailRepeat() {
		return this.emailRepeat;
	}

	/**
	 * Saves the user's new email.
	 * 
	 * @return A link to the edit profile page.
	 * @throws DataSourceException
	 */
	public void save() {
		User user = userData.getUser();

		if (email.length() > 0 && emailRepeat.length() > 0 
				&& email.equals(emailRepeat)) {
			try {
				if (DAOUser.getUserByEmail(this.email) != null) {
					FacesContext.getCurrentInstance().addMessage("success",
							new FacesMessage(FacesMessage.SEVERITY_INFO, 
									lang.getLangString("email_taken"), null));
					//return "";
				}
				user.setEmail(email);
				user.store();
				FacesContext.getCurrentInstance().addMessage(
						"success",
						new FacesMessage(FacesMessage.SEVERITY_INFO, 
								lang.getLangString("email_change_successful"), null));
				Email email1 = EmailFactory.getEmailChangeConfirmationEmail(this.email, user.getName());
				email1.setRecipient(this.email);
				email1.send(null);
				//return "";

			} catch (DataSourceException e) {
				BBExceptionHandler.handleException(e);
				//return "";
			}
			// to avoid checking case that both are empty, insert condition
		} else if (email.length() > 0 || emailRepeat.length() > 0) {
			FacesContext.getCurrentInstance().addMessage("success",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, lang.getLangString("email_mismatch"), null));
		}
		//return "";
	}

	// /**
	// * Saves the user's new email.
	// *
	// * @return A link to the edit profile page.
	// * @throws DataSourceException
	// */
	// public void saveVoid() {
	// User user = userData.getUser();
	//
	// if (this.email.equals(emailRepeat) && email.length() > 0
	// && emailRepeat.length() > 0) {
	//
	// try {
	// if (DAOUser.getUserByEmail(this.email) != null) {
	// FacesContext.getCurrentInstance()
	// .addMessage(
	// "error",
	// new FacesMessage(lang
	// .getLangString("email_taken")));
	// }
	// } catch (DataSourceException e) {
	// BBExceptionHandler.handleException(e);
	// }
	// user.setEmail(this.email);
	// Email email1 = EmailFactory.getEmailChangeConfirmationEmail(user);
	// email1.setRecipient(this.email);
	// email1.send(null);
	//
	// }
	// }

	public BBChangeLanguage getLang() {
		return lang;
	}

	public void setLang(BBChangeLanguage lang) {
		this.lang = lang;
	}
}
