package de.sep16g01.rip.businessLogicLayer.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

@WebServlet("/data/*")
public class FileProviderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// size of byte buffer to send file
	private static final int BUFFER_SIZE = 4096;

	private final static Logger logger = Logger.getLogger(FileProviderServlet.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// get upload id from URL's parameters
		String type = request.getParameter("type");
		int uploadId = -1;
		if (request.getParameter("id") != null) {
			uploadId = Integer.parseInt(request.getParameter("id"));
		}

		if ("profilepicture".equals(type)) {
			byte[] profilePictureById;
			try {
				profilePictureById = DAOUser.getProfilePictureById(uploadId);
				if (profilePictureById == null) {
					return;
				}
			} catch (DataSourceException e) {
				logger.error("Error while accessing profilePicture", e);
				// throw new IOException(e);
				return;
			}
			// gets file name and file blob data
			String fileName = "profilePicture" + uploadId + ".jpg"; // TODO add
																	// filename
																	// to DB
			ByteArrayInputStream is = new ByteArrayInputStream(profilePictureById);
			int fileLength = profilePictureById.length;

			logger.log(Level.DEBUG, "Accessing profilePicture of userId=" + uploadId);

			ServletContext context = getServletContext();

			// sets MIME type for the file download
			String mimeType = context.getMimeType(fileName);
			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}

			// set content properties and header attributes for the response
			response.setContentType(mimeType);
			response.setContentLength(fileLength);
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileName);
			response.setHeader(headerKey, headerValue);

			// writes the file to the client
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;

			while ((bytesRead = is.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			is.close();
			outStream.close();
		} else if ("systemlogo".equals(type)) {
			byte[] systemLogo;
			try {
				SystemSettings systemSettings = DAOSystemSettings.loadSystemSettings();
				if (systemSettings.getLogo() == null) {
					return;
				}
				systemLogo = systemSettings.getLogo().getContent();
			} catch (DataSourceException e) {
				logger.error("Error while accessing systemLogo", e);
				// throw new IOException(e);
				return;
			}
			// gets file name and file blob data
			String fileName = "system.jpg"; // TODO add filename to DB
			ByteArrayInputStream is = new ByteArrayInputStream(systemLogo);
			int fileLength = systemLogo.length;

			ServletContext context = getServletContext();

			// sets MIME type for the file download
			String mimeType = context.getMimeType(fileName);
			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}

			// set content properties and header attributes for the response
			response.setContentType(mimeType);
			response.setContentLength(fileLength);
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileName);
			response.setHeader(headerKey, headerValue);

			// writes the file to the client
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;

			while ((bytesRead = is.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			is.close();
			outStream.close();
		} else if ("pastefile".equals(type)) {
			File file;
			try {
				file = DAOPaste.getFile(uploadId);
			} catch (DataSourceException e) {
				logger.error("Error while accessing systemLogo", e);
				// throw new IOException(e);
				return;
			}
			// gets file name and file blob data
			String fileName = file.getFilename();
			ByteArrayInputStream is = new ByteArrayInputStream(file.getContent());
			int fileLength = file.getContent().length;

			ServletContext context = getServletContext();

			// sets MIME type for the file download
			String mimeType = context.getMimeType(fileName);
			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}

			// set content properties and header attributes for the response
			response.setContentType(mimeType);
			response.setContentLength(fileLength);
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileName);
			response.setHeader(headerKey, headerValue);

			// writes the file to the client
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;

			while ((bytesRead = is.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			is.close();
			outStream.close();
		}

	}
}
