package de.sep16g01.rip.lang;

//import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Cookie;

/**
 * Sessionscoped bean responsible for holding the localization of the user. Once
 * set, the localization is stored in a usercookie. On initialization the
 * language is set from the following sources in this order: 1. cookie 2.
 * browser set locale 3. English
 *
 */
@SessionScoped
@Named
public class BBChangeLanguage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8605384436726610720L;

	/**
	 * Constant name of the cookie used by rip to store locale settings.
	 */
	private final static String cookieName = "RIPLOCALE";

	/**
	 * Currently set Locale for Language setting. If an unsupported Locale is
	 * set, the system defaults to English.
	 */
	
	public static String currentURL = "";
	
	public String getCurrentURL() {
		return currentURL;
	}
	
	
	private Locale locale;

	public Locale getLocale() {
		return locale;
	}

	@PostConstruct
	public void init() {
		locale = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale();
		setLangFromCookie();
	}

	/**
	 * Changes the language to German
	 */
	public void setLocaleToGerman() {
		applyLocale(Locale.GERMAN);

	}


	
	
	
	/**
	 * Changes language to English
	 */
	public void setLocaleToEnglish() {
		applyLocale(Locale.ENGLISH);
	}
	

	/**
	 * Stores currently used locale to cookie.
	 */
	private void addLangCookie() {

		String value = locale.getLanguage();
		Map<String, Object> properties = new HashMap<>();
		properties.put("maxAge", 31536000);
		properties.put("path", "/");
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		externalContext.addResponseCookie(cookieName, value, properties);
	}

	/**
	 * Sets the locale from cookie, if possible.
	 * 
	 * @return true if a cookie was found, false otherwise
	 */
	private boolean setLangFromCookie() {

		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		Cookie localecookie = (Cookie) externalContext.getRequestCookieMap()
				.get(cookieName);
		if (localecookie != null) {
			String lang = localecookie.getValue();
			locale = Locale.forLanguageTag(lang);
			return true;
		}
		return false;
	}

	/**
	 * Changes the localization of a User.
	 * 
	 * @param loc
	 *            localization to apply
	 */
	private void applyLocale(Locale loc) {
		locale = loc;
		addLangCookie();
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
	}

	public static String getLangStringFromLocale(String identifier,
			Locale locale) {
		if (locale == Locale.GERMAN) {
			return ResourceBundle.getBundle("de.sep16g01.rip.lang.lang_de")
					.getString(identifier);
		} else if (locale == Locale.ENGLISH) {
			return ResourceBundle.getBundle("de.sep16g01.rip.lang.lang_en")
					.getString(identifier);
		} else {
			return ResourceBundle.getBundle("de.sep16g01.rip.lang.lang_en")
					.getString(identifier);
		}
	}

	// public String getLangString(String identifier) {
	// return getLangStringFromLocale(identifier, locale);
	// }

	public String getLangString(String identifier) {
		return getLangStringFromLocale(identifier, locale);
	}

	public static String getLangStringStatic(String identifier) {
		try {
			Locale loc = FacesContext.getCurrentInstance().getViewRoot()
					.getLocale();
			return getLangStringFromLocale(identifier, loc);
		} catch (NullPointerException e) {
			return getLangStringFromLocale(identifier, Locale.ENGLISH);
		}
	}
}
