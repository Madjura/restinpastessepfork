package de.sep16g01.rip.dataAccessLayer.comments;

import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.sep16g01.rip.businessLogicLayer.comments.Comment;
import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.util.QueryParser;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

/**
 * DAO for the comments. Handles storing and loading comments to and from the
 * database.
 */
public class DAOComment {

	/**
	 * Stores a comment to the database.
	 * 
	 * @param c
	 *            The comment.
	 * @param e
	 *            The element the comment belongs to.
	 * @throws DataSourceException
	 *             When the database is down.
	 */
	public static void store(Comment c, String type)
			throws DataSourceException {

		// insert the comment into the list of comments
		String query = "INSERT INTO TComments (content, creationDate,"
				+ " creatorId ) VALUES (?, ?, ?)";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper psInsert = new PreparedStatementWrapper(
						cw.prepareStatement(query,
								Statement.RETURN_GENERATED_KEYS));) {
			psInsert.setTimestamp(2,
					new Timestamp(c.getCreationDate().getTime()));
			psInsert.setInt(3, c.getCreator().getId());
			if (c.getContent() != null) {
				psInsert.setString(1, c.getContent());
			}
			psInsert.executeUpdate();
			try (ResultSetWrapper rs = psInsert.getGeneratedKeys();) {
				if (rs.next()) {
					c.setId(rs.getInt(1));
				}
			}
		}

		// insert the comment into the list where the relation between comments
		// and Users are saved if comment belongs to a user
		if (type.equals("User")) {
			String queryUser = "INSERT INTO RUserComments (userProfileId, "
					+ "commentId) VALUES(?, ? )";
			try (ConnectionWrapper cw = new ConnectionWrapper(
					DBPool.getInstance().getConnection());
					PreparedStatementWrapper psInsert2 = new PreparedStatementWrapper(
							cw.prepareStatement(queryUser));) {
				psInsert2.setInt(1, c.getBelongsTo().getId());
				psInsert2.setInt(2, c.getId());
				psInsert2.executeUpdate();
			}
		}

		// insert the comment into the list where the relation between comments
		// and Pastes are saved if comment belongs to a paste
		else if (type.equals("Paste")) {
			String queryPaste = "INSERT INTO RPasteComments (pasteId, "
					+ "commentId) VALUES(?, ? )";
			try (ConnectionWrapper cw = new ConnectionWrapper(
					DBPool.getInstance().getConnection());
					PreparedStatementWrapper psInsert3 = new PreparedStatementWrapper(
							cw.prepareStatement(queryPaste));) {
				psInsert3.setInt(1, c.getBelongsTo().getId());
				psInsert3.setInt(2, c.getId());
				psInsert3.executeUpdate();
			}
		}

		// insert the comment into the list where the relation between comments
		// and clipboards are saved if comment belongs to a clipboard
		else if (type.equals("Clipboard")) {
			String queryClipboard = "INSERT INTO RClipboardComments (clipboardId, "
					+ "commentId)" + " VALUES(?, ? )";
			try (ConnectionWrapper cw = new ConnectionWrapper(
					DBPool.getInstance().getConnection());
					PreparedStatementWrapper psInsert4 = new PreparedStatementWrapper(
							cw.prepareStatement(queryClipboard));) {
				psInsert4.setInt(1, c.getBelongsTo().getId());
				psInsert4.setInt(2, c.getId());
				psInsert4.executeUpdate();
			}
		}

		// insert the comment into the list where the relation between comments
		// and groups are saved if comment belongs to a group
		else if (type.equals("Group")) {
			String queryGroup = "INSERT INTO RGroupComments (groupId, commentId)"
					+ " VALUES(?, ? )";
			try (ConnectionWrapper cw = new ConnectionWrapper(
					DBPool.getInstance().getConnection());
					PreparedStatementWrapper psInsert5 = new PreparedStatementWrapper(
							cw.prepareStatement(queryGroup));) {
				psInsert5.setInt(1, c.getBelongsTo().getId());
				psInsert5.setInt(2, c.getId());
				psInsert5.executeUpdate();
			}
		}
	}

	/**
	 * Returns a comment from the database based on ID.
	 * 
	 * @param id
	 *            The ID of the comment.
	 * @throws DataSourceException
	 *             When the database is down.
	 */
	public static Comment get(int id) throws DataSourceException {
		String query = "SELECT id, content, creationDate, creatorId FROM "
				+ "TComments WHERE id = ?;";

		// try with resources to avoid leaking connection
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, id);
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {

				// fill user object
				while (result.next()) {
					Comment com = new Comment();
					com.setId(result.getInt(1));
					com.setContent(result.getString(2));
					com.setCreationDate(result.getDate(3));
					int creatorid = result.getInt(4);
					com.setCreator(DAOUser.getById(creatorid));
					return com;
				}
			}
		}
		return null;
	}

	/**
	 * Method to remove comment from database
	 * 
	 * @param id
	 *            the id of the comment
	 * @throws DataSourceException
	 *             when the Database is down
	 */
	/**
	 * Deletes a comment from the database.
	 * 
	 * @param id
	 *            comment to be deleted.
	 * @throws DataSourceException
	 *             on error
	 */
	public static void delete(int id) throws DataSourceException {
		String query = "DELETE FROM TComments WHERE id = ?;";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, id);
			ps.executeUpdate();
		}
	}

	/**
	 * Method that is useful for paginating the comments.
	 * 
	 * @param query
	 *            The query whose output should be paginated
	 * @param page
	 *            The page of the entry that will be shown
	 * @param entriesPerPage
	 *            The amount of comments on one page
	 * @return a PaginatedDataModel of Comments
	 * @throws DataSourceException
	 *             when the database is down.
	 */
	public static PaginatedDataModel<Comment> paginate(String query, int page,
			int entriesPerPage) throws DataSourceException {
		SystemElement e = null;
		String type = "";
		Map<String, String> queryValues = QueryParser
				.parsePaginationQuery(query);
		if (queryValues.containsKey("userId")) {
			e = DAOUser.getById(Integer.parseInt(queryValues.get("userId")));
			type = "User";
		} else if (queryValues.containsKey("groupId")) {
			e = DAOGroup.get(Integer.parseInt(queryValues.get("groupId")));
			type = "Group";
		} else if (queryValues.containsKey("pasteId")) {
			e = DAOPaste.getById(Integer.parseInt(queryValues.get("pasteId")));
			type = "Paste";
		} else if (queryValues.containsKey("clipboardId")) {
			e = DAOClipboard
					.get(Integer.parseInt(queryValues.get("clipboardId")));
			type = "Clipboard";
		}

		List<Comment> comments = new ArrayList<Comment>();
		int totalCount = -1;
		if (type.equals("User")) {
			// load all the comments by Id that a User has
			String queryUser = "SELECT commentId, count(*) OVER() AS full_count "
					+ "FROM RUserComments WHERE userProfileId = ? LIMIT ? OFFSET ?";
			try (ConnectionWrapper cw = new ConnectionWrapper(
					DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(
							cw.prepareStatement(queryUser));) {
				ps.setInt(1, e.getId());
				ps.setInt(2, entriesPerPage);
				ps.setInt(3, page * entriesPerPage - entriesPerPage);
				try (ResultSetWrapper result = new ResultSetWrapper(
						ps.executeQuery())) {

					// fill user object
					while (result.next()) {
						int commentId = result.getInt(1);
						Comment comment = get(commentId);
						comments.add(comment);
						totalCount = result.getInt(2);
					}
				}
			}
		} else if (type.equals("Group")) {
			String queryUser = "SELECT commentId, count (*) OVER() AS full_count "
					+ "FROM RGroupComments WHERE groupId = ? LIMIT ? OFFSET ?";
			try (ConnectionWrapper cw = new ConnectionWrapper(
					DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps2 = new PreparedStatementWrapper(
							cw.prepareStatement(queryUser));) {
				ps2.setInt(1, e.getId());
				ps2.setInt(2, entriesPerPage);
				ps2.setInt(3, page * entriesPerPage - entriesPerPage);
				try (ResultSetWrapper result = new ResultSetWrapper(
						ps2.executeQuery())) {

					// fill user object
					while (result.next()) {
						int commentId = result.getInt(1);
						Comment comment = get(commentId);
						comments.add(comment);
						totalCount = result.getInt(2);
					}
				}
			}
		} else if (type.equals("Paste")) {
			String queryUser = "SELECT commentId, count(*) OVER() AS full_count "
					+ "FROM RPasteComments WHERE pasteId = ? LIMIT ? OFFSET ?";
			try (ConnectionWrapper cw = new ConnectionWrapper(
					DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps3 = new PreparedStatementWrapper(
							cw.prepareStatement(queryUser));) {
				ps3.setInt(1, e.getId());
				ps3.setInt(2, entriesPerPage);
				ps3.setInt(3, page * entriesPerPage - entriesPerPage);
				try (ResultSetWrapper result = new ResultSetWrapper(
						ps3.executeQuery())) {

					// fill user object
					while (result.next()) {
						int commentId = result.getInt(1);
						Comment comment = get(commentId);
						comments.add(comment);
						totalCount = result.getInt(2);
					}
				}
			}
		} else if (type.equals("Clipboard")) {
			String queryUser = "SELECT commentId, count(*) OVER() AS full_count "
					+ "FROM RClipboardComments WHERE clipboardId = ? LIMIT ? OFFSET ?";
			try (ConnectionWrapper cw = new ConnectionWrapper(
					DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps4 = new PreparedStatementWrapper(
							cw.prepareStatement(queryUser));) {
				ps4.setInt(1, e.getId());
				ps4.setInt(2, entriesPerPage);
				ps4.setInt(3, page * entriesPerPage - entriesPerPage);
				try (ResultSetWrapper result = new ResultSetWrapper(
						ps4.executeQuery())) {

					// fill user object
					while (result.next()) {
						int commentId = result.getInt(1);
						Comment comment = get(commentId);
						comments.add(comment);
						totalCount = result.getInt(2);
					}
				}
			}
		}
		return new PaginatedDataModel<Comment>(page, totalCount, comments);
	}
}
