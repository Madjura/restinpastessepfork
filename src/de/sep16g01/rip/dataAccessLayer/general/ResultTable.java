/**
 *   File:     de.sep16g01.rip.dataAccessLayer.general.ResultTable.java
 *   Created:  09.06.2016, 17:05:25 by Albert
 */
package de.sep16g01.rip.dataAccessLayer.general;

import java.util.List;

public class ResultTable {
	private List<String> columnNames;
	private List<List<Object>> data;

	public ResultTable(List<String> columnNames, List<List<Object>> data) {
		this.columnNames = columnNames;
		this.data = data;
	}

	private int getIndexColumnName(String name) {
		for (int i = 0; i < columnNames.size(); i++) {
			if (columnNames.get(i).equals(name)) {
				return i;
			}
		}

		return -1;
	}

	public <T> T getDataFromFirstRow(String columnName, Class<T> type) {
		// columnName = columnName.toUpperCase();
		return getDataFromSpecificRow(columnName, 0, type);
	}

	public <T> T getDataFromSpecificRow(String columnName, int rowIndex,
			Class<T> type) {
		// columnName = columnName.toUpperCase();
		columnName = columnName.toLowerCase();
		if (data.isEmpty()) {
			return null;
		}
		List<Object> row;
		try {
			row = data.get(rowIndex);
		} catch (ArrayIndexOutOfBoundsException e1) {
			e1.printStackTrace();
			return null;
		}
		if (row.isEmpty()) {
			return null;
		}
		try {
			int index = getIndexColumnName(columnName);
			if (index != -1) {
				if (row.get(index) == null)
					return null;
				return type.cast(row.get(index));
			}
			throw new RuntimeException("Columnname is not found in result!");
		} catch (ClassCastException e) {
			e.printStackTrace();
			return null;
		}
	}

	public int size() {
		return data.size();
	}
}
