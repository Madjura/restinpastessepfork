package de.sep16g01.rip.dataAccessLayer.general;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

public class PreparedStatementWrapper implements AutoCloseable {
	private PreparedStatement ps;

	public PreparedStatementWrapper(PreparedStatement ps) {
		this.ps = ps;
	}

	public ResultSet executeQuery() throws DataSourceException {
		try {
			return ps.executeQuery();
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public int executeUpdate() throws DataSourceException {
		try {
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}
	
	public void setInt(int parameterIndex, int x) throws DataSourceException {
		try {
			ps.setInt(parameterIndex, x);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public void setString(int parameterIndex, String x) throws DataSourceException {
		try {
			ps.setString(parameterIndex, x);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		};
	}

	@Override
	public void close() throws DataSourceException {
		try {
			ps.close();
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public void setLong(int parameterIndex, long x) throws DataSourceException {
		try {
			ps.setLong(parameterIndex, x);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public void setBytes(int parameterIndex, byte[] x) throws DataSourceException {
		try {
			ps.setBytes(parameterIndex, x);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
		
	}

	public void setTimestamp(int parameterIndex, Timestamp x) throws DataSourceException {
		try {
			ps.setTimestamp(parameterIndex, x);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
		
	}

	public void setBoolean(int parameterIndex, boolean x) throws DataSourceException {
		try {
			ps.setBoolean(parameterIndex, x);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public ResultSetWrapper getGeneratedKeys() throws DataSourceException {
		try {
			return new ResultSetWrapper(ps.getGeneratedKeys());
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public void setNull(int parameterIndex, int sqlType) throws DataSourceException {
		try {
			ps.setNull(parameterIndex, sqlType);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public void setDate(int parameterIndex, Date x) throws DataSourceException {
		try {
			ps.setDate(parameterIndex, x);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}		
	}
}
