package de.sep16g01.rip.dataAccessLayer.general;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

public class ResultSetWrapper implements AutoCloseable {
	private ResultSet rs;

	public ResultSetWrapper(ResultSet rs) {
		this.rs = rs;
	}

	@Override
	public void close() throws DataSourceException {
		try {
			rs.close();
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public boolean getBoolean(int columnIndex) throws DataSourceException {
		try {
			return rs.getBoolean(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public boolean getBoolean(String columnLabel) throws DataSourceException {
		try {
			return rs.getBoolean(columnLabel);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public byte getByte(int columnIndex) throws DataSourceException {
		try {
			return rs.getByte(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public byte[] getBytes(int columnIndex) throws DataSourceException {
		try {
			return rs.getBytes(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public Date getDate(int columnIndex) throws DataSourceException {
		try {
			return rs.getDate(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public long getLong(int columnIndex) throws DataSourceException {
		try {
			return rs.getLong(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public int getInt(int columnIndex) throws DataSourceException {
		try {
			return rs.getInt(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public Object getObject(int columnIndex) throws DataSourceException {
		try {
			return rs.getObject(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public String getString(int columnIndex) throws DataSourceException {
		try {
			return rs.getString(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public Time getTime(int columnIndex) throws DataSourceException {
		try {
			return rs.getTime(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public Time getTime(int columnIndex, Calendar cal) throws DataSourceException {
		try {
			return rs.getTime(columnIndex, cal);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public Timestamp getTimestamp(int columnIndex) throws DataSourceException {
		try {
			return rs.getTimestamp(columnIndex);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public boolean next() throws DataSourceException {
		try {
			return rs.next();
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}
}
