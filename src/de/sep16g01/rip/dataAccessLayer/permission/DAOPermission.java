package de.sep16g01.rip.dataAccessLayer.permission;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultTable;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.dataAccessLayer.util.DBUtils;
import de.sep16g01.rip.dataAccessLayer.util.QueryStringUtil;

/**
 * DAO for the permissions. Handles storing and loading permission to the
 * database.
 */
public class DAOPermission {
	private final transient static Logger logger = Logger.getLogger(DAOPermission.class);

	/**
	 * Checks whether or not a user has access to a clipboard.
	 * 
	 * @param clipboardId
	 *            The id of the clipboard.
	 * @param userId
	 *            The id of the user.
	 * @return The access object representing the user's access, or None.
	 * @throws DataSourceException
	 */
	public static Access checkClipboardUserAccess(int clipboardId, int userId) throws DataSourceException {
		String queryClipboard = "SELECT creatorId, visibilityRead, "
				+ "visibilityWrite, groupId FROM TClipboards WHERE id = ?";
		ResultTable resultClipboard = DBUtils.executeQuery(queryClipboard, new Object[] { clipboardId });
		int creatorId = 0;
		int visibilityRead = 0;
		int visibilityWrite = 0;
		Integer groupId = 0;
		if (resultClipboard.size() == 1) {
			creatorId = resultClipboard.getDataFromFirstRow("creatorid", Integer.class);
			visibilityRead = resultClipboard.getDataFromFirstRow("visibilityread", Integer.class);
			visibilityWrite = resultClipboard.getDataFromFirstRow("visibilitywrite", Integer.class);
			groupId = resultClipboard.getDataFromFirstRow("groupid", Integer.class);
		}
		// user is creator
		if (userId != 0 && userId == creatorId) {
			return Access.WRITE;
		}
		//

		// visibility is public
		// OR visibility is registered and user is registered
		if (visibilityWrite == Visibility.PUBLIC.getId()
				|| (visibilityWrite == Visibility.REGISTERED.getId() && userId != 0)) {
			return Access.WRITE;
		}
		//

		// user is admin
		String queryUserIsAdmin = "SELECT userrole FROM TUsers " + "WHERE id = ?";

		ResultTable resultUserIsAdmin = DBUtils.executeQuery(queryUserIsAdmin, new Object[] { userId });
		if (resultUserIsAdmin.size() > 0) {
			Integer roleId = resultUserIsAdmin.getDataFromFirstRow("userrole", Integer.class);
			if (roleId != null && Role.getById(roleId) == Role.ADMINISTRATOR) {
				return Access.WRITE;
			}
		}

		// clipboard is in group and user is member of group
		if (groupId != null && groupId != 0) {
			if (DAOGroup.checkMembership(userId, groupId)) {
				return Access.WRITE;
			}
		}
		//

		// user is in RUserAccessClipboard relation
		String queryUserAccessRelation = "SELECT accessId FROM " + "RUserAccessClipboard "
				+ "WHERE userId = ? AND clipboardId = ?";

		ResultTable resultUserAccessRelation = DBUtils.executeQuery(queryUserAccessRelation,
				new Object[] { userId, clipboardId });
		Integer accessId = resultUserAccessRelation.getDataFromFirstRow("accessid", Integer.class);
		if (accessId != null && accessId != 0) {
			return Access.getById(accessId);
		}
		//

		// visibility is public
		// OR visibility is registered and user is registered
		if (visibilityRead == Visibility.PUBLIC.getId()
				|| (visibilityRead == Visibility.REGISTERED.getId() && userId != 0)) {
			return Access.READ;
		}
		//

		return null;
	}

	/**
	 * Checks whether or not a user has access to a group.
	 * 
	 * @param groupId
	 *            The id of the group.
	 * @param userId
	 *            The id of the user.
	 * @return The access object representing the user's access, or None.
	 * @throws DataSourceException
	 */
	public static Access checkGroupUserAccess(int groupId, int userId) throws DataSourceException {
		String queryGroup = "SELECT visibilityRead, visibilityWrite FROM " + "TGroups WHERE id = ?";
		ResultTable resultGroup = DBUtils.executeQuery(queryGroup, new Object[] { groupId });
		int visibilityRead = 0;
		int visibilityWrite = 0;
		visibilityRead = resultGroup.getDataFromFirstRow("visibilityread", Integer.class);
		visibilityWrite = resultGroup.getDataFromFirstRow("visibilitywrite", Integer.class);

		// visibility is public
		// OR visibility is registered and user is registered
		if (visibilityWrite == Visibility.PUBLIC.getId()
				|| (visibilityWrite == Visibility.REGISTERED.getId() && userId != 0)) {
			return Access.WRITE;
		}
		//

		// user is admin
		String queryUserIsAdmin = "SELECT userrole FROM TUsers " + "WHERE id = ?";
		ResultTable resultUserIsAdmin = DBUtils.executeQuery(queryUserIsAdmin, new Object[] { userId });
		if (resultUserIsAdmin.size() > 0) {
			Integer roleId = resultUserIsAdmin.getDataFromFirstRow("userrole", Integer.class);
			if (roleId != null && Role.getById(roleId) == Role.ADMINISTRATOR) {
				return Access.WRITE;
			}
		}
		//

		// user is member of the group
		if (DAOGroup.checkMembership(userId, groupId)) {
			return Access.WRITE;
		}
		//

		// user is in RUserAccessGroup relation
		String queryUserAccessRelation = "SELECT accessId FROM RUserAccessGroup " + "WHERE userId = ? AND groupId = ?";

		ResultTable resultUserAccessRelation = DBUtils.executeQuery(queryUserAccessRelation,
				new Object[] { userId, groupId });
		Integer accessId = resultUserAccessRelation.getDataFromFirstRow("accessid", Integer.class);
		if (accessId != null && accessId != 0) {
			return Access.getById(accessId);
		}
		//

		// visibility is public
		// OR visibility is registered and user is registered
		if (visibilityRead == Visibility.PUBLIC.getId()
				|| (visibilityRead == Visibility.REGISTERED.getId() && userId != 0)) {
			return Access.READ;
		}
		//
		return null;
	}

	/**
	 * Checks whether or not a user has access to a paste.
	 * 
	 * @param pasteId
	 *            The id of the paste.
	 * @param userId
	 *            The id of the user.
	 * @return The access object representing the user's access, or None.
	 * @throws DataSourceException
	 */
	public static Access checkPasteUserAccess(int pasteId, int userId) throws DataSourceException {
		int clipboardId = DAOClipboard.getClipboardIdFromPasteId(pasteId);

		// check clipboard access (equals access to paste)
		return checkClipboardUserAccess(clipboardId, userId);
	}

	/**
	 * Returns a list of users with a certain access to a SystemElement.
	 * 
	 * @param permissible
	 *            The element.
	 * @param access
	 *            The access that is being checked.
	 * @return A list of users with the access to the element.
	 * @throws DataSourceException
	 */
	public static List<String> getClipboardUsersWithAccess(int clipboardId, Access access) throws DataSourceException {
		String query = "SELECT TUsers.name FROM TUsers, RUserAccessClipboard "
				+ "WHERE RUserAccessClipboard.clipboardId = ? AND RUserAccessClipboard.accessId = ? AND RUserAccessClipboard.userId = TUsers.id ";
		List<String> userNames = new ArrayList<String>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, clipboardId);
			ps.setInt(2, access.getPriority());
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					userNames.add(rs.getString(1));
				}
			}
		}
		return userNames;
	}

	/**
	 * Returns a list of users with a certain access to a SystemElement.
	 * 
	 * @param permissible
	 *            The element.
	 * @param access
	 *            The access that is being checked.
	 * @return A list of users with the access to the element.
	 * @throws DataSourceException
	 */
	public static List<String> getGroupUsersWithAccess(int groupId, Access access) throws DataSourceException {
		String query = "SELECT TUsers.name FROM RUserAccessGroup, TUsers "
				+ "WHERE RUserAccessGroup.groupId = ? AND RUserAccessGroup.accessId = ? "
				+ " AND RUserAccessGroup.userId = TUsers.id ";
		List<String> userNames = new ArrayList<String>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, groupId);
			ps.setInt(2, access.getPriority());
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					userNames.add(rs.getString(1));
				}
			}
		}
		return userNames;
	}

	/**
	 * Sets users access to a clipboard.
	 * 
	 * @param clipboardId
	 *            The id of the clipboard.
	 * @param users
	 *            The users being given access.
	 * @param access
	 *            The access that is being given.
	 * @throws DataSourceException
	 */
	public static void assignClipboardUsersAccess(int clipboardId, List<String> users, Access access)
			throws DataSourceException {

		// String queryCheck = "SELECT name, id FROM TUsers " + " RIGHT JOIN
		// "
		// + " (SELECT userId FROM RUserAccessClipboard WHERE userid IN "
		// + QueryStringUtil.createValueListString(users.size())
		// + " AND clipboardId = ? ) AS FOO "
		// + " ON TUsers.id = FOO.userId ";
		String queryCheck = "SELECT RUserAccessClipboard.userId, TUsers.name" + " FROM TUsers, RUserAccessClipboard"
				+ " WHERE RUserAccessClipboard.clipboardId = ?" + " AND RUserAccessClipboard.userId = TUsers.id";

		List<Integer> usersToRemove = new ArrayList<Integer>();
		List<Integer> usersToUpdate = new ArrayList<Integer>();
		List<String> usersToUpdateNames = new ArrayList<String>();
		List<String> usersToInsert = new ArrayList<String>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(queryCheck));) {
			ps.setInt(1, clipboardId);
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					int userId = rs.getInt(1);
					String username = rs.getString(2);
					if (users.contains(username)) {
						usersToUpdate.add(userId);
						usersToUpdateNames.add(username);
					} else {
						usersToRemove.add(userId);
					}
				}
				for (String name : users) {
					if (!usersToUpdateNames.contains(name)) {
						usersToInsert.add(name);
					}
				}
			}
		}

		if (usersToUpdate.size() > 0) {
			String updateQuery = "UPDATE RUserAccessClipboard SET accessId = ? " + "WHERE userId IN "
					+ QueryStringUtil.createValueListString(usersToUpdate.size());

			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(updateQuery));) {
				ps.setInt(1, access.getPriority());
				for (int i = 1; i <= usersToUpdate.size(); i++) {
					ps.setInt(i + 1, usersToUpdate.get(i - 1));
				}
				ps.executeUpdate();
			}
		}

		if (usersToInsert.size() > 0) {
			String insertQuery = "INSERT INTO RUserAccessClipboard (userId, " + "accessId, clipboardId) VALUES "
					+ QueryStringUtil.createValueListString(3, usersToInsert.size());

			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(insertQuery));) {
				for (int i = 0; i < usersToInsert.size(); i++) {
					String username = usersToInsert.get(i);
					ps.setInt(i * 3 + 1, DAOUser.getUserByName(username).getId());
					ps.setInt(i * 3 + 2, access.getPriority());
					ps.setInt(i * 3 + 3, clipboardId);
				}
				ps.executeUpdate();
			}
		}

		if (usersToRemove.size() > 0) {
			String deleteQuery = "DELETE" + " FROM RUserAccessClipboard" + " WHERE userId = ?" + " AND clipboardId = ?";
			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(deleteQuery));) {
				for (int i = 0; i < usersToRemove.size(); i++) {
					int userId = usersToRemove.get(i);
					ps.setInt(1, userId);
					ps.setInt(2, clipboardId);
				}
				ps.executeUpdate();
			} 
		}
	}

	/**
	 * Sets users access to a group.
	 * 
	 * @param groupId
	 *            The id of the group.
	 * @param users
	 *            The users being given access.
	 * @param access
	 *            The access being given.
	 * @throws DataSourceException
	 */
	public static void assignGroupUsersAccess(int groupId, List<String> users, Access access)
			throws DataSourceException {

		// String queryCheck = "SELECT title FROM TGroups " + " RIGHT JOIN "
		// + " (SELECT groupId FROM RUserAccessGroup WHERE userid IN "
		// + QueryStringUtil.createValueListString(users.size())
		// + " AND groupId = ?) AS FOO " + " ON TGroups.id = FOO.groupId";

		String queryCheck = "SELECT RUserAccessGroup.userId, TUsers.name" + " FROM TUsers, RUserAccessGroup"
				+ " WHERE RUserAccessGroup.groupId = ?" + " AND RUserAccessGroup.userId = TUsers.id";

		List<Integer> usersToRemove = new ArrayList<Integer>();
		List<Integer> usersToUpdate = new ArrayList<Integer>();
		List<String> usersToUpdateNames = new ArrayList<String>();
		List<String> usersToInsert = new ArrayList<String>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(queryCheck));) {
			ps.setInt(1, groupId);

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					int userId = rs.getInt(1);
					String username = rs.getString(2);
					if (users.contains(username)) {
						usersToUpdate.add(userId);
						usersToUpdateNames.add(username);
					} else {
						usersToRemove.add(userId);
					}
				}
			}
			for (String name : users) {
				if (!usersToUpdateNames.contains(name)) {
					usersToInsert.add(name);
				}
			}

		}
		if (usersToUpdate.size() > 0) {
			String updateQuery = "UPDATE RUserAccessGroup SET accessId = ? WHERE " + "userId IN "
					+ QueryStringUtil.createValueListString(usersToUpdate.size());

			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(updateQuery));) {
				ps.setInt(1, access.getPriority());
				for (int i = 1; i <= usersToUpdate.size(); i++) {
					ps.setInt(i + 1, usersToUpdate.get(i - 1));
				}
				ps.executeUpdate(); // TODO not call this if usersToAdd is empty
			}
		}

		if (usersToInsert.size() > 0) {
			String insertQuery = "INSERT INTO RUserAccessGroup (userId, " + "accessId, groupId) VALUES "
					+ QueryStringUtil.createValueListString(3, usersToInsert.size());

			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(insertQuery));) {
				for (int i = 0; i < usersToInsert.size(); i++) {
					String username = usersToInsert.get(i);
					ps.setInt(i * 3 + 1, DAOUser.getUserByName(username).getId());
					ps.setInt(i * 3 + 2, access.getPriority());
					ps.setInt(i * 3 + 3, groupId);
				}
				ps.executeUpdate();
			} 
		}

		if (usersToRemove.size() > 0) {
			String deleteQuery = "DELETE" + " FROM RUserAccessGroup" + " WHERE userId = ?" + " AND groupId = ?";
			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(deleteQuery));) {
				for (int i = 0; i < usersToRemove.size(); i++) {
					int userId = usersToRemove.get(i);
					ps.setInt(1, userId);
					ps.setInt(2, groupId);
				}
				ps.executeUpdate();
			} 
		}

	}

	/**
	 * Sets groups access to a clipboard.
	 * 
	 * @param clipboardId
	 *            The id of the clipboard.
	 * @param groups
	 *            The groups.
	 * @param access
	 *            The access.
	 * @throws DataSourceException
	 */
	public static void assignClipboardGroupsAccess(int clipboardId, List<String> groups, Access access)
			throws DataSourceException {
		String queryCheck = "SELECT TGroups.id, TGroups.title" + " FROM TGroups, RGroupAccessClipboard"
				+ " WHERE clipboardId = ?" + " AND RGroupAccessClipboard.groupId = TGroups.id";

		List<Integer> groupsToRemove = new ArrayList<Integer>();
		List<Integer> groupsToUpdate = new ArrayList<Integer>();
		List<String> groupsToUpdateNames = new ArrayList<String>();
		List<String> groupsToInsert = new ArrayList<String>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(queryCheck));) {
			ps.setInt(1, clipboardId);

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					int groupId = rs.getInt(1);
					String groupTitle = rs.getString(2);
					if (groups.contains(groupTitle)) {
						groupsToUpdate.add(groupId);
						groupsToUpdateNames.add(groupTitle);
					} else {
						groupsToRemove.add(groupId);
					}
				}
			}
			for (String name : groups) {
				if (!groupsToUpdateNames.contains(name)) {
					groupsToInsert.add(name);
				}
			}

		}
		if (groupsToUpdate.size() > 0) {
			String updateQuery = "UPDATE RGroupAccessClipboard SET accessId = ? " + "WHERE groupId IN "
					+ QueryStringUtil.createValueListString(groupsToUpdate.size());
			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(updateQuery));) {
				ps.setInt(1, access.getPriority());
				for (int i = 1; i <= groupsToUpdate.size(); i++) {
					ps.setInt(i + 1, groupsToUpdate.get(i - 1));
				}
				ps.executeUpdate();
			}
		}

		if (groupsToInsert.size() > 0) {
			String insertQuery = "INSERT INTO RGroupAccessClipboard (groupId, " + "accessId, clipboardId) VALUES "
					+ QueryStringUtil.createValueListString(3, groupsToInsert.size());

			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(insertQuery));) {
				for (int i = 0; i < groupsToInsert.size(); i++) {
					String groupname = groupsToInsert.get(i);
					ps.setInt(i * 3 + 1, DAOGroup.getGroupByName(groupname).getId());
					ps.setInt(i * 3 + 2, access.getPriority());
					ps.setInt(i * 3 + 3, clipboardId);
				}
				ps.executeUpdate();
			}
		}

		if (groupsToRemove.size() > 0) {
			String deleteQuery = "DELETE" + " FROM RGroupAccessClipboard" + " WHERE groupId = ?"
					+ " AND clipboardId = ?";

			try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
					PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(deleteQuery));) {
				for (int i = 0; i < groupsToRemove.size(); i++) {
					int groupId = groupsToRemove.get(i);
					ps.setInt(1, groupId);
					ps.setInt(2, clipboardId);
				}
				ps.executeUpdate();
			} 
		}
	}

	/**
	 * 
	 * @param clipboardId
	 *            The clipboard id.
	 * @param access
	 *            The access.
	 * @return A list of group names.
	 * @throws DataSourceException
	 */
	public static List<String> getClipboardGroupsWithAccess(int clipboardId, Access access) throws DataSourceException {
		String query = "SELECT TGroups.title FROM TGroups, RGroupAccessClipboard "
				+ "WHERE RGroupAccessClipboard.clipboardId = ?" + " AND RGroupAccessClipboard.accessId = ?"
				+ " AND RGroupAccessClipboard.groupId = TGroups.id ";
		List<String> groupNames = new ArrayList<String>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, clipboardId);
			ps.setInt(2, access.getPriority());

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					groupNames.add(rs.getString(1));
				}
			}
		}
		return groupNames;
	}
}
