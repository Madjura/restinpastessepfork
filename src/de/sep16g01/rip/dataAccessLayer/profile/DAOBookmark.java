package de.sep16g01.rip.dataAccessLayer.profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.businessLogicLayer.util.QueryParser;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;

/**
 * DAO for bookmarks.
 */
public class DAOBookmark {

	/**
	 * Bookmarks a paste for the given user.
	 * 
	 * @param userId the user who bookmarks the paste
	 * @param pasteId the paste to be bookmarked
	 * @throws DataSourceException
	 */
	public static void bookmarkPaste(int userId, int pasteId) throws DataSourceException {
		String query = "INSERT INTO RUserBookmarkPaste (userId, pasteId) VALUES (?, ?)";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, pasteId);
			ps.executeUpdate();
		}
	}

	/**
	 * Bookmarks a clipboard for the given user.
	 * 
	 * @param userId the user who bookmarks the clipboard
	 * @param clipboardId the clipboard to be bookmarked
	 * @throws DataSourceException
	 */
	public static void bookmarkClipboard(int userId, int clipboardId) throws DataSourceException {
		String query = "INSERT INTO RUserBookmarkClipboard (userId, clipboardId) VALUES (?, ?)";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, clipboardId);
			ps.executeUpdate();
		}
	}

	/**
	 * Removes a bookmarked clipboard for a given user.
	 * 
	 * @param userId the user to remove the bookmark from
	 * @param clipboardId the bookmarked clipboard to be removed
	 * @throws DataSourceException
	 */
	public static void unbookmarkClipboard(int userId, int clipboardId) throws DataSourceException {
		String query = "DELETE FROM RUserBookmarkClipboard WHERE userId = ? AND clipboardId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, clipboardId);
			ps.executeUpdate();
		}
	}

	/**
	 * Removes a bookmarked paste for a given user.
	 * 
	 * @param userId the user to remove the bookmark from
	 * @param pasteId the bookmarked paste to be removed
	 * @throws DataSourceException
	 */
	public static void unbookmarkPaste(int userId, int pasteId) throws DataSourceException {
		String query = "DELETE FROM RUserBookmarkPaste WHERE userId = ? AND pasteId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, pasteId);
			ps.executeUpdate();
		}
	}

	/**
	 * Checks whether a given paste is already bookmarked for a user.
	 * 
	 * @param userId the user
	 * @param pasteId the paste
	 * @return true if userId has already bookmarked paste pasteId, false otherwise
	 * @throws DataSourceException
	 */
	public static boolean isPasteBookmarked(int userId, int pasteId) throws DataSourceException {
		String query = "SELECT userId FROM RUserBookmarkPaste WHERE userId = ? AND pasteId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, pasteId);
			try (ResultSetWrapper result = new ResultSetWrapper(ps.executeQuery())) {
				if (result.next()) {
					return true;
				}
			}

		}
		return false;
	}

	/**
	 * Checks whether a given clipboard is already bookmarked for a user.
	 * 
	 * @param userId the user
	 * @param clipboardId the clipboard
	 * @return true if userId has already bookmarked paste clipboard clipboardId, false otherwise
	 * @throws DataSourceException
	 */
	public static boolean isClipboardBookmarked(int userId, int clipboardId) throws DataSourceException {
		String query = "SELECT userId FROM RUserBookmarkClipboard WHERE userId = ? AND clipboardId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, clipboardId);
			try (ResultSetWrapper result = new ResultSetWrapper(ps.executeQuery())) {
				if (result.next()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * pagination helper.
	 * @param query
	 * @param page
	 * @param entriesPerPage
	 * @return
	 * @throws DataSourceException
	 */
	public static PaginatedDataModel<Clipboard> paginateClipboards(String query, int page, int entriesPerPage)
			throws DataSourceException {

		String sql = "";
		Map<String, String> queryValues = QueryParser.parsePaginationQuery(query);
		sql = "SELECT id, title, creatorId, creationDate, lastModificationDate,"
				+ "expirationDate, description , visibilityRead, " + "visibilityWrite,"
				+ "groupId, notify, count(*) OVER() AS full_count FROM TClipboards WHERE id IN "
				+ "(SELECT clipboardId FROM RUserBookmarkClipboard WHERE userId = ?) AND (TClipboards.visibilityRead != 4 OR TClipboards.groupId IN "
				+ "(SELECT groupId FROM RUserGroup WHERE userId = ?) OR TClipboards.creatorId = ?) "
				+ "AND (TClipboards.visibilityRead != 3 OR ? IN (SELECT id FROM TUsers WHERE id = ?))"
				+ " AND (TClipboards.visibilityRead != 1 OR ? IN (SELECT userId FROM RUserAccessClipboard "
				+ "WHERE userId = ? AND clipboardId = TCLipboards.id) OR TClipboards.creatorId = ?) LIMIT ? OFFSET ?";

		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(sql));) {

			int totalCount = -1;
			List<Clipboard> bookmarks = new ArrayList<Clipboard>();
			ps.setInt(1, Integer.parseInt(queryValues.get("ownerId")));
			
			for (int i = 2; i < 9; i++) {
				ps.setInt(i, Integer.parseInt(queryValues.get("userId")));
			}
			
			ps.setInt(9, entriesPerPage);
			ps.setInt(10, page * entriesPerPage - entriesPerPage);
			
			try (ResultSetWrapper result = new ResultSetWrapper(ps.executeQuery())) {
				while (result.next()) {
					Clipboard clipboard = new Clipboard();
					clipboard.setId(result.getInt(1));
					clipboard.setTitle(result.getString(2));
					clipboard.setCreator(DAOUser.getCreatorById(result.getInt(3)));
					clipboard.setCreationDate(result.getDate(4));
					clipboard.setLastModifiedDate(result.getDate(5));
					clipboard.setExpirationDate(result.getDate(6));
					clipboard.setDescription(result.getString(7));
					clipboard.setVisibilityRead(Visibility.getById(result.getInt(8)));
					clipboard.setVisibilityWrite(Visibility.getById(result.getInt(9)));
					clipboard.setGroup(DAOGroup.get(result.getInt(10)));
					clipboard.setNotify(result.getBoolean(11));
					bookmarks.add(clipboard);
					totalCount = result.getInt(12);
				}

				PaginatedDataModel<Clipboard> pdm = new PaginatedDataModel<Clipboard>(page, totalCount, bookmarks);
				return pdm;
			}
		}
	}

	/**
	 * pagination helper
	 * 
	 * @param query
	 * @param page
	 * @param entriesPerPage
	 * @return
	 * @throws DataSourceException
	 */
	public static PaginatedDataModel<Paste> paginatePastes(String query, int page, int entriesPerPage)
			throws DataSourceException {

		String sql = "";
		Map<String, String> queryValues = QueryParser.parsePaginationQuery(query);
		sql = "SELECT id, title, creatorId, creationDate, lastModificationDate, "
				+ "expirationDate, description, content, clipboardId, notify, count(*) OVER() AS full_count FROM TPastes WHERE id IN "
				+ "(SELECT pasteId FROM RUserBookmarkPaste WHERE userId = ?) AND ( "
				+ "                4 != (SELECT visibilityRead FROM TClipboards WHERE TClipboards.id = TPastes.clipboardId) "
				+ "                OR "
				+ "                (SELECT groupId FROM TClipboards WHERE TClipboards.id = TPastes.clipboardId) IN (SELECT groupId FROM RUserGroup WHERE userId = ?) "
				+ "            ) " + "        AND ( "
				+ "                3 != (SELECT visibilityRead FROM TClipboards WHERE TClipboards.id = TPastes.clipboardId) "
				+ "                OR " + "                ? IN (SELECT id FROM TUsers WHERE id = ?) "
				+ "            ) " + "        AND ( "
				+ "                1 != (SELECT visibilityRead FROM TClipboards WHERE TClipboards.id = TPastes.clipboardId) "
				+ "                OR "
				+ "                ? IN (SELECT userId FROM RUserAccessClipboard WHERE userId = ? AND clipboardId = TPastes.clipboardId) "
				+ "            ) " + "       LIMIT ? OFFSET ?";

		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(sql));) {

			int totalCount = -1;
			List<Paste> bookmarks = new ArrayList<Paste>();
			ps.setInt(1, Integer.parseInt(queryValues.get("ownerId")));
			
			for (int i = 2; i < 7; i++) {
				ps.setInt(i, Integer.parseInt(queryValues.get("userId")));
			}

			ps.setInt(7, entriesPerPage);
			ps.setInt(8, page * entriesPerPage - entriesPerPage);
			try (ResultSetWrapper result = new ResultSetWrapper(ps.executeQuery())) {

				while (result.next()) {
					Paste paste = new Paste();
					paste.setId(result.getInt(1));
					paste.setTitle(result.getString(2));
					paste.setCreator(DAOUser.getCreatorById(result.getInt(3)));
					paste.setCreationDate(result.getDate(4));
					paste.setLastModifiedDate(result.getDate(5));
					paste.setExpirationDate(result.getDate(6));
					paste.setDescription(result.getString(7));
					paste.setContent(result.getString(8));
					paste.setClipboardId(result.getInt(9));
					paste.setNotify(result.getBoolean(10));
					bookmarks.add(paste);
					totalCount = result.getInt(11);
				}

				PaginatedDataModel<Paste> pdm = new PaginatedDataModel<Paste>(page, totalCount, bookmarks);
				return pdm;
			}
		}
	}
}
