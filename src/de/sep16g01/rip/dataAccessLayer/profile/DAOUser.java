package de.sep16g01.rip.dataAccessLayer.profile;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.util.QueryParser;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultTable;
import de.sep16g01.rip.dataAccessLayer.util.DBUtils;
import de.sep16g01.rip.dataAccessLayer.util.QueryStringUtil;

/**
 * DAO for the user objects. Handles loading and storing users to and from the
 * database.
 */
public class DAOUser {

	private static transient final Logger logger = Logger
			.getLogger(DAOUser.class);

	/**
	 * Loads a user from the database by name.
	 * 
	 * @param name
	 *            The name of the user.
	 * @return The corresponding User object.
	 * @throws DataSourceException
	 *             When the database is down.
	 * @throws SQLException
	 */
	public static User getUserByName(String name) throws DataSourceException {
		String query = "SELECT name, userPassword, passwordSalt, "
				+ "registrationDate, freeCapacity, email, userRole, "
				+ "profilePicture, id FROM TUsers WHERE name = ?";

		// try with resources to avoid leaking connection
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, name);
			User user = null;
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				// fill user object
				while (result.next()) {
					user = new User(result.getString(1), result.getBytes(2),
							result.getBytes(3));
					user.setEmail(result.getString(6));
					user.setRole(Role.getById(result.getInt(7)));
					user.setRegistrationDate(result.getTimestamp(4));
					user.setFreeCapacity(result.getLong(5));
					user.setProfilePicture(new File(result.getBytes(8)));
					user.setId(result.getInt(9));
				}
			}
			return user;
		}
	}

	/**
	 * Stores the new role of the user in the database.
	 * 
	 * @param userId
	 *            The id of the user whose role should be changed.
	 * @param roleId
	 *            The new role of the user.
	 * @throws DataSourceException
	 */
	public static void changeUserRole(int userId, int roleId)
			throws DataSourceException {
		String query = "UPDATE TUsers SET userRole = ? WHERE id = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, roleId);
			ps.setInt(2, userId);
			ps.executeUpdate();
		}
	}

	/**
	 * Decreases the free capacity of a user.
	 * 
	 * @param userId
	 *            The user whose free capacity is decreased.
	 * @param fileSize
	 *            The size in kilobyte the capacity should be decreased.
	 * @return new free capacity of user
	 * @throws DataSourceException
	 */
	public static long decreaseUserCapacity(int userId, long fileSize)
			throws DataSourceException {
		if (userId == -1) {
			return DAOSystemSettings.loadMaxFileSizePerRole().get(Role.ANONYM);
		}
		String query = "UPDATE TUsers SET freeCapacity = freecapacity - ? WHERE id = ?";
		String getQuery = "SELECT freeCapacity FROM TUsers WHERE id = ?";
		long newFreeCapacity = 0;

		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));
				PreparedStatementWrapper ps2 = new PreparedStatementWrapper(
						cw.prepareStatement(getQuery));) {
			ps.setLong(1, fileSize);
			ps.setInt(2, userId);
			ps.executeUpdate();
			ps2.setInt(1, userId);
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps2.executeQuery())) {
				if (result.next()) {
					newFreeCapacity = result.getLong(1);
				}
			}
		}
		return newFreeCapacity;

	}

	/**
	 * Increases the free capacity of a user.
	 * 
	 * @param userId
	 *            The user whose free capacity is increased.
	 * @param fileSize
	 *            The size in kilobyte the capacity should be increased.
	 * @return new free capacity of user
	 * @throws DataSourceException
	 */
	public static long increaseUserCapacity(int userId, long fileSize)
			throws DataSourceException {
		if (userId == -1) {
			return DAOSystemSettings.loadMaxFileSizePerRole().get(Role.ANONYM);
		}
		String query = "UPDATE TUsers SET freeCapacity = freecapacity + ? WHERE id = ?";
		String getQuery = "SELECT freeCapacity FROM TUsers WHERE id = ?";
		long newFreeCapacity = 0;

		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));
				PreparedStatementWrapper ps2 = new PreparedStatementWrapper(
						cw.prepareStatement(getQuery));) {
			ps.setLong(1, fileSize);
			ps.setInt(2, userId);
			ps.executeUpdate();
			ps2.setInt(1, userId);
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps2.executeQuery())) {
				if (result.next()) {
					newFreeCapacity = result.getLong(1);
				}
			}

		}

		return newFreeCapacity;

	}

	/**
	 * Loads a user from the database by email.
	 * 
	 * @param email
	 *            The email of the user.
	 * @return The name of the corresponding user.
	 * @throws DataSourceException
	 *             When the database is down.
	 * @throws SQLException
	 */
	public static String getUserByEmail(String email)
			throws DataSourceException {
		String query = "SELECT name, confirmationToken FROM TUsers "
				+ "WHERE email = ? OR newEmail = ?";
		String userName = null;
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {

			ps.setString(1, email);
			ps.setString(2, email);
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				if (rs.next()) {
					userName = rs.getString(1);
				}
			}

		}
		return userName;
	}

	/**
	 * @return A list of all users in the database.
	 * @throws DataSourceException
	 *             When the database is down.
	 */

	/**
	 * 
	 * @param id
	 *            The ID of a user.
	 * @return The user with the ID.
	 * @throws DataSourceException
	 * @throws SQLException
	 */
	public static User getById(int id) throws DataSourceException {
		String query = "SELECT name, userPassword, passwordSalt, email, "
				+ "userRole, registrationDate, freeCapacity, profilePicture, "
				+ "confirmationToken, id FROM TUsers WHERE id = ?";
		User user = null;
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {

			ps.setInt(1, id);
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				if (result.next()) {
					if (result.getString(9) == null) {
						user = new User(result.getString(1),
								result.getBytes(2), result.getBytes(3));
						user.setEmail(result.getString(4));
						user.setRole(Role.getById(result.getInt(5)));
						user.setRegistrationDate(result.getTimestamp(6));
						user.setFreeCapacity(result.getLong(7));
						user.setProfilePicture(new File(result.getBytes(8)));
						user.setId(result.getInt(10));

					}
				}
			}
		}
		return user;
	}

	/**
	 * Gets the creator of a paste/clipboard out of the database. Doesn't fill
	 * the whole user object, only the id, email and name.
	 * 
	 * @param id
	 *            The id of the user
	 * @return the user
	 * @throws DataSourceException
	 */
	public static User getCreatorById(int id) throws DataSourceException {
		String query = "SELECT name, email FROM TUsers " + "WHERE id = ?";
		User user = null;
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {

			ps.setInt(1, id);
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				if (rs.next()) {
					user = new User();
					user.setId(id);
					user.setName(rs.getString(1));
					user.setEmail(rs.getString(2));
				}
			}

		}
		return user;
	}

	/**
	 * Stores a {@link de.sep16g01.rip.businessLogicLayer.profile.User User} to
	 * the database.
	 * 
	 * @param user
	 *            The user that is being stored to the database.
	 * @throws DataSourceException
	 */
	public static void store(User user) throws DataSourceException {

		boolean alreadyExists = user.getId() != SystemElement.DEFAULT_ID;

		String query;

		// user exists, update
		if (alreadyExists) {
			query = "UPDATE TUsers SET userPassword = ?, passwordSalt = ?, "
					+ "registrationDate = ?,"
					+ " freeCapacity = ?, email = ?, userRole = ?, "
					+ "profilePicture = ? WHERE name = ?";
			// user does not exist, insert
		} else {
			query = "INSERT INTO TUsers (userPassword, passwordSalt, "
					+ "registrationDate,"
					+ " freeCapacity, email, userRole, profilePicture, "
					+ "name) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		}

		// insert user
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper psInsert = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			psInsert.setBytes(1, user.getPasswordHash());
			psInsert.setBytes(2, user.getPasswordSalt());
			psInsert.setTimestamp(3, new Timestamp(user.getRegistrationDate()
					.getTime()));
			psInsert.setLong(4, user.getFreeCapacity());
			psInsert.setString(5, user.getEmail());
			psInsert.setInt(6, user.getRole().getPriority());
			if (user.getProfilePicture() != null) {
				psInsert.setBytes(7, user.getProfilePicture().getContent());
			} else {
				psInsert.setBytes(7, new byte[0]);
			}
			psInsert.setString(8, user.getName());
			if (psInsert.executeUpdate() == 0) {
				logger.warn("No user inserted for name " + user.getName());
			}
		}
	}

	/**
	 * return the profile picture of a user
	 * 
	 * @param id
	 * @return
	 * @throws DataSourceException
	 * @throws SQLException
	 */
	public static byte[] getProfilePictureById(int id)
			throws DataSourceException {
		String query = "SELECT profilepicture FROM TUsers WHERE id = ?";
		byte[] profilePicture = null;
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, id);
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				if (result.next())
					profilePicture = result.getBytes(1);
			}
		}

		return profilePicture;
	}

	/**
	 * {@inheritDoc}
	 */
	public static PaginatedDataModel<User> paginate(String query, int page,
			int entriesPerPage) throws DataSourceException {
		String sql = "";
		List<User> users = new ArrayList<User>();
		Map<String, String> queryValues = QueryParser
				.parsePaginationQuery(query);

		String[] searchTerms = null;
		if (queryValues.get("type").equals("group")) {
			int totalCount = getUsersInGroup(
					Integer.parseInt(queryValues.get("groupId")), page,
					entriesPerPage, users);
			PaginatedDataModel<User> pdm = new PaginatedDataModel<User>(page,
					totalCount, users);
			return pdm;
		} else if (queryValues.get("type").equals("content")) {
			if (queryValues.get("value") != null) {
				searchTerms = queryValues.get("value").split(
						QueryParser.DELIMITER_QUERY);
				sql = "SELECT id, name, userPassword, passwordSalt, "
						+ "registrationDate, freeCapacity, email, userRole, "
						+ "count(*) OVER() AS full_count "
						+ "FROM TUsers WHERE "
						+ QueryParser.createWHEREQuery(searchTerms.length,
								"name") + " LIMIT ? OFFSET ?";
			}
		}
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(sql));) {
			int afterSearchIndex = 1;
			if (searchTerms != null) {
				for (int i = 1; i <= searchTerms.length; i++) {
					ps.setString(i, "%"
							+ searchTerms[(i - 1) % searchTerms.length] + "%");
					afterSearchIndex = i + 1;
				}
			}
			ps.setInt(afterSearchIndex, entriesPerPage);
			ps.setInt(afterSearchIndex + 1, page * entriesPerPage
					- entriesPerPage);
			int totalCount = -1;
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					User user = new User();
					user.setId(rs.getInt(1));
					user.setName(rs.getString(2));
					user.setPasswordHash(rs.getBytes(3));
					user.setPasswordSalt(rs.getBytes(4));
					user.setRegistrationDate(rs.getDate(5));
					user.setFreeCapacity(rs.getLong(6));
					user.setEmail(rs.getString(7));
					user.setRole(Role.getById(rs.getInt(8)));
					users.add(user);
					totalCount = rs.getInt(9);
				}
			}

			PaginatedDataModel<User> pdm = new PaginatedDataModel<User>(page,
					totalCount, users);
			return pdm;
		}
	}

	/**
	 * 
	 * Loads all users in a certain group from the database.
	 * 
	 * 
	 * 
	 * @param groupId
	 * 
	 *            The id of the group.
	 * 
	 * @param page
	 * 
	 *            The page of results that is being checked.
	 * 
	 * @param entriesPerPage
	 * 
	 *            How many entries are being loaded.
	 * @param toFill
	 * 
	 *            Where the current page of users will be returned
	 * 
	 * @return the total amount of found members
	 * 
	 * @throws DataSourceException
	 * 
	 */

	public static int getUsersInGroup(int groupId, int page,
			int entriesPerPage, List<User> toFill) throws DataSourceException {

		String query = "SELECT userId FROM RUserGroup WHERE groupId = ?";
		int memberCount = 0;
		String query2 = "SELECT COUNT(*) "
				+ "FROM RUserGroup WHERE groupId = ?";

		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query2));) {
			ps.setInt(1, groupId);
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				rs.next();
				memberCount = rs.getInt(1);
				if (memberCount == 0) {
					toFill.clear();
					return 0;
				}
			}
		}

		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, groupId);
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				String queryUsers = "SELECT name, userPassword, passwordSalt, email, "
						+ "userRole, registrationDate, freeCapacity, profilePicture, "
						+ "confirmationToken, id FROM TUsers WHERE id in "
						+ QueryStringUtil.createValueListString(memberCount)
						+ "LIMIT ? OFFSET ?";
				try (PreparedStatementWrapper psUsers = new PreparedStatementWrapper(
						cw.prepareStatement(queryUsers));) {
					for (int i = 1; i <= memberCount && rs.next(); i++) {
						psUsers.setInt(i, rs.getInt(1));
					}

					psUsers.setInt(memberCount + 1, entriesPerPage);
					psUsers.setInt(memberCount + 2, page * entriesPerPage
							- entriesPerPage);
					try (ResultSetWrapper rsUsers = new ResultSetWrapper(
							psUsers.executeQuery())) {

						while (rsUsers.next()) {
							User user = new User(rsUsers.getString(1),
									rsUsers.getBytes(2), rsUsers.getBytes(3));
							user.setEmail(rsUsers.getString(4));
							user.setRole(Role.getById(rsUsers.getInt(5)));
							user.setRegistrationDate(rsUsers.getTimestamp(6));
							user.setFreeCapacity(rsUsers.getLong(7));
							user.setProfilePicture(new File(rsUsers.getBytes(8)));
							user.setId(rsUsers.getInt(10));
							toFill.add(user);
						}
					}
					return memberCount;
				}
			}
		}
	}

	/**
	 * Deletes a user from the database.
	 * 
	 * @param user
	 *            The user being deleted.
	 * @return The number of deleted users (should always be 1).
	 * @throws DataSourceException
	 */
	public static int dropUser(int id) throws DataSourceException {
		String query = "DELETE FROM TUsers WHERE id = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, id);
			int affected = ps.executeUpdate();
			return affected;
		}
	}

	/**
	 * checks if a user already exists.
	 * 
	 * @param id
	 *            the id of the user which should be checked.
	 * @return
	 * @throws DataSourceException
	 */
	public static boolean checkExistance(int id) throws DataSourceException {
		String query = "SELECT 1 FROM TUsers WHERE id = ?";
		ResultTable resultTable = DBUtils.executeQuery(query, id);
		return resultTable.size() == 1;
	}
}
