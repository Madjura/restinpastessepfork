package de.sep16g01.rip.dataAccessLayer.database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.comments.Comment;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.util.HashString;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.comments.DAOComment;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

public class PresentationTableInitializer {

	/**
	 * Logger instance for this class.
	 */
	private final static Logger logger = Logger
			.getLogger(PresentationTableInitializer.class);

	private PresentationTableInitializer() {
	}

	public static void main(String[] args) throws DataSourceException {
		dropTablesAndCreatePresentationState();
	}

	public static void dropTablesAndCreatePresentationState()
			throws DataSourceException {
		logger.info("Starting DB reset to presentation state!");
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());) {
			TableInitializer.dropAllTables(cw);
			TableInitializer.createAllTables(cw);
			createPresentationValues();
		}
		logger.info("DB reset to presentation state complete!");
	}

	/**
	 * Creates initial values for testing.
	 * 
	 * @throws DataSourceException
	 */
	public static void createPresentationValues() throws DataSourceException {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2016, 07, 04, 10, 30, 00);
		Date creationDate = calendar.getTime();

		Calendar calendarExpiration = Calendar.getInstance();
		calendarExpiration.add(Calendar.DATE, 14);
		Date expirationDate = calendarExpiration.getTime();

		if (DAOUser.getUserByName("admin") != null) {
			return;
		}

		// ------------ systemsettings
		byte[] content;
		SystemSettings ss = DAOSystemSettings.loadSystemSettings();
		try {
			content = Files.readAllBytes(Paths.get("scythe.png"));
			File logo = new File(content);
			ss.setLogo(logo);
			DAOSystemSettings.store(ss);
		} catch (IOException e2) {
		}

		// ------- admin and mods
		User admin = new User();
		admin.setName("admin");
		admin.setEmail("restinpastes.host@gmail.com");
		byte[] salt = HashString.getNextSalt();
		admin.setPasswordHash(HashString.hashPassword("passwort", salt));
		admin.setPasswordSalt(salt);
		admin.setRole(Role.ADMINISTRATOR);
		admin.setRegistrationDate(creationDate);
		{
			File logo = null;
			try {
				content = Files.readAllBytes(Paths.get("scythe.png"));
				logo = new File(content);
				admin.setProfilePicture(logo);
			} catch (IOException e2) {
			}
			admin.setProfilePicture(logo);
		}
		DAOUser.store(admin);
		admin = DAOUser.getUserByName("admin");

		User moderator = new User();
		moderator.setName("moderator");
		moderator.setEmail("restinpastes.moderator@gmail.com");
		byte[] saltMod = HashString.getNextSalt();
		moderator.setPasswordHash(HashString.hashPassword("passwort", saltMod));
		moderator.setPasswordSalt(saltMod);
		moderator.setRole(Role.MODERATOR);
		moderator.setRegistrationDate(creationDate);

		DAOUser.store(moderator);
		// -------- users
		User user = new User();
		user.setName("hunklinger");
		user.setEmail("restinpastes.hunklinger@gmail.com");
		byte[] saltUser = HashString.getNextSalt();
		user.setPasswordHash(HashString.hashPassword("passwort", saltUser));
		user.setPasswordSalt(saltUser);
		user.setRole(Role.REGISTERED);
		user.setRegistrationDate(creationDate);
		user.setFreeCapacity(5000);
		{
			File logo = null;
			try {
				content = Files.readAllBytes(Paths.get("scythe.png"));
				logo = new File(content);
				admin.setProfilePicture(logo);
			} catch (IOException e2) {
			}
			user.setProfilePicture(logo);
		}
		DAOUser.store(user);
		user = DAOUser.getUserByName("hunklinger");

		User user1 = new User();
		user1.setName("wendlinger");
		user1.setEmail("restinpastes.wendlinger@gmail.com");
		byte[] saltUser1 = HashString.getNextSalt();
		user1.setPasswordHash(HashString.hashPassword("passwort", saltUser1));
		user1.setPasswordSalt(saltUser1);
		user1.setRole(Role.REGISTERED);
		user1.setRegistrationDate(creationDate);
		user1.setFreeCapacity(5000);
		{
			File logo = null;
			try {
				content = Files.readAllBytes(Paths.get("scythe.png"));
				logo = new File(content);
				admin.setProfilePicture(logo);
			} catch (IOException e2) {
			}
			user1.setProfilePicture(logo);
		}
		DAOUser.store(user1);
		user1 = DAOUser.getUserByName("wendlinger");

		User user2 = new User();
		user2.setName("schrottenbaum");
		user2.setEmail("restinpastes.schrottenbaum@gmail.com");
		byte[] saltuser2 = HashString.getNextSalt();
		user2.setPasswordHash(HashString.hashPassword("passwort", saltuser2));
		user2.setPasswordSalt(saltuser2);
		user2.setRole(Role.REGISTERED);
		user2.setRegistrationDate(creationDate);
		user2.setFreeCapacity(5000);
		{
			File logo = null;
			try {
				content = Files.readAllBytes(Paths.get("scythe.png"));
				logo = new File(content);
				admin.setProfilePicture(logo);
			} catch (IOException e2) {
			}
			user2.setProfilePicture(logo);
		}
		DAOUser.store(user2);
		user2 = DAOUser.getUserByName("schrottenbaum");
		
		User user3 = new User();
		user3.setName("zwierz");
		user3.setEmail("restinpastes.zwierz@gmail.com");
		byte[] saltuser3 = HashString.getNextSalt();
		user3.setPasswordHash(HashString.hashPassword("passwort", saltuser2));
		user3.setPasswordSalt(saltuser3);
		user3.setRole(Role.REGISTERED);
		user3.setRegistrationDate(creationDate);
		user3.setFreeCapacity(5000);
		{
			File logo = null;
			try {
				content = Files.readAllBytes(Paths.get("scythe.png"));
				logo = new File(content);
				admin.setProfilePicture(logo);
			} catch (IOException e2) {
			}
			user3.setProfilePicture(logo);
		}
		DAOUser.store(user3);
		user3 = DAOUser.getUserByName("zwierz");
		
		User user4 = new User();
		user4.setName("fichtner");
		user4.setEmail("restinpastes.fichtner@gmail.com");
		byte[] saltuser4 = HashString.getNextSalt();
		user4.setPasswordHash(HashString.hashPassword("passwort", saltuser2));
		user4.setPasswordSalt(saltuser4);
		user4.setRole(Role.REGISTERED);
		user4.setRegistrationDate(creationDate);
		user4.setFreeCapacity(5000);
		{
			File logo = null;
			try {
				content = Files.readAllBytes(Paths.get("scythe.png"));
				logo = new File(content);
				admin.setProfilePicture(logo);
			} catch (IOException e2) {
			}
			user4.setProfilePicture(logo);
		}
		DAOUser.store(user4);
		user4 = DAOUser.getUserByName("fichtner");
		

		// ------------ groups
		Group g02 = new Group();
		g02.setTitle("g02");
		g02.setDescription("public Group");
		g02.setCreationDate(creationDate);
		g02.setLastModifiedDate(creationDate);
		g02.setVisibilityRead(Visibility.PUBLIC);
		g02.setVisibilityWrite(Visibility.PUBLIC);
		g02.setId(DAOGroup.store(g02));


		// ------------ clipboards

		Clipboard z1 = new Clipboard();
		z1.setTitle("My first Clipboard");
		z1.setCreator(user);
		z1.setCreationDate(creationDate);
		z1.setLastModifiedDate(creationDate);
		z1.setExpirationDate(new Date(10000));
		z1.setDescription("public Clipboard");
		z1.setVisibilityRead(Visibility.PUBLIC);
		z1.setVisibilityWrite(Visibility.PUBLIC);
		z1.setNotify(false);
		z1.setId(DAOClipboard.store(z1));
		
		Clipboard z2 = new Clipboard();
		z2.setTitle("FIM Grillfeier");
		z2.setCreator(user);
		z2.setCreationDate(creationDate);
		z2.setLastModifiedDate(creationDate);
		z2.setExpirationDate(new Date(10000));
		z2.setDescription("private Clipboard");
		z2.setVisibilityRead(Visibility.PUBLIC);
		z2.setVisibilityWrite(Visibility.PUBLIC);
		z2.setNotify(false);
		z2.setId(DAOClipboard.store(z2));
		z2.setGroup(g02);


		// --------- comment
		{
			Comment comment = new Comment();
			comment.setContent("Stellt bitte die Bilder von den letzten Partys hier rein!");
			comment.setBelongsTo(z2);
			comment.setCreationDate(creationDate);
			comment.setCreator(user);
			comment.setDescription("Comment user Description");
			comment.setTitle("Comment admin Title");
			DAOComment.store(comment, "Clipboard");
		}
		{
			Comment comment = new Comment();
			comment.setContent("Ja klar");
			comment.setBelongsTo(z2);
			comment.setCreationDate(creationDate);
			comment.setCreator(user3);
			comment.setDescription("Comment user1 Description");
			comment.setTitle("Comment user1 Title");
			DAOComment.store(comment, "Clipboard");
		}
		{
			Comment comment = new Comment();
			comment.setContent("Oh nein...");
			comment.setBelongsTo(z2);
			comment.setCreationDate(creationDate);
			comment.setCreator(user2);
			comment.setDescription("Comment user2 Description");
			comment.setTitle("Comment user2 Title");
			DAOComment.store(comment, "Clipboard");
		}
		
		for (int i = 0; i<12; i++) {
			Comment comment = new Comment();
			comment.setContent("spam");
			comment.setBelongsTo(user3);
			comment.setCreationDate(creationDate);
			comment.setCreator(user);
			comment.setDescription("Comment user2 Description");
			comment.setTitle("Comment user2 Title");
			DAOComment.store(comment, "User");
			
		}

		// ----------- relations
		DAOGroup.addUser(user1.getId(), g02.getId());
		DAOGroup.addUser(user2.getId(), g02.getId());
		DAOGroup.addUser(user3.getId(), g02.getId());

	}
}
