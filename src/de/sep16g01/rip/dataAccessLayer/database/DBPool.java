package de.sep16g01.rip.dataAccessLayer.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.Config;
//import de.sep16g01.rip.businessLogicLayer.general.CleanupThread;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;

/**
 * Holds a pool of connections to the database. Responsible for giving and
 * holding connections to the user when requested.
 */
public class DBPool {

	/**
	 * Logger instance for this class.
	 */
	private final static Logger logger = Logger.getLogger(DBPool.class);

	/**
	 * The singleton instance of the pool.
	 */
	private static DBPool poolinstance = new DBPool();

	private Queue<Connection> connections = new LinkedList<Connection>();
	private Queue<Connection> connectionsInUse = new LinkedList<Connection>();

	public static DBPool getInstance() {
		return poolinstance;
	}

	private DBPool() {
		init();
		// CleanupThread clean = new CleanupThread();
		// clean.run(); //--> führt zu endlosschleife hier!
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public final void run() {
				for (Connection c : connections) {
					try {
						c.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

				for (Connection c : connectionsInUse) {
					try {
						c.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private final Config config = Config.getInstance();
	private int maxConnections = config.getDbPoolSize();
	private String dbDriver = config.getDbDriver();
	private String dbHost = config.getDbHost();
	private String dbUser = config.getDbUser();
	private String dbPassword = config.getDbPassword();
	private String dbName = config.getDbName();

	private final static int MAX_CONNECTION_TIMEOUT = 5000;

	public int getMaxConnections() {
		return maxConnections;
	}

	/**
	 * Creates number of database connections for the pool.
	 * 
	 * @throws DataSourceException
	 * @throws SQLException
	 */
	private void init() {
		// Loads the driver of the database.
		try {
			Class.forName(dbDriver);
			logger.info("Database Driver initialized.");

		} catch (ClassNotFoundException e) {
			logger.info("Database Driver not found.", e);
			return;
		}
		// Adds connections to the connection pool.
		for (int i = 0; i < maxConnections; i++) {
			try {
				Connection con = DriverManager.getConnection(
						"jdbc:postgresql://" + dbHost + "/" + dbName, dbUser,
						dbPassword);
				connections.add(con);
				logger.info("Connections initialized.");
			} catch (SQLException e) {
				logger.fatal("Connection could not be initialized.", e);
				return;
			}
		}

	}

	/**
	 * Closes all connections to the Database when the system is shut down.
	 * 
	 * @throws SQLException
	 * 
	 * @PreDestroy private void closeConnections() throws SQLException { for
	 *             (Connection c : connections) { c.close(); } }
	 * 
	 * 
	 *             /** Puts a connection back in the Queue.
	 * 
	 * @param con
	 *            The connection that is being put back-
	 */
	public synchronized void returnConnection(Connection con) {
		if (connectionsInUse.contains(con)) {
			connectionsInUse.remove(con);
		}
		if (con != null) {
			connections.add(con);
		}
	}

	/**
	 * Synchronized part of getConnection()
	 * 
	 * @return a connection from the pool
	 * @throws DataSourceException
	 */
	private synchronized Connection getConnectionInternal()
			throws DataSourceException {

		Connection con = connections.poll();
		if (con != null) {
			String checkQuery = "SELECT 1";

			try (PreparedStatement psCheck = con
					.prepareStatement(checkQuery);) {
				ResultSet rs = psCheck.executeQuery();
				rs.next();
				rs.close();
			} catch (SQLException e) {
				try {
					con = DriverManager.getConnection(
							"jdbc:postgresql://" + dbHost + "/" + dbName,
							dbUser, dbPassword);
				} catch (Exception e1) {
					throw new DataSourceException(e1);
				}
			}
			connectionsInUse.add(con);
		}
		return con;
	}

	/**
	 * @return A connection from the pool.
	 * @throws DataSourceException
	 *             When the database is down.
	 */
	public Connection getConnection() throws DataSourceException {

		long time = 0;
		while (time < MAX_CONNECTION_TIMEOUT) {

			Connection con = getConnectionInternal();
			if (con != null)
				return con;

			try {
				Thread.sleep(100);
				time += 100;
			} catch (InterruptedException e) {
				logger.fatal("no connection available");
				throw new DataSourceException(e);
			}
		}

		return null;
	}

}
