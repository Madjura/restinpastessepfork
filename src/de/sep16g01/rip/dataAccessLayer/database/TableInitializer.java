package de.sep16g01.rip.dataAccessLayer.database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.Config;
import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.comments.Comment;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.businessLogicLayer.util.HashString;
import de.sep16g01.rip.dataAccessLayer.administration.DAOSystemSettings;
import de.sep16g01.rip.dataAccessLayer.clipboard.DAOClipboard;
import de.sep16g01.rip.dataAccessLayer.comments.DAOComment;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.group.DAOGroup;
import de.sep16g01.rip.dataAccessLayer.paste.DAOPaste;
import de.sep16g01.rip.dataAccessLayer.permission.DAOPermission;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;

/**
 *
 */
public class TableInitializer {

	/**
	 * Logger instance for this class.
	 */
	private final static Logger logger = Logger
			.getLogger(TableInitializer.class);

	public static void createAllTables(ConnectionWrapper con)
			throws DataSourceException {

		createTables(con);
		fillRoleTable(con);
		createUserTable(con);
		createSystemTable(con);
	}

	public static void createTables(ConnectionWrapper con)
			throws DataSourceException {

		String query = "CREATE TABLE IF NOT EXISTS TRoles ("
				+ "id SERIAL PRIMARY KEY, " + "role INTEGER NOT NULL UNIQUE,"
				+ "fileSizeLimit BIGINT NOT NULL, singleFileSizeLimit BIGINT"
				+ " NOT NULL,"
				+ "maxExpirationDuration VARCHAR(128) NOT NULL);";

		query += "CREATE TABLE IF NOT EXISTS TRoles ("
				+ "id SERIAL PRIMARY KEY, " + "role INTEGER NOT NULL UNIQUE,"
				+ "fileSizeLimit BIGINT NOT NULL, singleFileSizeLimit BIGINT"
				+ " NOT NULL,"
				+ "maxExpirationDuration VARCHAR(128) NOT NULL);";

		query += "CREATE TABLE IF NOT EXISTS TUsers " + "("
				+ "id SERIAL PRIMARY KEY, "
				+ "name VARCHAR(32) NOT NULL UNIQUE,"
				+ "userPassword BYTEA NOT NULL,"
				+ "passwordSalt BYTEA NOT NULL,"
				+ "registrationDate TIMESTAMP NOT NULL, "
				+ "freeCapacity BIGINT NOT NULL,"
				+ "confirmationToken CHAR(36), "
				+ "email VARCHAR(256) NOT NULL UNIQUE,"
				+ "emailConfirmationToken CHAR(36),"
				+ "newEmail VARCHAR(256) UNIQUE,"
				+ "userRole INTEGER NOT NULL REFERENCES TRoles(id) ON DELETE RESTRICT,"
				+ "profilePicture BYTEA);";

		query += "CREATE TABLE IF NOT EXISTS TGroups ("
				+ "id SERIAL PRIMARY KEY, "
				+ "title VARCHAR(64) NOT NULL UNIQUE,"
				+ "creationDate TIMESTAMP NOT NULL, "
				+ "lastModificationDate TIMESTAMP NOT NULL,"
				+ "description VARCHAR(512) NOT NULL, "
				+ "visibilityRead INTEGER NOT NULL, "
				+ "visibilityWrite INTEGER NOT NULL);";

		query += "CREATE TABLE IF NOT EXISTS TClipboards ("
				+ "id SERIAL PRIMARY KEY, " + "title VARCHAR(64) NOT NULL,"
				+ "creatorId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE, "
				+ "creationDate TIMESTAMP NOT NULL, "
				+ "lastModificationDate TIMESTAMP NOT NULL,"
				+ "expirationDate TIMESTAMP NOT NULL, "
				+ "description VARCHAR(512) NOT NULL, "
				+ "visibilityRead INTEGER NOT NULL, "
				+ "visibilityWrite INTEGER NOT NULL,"
				+ "groupId INTEGER REFERENCES TGroups(id) ON DELETE CASCADE, "
				+ "notify BOOLEAN NOT NULL);";
		query += "CREATE TABLE IF NOT EXISTS TPastes ("
				+ "id SERIAL PRIMARY KEY, " + "title VARCHAR(64) NOT NULL, "
				+ "creatorId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE, "
				+ "creationDate TIMESTAMP NOT NULL, "
				+ "lastModificationDate TIMESTAMP NOT NULL, "
				+ "expirationDate TIMESTAMP NOT NULL, "
				+ "description VARCHAR(512) NOT NULL, "
				+ "content VARCHAR(65536),"
				+ "clipboardId INTEGER NOT NULL REFERENCES TClipboards(id) ON DELETE CASCADE, "
				+ "notify BOOLEAN NOT NULL);";

		query += "CREATE TABLE IF NOT EXISTS TComments ("
				+ "id SERIAL PRIMARY KEY, " + "content VARCHAR(512) NOT NULL,"
				+ "creationDate TIMESTAMP NOT NULL, "
				+ "creatorId INTEGER NOT NULL);";

		query += "CREATE TABLE IF NOT EXISTS TKeywords ("
				+ "id SERIAL PRIMARY KEY, "
				+ "name VARCHAR(128) NOT NULL UNIQUE);";

		query += "CREATE TABLE IF NOT EXISTS TFiles ("
				+ "id SERIAL PRIMARY KEY, " + "fileName VARCHAR(64) NOT NULL,"
				+ "content BYTEA NOT NULL, filesize BIGINT, ownerId INT NOT NULL,"
				+ "pasteId INTEGER NOT NULL REFERENCES TPastes(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS TSystem (id "
				+ "SERIAL PRIMARY KEY, name VARCHAR(128) NOT NULL, logo BYTEA,"
				+ "impressum TEXT, AGB TEXT);";

		query += "CREATE TABLE IF NOT EXISTS RUserGroup ("
				+ "userId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE, "
				+ "groupId INTEGER NOT NULL REFERENCES TGroups(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RUserComments ("
				+ "userProfileId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE, "
				+ "commentId INTEGER NOT NULL REFERENCES TComments(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RUserKeyword ("
				+ "userId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE, "
				+ "keywordId INTEGER NOT NULL REFERENCES TKeywords(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RUserReportComment ("
				+ "id SERIAL PRIMARY KEY," + "pending BOOLEAN NOT NULL,"
				+ "expirationDate TIMESTAMP, " + "userId INTEGER NOT NULL, "
				+ "commentId INTEGER NOT NULL);";

		query += "CREATE TABLE IF NOT EXISTS RUserBookmarkClipboard ("
				+ "userId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE, "
				+ "clipboardId INTEGER NOT NULL REFERENCES TClipboards(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RUserBookmarkPaste ("
				+ "userId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE, "
				+ "pasteId INTEGER NOT NULL REFERENCES TPastes(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS  RUserAccessGroup ("
				+ "userId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE, accessId INTEGER NOT NULL,"
				+ "groupId INTEGER NOT NULL REFERENCES TGroups(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RUserAccessClipboard ("
				+ "userId INTEGER NOT NULL, accessId INTEGER NOT NULL REFERENCES TUsers(id) ON DELETE CASCADE,"
				+ "clipboardId INTEGER NOT NULL REFERENCES TClipboards(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RGroupAccessClipboard ("
				+ "groupId INTEGER NOT NULL, accessId INTEGER NOT NULL REFERENCES TGroups(id) ON DELETE CASCADE,"
				+ "clipboardId INTEGER NOT NULL REFERENCES TClipboards(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RPasteComments ("
				+ "pasteId INTEGER NOT NULL REFERENCES TPastes(id) ON DELETE CASCADE, "
				+ "commentId INTEGER NOT NULL REFERENCES TComments(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RClipboardComments ("
				+ "clipboardId INTEGER NOT NULL REFERENCES TClipboards(id) ON DELETE CASCADE, "
				+ "commentId INTEGER NOT NULL REFERENCES TComments(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RGroupComments ("
				+ "groupId INTEGER NOT NULL REFERENCES TGroups(id) ON DELETE CASCADE, "
				+ "commentId INTEGER NOT NULL REFERENCES TComments(id) ON DELETE CASCADE);";

		query += "CREATE TABLE IF NOT EXISTS RInviteTokenGroup ("
				+ "groupId INTEGER NOT NULL REFERENCES TGroups(id) ON DELETE CASCADE, token CHAR(36) NOT NULL,"
				+ "accessId INTEGER NOT NULL);";

		query += "CREATE TABLE IF NOT EXISTS RInviteTokenClipboard ("
				+ "clipboardId INTEGER NOT NULL REFERENCES TClipboards(id) ON DELETE CASCADE, "
				+ "token CHAR(36) NOT NULL," + "accessId INTEGER NOT NULL);";

		try (PreparedStatementWrapper ps = new PreparedStatementWrapper(
				con.prepareStatement(query));) {
			ps.executeUpdate();
		}
	}

	private static void fillRoleTable(ConnectionWrapper connection)
			throws DataSourceException {
		String query = "INSERT INTO TRoles (role, fileSizeLimit,"
				+ " singleFileSizeLimit, maxExpirationDuration) " + "VALUES "
				+ "(?, ?, ?, '7 d')," + "(?, ?, ?, '7 d'),"
				+ "(?, ?, ?, '7 d')," + "(?, ?, ?, '7 d');";

		String checkExistance = "SELECT COUNT(*) FROM TRoles";

		try (PreparedStatementWrapper ps = new PreparedStatementWrapper(
				connection.prepareStatement(checkExistance))) {
			try (ResultSetWrapper rs = new ResultSetWrapper(
					ps.executeQuery())) {
				rs.next();
				if (rs.getInt(1) > 0) {
					return;
				}
			}
		}

		// connect = DBPool.getInstance().getConnection();
		try (PreparedStatementWrapper ps = new PreparedStatementWrapper(
				connection.prepareStatement(query));) {
			ps.setInt(1, 1);
			ps.setInt(2, 1000000);
			ps.setInt(3, 10000);
			ps.setInt(4, 2);
			ps.setInt(5, 1000000);
			ps.setInt(6, 10000);
			ps.setInt(7, 3);
			ps.setInt(8, 1000000);
			ps.setInt(9, 10000);
			ps.setInt(10, 4);
			ps.setInt(11, 1000000);
			ps.setInt(12, 10000);
			ps.executeUpdate();
		}
	}

	private static byte[] randomBytes(byte[] bytes) {
		Random rand = new Random();
		rand.nextBytes(bytes);
		return bytes;
	}

	private static void createUserTable(ConnectionWrapper connection)
			throws DataSourceException {

		String anon = "INSERT INTO TUsers (id, name, userpassword, "
				+ "passwordsalt, registrationDate, freeCapacity, "
				+ "email, userRole) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING";

		try (PreparedStatementWrapper ps = new PreparedStatementWrapper(
				connection.prepareStatement(anon));) {
			ps.setInt(1, -1);
			ps.setString(2, "Anonymous");
			ps.setBytes(3, randomBytes(new byte[300]));
			ps.setBytes(4, randomBytes(new byte[300]));
			ps.setDate(5, new Date(0));
			ps.setInt(6, 5000);
			ps.setString(7, "anonymous@restinpastes.com");
			ps.setInt(8, Role.ANONYM.getPriority());
			ps.executeUpdate();
		}

		String admin = "INSERT INTO TUsers (name, userpassword, "
				+ "passwordsalt, registrationDate, freeCapacity, "
				+ "email, userRole) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING";

		try (PreparedStatementWrapper ps = new PreparedStatementWrapper(
				connection.prepareStatement(admin));) {
			Config config = Config.getInstance();
			ps.setString(1, config.getAdminname());
			byte[] salt = HashString.getNextSalt();
			ps.setBytes(2,
					HashString.hashPassword(config.getAdminpasswort(), salt));
			ps.setBytes(3, salt);
			ps.setDate(4, new Date(0));
			ps.setInt(5, 1000000);
			ps.setString(6, config.getAdminemail());
			ps.setInt(7, Role.ADMINISTRATOR.getPriority());
			ps.executeUpdate();
		}
	}

	private static void createSystemTable(ConnectionWrapper connection)
			throws DataSourceException {

		String testQuery = "SELECT COUNT(*) FROM TSystem;";
		String insertQuery = "INSERT INTO TSystem (name, impressum, AGB) "
				+ "VALUES (?, ?, ?)";
		try (PreparedStatementWrapper ps = new PreparedStatementWrapper(
				connection.prepareStatement(testQuery));) {
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				result.next();
				if (result.getInt(1) < 1) {
					try (PreparedStatementWrapper pst = new PreparedStatementWrapper(
							connection.prepareStatement(insertQuery));) {
						pst.setString(1, "Rest in Pastes");
						pst.setString(2, "imprint");
						pst.setString(3, "terms of service");
						pst.executeUpdate();
					}
				}
			}
		}

	}

	/**
	 * Creates initial values for testing.
	 * 
	 * @throws DataSourceException
	 */
	public static void createTestValues() throws DataSourceException {

		// ------------ systemsettings
		byte[] content;
		SystemSettings ss = DAOSystemSettings.loadSystemSettings();
		try {
			content = Files.readAllBytes(Paths.get("scythe.png"));
			File logo = new File(content);
			ss.setLogo(logo);
			DAOSystemSettings.store(ss);
		} catch (IOException e2) {
		}

		// ------- admin and mods
		User admin = new User();
		admin.setName("admin");
		admin.setEmail("restinpastes.host@gmail.com");
		byte[] salt = HashString.getNextSalt();
		admin.setPasswordHash(HashString.hashPassword("passwort", salt));
		admin.setPasswordSalt(salt);
		admin.setRole(Role.ADMINISTRATOR);
		admin.setRegistrationDate(new Date(0));
		File logo = null;
		try {
			content = Files.readAllBytes(Paths.get("scythe.png"));
			logo = new File(content);
			admin.setProfilePicture(logo);
		} catch (IOException e2) {
		}
		DAOUser.store(admin);
		admin = DAOUser.getUserByName("admin");
		admin.setProfilePicture(logo);
		DAOUser.store(admin);

		User moderator = new User();
		moderator.setName("moderator");
		moderator.setEmail("restinpastes.moderator@gmail.com");
		byte[] saltMod = HashString.getNextSalt();
		moderator.setPasswordHash(HashString.hashPassword("passwort", saltMod));
		moderator.setPasswordSalt(saltMod);
		moderator.setRole(Role.MODERATOR);
		moderator.setRegistrationDate(new Date(0));

		DAOUser.store(moderator);
		// moderator = DAOUser.getUserByName("moderator");

		// -------- users
		User user = new User();
		user.setName("user");
		user.setEmail("restinpastes.user@gmail.com");
		byte[] saltUser = HashString.getNextSalt();
		user.setPasswordHash(HashString.hashPassword("passwort", saltUser));
		user.setPasswordSalt(saltUser);
		user.setRole(Role.REGISTERED);
		user.setRegistrationDate(new Date(0));
		user.setFreeCapacity(5000);
		DAOUser.store(user);
		user = DAOUser.getUserByName("user");

		User user1 = new User();
		user1.setName("user1");
		user1.setEmail("restinpastes.user1@gmail.com");
		byte[] saltUser1 = HashString.getNextSalt();
		user1.setPasswordHash(HashString.hashPassword("passwort", saltUser1));
		user1.setPasswordSalt(saltUser1);
		user1.setRole(Role.REGISTERED);
		user1.setRegistrationDate(new Date(0));
		user.setFreeCapacity(5000);
		DAOUser.store(user1);
		user1 = DAOUser.getUserByName("user1");

		User user2 = new User();
		user2.setName("user2");
		user2.setEmail("restinpastes.user2@gmail.com");
		byte[] saltuser2 = HashString.getNextSalt();
		user2.setPasswordHash(HashString.hashPassword("passwort", saltuser2));
		user2.setPasswordSalt(saltuser2);
		user2.setRole(Role.REGISTERED);
		user2.setRegistrationDate(new Date(0));
		user2.setFreeCapacity(5000);
		DAOUser.store(user2);

		User user3 = new User();
		user3.setName("user3");
		user3.setEmail("restinpastes.user3@gmail.com");
		byte[] saltuser3 = HashString.getNextSalt();
		user3.setPasswordHash(HashString.hashPassword("passwort", saltuser3));
		user3.setPasswordSalt(saltuser3);
		user3.setRole(Role.REGISTERED);
		user3.setRegistrationDate(new Date(0));
		user3.setFreeCapacity(5000);
		DAOUser.store(user3);
		user3 = DAOUser.getUserByName("user3");

		User privat = new User();
		privat.setName("privat");
		privat.setEmail("restinpastes.privat@gmail.com");
		byte[] saltprivat = HashString.getNextSalt();
		privat.setPasswordHash(HashString.hashPassword("passwort", saltprivat));
		privat.setPasswordSalt(saltprivat);
		privat.setRole(Role.REGISTERED);
		privat.setRegistrationDate(new Date(0));
		privat.setFreeCapacity(5000);
		DAOUser.store(privat);
		privat = DAOUser.getUserByName("privat");

		// ------------ groups
		Group testGruppe = new Group();
		testGruppe.setTitle("Testgruppe");
		testGruppe.setDescription("Testgruppe");
		testGruppe.setCreationDate(new Date(0));
		testGruppe.setLastModifiedDate(new Date(0));
		testGruppe.setVisibilityRead(Visibility.PUBLIC);
		testGruppe.setVisibilityWrite(Visibility.PUBLIC);
		testGruppe.setId(DAOGroup.store(testGruppe));

		Group testGruppeP = new Group();
		testGruppeP.setTitle("TestgruppeP");
		testGruppeP.setDescription("TestgruppeP");
		testGruppeP.setCreationDate(new Date(0));
		testGruppeP.setLastModifiedDate(new Date(0));
		testGruppeP.setVisibilityRead(Visibility.PRIVATE);
		testGruppeP.setVisibilityWrite(Visibility.PRIVATE);
		try {
			testGruppeP.setId(DAOGroup.store(testGruppeP));
		} catch (DataSourceException e) {
			try {
				testGruppeP.setId(DAOGroup
						.getGroupByName(testGruppeP.getTitle()).getId());
			} catch (DataSourceException e1) {
				logger.fatal("Unable to finish creating test values.", e1);
			}
			logger.fatal("Unable to finish creating test values.", e);
		}

		// ------------ clipboards
		Clipboard z1 = new Clipboard();
		z1.setTitle("Zwischenablage1");
		z1.setCreator(user);
		z1.setCreationDate(new Date(0));
		z1.setLastModifiedDate(new Date(0));
		z1.setExpirationDate(new Date(10000));
		z1.setDescription("Zwischenablage1");
		z1.setVisibilityRead(Visibility.PUBLIC);
		z1.setVisibilityWrite(Visibility.PUBLIC);
		z1.setNotify(false);
		z1.setId(DAOClipboard.store(z1));

		Clipboard z2 = new Clipboard();
		z2.setTitle("Zwischenablage2");
		z2.setCreator(user);
		z2.setCreationDate(new Date(0));
		z2.setLastModifiedDate(new Date(0));
		z2.setExpirationDate(new Date(1000));
		z2.setDescription("Zwischenablage2");
		z2.setVisibilityRead(Visibility.CUSTOM);
		z2.setVisibilityWrite(Visibility.PRIVATE);
		z2.setNotify(false);
		z2.setId(DAOClipboard.store(z2));

		Clipboard z3 = new Clipboard();
		z3.setTitle("Zwischenablage3");
		z3.setCreator(user);
		z3.setCreationDate(new Date(0));
		z3.setLastModifiedDate(new Date(0));
		z3.setExpirationDate(new Date(1000));
		z3.setDescription("Zwischenablage3");
		z3.setVisibilityRead(Visibility.PUBLIC);
		z3.setVisibilityWrite(Visibility.PRIVATE);
		z3.setNotify(false);
		z3.setId(DAOClipboard.store(z3));

		Clipboard z4 = new Clipboard();
		z4.setTitle("Zwischenablage4");
		z4.setCreator(user1);
		z4.setCreationDate(new Date(0));
		z4.setLastModifiedDate(new Date(0));
		z4.setExpirationDate(new Date(1000));
		z4.setDescription("Zwischenablage4");
		z4.setVisibilityRead(Visibility.PUBLIC);
		z4.setVisibilityWrite(Visibility.PRIVATE);
		z4.setNotify(false);
		z4.setId(DAOClipboard.store(z4));

		Clipboard zprivate = new Clipboard();
		zprivate.setTitle("ZwischenablagP");
		zprivate.setCreator(privat);
		zprivate.setCreationDate(new Date(0));
		zprivate.setLastModifiedDate(new Date(0));
		zprivate.setExpirationDate(new Date(1000));
		zprivate.setDescription("ZwischenablageP");
		zprivate.setVisibilityRead(Visibility.PUBLIC);
		zprivate.setVisibilityWrite(Visibility.PRIVATE);
		zprivate.setNotify(false);
		zprivate.setId(DAOClipboard.store(zprivate));

		Clipboard zgroup = new Clipboard();
		zgroup.setTitle("ZwischenablagG");
		zgroup.setCreator(user1);
		zgroup.setCreationDate(new Date(0));
		zgroup.setLastModifiedDate(new Date(0));
		zgroup.setExpirationDate(new Date(1000));
		zgroup.setDescription("ZwischenablageG");
		zgroup.setVisibilityRead(Visibility.PUBLIC);
		zgroup.setVisibilityWrite(Visibility.PRIVATE);
		zgroup.setNotify(false);
		zgroup.setGroup(testGruppe);
		zgroup.setId(DAOClipboard.store(zgroup));

		// ---------- pastes
		Paste paste1 = new Paste();
		paste1.setTitle("PasteZ1");
		paste1.setCreator(user1);
		paste1.setCreationDate(new Date(0));
		paste1.setLastModifiedDate(new Date(0));
		paste1.setExpirationDate(new Date(1000));
		paste1.setDescription("Hello world");
		paste1.setContent("Hello world");
		paste1.setClipboardId(z1.getId());
		paste1.setId(DAOPaste.store(paste1, true).getFirst());

		Paste paste12 = new Paste();
		paste12.setTitle("PasteZ12");
		paste12.setCreator(user1);
		paste12.setCreationDate(new Date(0));
		paste12.setLastModifiedDate(new Date(0));
		paste12.setExpirationDate(new Date(1000));
		paste12.setDescription("");
		paste12.setContent("");
		paste12.setClipboardId(z1.getId());
		File file = DAOSystemSettings.loadSystemSettings().getLogo();
		paste12.setId(DAOPaste.store(paste12, true).getFirst());
		file.setFilename("scythe.png");
		file.setPasteId(paste12.getId());
		DAOPaste.storeFile(file);

		Paste paste13 = new Paste();
		paste13.setTitle("PasteZ13");
		paste13.setCreator(user1);
		paste13.setCreationDate(new Date(0));
		paste13.setLastModifiedDate(new Date(0));
		paste13.setExpirationDate(new Date(1000));
		paste13.setDescription("Das ist ein Bild");
		paste13.setContent("Das ist ein Bild");
		paste13.setClipboardId(z1.getId());
		file = DAOSystemSettings.loadSystemSettings().getLogo();
		file.setFilename("scythe.png");
		file.setPasteId(paste12.getId());
		paste13.setId(DAOPaste.store(paste13, true).getFirst());
		file.setPasteId(paste13.getId());
		file.setOwner(user.getId());
		DAOPaste.storeFile(file);

		Paste pastez2 = new Paste();
		pastez2.setTitle("PasteZ2");
		pastez2.setCreator(user1);
		pastez2.setCreationDate(new Date(0));
		pastez2.setLastModifiedDate(new Date(0));
		pastez2.setExpirationDate(new Date(1000));
		pastez2.setDescription("Goodbye world");
		pastez2.setContent("Goodbye world");
		pastez2.setClipboardId(z1.getId());
		pastez2.setId(DAOPaste.store(pastez2, true).getFirst());

		Paste pastez1 = new Paste();
		pastez1.setTitle("Pastez1");
		pastez1.setCreator(user1);
		pastez1.setCreationDate(new Date(0));
		pastez1.setLastModifiedDate(new Date(0));
		pastez1.setExpirationDate(new Date(1000));
		pastez1.setDescription("This is Zwischenablage 1");
		pastez1.setContent("This is Zwischenablage 1");
		pastez1.setClipboardId(z4.getId());
		pastez1.setId(DAOPaste.store(pastez1, true).getFirst());

		Paste pasteP = new Paste();
		pasteP.setTitle("Privat");
		pasteP.setCreator(user1);
		pasteP.setCreationDate(new Date(0));
		pasteP.setLastModifiedDate(new Date(0));
		pasteP.setExpirationDate(new Date(1000));
		pasteP.setDescription("Privat");
		pasteP.setContent("Privat");
		pasteP.setClipboardId(zprivate.getId());
		pasteP.setId(DAOPaste.store(pasteP, true).getFirst());

		Paste pasteG = new Paste();
		pasteG.setTitle("Hello group");
		pasteG.setCreator(user1);
		pasteG.setCreationDate(new Date(0));
		pasteG.setLastModifiedDate(new Date(0));
		pasteG.setExpirationDate(new Date(1000));
		pasteG.setDescription("Privat");
		pasteG.setContent("Privat");
		pasteG.setClipboardId(zgroup.getId());
		pasteG.setId(DAOPaste.store(pasteG, true).getFirst());

		// --------- comment
		Comment comment = new Comment();
		comment.setContent("Hello");
		comment.setBelongsTo(user);
		comment.setCreationDate(new java.util.Date());
		comment.setCreator(admin);
		comment.setDescription("Hello");
		comment.setTitle("Hello");
		DAOComment.store(comment, "User");

		// ----------- relations
		List<String> user3L = new ArrayList<String>();
		user3L.add(user3.getName());
		DAOPermission.assignClipboardUsersAccess(z1.getId(), user3L,
				Access.WRITE);
		DAOPermission.assignGroupUsersAccess(testGruppeP.getId(), user3L,
				Access.READ);

		DAOGroup.addUser(user.getId(), testGruppe.getId());
		DAOGroup.addUser(privat.getId(), testGruppeP.getId());

	}

	public static void dropAllTables(ConnectionWrapper con)
			throws DataSourceException {

		List<String> tables = Arrays.asList("rclipboardcomments",
				"rgroupaccessclipboard", "rgroupcomments",
				"rinvitetokenclipboard", "rinvitetokengroup", "rpastecomments",
				"ruseraccessclipboard", "ruseraccessgroup",
				"ruserbookmarkclipboard", "ruserbookmarkpaste", "rusercomments",
				"rusergroup", "ruserkeyword", "ruserreportcomment",
				"tclipboards", "tcomments", "tfiles", "tgroups", "tkeywords",
				"tpastes", "troles", "tsystem", "tusers", "tusers2");

		StringBuilder sb = new StringBuilder();
		for (String table : tables) {
			sb.append("DROP TABLE IF EXISTS " + table + " CASCADE;");
		}

		try (PreparedStatementWrapper ps = new PreparedStatementWrapper(
				con.prepareStatement(sb.toString()));) {
			ps.executeUpdate();
		}
	}

	public static void resetDb() throws DataSourceException {
		logger.info("Starting DB reset!");
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());) {
			TableInitializer.dropAllTables(cw);
			TableInitializer.createAllTables(cw);
			TableInitializer.createTestValues();
		}
		logger.info("DB reset complete!");
	}

	public static void main(String[] args) throws DataSourceException {
		resetDb();
	}
}
