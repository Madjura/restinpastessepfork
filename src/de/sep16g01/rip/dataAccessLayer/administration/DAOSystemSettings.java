package de.sep16g01.rip.dataAccessLayer.administration;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.administration.SystemSettings;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;

/**
 * DAO for the system settings. Handles storing and loading the system settings
 * to and from the database.
 */
public class DAOSystemSettings {

	/**
	 * Loads the system settings from the database.
	 * 
	 * @return The system settings as
	 *         {@link de.sep16g01.rip.businessLogicLayer.administration.SystemSettings
	 *         SystemSettings} object.
	 * @throws DataSourceException
	 *             When the database is down.
	 */

	private final transient static Logger logger = Logger
			.getLogger(DAOSystemSettings.class);

	/**
	 * Loads SystemSettings from the database.
	 * 
	 * @return SystemSettings object
	 * @throws DataSourceException on error
	 */
	public static SystemSettings loadSystemSettings()
			throws DataSourceException {
		String query = "SELECT name, logo, impressum, AGB FROM TSystem WHERE id = 1;";
		SystemSettings settings = new SystemSettings();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				if (result.next()) {
					if (result.getString(1) != null) {
						settings.setName(result.getString(1));
					}
					if (result.getString(2) != null) {
						settings.setLogo(new File(result.getBytes(2)));
					}

					if (result.getString(3) != null) {
						settings.setImprint(result.getString(3));
					}

					if (result.getString(4) != null) {
						settings.setTermsOfService(result.getString(4));
					}
				}
			}
		}
		return settings;
	}

	/**
	 * Retrieve system name from database.
	 * 
	 * @return the system name
	 * @throws DataSourceException on error
	 */
	public static String loadSystemName() throws DataSourceException {
		String query = "SELECT name FROM TSystem WHERE id = 1;";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				if (result.next()) {
					return result.getString(1);
				}
			}
		}
		return "";
	}

	/**
	 * Stores a
	 * {@link de.sep16g01.rip.businessLogicLayer.administration.SystemSettings
	 * SystemSettings} object to the database.
	 * 
	 * @param settings
	 *            The object that is to be stored.
	 * @throws DataSourceException
	 *             When the database is down.
	 */
	public static void store(SystemSettings settings)
			throws DataSourceException {
		String query = "UPDATE TSystem SET (name, logo, impressum, AGB) = (?, ?, ?, ?) WHERE id = 1";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, settings.getName());
			if (settings.getLogo() != null) {
				ps.setBytes(2, settings.getLogo().getContent());
			} else {
				ps.setBytes(2, null);
			}
			ps.setString(3, settings.getImprint());
			ps.setString(4, settings.getTermsOfService());
			int inserted = ps.executeUpdate();
			if (inserted == 0) {
				logger.warn("no System Settings inserted");
			}
		}

	}

	/**
	 * Get the max allowed filesize for all usergrups from the database.
	 * 
	 * Filesize in kilobytes (!)
	 * 
	 * @return a map mapping max filesize to user roles
	 * @throws DataSourceException on error
	 */
	public static HashMap<Role, Long> loadMaxFileSizePerRole()
			throws DataSourceException {
		String query = "SELECT role, singleFileSizeLimit FROM TRoles";
		HashMap<Role, Long> limits = new HashMap<Role, Long>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				while (result.next()) {
					limits.put(Role.getById(result.getInt(1)),
							result.getLong(2));
				}
			}

		}
		return limits;

	}

	/**
	 * Get the max allowed total filesize usage per user for a user role.
	 * 
	 * Filesize in kilobytes (!)
	 * 
	 * @return a map mapping max total filesize to user roles
	 * @throws DataSourceException on error
	 */
	public static HashMap<Role, Long> loadMaxSizePerRole()
			throws DataSourceException {
		String query = "SELECT role, fileSizeLimit FROM TRoles";
		HashMap<Role, Long> limits = new HashMap<Role, Long>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				while (result.next()) {
					limits.put(Role.getById(result.getInt(1)),
							result.getLong(2));
				}
			}

		}
		return limits;
	}

	/**
	 * Loads the max expiration date for pastes, clipboards a user of certain role
	 * may set. 
	 * 
	 * Stored as a string to be parsed by @{TimeSpanValidator.parseExpirationDateFromString}.
	 * 
	 * @return map mapping a user role to max expiration strings
	 * @throws DataSourceException on error
	 */
	public static HashMap<Role, String> loadMaxExpirationDurationPerRole()
			throws DataSourceException {
		String query = "SELECT role, maxExpirationDuration FROM TRoles";
		HashMap<Role, String> durations = new HashMap<Role, String>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				while (result.next()) {
					durations.put(Role.getById(result.getInt(1)),
							result.getString(2));
				}
			}

		}
		return durations;

	}

	/**
	 * Stores the max expiration date for pastes, clipboards a user of certain role
	 * may set. 
	 * 
	 * Stored as a string to be parsed by @{TimeSpanValidator.parseExpirationDateFromString}.
	 * 
	 * @return map mapping a user role to max expiration strings
	 * @throws DataSourceException on error
	 */
	public static void storeMaxExpirationDurationPerRole(
			HashMap<Role, String> newDurations) throws DataSourceException {
		String query = "BEGIN;"
				+ "UPDATE TRoles SET maxExpirationDuration = ? WHERE role = ?;"
				+ "UPDATE TRoles SET maxExpirationDuration = ? WHERE role = ?;"
				+ "UPDATE TRoles SET maxExpirationDuration = ? WHERE role = ?;"
				+ "UPDATE TRoles SET maxExpirationDuration = ? WHERE role = ?;"
				+ "COMMIT;";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			int i = 0;

			for (Map.Entry<Role, String> entry : newDurations.entrySet()) {
				ps.setString((i * 2) + 1, entry.getValue());
				ps.setInt((i * 2) + 2, entry.getKey().getPriority());
				i += 1;
			}
			ps.executeUpdate();
		}
	}

	/**
	 * Set a total filsize limit for all user roles.
	 * 
	 * Filesize in kilobytes (!).
	 * 
	 * @param limit size in kilobytes
	 * @throws DataSourceException on error
	 */
	public static void storeFileSizeLimit(long limit)
			throws DataSourceException {
		String query = "BEGIN;"
				+ "UPDATE TRoles SET fileSizeLimit = ? WHERE role = 1;"
				+ "UPDATE TRoles SET fileSizeLimit = ? WHERE role = 2;"
				+ "UPDATE TRoles SET fileSizeLimit = ? WHERE role = 3;"
				+ "UPDATE TRoles SET fileSizeLimit = ? WHERE role = 4;"
				+ "COMMIT;";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setLong(1, limit);
			ps.setLong(2, limit);
			ps.setLong(3, limit);
			ps.setLong(4, limit);
			ps.executeUpdate();
		}
	}

	/**
	 * Set a filesize limit for single files for all user roles.
	 * 
	 * Filesize in kilobytes(!)
	 * 
	 * @param limit size in kilobytes
	 * @throws DataSourceException on error
	 */
	public static void storeSingleFileSizeLimit(long limit)
			throws DataSourceException {
		String query = "BEGIN;"
				+ "UPDATE TRoles SET singleFileSizeLimit = ? WHERE role = 1;"
				+ "UPDATE TRoles SET singleFileSizeLimit = ? WHERE role = 2;"
				+ "UPDATE TRoles SET singleFileSizeLimit = ? WHERE role = 3;"
				+ "UPDATE TRoles SET singleFileSizeLimit = ? WHERE role = 4;"
				+ "COMMIT;";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setLong(1, limit);
			ps.setLong(2, limit);
			ps.setLong(3, limit);
			ps.setLong(4, limit);
			ps.executeUpdate();
		}
	}
}