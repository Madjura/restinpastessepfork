package de.sep16g01.rip.dataAccessLayer.email;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.clipboard.Clipboard;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.permissions.Access;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;

/**
 * DAO for the tokens of emails. Responsible for storing and loading tokens to
 * and from the database.
 */
public class DAOEmailToken {

	/**
	 * Logger instance for this class.
	 */
	private final transient static Logger logger = Logger
			.getLogger(DAOEmailToken.class);

	/**
	 * Stores the token of a registration confirmation email.
	 * 
	 * @param id
	 *            The token.
	 * @param user
	 *            The user that the token belongs to.
	 * @throws DataSourceException
	 */
	public static void storeRegistrationToken(UUID id, String user)
			throws DataSourceException {
		String query = "UPDATE TUsers set confirmationToken = ? WHERE name = ?";
		// try with resource to close statement
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, id.toString());
			ps.setString(2, user);
			// check affected rows and log unusual behaviour
			int rows = ps.executeUpdate();

			// no rows means somehow registration failed to store user to
			// database
			if (rows == 0) {
				logger.warn("Unable to insert token for user with name " + user
						+ ".");
			}
		}
	}

	/**
	 * Stores the token of a clipboard invite email.
	 * 
	 * @param id
	 *            The token.
	 * @param clipboard
	 *            The user that the token belongs to.
	 * @throws DataSourceException
	 */
	public static void storeClipboardToken(UUID id, Clipboard clipboard,
			Access access) throws DataSourceException {
		String query = "INSERT INTO RInviteTokenClipboard (clipboardId, token, "
				+ "accessId) VALUES (?, ?, ?)";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, clipboard.getId());
			ps.setString(2, id.toString());
			ps.setInt(3, access.getPriority());
			ps.executeUpdate();
		}
	}

	/**
	 * Stores the token of a group invite email.
	 * 
	 * @param id
	 *            The token.
	 * @param group
	 *            The user that the token belongs to.
	 * @throws DataSourceException
	 */
	public static void storeGroupToken(UUID id, Group group, Access access)
			throws DataSourceException {
		String query = "INSERT INTO RInviteTokenGroup (groupId, token, "
				+ "accessId) VALUES (?, ?, ?)";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, group.getId());
			ps.setString(2, id.toString());
			ps.setInt(3, access.getPriority());
			ps.executeUpdate();
		}
	}

	/**
	 * Stores the token of an email change email.
	 * 
	 * @param id
	 *            The token.
	 * @param user
	 *            The user the token belongs to.
	 * @throws DataSourceException
	 */
	public static void storeEmailChangeToken(UUID id, String user,
			String newEmail) throws DataSourceException {

		String query = "UPDATE TUsers set emailConfirmationToken = ?, "
				+ "newEmail = ? WHERE name = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, id.toString()); // token
			ps.setString(2, newEmail); // email
			ps.setString(3, user); // name
			ps.executeUpdate();
		}
	}

	/**
	 * Stores the token of a password reset email.
	 * 
	 * @param id
	 *            The token.
	 * @param user
	 *            The user the token belongs to.
	 * @throws DataSourceException
	 */
	public static void storePasswordResetToken(UUID id, String user)
			throws DataSourceException {
		String query = "UPDATE TUsers set emailConfirmationToken = ? WHERE name = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {

			ps.setString(1, id.toString());
			ps.setString(2, user);
			// check affected rows and log unusual behaviour
			int rows = ps.executeUpdate();

			// no rows means somehow registration failed to store user to
			// database
			if (rows == 0) {
				logger.warn("Unable to insert token for user with name " + user
						+ ".");
			}
		}
	}

	/**
	 * Checks the validity of a registration confirmation token.
	 * 
	 * @param id
	 *            The token.
	 * @param user
	 *            The user that the token belongs to.
	 * @return {@code true} if the token is valid, {@code false} if it is not.
	 * @throws DataSourceException
	 * @throws SQLException
	 */
	public static boolean checkTokenRegistration(UUID id, String user)
			throws DataSourceException {
		String query = "SELECT confirmationToken FROM TUsers WHERE name = ?";
		// take token from database and compare
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, user);

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				String dbId = null;
				while (rs.next()) {
					dbId = rs.getString(1);
				}
				return id.toString().equals(dbId);
			}
		}
	}

	/**
	 * Removes a registration token from a user. Called after the token has been
	 * validated.
	 * 
	 * @param user
	 *            The user who has been validated.
	 * @throws DataSourceException
	 */
	public static void removeRegistrationToken(String user)
			throws DataSourceException {
		String query = "UPDATE TUsers set confirmationToken = ? WHERE name = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, null);
			ps.setString(2, user);
			ps.executeUpdate();
		}
	}

	/**
	 * Checks the validity of a clipboard invite token.
	 * 
	 * @param id
	 *            The token.
	 * @param clipboard
	 *            The clipboard the token belongs to.
	 * @return {@code true} if the token is valid, {@code false} if it is not.
	 * @throws DataSourceException
	 */
	public static Access checkTokenClipboard(UUID id, int clipboardId,
			boolean removeToken) throws DataSourceException {
		String query = "SELECT clipboardId, token, accessId "
				+ "FROM RInviteTokenClipboard WHERE clipboardId = ?";

		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, clipboardId);

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					if (rs.getString(2).equals(id.toString())) {
						if (removeToken) {
							String remove = "DELETE FROM RInviteTokenGroup WHERE token = ?";
							Connection connection2 = DBPool.getInstance()
									.getConnection();
							try (PreparedStatementWrapper ps2 = new PreparedStatementWrapper(
									cw.prepareStatement(remove));) {
								ps2.setString(1, id.toString());
								if (ps2.executeUpdate() != 1) {
									logger.warn("More than one token (or none) was deleted "
											+ "for token " + id.toString());

								}
							} finally {
								DBPool.getInstance().returnConnection(
										connection2);
							}
						}
						return Access.getById(rs.getInt(3));
					}
				}
			}
		}
		return null;
	}

	/**
	 * Checks the validity of a group invite token.
	 * 
	 * @param id
	 *            The token.
	 * @param group
	 *            The group the token belongs to.
	 * @return The access that is being given if the token is valid, otherwise
	 *         null.
	 * @throws DataSourceException
	 */
	public static Access checkTokenGroup(UUID id, int groupId,
			boolean removeToken) throws DataSourceException {

		String query = "SELECT groupId, token, accessId FROM RInviteTokenGroup "
				+ "WHERE groupId = ?";

		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, groupId);
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					if (rs.getString(2).equals(id.toString())) {
						if (removeToken) {
							String remove = "DELETE FROM RInviteTokenGroup WHERE token = ?";
							Connection connection2 = DBPool.getInstance()
									.getConnection();
							try (PreparedStatementWrapper ps2 = new PreparedStatementWrapper(
									cw.prepareStatement(remove));) {
								ps2.setString(1, id.toString());
								if (ps2.executeUpdate() != 1) {
									logger.warn("More than one (or none) token was deleted "
											+ "for token " + id.toString());
								}
							} finally {
								DBPool.getInstance().returnConnection(
										connection2);
							}
						}
						return Access.getById(rs.getInt(3));
					}
				}
			}
			return null;
		}
	}

	/**
	 * Checks the validity of a user email change token.
	 * 
	 * @param id
	 *            The token.
	 * @param user
	 *            The user that changed the email.
	 * @return {@code true} if the token is valid, {@code false} if it is not.
	 * @throws DataSourceException
	 * @throws SQLException
	 */
	public static boolean checkEmailChangeToken(UUID id, String user)
			throws DataSourceException {
		String query = "SELECT emailConfirmationToken FROM TUsers "
				+ "WHERE name = ?";

		// take token from database and compare
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, user);
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				String dbId = null;
				while (rs.next()) {
					dbId = rs.getString(1);
				}
				return id.toString().equals(dbId);
			}
		}
	}

	/**
	 * Checks the validity of a user password reset token.
	 * 
	 * @param id
	 *            The token.
	 * @param user
	 *            The user that wants to reset the password.
	 * @return {@code true} if the token is valid, {@code false} if it is not.
	 * @throws DataSourceException
	 * @throws SQLException
	 */
	public static boolean checkPasswordResetToken(UUID id, String user)
			throws DataSourceException {
		String query = "SELECT emailConfirmationToken FROM TUsers WHERE name = ?";

		// take token from database and compare
		boolean valid = false;
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			String dbId = null;
			ps.setString(1, user);

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					dbId = rs.getString(1);
				}
				valid = id.toString().equals(dbId);
			}

			String removeToken = "UPDATE TUsers SET emailConfirmationToken = NULL WHERE name = ?";
			if (valid) {
				try (PreparedStatementWrapper update = new PreparedStatementWrapper(
						cw.prepareStatement(removeToken));) {
					update.setString(1, user);
					if (update.executeUpdate() == 0) {
						logger.warn("Unable to remove token for user with name "
								+ user);
					}
				}
			}
			return valid;
		}
	}

	/**
	 * Checks whether or not a user has a registration token active.
	 * 
	 * @param user
	 *            The name of the user being checked.
	 * @return True if the user has a token (which means he is unconfirmed),
	 *         false if he does not have a token.
	 * @throws DataSourceException
	 */
	public static boolean hasRegistrationToken(String user)
			throws DataSourceException {
		String query = "SELECT confirmationToken FROM TUsers WHERE name = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, user);

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				rs.next();
				return !(rs.getString(1) == null);
			}
		}
	}

	/**
	 * Updates the email of a user when they confirm their new email.
	 * 
	 * @param user
	 * @throws DataSourceException
	 */
	public static void updateEmail(String user) throws DataSourceException {
		String query = "SELECT newEmail FROM TUsers WHERE name = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance()
				.getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, user);
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				rs.next();
				String newEmail = rs.getString(1);

				String update = "UPDATE TUsers SET email = ?, "
						+ "emailConfirmationToken = NULL, newEmail = NULL "
						+ "WHERE name = ?";
				try (PreparedStatementWrapper ps2 = new PreparedStatementWrapper(
						cw.prepareStatement(update));) {
					ps2.setString(1, newEmail);
					ps2.setString(2, user);
					ps2.executeUpdate();
				}
			}
		}
	}
}
