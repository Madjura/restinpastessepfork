package de.sep16g01.rip.dataAccessLayer.group;

import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.group.Group;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.permissions.Visibility;
import de.sep16g01.rip.businessLogicLayer.util.QueryParser;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultTable;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.dataAccessLayer.util.DBUtils;

/**
 * DAO for the groups. Handles storing and loading groups to and from the
 * database.
 */
public class DAOGroup {

	private final transient static Logger logger = Logger
			.getLogger(DAOGroup.class);

	/**
	 * Stores a group to the database.
	 * 
	 * @param group
	 *            The group.
	 * @throws DataSourceException
	 *             When the database is down.
	 */
	public static int store(Group group) throws DataSourceException {

		boolean exists = group.getId() != SystemElement.DEFAULT_ID;

		// update query based on whether or not group is being created/updated
		String query;

		if (!exists) {
			query = "INSERT INTO TGroups (title, creationDate, "
					+ "lastModificationDate, description, visibilityRead, "
					+ "visibilityWrite) VALUES (?, ?, ?, ?, ?, ?)";
		} else {
			query = "UPDATE TGroups SET title = ?, creationDate = ?, "
					+ "lastModificationDate = ?, description = ?, "
					+ "visibilityRead = ?, visibilityWrite = ? WHERE id = ?";
		}

		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps2 = new PreparedStatementWrapper(
						cw.prepareStatement(query,
								Statement.RETURN_GENERATED_KEYS));) {

			ps2.setString(1, group.getTitle()); // title
			ps2.setTimestamp(2,
					new Timestamp(group.getCreationDate().getTime())); // creation
			ps2.setTimestamp(3,
					new Timestamp(group.getLastModifiedDate().getTime())); // modifiy
			ps2.setString(4, group.getDescription()); // description

			ps2.setInt(5, group.getVisibilityRead().getId()); // read
			ps2.setInt(6, group.getVisibilityWrite().getId()); // write

			// update entry
			if (exists) {
				ps2.setInt(7, group.getId());
			}

			ps2.executeUpdate();
			try (ResultSetWrapper keys = ps2.getGeneratedKeys()) {
				keys.next();
				return keys.getInt(1); // return ID of new element
			}
		}
	}

	/**
	 * Returns a group from the database based in ID.
	 * 
	 * @param id
	 *            The ID of the group.
	 * @return The group with the ID.
	 * @throws DataSourceException
	 *             When the database is down.
	 */
	public static Group get(int id) throws DataSourceException {
		String query = "SELECT title, creationDate, lastModificationDate, "
				+ "description, visibilityRead, visibilityWrite "
				+ "FROM TGroups WHERE id = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, id);

			try (ResultSetWrapper rs = new ResultSetWrapper(
					ps.executeQuery())) {
				if (rs.next()) {
					Group group = new Group();
					group.setTitle(rs.getString(1));
					group.setCreationDate(rs.getDate(2));
					group.setLastModifiedDate(rs.getDate(3));
					group.setDescription(rs.getString(4));
					group.setVisibilityRead(Visibility.getById(rs.getInt(5)));
					group.setVisibilityWrite(Visibility.getById(rs.getInt(6)));
					group.setId(id);
					return group;
				}
			}
		}
		return null;
	}

	public static PaginatedDataModel<Group> paginate(String query, int page,
			int entriesPerPage) throws DataSourceException {
		String sql = "";
		List<Group> groups = new ArrayList<Group>();
		Map<String, String> queryValues = QueryParser
				.parsePaginationQuery(query);
		String[] searchTerms = null;
		if (queryValues.get("type").equals("content")) {

			if (queryValues.get("value") != null) {
				int userId = Integer.parseInt(queryValues.get("userId"));
				if (DAOUser.getById(userId).getRole()
						.getPriority() >= Role.MODERATOR.getPriority()) {
					return DAOGroup.paginateAdmin(query, page, entriesPerPage,
							queryValues);
				}

				searchTerms = queryValues.get("value")
						.split(QueryParser.DELIMITER_QUERY);
				sql = "SELECT id, title, creationDate, lastModificationDate, "
						+ "description, visibilityRead, visibilityWrite, count(*) OVER() AS full_count FROM TGroups "
						+ "WHERE  ("

						+ "("
						+ QueryParser.createWHEREQuery(searchTerms.length,
								"title")
						+ " OR  "
						+ QueryParser.createWHEREQuery(searchTerms.length,
								"description")
						+ ")" + "        AND ("
						+ "                visibilityRead != 4"
						+ "                OR"
						+ "                TGroups.id IN (SELECT groupId FROM "
						+ "RUserGroup WHERE userId = ?)" + "        )"
						+ "        AND ("
						+ "                visibilityRead != 3"
						+ "               OR"
						+ "                ? IN (SELECT id FROM TUsers WHERE id = ?)"
						+ "        )" + "        AND ("
						+ "                visibilityRead != 1"
						+ "                OR"
						+ "                ? IN (SELECT userId from "
						+ "RUserAccessGroup WHERE userId = ? AND groupId = TGroups.id)"
						+ "        )" + ") LIMIT ? OFFSET ?";
			} else {
				sql = "SELECT id, title, creationDate, lastModificationDate, "
						+ "description, visibilityRead, visibilityWrite FROM TGroups "
						+ "WHERE  ( (" + "                visibilityRead != 4"
						+ "                OR"
						+ "                TGroups.id IN (SELECT groupId FROM "
						+ "RUserGroup WHERE userId = ?)" + "        )"
						+ "        AND ("
						+ "                visibilityRead != 3"
						+ "               OR"
						+ "                ? IN (SELECT id FROM TUsers WHERE id = ?)"
						+ "        )" + "        AND ("
						+ "                visibilityRead != 1"
						+ "                OR"
						+ "                ? IN (SELECT userId from "
						+ "RUserAccessGroup WHERE userId = ? AND groupId = TGroups.id)"
						+ "        )" + ") LIMIT ? OFFSET ?";
			}

		}

		// select groups matching the search query
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(sql));) {
			int totalCount = -1;
			if (queryValues.get("type").equals("content")) {
				int afterSearchIndex = 1;
				if (searchTerms != null) {
					for (int i = 1; i <= searchTerms.length * 2; i++) {
						ps.setString(i,
								"%" + searchTerms[(i - 1) % searchTerms.length]
										+ "%");
						afterSearchIndex = i + 1;
					}
				}
				for (int i = afterSearchIndex; i < afterSearchIndex + 5; i++) {
					ps.setInt(i, Integer.parseInt(queryValues.get("userId")));
				}
				ps.setInt(afterSearchIndex + 5, entriesPerPage);
				ps.setInt(afterSearchIndex + 6,
						page * entriesPerPage - entriesPerPage);
				try (ResultSetWrapper rs = new ResultSetWrapper(
						ps.executeQuery())) {

					// fill the group objects
					while (rs.next()) {
						Group group = new Group();
						group.setId(rs.getInt(1));
						group.setTitle(rs.getString(2));
						group.setCreationDate(rs.getDate(3));
						group.setLastModifiedDate(rs.getDate(4));
						group.setDescription(rs.getString(5));
						group.setVisibilityRead(
								Visibility.getById(rs.getInt(6)));
						group.setVisibilityWrite(
								Visibility.getById(rs.getInt(7)));
						groups.add(group);
						totalCount = rs.getInt(8);
					}
				}
				PaginatedDataModel<Group> pdm = new PaginatedDataModel<Group>(
						page, totalCount, groups);
				return pdm;
			}
		}
		return null;
	}

	/**
	 * Pagination for admins/moderators, gets all groups regardless of
	 * visibility/permissions.
	 * 
	 * @param query
	 *            The query of the pagination.
	 * @param page
	 *            The page of the pagination.
	 * @param entriesPerPage
	 *            How many entries per page.
	 * @param queryValues
	 *            The values of the query.
	 * @return A PaginatedDataModel of all clipboards.
	 * @throws DataSourceException
	 */
	private static PaginatedDataModel<Group> paginateAdmin(String query,
			int page, int entriesPerPage, Map<String, String> queryValues)
			throws DataSourceException {
		List<Group> groups = new ArrayList<Group>();
		String[] searchTerms = queryValues.get("value")
				.split(QueryParser.DELIMITER_QUERY);
		String sql = "SELECT id, title, creationDate, lastModificationDate, "
				+ "description, visibilityRead, visibilityWrite, count(*) OVER() AS full_count FROM TGroups "
				+ "WHERE  ("
				+ QueryParser.createWHEREQuery(searchTerms.length, "title")
				+ " OR  " + QueryParser.createWHEREQuery(searchTerms.length,
						"description")
				+ ")  LIMIT ? OFFSET ? ";

		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(sql));) {
			int totalCount = -1;
			if (queryValues.get("type").equals("content")) {
				int afterSearchIndex = 1;
				for (int i = 1; i <= searchTerms.length * 2; i++) {
					ps.setString(i, "%"
							+ searchTerms[(i - 1) % searchTerms.length] + "%");
					afterSearchIndex = i + 1;
				}
				ps.setInt(afterSearchIndex, entriesPerPage);
				ps.setInt(afterSearchIndex + 1,
						page * entriesPerPage - entriesPerPage);

				try (ResultSetWrapper rs = new ResultSetWrapper(
						ps.executeQuery())) {

					// fill the group objects
					while (rs.next()) {
						Group group = new Group();
						group.setId(rs.getInt(1));
						group.setTitle(rs.getString(2));
						group.setCreationDate(rs.getDate(3));
						group.setLastModifiedDate(rs.getDate(4));
						group.setDescription(rs.getString(5));
						group.setVisibilityRead(
								Visibility.getById(rs.getInt(6)));
						group.setVisibilityWrite(
								Visibility.getById(rs.getInt(7)));
						groups.add(group);
						totalCount = rs.getInt(8);
					}
				}
				PaginatedDataModel<Group> pdm = new PaginatedDataModel<Group>(
						page, totalCount, groups);
				return pdm;
			}
		}

		return null;
	}

	/**
	 * Adds a user to a group.
	 * 
	 * @param userId
	 *            The id of the user.
	 * @param groupId
	 *            The id of the group.
	 * @throws DataSourceException
	 */
	public static void addUser(int userId, int groupId)
			throws DataSourceException {
		String query = "INSERT INTO RUserGroup (userId, groupId) VALUES (?, ?)";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, groupId);
			ps.executeUpdate();
		}
	}

	/**
	 * Removes a user from a group.
	 * 
	 * @param userId
	 *            The id of the user.
	 * @param groupId
	 *            The id of the group.
	 * @throws DataSourceException
	 */
	public static void removeUser(int userId, int groupId)
			throws DataSourceException {
		String query = "DELETE FROM RUserGroup WHERE userId = ? AND groupId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, groupId);
			ps.executeUpdate();
		}
	}

	/**
	 * Checks the membership of a user in a group.
	 * 
	 * @param userId
	 *            The id of the user.
	 * @param groupId
	 *            The id of the group.
	 * @return True if the user is in the group, false if not.
	 * @throws DataSourceException
	 */
	public static boolean checkMembership(int userId, int groupId)
			throws DataSourceException {
		String query = "SELECT COUNT(*) FROM RUserGroup WHERE "
				+ "userId = ? AND groupId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, userId);
			ps.setInt(2, groupId);
			try (ResultSetWrapper rs = new ResultSetWrapper(
					ps.executeQuery())) {
				while (rs.next()) {
					int result = rs.getInt(1);
					if (result >= 1) {
						return true;
					}
				}
				return false;
			}
		}
	}

	/**
	 * Drops a group from the groups table.
	 * 
	 * @param id
	 *            The id of the group.
	 * @throws DataSourceException
	 */
	public static void dropGroup(String name) throws DataSourceException {
		String query = "DELETE FROM TGroups WHERE title = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, name);
			if (ps.executeUpdate() == 0) {
				logger.warn("Non-existing group was attempted to be dropped. "
						+ "Name: " + name);
			}
		}
	}

	/**
	 * Returns the number of members in a group.
	 * 
	 * @param groupId
	 *            The id of the group.
	 * @return The number of members or -1 if the group does not exist.
	 * @throws DataSourceException
	 */
	public static int getMemberCount(int groupId) throws DataSourceException {
		String query = "SELECT COUNT(*) FROM RUserGroup WHERE groupId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, groupId);
			try (ResultSetWrapper rs = new ResultSetWrapper(
					ps.executeQuery())) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			}
		}
		return -1;
	}

	/**
	 * Returns a group by name. Used in testing only.
	 * 
	 * @param name
	 *            The name of the group.
	 * @return The group.
	 * @throws DataSourceException
	 */
	public static Group getGroupByName(String name) throws DataSourceException {
		String query = "SELECT id FROM TGroups WHERE title = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setString(1, name);

			try (ResultSetWrapper rs = new ResultSetWrapper(
					ps.executeQuery())) {
				if (rs.next()) {
					return DAOGroup.get(rs.getInt(1));
				}
				return null;
			}
		}
	}

	/**
	 * Checks if a given group already exists.
	 * 
	 * @param id
	 *            the group
	 * @return true if the groupid exists, false otherwise
	 * @throws DataSourceException
	 */
	public static boolean checkExistance(int id) throws DataSourceException {
		String query = "SELECT 1 FROM TGroups WHERE id = ?";
		ResultTable resultTable = DBUtils.executeQuery(query, id);
		return resultTable.size() == 1;
	}
}
