package de.sep16g01.rip.dataAccessLayer.paste;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.general.SystemElement;
import de.sep16g01.rip.businessLogicLayer.pagination.PaginatedDataModel;
import de.sep16g01.rip.businessLogicLayer.paste.File;
import de.sep16g01.rip.businessLogicLayer.paste.Paste;
import de.sep16g01.rip.businessLogicLayer.permissions.Role;
import de.sep16g01.rip.businessLogicLayer.util.Pair;
import de.sep16g01.rip.businessLogicLayer.util.QueryParser;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultTable;
import de.sep16g01.rip.dataAccessLayer.profile.DAOUser;
import de.sep16g01.rip.dataAccessLayer.util.DBUtils;

/**
 * DAO for the pastes. Handles storing and loading pastes to and from the
 * database.
 */
public class DAOPaste {

	private final transient static Logger logger = Logger
			.getLogger(DAOPaste.class);

	/**
	 * Stores a paste to the database.
	 * 
	 * @param paste
	 *            The paste being stored.
	 * @param ignoreUpdate
	 *            Whether or not interim changes are to be ignored.
	 * @return The id of the inserted paste, or the id of the paste if the paste
	 *         already existed and no new entry was created, or -1 if insertion
	 *         failed.
	 * @throws DataSourceException
	 */
	public static Pair<Integer, Boolean> store(Paste paste,
			boolean ignoreUpdate) throws DataSourceException {

		boolean alreadyExists = paste.getId() != SystemElement.DEFAULT_ID;

		String query;
		// pastes exists, update

		Paste checkPaste = DAOPaste.getById(paste.getId());

		if (alreadyExists) {
			if (checkPaste.getLastModifiedDate()
					.after(paste.getLastModifiedDate()) && !ignoreUpdate) {
				return new Pair<Integer, Boolean>(checkPaste.getId(), true);
			}

			query = "UPDATE TPastes SET title = ?, creatorId = ?, "
					+ "creationDate = ?,"
					+ " lastModificationDate = ?, expirationDate = ?, description = ?, "
					+ "content = ?, clipboardId = ?, notify = ? WHERE id = ?";
			// paste does not exist, insert
		} else {
			query = "INSERT INTO TPastes (title, creatorId, "
					+ " creationDate, lastModificationDate, "
					+ " expirationDate, description, content, clipboardId, notify)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		}
		paste.setLastModifiedDate(new Timestamp(new Date().getTime()));
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper psInsert = new PreparedStatementWrapper(
						cw.prepareStatement(query,
								Statement.RETURN_GENERATED_KEYS));) {
			psInsert.setString(1, paste.getTitle());
			psInsert.setInt(2, paste.getCreator().getId());
			psInsert.setTimestamp(3,
					new Timestamp(paste.getCreationDate().getTime()));
			psInsert.setTimestamp(4,
					new Timestamp(paste.getLastModifiedDate().getTime()));
			psInsert.setTimestamp(5,
					new Timestamp(paste.getExpirationDate().getTime()));
			psInsert.setString(6, paste.getDescription());
			if (paste.getContent() != null) {
				psInsert.setString(7, paste.getContent());
			}
			psInsert.setInt(8, paste.getClipboardId());
			psInsert.setBoolean(9, paste.getNotify());

			if (alreadyExists) {
				psInsert.setInt(10, paste.getId());
			}
			psInsert.executeUpdate();
			if (alreadyExists) {
				return new Pair<Integer, Boolean>(paste.getId(), false);
			}
			try (ResultSetWrapper generatedKeys = psInsert.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					return new Pair<Integer, Boolean>(generatedKeys.getInt(1),
							false);
				}
			}
		}

		// read id if inserted
		//
		return new Pair<Integer, Boolean>(-1, false);
	}

	/**
	 * Store an uploaded file in the database.
	 * 
	 * @param file
	 *            the file to be stored
	 * @return true if the store was successful, false otherwise
	 * @throws DataSourceException
	 */
	public static boolean storeFile(File file) throws DataSourceException {
		String queryInsert = "INSERT INTO TFiles(fileName, content, filesize, pasteId, ownerId) "
				+ "VALUES (?, ?, ?, ?, ?)";
		String queryCheck = "SELECT id FROM TFiles WHERE fileName = ? "
				+ "AND content = ? AND pasteId = ? ";
		Connection connection = DBPool.getInstance().getConnection();
		boolean stored = false;
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper psCheck = new PreparedStatementWrapper(
						cw.prepareStatement(queryCheck));
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(queryInsert));) {

			psCheck.setString(1, file.getFilename());
			psCheck.setBytes(2, file.getContent());
			psCheck.setInt(3, file.getPasteId());
			ResultSet result = psCheck.executeQuery();
			if (!result.next()) {
				stored = true;
				ps.setString(1, file.getFilename());
				ps.setBytes(2, file.getContent());
				ps.setLong(3, file.getSize());
				ps.setInt(4, file.getPasteId());
				ps.setInt(5, file.getOwner());
				ps.executeUpdate();
			}
			return stored;
		} catch (SQLException e) {
			logger.fatal("Unable to store file: " + e.getMessage());
			throw new DataSourceException(e);
		} finally {
			DBPool.getInstance().returnConnection(connection);
		}
	}

	/**
	 * Retrieves a file by id.
	 * 
	 * @param fileId
	 *            the file to be retrieved.
	 * @return the file
	 * @throws DataSourceException
	 *             on error
	 */
	public static File getFile(int fileId) throws DataSourceException {
		String queryFile = "SELECT fileName as filename, content, filesize, ownerId FROM TFiles WHERE id = ? ";
		ResultTable resultTable = DBUtils.executeQuery(queryFile, fileId);
		if (resultTable.size() == 1) {
			String filename = resultTable.getDataFromFirstRow("filename",
					String.class);
			byte[] content = resultTable.getDataFromFirstRow("content",
					byte[].class);
			long size = resultTable.getDataFromFirstRow("filesize", Long.class);
			Integer owner = resultTable.getDataFromFirstRow("ownerId",
					Integer.class);
			File f = new File(content);
			f.setFilename(filename);
			f.setSize(size);
			f.setOwner(owner);
			return f;
		}
		return null;
	}

	/**
	 * Deletes a given file.
	 * 
	 * @param file
	 *            the file to delete
	 * @throws DataSourceException
	 *             on error
	 */
	public static void deleteFile(File file) throws DataSourceException {
		String queryDelete = "DELETE FROM TFiles WHERE id = ?;";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(queryDelete));) {
			ps.setInt(1, file.getId());
			if (ps.executeUpdate() == 0) {
				logger.warn("No file deleted for file " + file);
			}
		}
	}

	/**
	 * Retrieves a paste from the database.
	 * 
	 * @param id
	 *            the paste to retrieve
	 * @return the paste
	 * @throws DataSourceException
	 *             on error
	 */
	public static Paste getById(int id) throws DataSourceException {
		String query = "SELECT title, creatorId,"
				+ "creationDate, lastModificationDate, "
				+ "expirationDate, description, content,"
				+ "clipboardId, notify FROM TPastes WHERE id = ?";

		Paste paste = new Paste();
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, id);
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				if (result.next()) {
					paste.setId(id);
					paste.setTitle(result.getString(1));
					paste.setCreator(DAOUser.getCreatorById(result.getInt(2)));
					paste.setCreationDate(result.getTimestamp(3));
					paste.setLastModifiedDate(result.getTimestamp(4));
					paste.setExpirationDate(result.getTimestamp(5));
					paste.setDescription(result.getString(6));
					paste.setContent(result.getString(7));
					paste.setClipboardId(result.getInt(8));
					paste.setNotify(result.getBoolean(9));
				}
			}
		}

		String fileQuery = "SELECT id, fileName, filesize, ownerId FROM TFiles WHERE pasteId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(fileQuery));) {
			ps.setInt(1, id);
			try (ResultSetWrapper result = new ResultSetWrapper(
					ps.executeQuery())) {
				List<File> files = new ArrayList<File>();
				while (result.next()) {
					File file = new File();
					file.setId(result.getInt(1));
					file.setFilename(result.getString(2));
					file.setSize(result.getLong(3));
					file.setOwner(result.getInt(4));
					files.add(file);
				}
				paste.setFiles(files);
			}

		}
		return paste;
	}

	/**
	 * Delete a paste from the database.
	 * 
	 * @param id
	 *            the paste to be deleted
	 * @throws DataSourceException
	 *             on error
	 */
	public static void delete(int id) throws DataSourceException {
		String query = "DELETE FROM TPastes WHERE id = ?;";
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(query));) {
			ps.setInt(1, id);
			ps.executeUpdate();
		}
	}

	/**
	 * pagination helper.
	 * 
	 * @param query
	 * @param page
	 * @param entriesPerPage
	 * @return
	 * @throws DataSourceException
	 */
	public static PaginatedDataModel<Paste> paginate(String query, int page,
			int entriesPerPage) throws DataSourceException {

		String sql = "";
		List<Paste> pastes = new ArrayList<Paste>();
		Map<String, String> queryValues = QueryParser
				.parsePaginationQuery(query);
		String[] searchTerms = null;
		if (queryValues.get("type").equals("content")) {
			if (queryValues.get("value") != null) {
				int userId = Integer.parseInt(queryValues.get("userId"));
				if (DAOUser.getById(userId).getRole()
						.getPriority() >= Role.MODERATOR.getPriority()) {
					return DAOPaste.paginateAdmin(query, page, entriesPerPage,
							queryValues);
				}

				searchTerms = queryValues.get("value")
						.split(QueryParser.DELIMITER_QUERY);
				sql = "SELECT TPastes.id, TPastes.title, TPastes.creatorId, "
						+ "TPastes.creationDate, TPastes.lastModificationDate, "
						+ "TPastes.expirationDate, TPastes.description, "
						+ "TPastes.content, clipboardId, count(*) OVER() AS full_count FROM TPastes "
						+ "INNER JOIN TClipboards "
						+ "ON TPastes.clipboardId = TClipboards.id WHERE ( ( "
						+ QueryParser.createWHEREQuery(searchTerms.length,
								"TPastes.title")
						+ " ) OR ( "
						+ QueryParser.createWHEREQuery(searchTerms.length,
								"TPastes.description")
						+ " ) OR ( "
						+ QueryParser.createWHEREQuery(searchTerms.length,
								"TPastes.content")
						+ " )" + "        AND ( "
						+ "                4 != (SELECT visibilityRead FROM TClipboards WHERE TClipboards.id = TPastes.clipboardId) "
						+ "                OR "
						+ "                TClipboards.groupId IN (SELECT groupId FROM RUserGroup WHERE userId = ?) "
						+ "            ) " + "        AND ( "
						+ "                3 != (SELECT visibilityRead FROM TClipboards WHERE TClipboards.id = TPastes.clipboardId) "
						+ "                OR "
						+ "                ? IN (SELECT id FROM TUsers WHERE id = ?) "
						+ "            ) " + "        AND ( "
						+ "                1 != (SELECT visibilityRead FROM TClipboards WHERE TClipboards.id = TPastes.clipboardId) "
						+ "                OR "
						+ "                ? IN (SELECT userId FROM RUserAccessClipboard WHERE userId = ? AND clipboardId = TClipboards.id) "
						+ "            ) " + "      ) LIMIT ? OFFSET ?";
			}
		} else if (queryValues.get("type").equals("clipboard")) {
			sql = "SELECT id, title, creatorId,"
					+ "creationDate, lastModificationDate, "
					+ "expirationDate, description, content,"
					+ "clipboardId, count(*) OVER() AS full_count FROM TPastes WHERE clipboardId = ? LIMIT ? OFFSET ?";
		}
		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(sql));) {

			if (queryValues.get("type").equals("content")) {
				int afterSearchIndex = 1;
				if (searchTerms != null) {
					for (int i = 1; i <= searchTerms.length * 3; i++) {
						ps.setString(i,
								"%" + searchTerms[(i - 1) % searchTerms.length]
										+ "%");
						afterSearchIndex = i + 1;
					}
				}
				for (int i = afterSearchIndex; i < afterSearchIndex + 5; i++) {
					ps.setInt(i, Integer.parseInt(queryValues.get("userId")));
				}
				ps.setInt(afterSearchIndex + 5, entriesPerPage);
				ps.setInt(afterSearchIndex + 6,
						page * entriesPerPage - entriesPerPage);
			} else if (queryValues.get("type").equals("clipboard")) {
				ps.setInt(1, Integer.parseInt(queryValues.get("clipboardId")));
				ps.setInt(2, entriesPerPage);
				ps.setInt(3, page * entriesPerPage - entriesPerPage);
			}

			try (ResultSetWrapper rs = new ResultSetWrapper(
					ps.executeQuery())) {
				int totalCount = -1;
				while (rs.next()) {
					Paste paste = new Paste();
					paste.setId(rs.getInt(1));
					paste.setTitle(rs.getString(2));
					paste.setCreator(DAOUser.getById(rs.getInt(3)));
					paste.setCreationDate(rs.getDate(4));
					paste.setLastModifiedDate(rs.getDate(5));
					paste.setExpirationDate(rs.getDate(6));
					paste.setDescription(rs.getString(7));
					paste.setContent(rs.getString(8));
					paste.setClipboardId(rs.getInt(9));
					pastes.add(paste);
					totalCount = rs.getInt(10);
				}
				PaginatedDataModel<Paste> pdm = new PaginatedDataModel<Paste>(
						page, totalCount, pastes);
				return pdm;
			}
		}
	}

	/**
	 * Pagination for admins/moderators, gets all pastes regardless of
	 * visibility/permission.
	 * 
	 * @param query
	 *            The pagination query.
	 * @param page
	 *            The page of the pagination.
	 * @param entriesPerPage
	 *            How many entries per page.
	 * @param queryValues
	 *            The values of the query.
	 * @return A PaginateDataModel of pastes regardless of
	 *         visibility/permission.
	 * @throws DataSourceException
	 */
	private static PaginatedDataModel<Paste> paginateAdmin(String query,
			int page, int entriesPerPage, Map<String, String> queryValues)
			throws DataSourceException {
		String[] searchTerms = queryValues.get("value")
				.split(QueryParser.DELIMITER_QUERY);
		searchTerms = queryValues.get("value")
				.split(QueryParser.DELIMITER_QUERY);
		List<Paste> pastes = new ArrayList<Paste>();
		String sql = "SELECT TPastes.id, TPastes.title, TPastes.creatorId, "
				+ "TPastes.creationDate, TPastes.lastModificationDate, "
				+ "TPastes.expirationDate, TPastes.description, "
				+ "TPastes.content, clipboardId, count(*) OVER() AS full_count FROM TPastes "
				+ "INNER JOIN TClipboards "
				+ "ON TPastes.clipboardId = TClipboards.id WHERE (  "
				+ QueryParser.createWHEREQuery(searchTerms.length,
						"TPastes.title")
				+ " ) OR ( "
				+ QueryParser.createWHEREQuery(searchTerms.length,
						"TPastes.description")
				+ " ) OR ( " + QueryParser.createWHEREQuery(searchTerms.length,
						"TPastes.content")
				+ " ) LIMIT ? OFFSET ? ";

		try (ConnectionWrapper cw = new ConnectionWrapper(
				DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(
						cw.prepareStatement(sql));) {
			if (queryValues.get("type").equals("content")) {
				int afterSearchIndex = 1;
				for (int i = 1; i <= searchTerms.length * 3; i++) {
					ps.setString(i, "%"
							+ searchTerms[(i - 1) % searchTerms.length] + "%");
					afterSearchIndex = i + 1;
				}
				ps.setInt(afterSearchIndex, entriesPerPage);
				ps.setInt(afterSearchIndex + 1,
						page * entriesPerPage - entriesPerPage);
				try (ResultSetWrapper rs = new ResultSetWrapper(
						ps.executeQuery())) {
					int totalCount = -1;
					while (rs.next()) {
						Paste paste = new Paste();
						paste.setId(rs.getInt(1));
						paste.setTitle(rs.getString(2));
						paste.setCreator(DAOUser.getById(rs.getInt(3)));
						paste.setCreationDate(rs.getDate(4));
						paste.setLastModifiedDate(rs.getDate(5));
						paste.setExpirationDate(rs.getDate(6));
						paste.setDescription(rs.getString(7));
						paste.setContent(rs.getString(8));
						paste.setClipboardId(rs.getInt(9));
						pastes.add(paste);
						totalCount = rs.getInt(10);
					}
					PaginatedDataModel<Paste> pdm = new PaginatedDataModel<Paste>(
							page, totalCount, pastes);
					return pdm;
				}
			}
		}

		return null;
	}

	/**
	 * Checks if a given Paste already exists.
	 * 
	 * @param id
	 *            paste to check.
	 * @return true if it exists, false otherwise
	 * @throws DataSourceException
	 */
	public static boolean checkExistance(int id) throws DataSourceException {
		String query = "SELECT 1 FROM TPastes WHERE id = ?";
		ResultTable resultTable = DBUtils.executeQuery(query, id);
		return resultTable.size() == 1;
	}
}
