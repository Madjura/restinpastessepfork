package de.sep16g01.rip.dataAccessLayer.keyword;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import de.sep16g01.rip.businessLogicLayer.profile.User;
import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ConnectionWrapper;
import de.sep16g01.rip.dataAccessLayer.general.PreparedStatementWrapper;
import de.sep16g01.rip.dataAccessLayer.general.ResultSetWrapper;
import de.sep16g01.rip.dataAccessLayer.util.QueryStringUtil;

/**
 * DAO for the keywords. Handles storing and loading to keywords to and from the
 * database.
 */
public class DAOKeyword {

	private final transient static Logger logger = Logger.getLogger(DAOKeyword.class);

	/**
	 * Stores all the keywords of a user.
	 * 
	 * @param keywords
	 *            A set of keywords of a user.
	 * @param id
	 *            The id of the user.
	 * @throws DataSourceException
	 */
	public static void storeKeywordsMultiple(Set<String> keywords, int id) throws DataSourceException {
		// if keyword already exists, dont care
		String insertKeywordTable = "INSERT INTO TKeywords (name) VALUES "
				+ QueryStringUtil.createInsert(keywords.size()) + " ON CONFLICT DO NOTHING";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(insertKeywordTable));) {
			String[] keywordArray = keywords.toArray(new String[keywords.size()]);

			// ps starts at 1, array at 0, looks weird but necessary
			for (int i = 1; i <= keywordArray.length; i++) {
				ps.setString(i, keywordArray[i - 1]);
			}
			if (ps.executeUpdate() == 0) {
				logger.warn("No keywords were inserted for user with id " + id);
			}
			ps.close();
		} finally {
			storeKeywordsToUser(keywords, id);
		}
	}

	/**
	 * Called by {@link #storeKeywordsMultiple(Set, int)
	 * storeKeywordsMultiple()}. Stores the keywords of a user in the
	 * RUserKeyword table.
	 * 
	 * @param keywords
	 * @param id
	 * @throws DataSourceException
	 */
	private static void storeKeywordsToUser(Set<String> keywords, int id) throws DataSourceException {

		// get rid of all the old keywords
		// if user removes a keyword he added previously, but removes & saves,
		// he doesnt want it anymore, so remove everything + add is
		// quicker+easier
		removeAllKeywordsForUser(id);

		// delete keywords then if empty keep empty
		if (keywords.isEmpty()) {
			return;
		}

		String insert = "INSERT INTO RUserKeyword (userId, keywordId) VALUES "
				+ QueryStringUtil.createInsertDouble(keywords.size()) + " ON CONFLICT DO NOTHING";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(insert));) {
			String[] keywordArray = keywords.toArray(new String[keywords.size()]);
			for (int i = 0; i < keywordArray.length; i++) {
				ps.setInt(i * 2 + 1, id);

				// id of keyword so relation works
				ps.setInt(i * 2 + 2, getKeywordId(keywordArray[i]));
			}
			// insert + check if was inserted
			if (ps.executeUpdate() == 0) {
				logger.warn("No keywords inserted for user with id " + id + " and keywords " + keywords.toString());
			}
		}
	}

	/**
	 * Called by {@link #storeKeyword(String, User) storeKeyWord()} method.
	 * Removes all keywords of a certain user.
	 * 
	 * @param id
	 *            The id of the user.
	 * @throws DataSourceException
	 */
	private static void removeAllKeywordsForUser(int id) throws DataSourceException {

		String delete = "DELETE FROM RUserKeyword WHERE userId = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(delete));) {
			ps.setInt(1, id);
			if (ps.executeUpdate() == 0) {
				logger.warn("No keyword entries for user with id " + id
						+ ". No entries removed, but removal was attempted.");
			}
		}
	}

	/**
	 * Loads all keywords for user.
	 * 
	 * @param id
	 *            The of the user.
	 * @return A list of all keywords for a user.
	 * @throws DataSourceException
	 *             When the database is down.
	 */
	public static List<String> loadKeywordsForUser(int id) throws DataSourceException {

		String query = "SELECT name FROM  " + " (SELECT id, name FROM TKeywords) a "
				+ " RIGHT JOIN (SELECT DISTINCT keywordId FROM RUserKeyword "
				+ " WHERE userId = ?) b  ON a.id = b.keywordId ";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			ps.setInt(1, id);

			List<String> keywords = new ArrayList<String>();
			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					keywords.add(rs.getString(1));
				}
			}
			return keywords;
		}
	}

	/**
	 * Loads all keywords from the database.
	 * 
	 * @return A HashMap with the keys being single keywords, and the values
	 *         being lists of user Id's who have the keyword.
	 * @throws DataSourceException
	 *             When the database is down.
	 */
	public static HashMap<String, List<Integer>> getAllKeyWords() throws DataSourceException {

		String query = "SELECT distinct userId, name FROM " + " (SELECT userId, keywordId FROM RUserKeyword) a "
				+ " LEFT JOIN (SELECT id, name FROM TKeywords) b " + " ON a.keywordId = b.id ";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			HashMap<String, List<Integer>> keywordUser = new HashMap<String, List<Integer>>();

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					String term = rs.getString(2);
					if (keywordUser.containsKey(term)) {
						keywordUser.get(term).add(rs.getInt(1));
					} else {
						List<Integer> userIdList = new ArrayList<Integer>();
						userIdList.add(rs.getInt(1));
						keywordUser.put(term, userIdList);
					}
				}
			}
			return keywordUser;
		}
	}

	/**
	 * Called by {@link #storeKeywordsToUser(Set, int) storeKeywordsToUser()}
	 * method. Returns the id of a keyword in the TKeywords table.
	 * 
	 * @param keyword
	 *            The keyword.
	 * @return The id of the keyword in the TKeywords table.
	 * @throws DataSourceException
	 */
	private static int getKeywordId(String keyword) throws DataSourceException {
		String query = "SELECT id FROM TKeywords WHERE name = ?";
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {

			ps.setString(1, keyword);

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				rs.next();
				return rs.getInt(1);
			}
		}
	}

	/**
	 * Returns a Hashmap of <UserId, Email> to identify where to send the
	 * keyword email to.
	 * 
	 * @param id
	 *            The list of user Ids.
	 * @return A HashMap of <UserId, Email>.
	 * @throws DataSourceException
	 */
	public static HashMap<Integer, String> getEmailsById(List<Integer> id) throws DataSourceException {
		if (id.isEmpty()) {
			logger.warn("Empty list of Ids was passed to getEmailsById()");
			return null;
		}

		String query = "SELECT id, email FROM TUsers WHERE id IN " + QueryStringUtil.createValueListString(id.size());
		HashMap<Integer, String> idEmail = new HashMap<Integer, String>();
		try (ConnectionWrapper cw = new ConnectionWrapper(DBPool.getInstance().getConnection());
				PreparedStatementWrapper ps = new PreparedStatementWrapper(cw.prepareStatement(query));) {
			// string is made up of only ?

			for (int i = 1; i <= id.size(); i++) {
				ps.setInt(i, id.get(i - 1));
			}

			try (ResultSetWrapper rs = new ResultSetWrapper(ps.executeQuery())) {
				while (rs.next()) {
					idEmail.put(rs.getInt(1), rs.getString(2));
				}
			}
			return idEmail;
		}
	}
}
