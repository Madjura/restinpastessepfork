package de.sep16g01.rip.dataAccessLayer.exceptions;

import de.sep16g01.rip.lang.BBChangeLanguage;

/**
 * Exception that is thrown by us when the database is down.
 */
public class DataSourceException extends Exception {

	private static final long serialVersionUID = 1L;

	public DataSourceException(Throwable e) {
		super(BBChangeLanguage.getLangStringStatic("error"), e);
	}
}
