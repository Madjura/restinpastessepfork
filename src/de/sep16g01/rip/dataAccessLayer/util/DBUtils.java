/**
 *   Copyright subkom GmbH. All rights reserved. subkom.de
 *   File:     de.sep16g01.rip.dataAccessLayer.util.DAOUtils.java
 *   Created:  09.06.2016, 14:48:49 by Albert
 */
package de.sep16g01.rip.dataAccessLayer.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.sep16g01.rip.dataAccessLayer.database.DBPool;
import de.sep16g01.rip.dataAccessLayer.exceptions.DataSourceException;
import de.sep16g01.rip.dataAccessLayer.general.ResultTable;

/**
 * Utility methods for database stuff.
 */
public class DBUtils {
	public static PreparedStatement prepareStatement(Connection con,
			String query, Object... params) throws DataSourceException {
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			for (int i = 1; i <= params.length; i++) {
				preparedStatement.setObject(i, params[i - 1]);
			}
			return preparedStatement;
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}
	}

	public static ResultTable executeQuery(String query, Object... params)
			throws DataSourceException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DBPool.getInstance().getConnection();
			ps = prepareStatement(con, query, params);
			ResultSet result = ps.executeQuery();

			List<List<Object>> resultData = new ArrayList<List<Object>>();

			ResultSetMetaData meta = result.getMetaData();
			int cols = meta.getColumnCount();
			List<String> columnNames = new ArrayList<String>();
			for (int i = 0; i < cols; i++) {
				columnNames.add(meta.getColumnName(i + 1));
			}

			while (result.next()) {
				List<Object> row = new ArrayList<Object>();
				for (int i = 0; i < cols; i++) {
					row.add(result.getObject(i + 1));
				}

				resultData.add(row);
			}

			ResultTable resultTable = new ResultTable(columnNames, resultData);
			return resultTable;
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			DBPool.getInstance().returnConnection(con);
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				throw new DataSourceException(e);
			}
		}
	}
}
