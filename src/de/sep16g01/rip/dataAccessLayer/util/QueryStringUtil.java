/**
 *   Copyright subkom GmbH. All rights reserved. subkom.de
 *   File:     de.sep16g01.rip.dataAccessLayer.util.QueryStringUtil.java
 *   Created:  06.06.2016, 09:58:24 by hubertho
 */
package de.sep16g01.rip.dataAccessLayer.util;

/**
 * Utility methods for SQL queries.
 */
public final class QueryStringUtil {
	/**
	 * Creates a list in the format (?,?,? ... ?) when querying for multiple
	 * values.
	 * 
	 * @param values
	 *            How many question marks are needed.
	 * @return A String in the format (?,?,? ... ?) with as many question marks
	 *         as specified.
	 */
	public static String createValueListString(int values) {
		StringBuilder sb = new StringBuilder(" (");
		for (int i = 0; i < values; i++) {
			sb.append("?");
			if (i < values - 1) {
				sb.append(", ");
			}
		}
		sb.append(") ");
		return sb.toString();
	}

	public static String createInsert(int values) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < values; i++) {
			sb.append("(?)");
			if (i < values - 1) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	public static String createInsertDouble(int values) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < values; i++) {
			sb.append(" (?, ?) ");
			if (i < values - 1) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	/**
	 * Creates a value list for query strings.
	 * 
	 * @param values
	 *            The values in the row.
	 * @param columns
	 *            The columns being searched.
	 * @return
	 */
	public static String createValueListString(int values, int columns) {
		if (columns == 1) {
			return createValueListString(values);
		}
		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < columns; i++) {
			sb.append(createValueListString(values));
			if (i < columns - 1) {
				sb.append(", ");
			}
		}
		// sb.append(") ");

		return sb.toString();
	}
}